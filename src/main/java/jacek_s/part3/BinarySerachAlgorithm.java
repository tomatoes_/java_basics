package jacek_s.part3;

/**
 * Created by jacek on 06.03.17.
 */
public class BinarySerachAlgorithm implements SearchAlgorithm {


    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        int lowNumbere = 0;
        int highNumber = array.length - 1;
        while (lowNumbere <= highNumber) {
            int middleNumber = lowNumbere + (highNumber - lowNumbere) / 2;
            if (value < array[middleNumber]) highNumber = middleNumber - 1;
            else if (value > array[middleNumber]) lowNumbere = middleNumber + 1;
            else return middleNumber;
        }
        return -1;
    }
}

