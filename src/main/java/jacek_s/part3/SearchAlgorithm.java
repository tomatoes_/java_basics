package jacek_s.part3;

/**
 * Created by jacek on 04.03.17.
 */
public interface SearchAlgorithm {
    int search(int[] array, int value);
}
