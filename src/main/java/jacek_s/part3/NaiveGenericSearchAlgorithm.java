package jacek_s.part3;

import java.util.function.Predicate;

/**
 * Created by jacek on 06.03.17.
 */
public class NaiveGenericSearchAlgorithm implements GenericSearchAlgorithm {

    public <T extends Comparable> int search(T[] array, Predicate<T> predicate) {

        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        for (int i = 0; i < array.length; i++) {
            if (predicate.test(array[i])) {
                return i;
            }
        }
        return -1;
    }
}