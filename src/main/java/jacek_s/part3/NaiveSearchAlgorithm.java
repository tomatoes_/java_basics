package jacek_s.part3;

/**
 * Created by jacek on 06.03.17.
 */
public class NaiveSearchAlgorithm implements SearchAlgorithm {

    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        if (array.length == 0) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                return i;
        }
        return -1;
    }
}