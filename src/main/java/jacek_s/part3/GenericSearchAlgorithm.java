package jacek_s.part3;

import java.util.function.Predicate;

/**
 * Created by jacek on 06.03.17.
 */
public interface GenericSearchAlgorithm {

    <T extends Comparable> int search(T[] array, Predicate<T> predicate);
}
