package jacek_s.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by jacek on 28.02.17.
 */

@Data
@Builder
public class Adress {
    private String streetName;
    private String flatNumber;
    private String postalCode;
    private String country;

    public static Adress getRandomAdress() {
        return Adress.builder()
                .streetName(randomAlphabetic(20))
                .flatNumber(randomAlphabetic(4))
                .postalCode(randomNumeric(5))
                .country(randomAlphabetic(15))
                .build();
    }
}
