package jacek_s.part1;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by jacek on 27.02.17.
 */
public class Problem6 {

    public static void main(String[] args) {


        int array[] = new int[10];

        for (int i = 0; i <array.length ; i++) {
            array[i] = i;
        }

       Arrays.stream(array)
                .forEach(i ->
                System.out.print(i+" "));

        System.out.println("\n");

    //------------------------------------------//

       Arrays.stream(array)
               .map(i-> i %2 != 0 ? i*2-1 : i)
               .forEach(i -> System.out.print(i+ " "));

        System.out.println("\n");

    //------------------------------------------//

        Arrays.stream(array)
                .map(i -> i % 2 != 0 ? (i * 2)-1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .forEach((i) -> System.out.print(i + " "));

        System.out.println("\n");

    //------------------------------------------//

        int sum = Arrays.stream(array)
                .map(i -> i % 2 != 0 ? (i * 2)-1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .sum();
        System.out.println(sum);
        System.out.println();

    //-----------------------------------------//

        int array1[] = new int[100];

        for (int i = 0; i <array1.length ; i++) {
            array1[i] = i;
        }

        int sum1 = Arrays.stream(array1)
                .map(i -> i % 2 != 0 ? (i * 2)-1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .sum();
        System.out.println(sum1);
    }
}