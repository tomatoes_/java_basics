package jacek_s.part1;

// @NoArgsConstructor
// @AllArgsConstructor
// @Getter
// @Setter
// zamiast wszystkiego powyżej wystarczy @Data (jak kto woli) lub wogóle bez Lomboka i wtedy
// normalnie konstruktory, getery i setery - tradycyjnie - jak na końcu to co zakomentowane)
import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by jacek on 28.02.17.
 */

@Data
@Builder

public class Person {
    private String firstName;
    private String lastName;
    private String pesel;
    private char gender;
    private Adress adress;

    public static Person getRandomPerson() {
        return  Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(6))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ? 'M' : 'F')
                .adress(Adress.getRandomAdress())
                .build();
    }


//    public Person(String firstName, String lastName, String pesel, char gender, Adress adress) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.pesel = pesel;
//        this.gender = gender;
//        this.adress = adress;
//    }

//    public String getFirstName() {
//        return firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public String getPesel() {
//        return pesel;
//    }
//
//    public char getGender() {
//        return gender;
//    }
//
//    public Adress getAdress() {
//        return adress;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }

//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public void setPesel(String pesel) {
//        this.pesel = pesel;
//    }
//
//    public void setGender(char gender) {
//        this.gender = gender;
//    }
//
//    public void setAdress(Adress adress) {
//        this.adress = adress;
//    }

}
