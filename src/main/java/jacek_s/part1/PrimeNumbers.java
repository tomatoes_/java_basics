package jacek_s.part1;

import java.util.Scanner;

/**
 * Created by jacek on 25.02.17.
 */
public class PrimeNumbers {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int problems = Integer.parseInt(input.nextLine());

        while (problems > 0) {
            --problems;

            int flag=0;
            String line = input.nextLine();
            String[] split = line.split(" ");

            int rangeFrom = Integer.parseInt(split[0].toString());
            int rangeTo = Integer.parseInt(split[1].toString());
             System.out.println();

            for (int j = rangeFrom; j <= rangeTo; j++) {

                for (int k = 2; k < j; k++) {
                    if (j % k == 0) {
                        flag = 0;
                        break;
                    } else {
                        flag = 1;
                    }
                }
                if (flag == 1) {
                    System.out.print(j + " ");
                }
            }
            System.out.println("\n");
        }
    }
}
