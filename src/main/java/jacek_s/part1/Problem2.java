package jacek_s.part1;

/**
 * Created by jacek on 22.02.17.
 */

public class Problem2 {

    public static void main(String[] args) {

        drawTriangle(10);
        drawSlashLine(10);
        drawSquare(10);
        drawSlashAndBackSlashLines(11);

    }

    public static void drawTriangle(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {

                System.out.print("*");
            }

            System.out.println();
        }

        System.out.println();
    }

    public static void drawSlashLine(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(" ");
            }
            System.out.println("*");
        }
        System.out.println();
    }

    public static void drawSquare(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if ((i == 0) || (i == height - 1)) {
                    System.out.print("*");
                } else if ((j == 0) || (j == height - 1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if ((i == 0) || (i == height - 1)) {
                    System.out.print("*");
                } else if ((j == 0) || (j == height - 1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }



        System.out.println();
    }

    public static void drawSlashAndBackSlashLines(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if (i == j || i + j == height - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }

            System.out.println();
        }
    }
}



