package jacek_s.part1;

/**
 * Created by jacek on 23.02.17.
 */
public class Problem4 {

    public static void main(String[] args) {

        int array[] = new int[10];

        for (int i = 0; i <array.length ; i++) {
            array[i] = i;
            System.out.print(array[i]+" ");
        }

        System.out.println();

/////////////////////////////////////////////////////////

        for (int i = 1; i <array.length ; i+=2) {

            array[i]+=array[i-1];
        }

        for (int element: array) {
            System.out.print(element+" ");
        }

        System.out.println();
///////////////////////////////////////////////

        for (int i = 0; i <array.length ; i++) {
            if (array[i] % 2 == 0) {
                array[i] /= 2;
            }
        }
            for (int element: array) {
                System.out.print(element+" ");
            }

 ////////////////////////////////////////////////

        System.out.println();


            int sum=0;
            for (int element : array) {
                sum+=element;
            }

            System.out.println(sum);
        }
    }

