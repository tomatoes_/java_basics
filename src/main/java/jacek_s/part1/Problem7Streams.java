package jacek_s.part1;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by jacek on 28.02.17.
 */
public class Problem7Streams {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;

            Stream.of(scanner.nextLine().split(" "))
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(BigInteger::new)
                    .reduce(BigInteger::add)
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(s -> s.replaceFirst("^0+(?!$)",""))
                    .ifPresent(System.out::println);
        }
    }
}
