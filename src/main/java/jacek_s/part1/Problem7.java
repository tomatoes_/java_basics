package jacek_s.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by jacek on 27.02.17.
 */
public class Problem7 {

    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
//        int problems = Integer.parseInt(scanner.nextLine());
//        while (problems > 0) {
//            --problems;
//            String line = scanner.nextLine();
//            String[] split = line.split(" ");
//
//            StringBuilder numberA = new StringBuilder(split[0]);
//            StringBuilder numberB = new StringBuilder(split[1]);
//
//            String numberAReversed = numberA.reverse().toString();
//            String numberBReversed = numberB.reverse().toString();
//
//            BigInteger bigNumberA = new BigInteger(numberAReversed);
//            BigInteger bigNumberB = new BigInteger(numberBReversed);
//            BigInteger sum = bigNumberA.add(bigNumberB);
//
////          long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);
//
//            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
//            System.out.println(sumReversed.replaceFirst("^0+(?!$)", ""));
//        }
   // }

//------------------------------------------------------------------------//

    // public static void main(String[] args) {

//        int lengthA = 5;
//        int lengthB = 8;
//        int length = lengthA > lengthB ? lengthA +1 : lengthB+1; // krótszy zapis warunku poniżej
//        if (lengthA>lengthB) {
//            length = lengthA +1;
//        } else {
//            length = lengthB +1;
//        }

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lengthA = arrayA.length;
            int lengthB = arrayB.length;

            int newLength;
            int smallerLength;
            int longerLength;
            char[] longerArray;

            if (lengthA == lengthB) {
                if (arrayA[lengthA - 1] + arrayB[lengthA - 1] >= '9') {
                    newLength = lengthA + 1;
                }
                else {
                    newLength = lengthA;
                }
                smallerLength = lengthA;
                longerArray = arrayA;
                }
                else if (lengthA > lengthB) {
                    newLength = lengthA;
                    smallerLength = lengthB;
                    longerArray = arrayA;
                }
                else {
                newLength = lengthB;
                smallerLength = lengthA;
                longerArray = arrayB;
                }

            longerLength = longerArray.length;


            char result[] = new char[newLength];

            int overflow = 0;

            for (int i = 0; i < smallerLength; i++) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                }
                else {
                    overflow = 0;
                }
            }

            for (int i = smallerLength; i < longerLength; i++) {
                result[i] = (char)(longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                }
                else {
                    overflow = 0;
                }
            }

            String tmpString = Arrays.toString(result);

            for (int i = 0; i < newLength; i++) {
                System.out.print(result[i]);
            }

            if (newLength > longerLength && overflow != 0) {
                System.out.print('1');}
                else {
                System.out.print(tmpString.replaceFirst("^0+(?!$)", ""));
            }
            System.out.println();
        }
//
//        char[] test = {'1', '2', '3'};
//
//        reverseInPlace(test);
//        System.out.println(Arrays.toString(test));
    }

    //    private static class TestInteger {
//        int value;
//
//        public void setValue(int value) {  //można pobrać
//            this.value = value;
//        }
//
//        public int getValue() { // można zmienic
//            return value;
//
//
//        }
//    }

//    public static void main(String[] args) {
//
//       int pirmitive = 1;
//        TestInteger object = new TestInteger();
//        testObjectPrimitive(pirmitive, object);
//        System.out.println(object);
//        System.out.println(pirmitive);
//    }
//
//    private static void testObjectPrimitive(int primitive, TestInteger object) {
//        primitive = 5;
//        object.value = 5;
//    }

    private static char[] reverse(char[] toReverse) {
        int length = toReverse.length;
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = toReverse[length - i - 1];
        }
        return result;
    }

    private static void reverseInPlace(char[] toReverse) {
        char tmp;
        int length = toReverse.length;
        for (int i = 0; i < length/2; i++) {
            tmp = toReverse[i];
            toReverse[i] = toReverse[length - i - 1];
            toReverse[length - i - 1] = tmp;
        }
    }
}


//Poprawić i rzucić na SPOJ