package jacek_s.utils;

import jacek_s.utils.ArrayGenerator;

/**
 * Created by jacek on 25.02.17.
 */
public class ZeroesArrayGenerator implements ArrayGenerator{

    @Override
    public int[] generate(int length, int rangeFrom, int rangeTo) {
        return new int[length];
    }

    @Override
    public int[] generate(int length) {
        return new int[length];
    }
}
