package jacek_s.utils;

/**
 * Created by jacek on 23.02.17.
 */
public interface ArrayGenerator {
    int[] generate(int length, int rangeFrom, int rangeTo);
    int[] generate(int length);
}
