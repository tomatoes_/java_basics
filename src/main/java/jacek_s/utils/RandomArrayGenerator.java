package jacek_s.utils;

import java.util.Random;

/**
 * Created by jacek on 23.02.17.
 */

public class RandomArrayGenerator implements ArrayGenerator {

    public int[] generate(int length, int rangeFrom, int rangeTo) { // final - programista sugeruje użytkownikowi kodu, ze metoda jest juz dobra i nie wymaga zmian
                                                                // gdy ma zamiar dziedziczyć i zmieniać metody, to wtedy nie stosujemy final
       if (rangeFrom>=rangeTo) {
           throw new IllegalArgumentException("Range is invalid");
       }

        int[] array = new int[length];
        Random random = new Random();
        for (int i = 0; i <length ; i++) {
            array[i] = random.nextInt(rangeTo-rangeFrom)+rangeFrom;
        }
        return array;
    }


//  @Override //domyslnie kiedy implementujemy interfejs musimy nadpisać metody, więc można tego nie pisać
    public final int[] generate(int length) {

        return generate(length, 0, Integer.MAX_VALUE);
    }
}
