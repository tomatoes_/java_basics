package jacek_s.part2;

import jacek_s.utils.RandomArrayGenerator;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by jacek on 01.03.17.
 */
public class BubbleSortAlgorithm implements SortAlgorithm {

//    public static void main(String[] args) {
//
//        Scanner input = new Scanner(System.in);
//
//        System.out.println("Podaj ilość liczb: ");
//        int arrayLength = Integer.parseInt(input.nextLine());
//
//        System.out.println("Podaj przedział: ");
//        String line = input.nextLine();
//        String[] split = line.split(" ");
//
//        int rangeFrom = Integer.parseInt(split[0].toString());
//        int rangeTo = Integer.parseInt(split[1].toString());
//
//        bubbleSorting(rangeFrom, rangeTo, arrayLength);
//    }
//
//
//    public static int[] bubbleSorting(int rangeFrom, int rangeTo, int arrayLength) {
//
//        int array[] = new RandomArrayGenerator()
//                .generate(arrayLength, rangeFrom, rangeTo);
//
//        System.out.println();
//        System.out.println("Przed sortowaniem: " + Arrays.toString(array));
//
//
//        int tmp;
//        int change = 1;
//
//        while (change > 0) {
//            change = 0;
//            for (int i = 0; i < arrayLength - 1; i++) {
//                if (array[i] > array[i + 1]) {
//                    tmp = array[i + 1];
//                    array[i + 1] = array[i];
//                    array[i] = tmp;
//                    change++;
//                }
//            }
//        }
//        System.out.println();
//        System.out.println("Po sortowaniu:     " + Arrays.toString(array));
//        return array;
//    }

    @Override
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (ascending && array[i - 1] > array[i] ||
                        !ascending && array[i - 1] < array[i]) {
                    int tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}