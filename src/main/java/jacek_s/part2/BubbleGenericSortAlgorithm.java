package jacek_s.part2;

import java.util.Comparator;

import static java.util.Objects.compare;

/**
 * Created by jacek on 04.03.17.
 */
public class BubbleGenericSortAlgorithm implements GenericSortAlgorithm {

    public <T> void sort(T[] array, Comparator<T> comparator) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (comparator.compare(array[i - 1], array[i])==1)  {
                    T tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}

