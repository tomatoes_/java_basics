package jacek_s.part2;

/**
 * Created by jacek on 01.03.17.
 */

public interface SortAlgorithm {
    default void sort(int[] array) {
        sort(array, true);
    }

    void sort(int[] array, boolean ascending); // prototyp metody (nie ma ciała - metoda abstrakcyjna)

}




