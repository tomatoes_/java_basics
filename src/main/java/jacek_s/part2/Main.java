package jacek_s.part2;

import jacek_s.utils.RandomArrayGenerator;

import java.util.Arrays;

/**
 * Created by jacek on 03.03.17.
 */
public class Main {
    public static void main(String[] args) {

        int arrayLength, rangeFrom, rangeTo;
        int[] array = new RandomArrayGenerator().generate(20, 1, 20);

        System.out.println("Przed sorotwaniem: " + Arrays.toString(array));

        long startTime = System.nanoTime();
        new MergeSortAlgorithm().sort(array, false);
        long estimatedTime = System.nanoTime() - startTime;

        long startTime2 = System.nanoTime();
        new BubbleSortAlgorithm().sort(array, false);
        long estimatedTime2 = System.nanoTime() - startTime2;

        System.out.println("Po sorotwaniu:     " + Arrays.toString(array));
        System.out.println();
        System.out.println("Czas wykonania MeregeSortAlgorithm: "+(estimatedTime));
        System.out.println();
        System.out.println("Czas wykonania BubbleSortAlgorithm: "+(estimatedTime2));

    }


}

