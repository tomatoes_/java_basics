package jacek_s.part2;

import java.util.function.Predicate;

/**
 * Created by jacek on 04.03.17.
 */
public interface GroupSortAlgorithm {
    void sort(int[] array, Predicate<Integer> predicate);
}
