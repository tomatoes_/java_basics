package maciej_b.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.ofNullable;


public class DynamicList<T> implements List<T> {

  private ListElement first;
    private ListElement last;
    private int size = 0;


    public T getFirst() {
//        if (first == null)
//            throw new IndexOutOfBoundsException();
//        return first.getValeu();
        return getChecked(first);
    }

    public T getLast() {
        return getChecked(last);
    }

    private T getChecked(ListElement element){
        return ofNullable(element)
                .map(ListElement::getValeu)
                .orElseThrow(IndexOutOfBoundsException::new);
    }

    public T get(int index) {
        ListElement element = first;
        while (index-- >0) {
            element = element.getNext();
        }
        return element.getValeu();
    }

    public void add(T element) {
        if (Objects.isNull(first)) {       // if (first == null)
            first = new ListElement(element, null, null);
            last = first;
        }else {
            ListElement newElement = new ListElement(element, last, null);
            last.next = newElement;
            last = newElement;
        }

    }

    public void remove(int index) {    // TODO: przypadki dla pierwszego i ostatniego elementu
        ListElement toRemove = getElement(index);
        toRemove.previous.next = toRemove.next;
        toRemove.next.previous = toRemove.previous;
    }

    private ListElement getElement(int index){
        ListElement element = first;
        while (index-- >0){
            element = element.getNext();
        }
        return element;
    }

    public int size() {
        return size;
    }

    public Iterator<T> iterator() {
        return new Iterator<T>() {
            ListElement ref = first;

            @Override
            public boolean hasNext() {
                return ref !=null;
            }

            @Override
            public T next() {
                ref = ref.next;
                return ref.previous.getValeu();
            }
        };
    }

    @Data
    @AllArgsConstructor
    private class ListElement {
        private T valeu;
        private ListElement previous;
        private ListElement next;


    }

}
