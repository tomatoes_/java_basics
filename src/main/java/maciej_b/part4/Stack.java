package maciej_b.part4;

/**
 * Created by RENT on 2017-03-07.
 */
public interface Stack<T> {
    // IMPLEMENTACJA GENERYCZNEGO STOSU DLA OBIEKTÓW

    void push(T t);
    T pop();
    T peek();
    int size();
    default boolean isEmpty() {
    return size() ==0;
    }

}

// IMPLEMENTACJA STOSU DLA INT

//public interface Stack {

//    void push (int element);
//    int pop();
//    int peek();
//    int size();
//    boolean isEmpty();

//}
