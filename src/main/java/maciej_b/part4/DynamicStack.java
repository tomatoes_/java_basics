package maciej_b.part4;

import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.Optional.*;

/**
 * Created by RENT on 2017-03-07.
 */
public class DynamicStack <T> implements Stack<T> {

    private StackElement<T> top;
    private int size = 0;


    public void push(T t) {
        top = new StackElement<>(t, top);
        ++size;

    }


    public T pop() {
        --size;
        StackElement<T> oldTop = top;  // ostatni element ze stosu przypisaliśmy do odlTop
        top = top.getPrevious();    // przypisanie referenjcji do niższego elementu
        return oldTop.getElement();

    }


    public T peek() {
        return ofNullable(top)
                .map(StackElement::getElement)
                .orElse(null);

//        return top != null ? top.getElement() : null;
//        if (nonNull(top)){
//            return top.getElement();
//        }
//        else{
//            return null;
//        }
    }


    public int size() {
        return size;
    }
}
