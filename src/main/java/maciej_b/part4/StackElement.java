package maciej_b.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

// tą klasę możemy przenieść do DynamicStack jako klasę i i usunąć typ generyczny
// wtedy wystarczy usunąć w StackElement typ generyczny
// klasie DynamicStack trzeba klasę stackElement zaimportować jako private

@Data
@AllArgsConstructor

public class StackElement<T> {
    private T element;
    private StackElement<T> previous;
}
