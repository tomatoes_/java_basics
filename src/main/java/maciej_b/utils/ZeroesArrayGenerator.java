package maciej_b.utils;

import maciej_b.utils.ArrayGenerator;

/**
 * Created by RENT on 2017-02-25.
 */
public class ZeroesArrayGenerator implements ArrayGenerator {


    @Override
    public int[] generate(int lenght, int rangeFrom, int  rangeTo) {
        return new int[0];
    }

    @Override
    public int[] generate(int lenght) {
        return new int[0];
    }
}
