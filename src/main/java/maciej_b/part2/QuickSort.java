package maciej_b.part2;

/**
 * Created by mac on 2017-03-03.
 */
public class QuickSort implements SortAlgorithm{

private int[] numbers;
private int number;

@Override
public void sort(int[] values, boolean ascending) {
    
        if (values == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        if (values.length==0){
            return;
        }
        this.numbers = values;
        number = values.length;
        if(ascending) {
            quicksort(0, number - 1);
        } else if (!ascending){
            quicksortDescending(0, number -1);
        }
        }

private void quicksortDescending(int low, int high) {
        int leftIndex = low, rightIndex = high;
        int pivot = numbers[low + (high-low)/2];
        while (leftIndex >= rightIndex) {
            while (numbers[leftIndex] > pivot) {
                leftIndex--;
        }
        while (numbers[rightIndex] < pivot) {
            rightIndex++;
        }
        if (leftIndex >= rightIndex) {
            exchange(leftIndex, rightIndex);
            leftIndex--;
            rightIndex++;
        }
        }
        if (low > rightIndex)
            quicksortDescending(low, rightIndex);
        if (leftIndex > high)
            quicksortDescending(leftIndex, high);
        }

private void quicksort(int low, int high) {
        int i = low, j = high;
        int pivot = numbers[low + (high-low)/2];
        while (i <= j) {
        while (numbers[i] < pivot) {
        i++;
        }
        while (numbers[j] > pivot) {
        j--;
        }
        if (i <= j) {
            exchange(i, j);
            i++;
            j--;
        }
        }
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
        }

private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
        }

}

//public class QuickSort {
//    int partition(int array[], int startIndex, int endIndex)
//    {
//        int pivot = array[endIndex];
//        int i = (startIndex-1); // index of smaller element
//        for (int j=startIndex; j<=endIndex-1; j++)
//        {
//            // If current element is smaller than or
//            // equal to pivot
//            if (array[j] <= pivot)
//            {
//                i++;
//
//                // swap array[i] and array[j]
//                int temp = array[i];
//                array[i] = array[j];
//                array[j] = temp;
//            }
//        }
//
//        // swap array[i+1] and array[endIndex] (or pivot)
//        int temp = array[i+1];
//        array[i+1] = array[endIndex];
//        array[endIndex] = temp;
//
//        return i+1;
//    }
//
//    /* The main function that implements QuickSort()
//      arr[] --> Array to be sorted,
//      low  --> Starting index,
//      high  --> Ending index */
//    void qSort(int arr[], int low, int high)
//    {
//        if (low < high)
//        {
//            /* pi is partitioning index, arr[pi] is
//              now at right place */
//            int pi = partition(arr, low, high);
//
//            // Recursively sort elements before
//            // partition and after partition
//            qSort(arr, low, pi-1);
//            qSort(arr, pi+1, high);
//        }
//    }
//}
