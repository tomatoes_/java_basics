package maciej_b.part2;



public interface SortAlgorithm {
    default void sort(int[] array)
    {sort(array,true);}

    void sort(int[] array, boolean ascending);

}