package maciej_b.part2;

import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-04.
 */
public interface GroupSortAlgorithm  {

    default void sort(int[] array, Predicate<Integer> predicate){}
    default void sort(int[] array, Predicate<Integer> predicate, Predicate<Integer> predicateSecond){}
}
