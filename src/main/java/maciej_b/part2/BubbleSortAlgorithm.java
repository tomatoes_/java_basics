package maciej_b.part2;

/**
 * Created by RENT on 2017-03-01.
 */
public class BubbleSortAlgorithm implements SortAlgorithm {

    @Override
    public void sort(int[] array, boolean ascending) {

        if (array==null){
            throw new IllegalArgumentException("Array should not be null");
        }
        int lastSortElement = array.length;
        while (lastSortElement >1){
            for (int i = 1; i < lastSortElement ; i++) {
                if (ascending && array[i - 1] > array[i] ||
                        !ascending && array[i - 1] < array[i]){
                    int tmp = array[i];
                    array[i] = array [i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortElement;
        }
    }
}

