package maciej_b.part2;

import java.util.function.Predicate;


public class NetherlandFlagGroupSortAlgorithm implements GroupSortAlgorithm {



    @Override
    public void sort(int[] array, Predicate<Integer> predicate, Predicate<Integer> predicateSecond) {

        if (array == null){
            throw new IllegalArgumentException("Array should not be null");
        }

        int left = -1;
        int midle = 0;
        int rigth = array.length - 1;

        while (midle < rigth) {
            if (!predicate.test(array[midle])) {
                int tmp = array[left];
                array[left] = array[rigth];
                array[rigth] = tmp;
                ++left;
                ++midle;
            } else if (predicate.test(array[midle])
                    && predicateSecond.test(array[rigth])) {
                midle++;
            } else if (!predicateSecond.test(array[rigth])) {
                int tmp = array[midle];
                array[midle] = array[rigth];
                array[rigth] = tmp;
                --rigth;
            }

        }
    }
}

//        int left = -1;
//        int middle = 0;
//        int right = length;
//
//        while (middle < right) {
//            if ((array[middle] == 0 && isAscending) || (array[middle] == 2 && !isAscending)) {
//                left++;
//                swapChars(&(array[left]), &(array[middle]));
//                middle++;
//            }
//
//            else if (array[middle] == 1) middle++;
//
//            else {
//                right--;
//                swapChars(&(array[middle]), &(array[right]));
//            }
//        }
//    }


