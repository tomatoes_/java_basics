package maciej_b.part2;

/**
 * Created by RENT on 2017-03-02.
 */
public class QuickSortAlgorithm implements SortAlgorithm {

    private int[] numbers;
    private int number;

    @Override
    public void sort(int[] array, boolean ascending) {
     // napisać implementację sortowania QuickSort Iteracyjnie
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        if (array.length==0){
            return;
        }
        this.numbers = array;
        number = array.length;
        if(ascending) {
            quicksort(0, number - 1);
        } else if (!ascending){
            quicksortDescending(0, number -1);
        }
    }

    private void quicksortDescending(int low, int high) {
        int leftIndex = low, rightIndex = high;
        int pivot = numbers[low + (high-low)/2];
        while (leftIndex >= rightIndex) {
            while (numbers[leftIndex] > pivot) {
                leftIndex--;
            }
            while (numbers[rightIndex] < pivot) {
                rightIndex++;
            }
            if (leftIndex >= rightIndex) {
                exchange(leftIndex, rightIndex);
                leftIndex--;
                rightIndex++;
            }
        }
        if (low > rightIndex)
            quicksortDescending(low, rightIndex);
        if (leftIndex > high)
            quicksortDescending(leftIndex, high);
    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        int pivot = numbers[low + (high-low)/2];
        while (i <= j) {
            while (numbers[i] < pivot) {
                i++;
            }
            while (numbers[j] > pivot) {
                j--;
            }
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }

}




