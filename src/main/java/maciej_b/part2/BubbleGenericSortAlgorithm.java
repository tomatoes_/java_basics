package maciej_b.part2;

import java.util.Comparator;

/**
 * Created by RENT on 2017-03-04.
 */
public class BubbleGenericSortAlgorithm implements GenericSortAlgorithm {
    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {
        if (array==null){    // ewentualnie if(Objects.isNull)
            throw new IllegalArgumentException("Array should not be null");
        }

        int lastSortElement = array.length;
        while (lastSortElement >1){
            for (int i = 1; i < lastSortElement ; i++) {
                if (comparator.compare(array[i - 1], array[i]) == 1) {
                    T tmp =  array[i];
                    array[i] = array [i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortElement;
        }
    }
}
