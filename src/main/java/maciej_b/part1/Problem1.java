package maciej_b.part1;

import java.util.Arrays;

import static java.lang.System.out;


public class Problem1 {

    public static void main(String[] args) {


        for (int i = 1; i <=61 ; i+=2) {
                out.print(i+" ");
        }

        out.println();

        for (int i = 0; i <=6 ; i+=2) {
            out.print(i+" ");
        }
        for (int i = 4; i >=0 ; i-=2) {
            out.print(i+" ");
        }
        out.println();

        for (int i = 1; i <=9 ; i++) {
            out.print(i*100+" "+i*10+" ");
        }
        out.println();

        // 1, 1, 2, 3, 5, 8, 13
        //  i -- poprzednik
        // j -- następnik

        for (int i=1, j=1, k; i<20;k=j,j=j+i,i=k ){    // fibonacci !!!
            out.print(i+" ");
        }

        out.println();
        out.println();
        out.println(Arrays.toString(createFirstArray()));  // wywołanie (wyświetlenie) metody createFirstArray
    }

    public static int[] createFirstArray(){
        int array[] = new  int[31];
        for (int i = 0, j = 1; i <array.length ; i++, j+=2) {
            array[i]=j;
        }
        return array;
    }



    }

