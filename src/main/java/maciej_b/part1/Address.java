package maciej_b.part1;

import lombok.Builder;
import lombok.Data;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Address {

   private String streetName;
   private String flatName;
   private String postalCode;
   private String country;

   public static Address getRandomAddress(){
      return Address.builder()
              .streetName(randomAlphabetic(5))
              .flatName(randomAlphabetic(3))
              .postalCode(randomNumeric(5))
              .country(randomAlphabetic(6))
              .build();
   }

}
