package maciej_b.part1;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by RENT on 2017-02-28.
 */
public class Problem7Streams {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
//            String line = scanner.nextLine();
//            String[] split = line.split(" ");

            Stream.of(scanner.nextLine()
                                .split(" "))
                    .map(StringBuilder::new)    // mapowanie na StringBuilder
                    .map(StringBuilder::reverse)   // odwracanie StringBuildera
                    .map(StringBuilder::toString)   // mapowanie na Stringi
                    .map(BigInteger::new)           //mapowanie na BigoInteger
                    .reduce(BigInteger::add)
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(s -> s.replaceFirst("^0+(?!$)",""))
                    .ifPresent(System.out::println);


//            StringBuilder  numberA = new StringBuilder(split[0]);
//            StringBuilder  numberB = new StringBuilder(split[1]);
//
//            String numberAReversed =  numberA.reverse()
//                    .toString() ;
//            String numberBReversed =  numberB.reverse()
//                    .toString() ;
//
//            BigInteger bigNumberA = new BigInteger(numberAReversed);
//            BigInteger bigNumberB = new BigInteger(numberBReversed);
//            BigInteger sum =  bigNumberA.add(bigNumberB);
//
////            parseLong(numberAReversed);
////            parseLong(numberBReversed);
////            long sum = parseLong(numberAReversed)+ parseLong(numberBReversed);
//
//            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse()
//                    .toString();
//            System.out.println(sumReversed);


        }
    }

}
