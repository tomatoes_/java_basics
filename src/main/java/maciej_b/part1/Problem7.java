package maciej_b.part1;

import java.util.Scanner;

/**
 * Created by mac on 2017-02-27.
 */
public class Problem7 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();

            String[] split = line.split(" ");


            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lenghtA = arrayA.length;
            int lenghtB = arrayB.length;
            int newLenght;
            int smallerLenght;
            int longerLenght;
            char[] longerArray;

            if (lenghtA == lenghtB) {
                if (arrayA[lenghtA - 1] + arrayB[lenghtA - 1] >= '9') {
                    newLenght = lenghtA + 1;
                } else {
                    newLenght = lenghtA;
                }
                smallerLenght = lenghtA;
                longerArray = arrayA;

            } else if (lenghtA > lenghtB) {
                newLenght = lenghtA;
                smallerLenght = lenghtB;
                longerArray = arrayA;

            } else {
                newLenght = lenghtB;
                smallerLenght = lenghtA;
                longerArray = arrayB;
            }
            longerLenght = longerArray.length;

            char[] result = new char[newLenght];

            int overflow = 0;
            for (int i = 0; i < smallerLenght; ++i) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }

            }
            for (int i = smallerLenght; i < longerLenght; i++) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = 0; i < newLenght; i++) {
                System.out.print(result[i]);
            }


            if (result.length > longerLenght && overflow != 0) {
                System.out.print('1');
            }
        }

    }

    public static void main2(String[] args) {


        System.out.println("NWD = "+nwd(25,105));
        System.out.println();
        System.out.println("NWW = "+nww(41,46));

    }
    public static int nwd(int value1, int value2) {
        while (value1 != value2) {
            if (value1 > value2)
                value1 -= value2;
            else
                value2 -= value1;
        }
        return value1;
    }
    public static int nww(int value1, int value2){
        int NWW= ((value1*value2)/nwd(value1,value2));
        return NWW;
    }

}
