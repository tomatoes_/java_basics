package maciej_b.part1;

import java.sql.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static jdk.nashorn.internal.objects.NativeArray.forEach;

/**
 * Created by mac on 2017-02-26.
 */
public class Problem6 {
    public static void main(String[] args) {


        IntStream.range(0,10)
                .forEach(a -> System.out.print(a+" "));
        System.out.println();

        IntStream.range(0,10)
                .map(a -> a%2!= 0 ? (a*2)-1 :a )
                .forEach((a) -> System.out.print(a+" ") );
        System.out.println();

        IntStream.range(0,10)
                .map(a -> a%2!= 0 ? (a*2)-1 :a )
                .map(a -> a%2 == 0 ? a/2 : a)
                .forEach((a) -> System.out.print(a+" ") );
        System.out.println();

        int sum = IntStream.range(0,10)
                .map(a -> a%2!= 0 ? (a*2)-1 :a )
                .map(a -> a%2 == 0 ? a/2 : a)
                .sum();
        System.out.println(sum);




    }
}
