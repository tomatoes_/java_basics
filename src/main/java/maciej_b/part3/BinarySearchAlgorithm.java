package maciej_b.part3;

/**
 * Created by RENT on 2017-03-06.
 */
public class BinarySearchAlgorithm implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value) {
        if (array == null ){
            throw new IllegalArgumentException(" Array cant be null");
        }

        final int length = array.length;
        int firstElement = 0;
        int lastElement = length - 1;
        while (firstElement <= lastElement) {
            // Key is in a[firstElement..lastElement] or not present.
            int middleElement = firstElement + (lastElement - firstElement) / 2;
            if      (value < array[middleElement]) {
                lastElement = middleElement - 1;
            }else if (value > array[middleElement]){
                firstElement = middleElement + 1;
            }
            else return middleElement;
        }
        return -1;
    }
}
