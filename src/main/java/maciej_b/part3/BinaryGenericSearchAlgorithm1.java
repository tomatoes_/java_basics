package maciej_b.part3;

import java.util.Comparator;
import java.util.function.Function;

/**
 * Created by RENT on 2017-03-07.
 */
public class BinaryGenericSearchAlgorithm1 {
    public <T> int search (T[] array, Function<T, Integer> function){    // to będzie w interfejsie ...
        final int length = array.length;
        int left = 0, rigth = length-1, middle;
        while (left<rigth) {
            middle = left + rigth / 2;
            if (function.apply(array[middle]) < 0){
                left = middle + 1;
            }
            else if (function.apply(array[middle]) > 0 ){
                rigth = middle - 1;
            }
            else {
                return middle;
            }
        }
        return -1;
    }
}
