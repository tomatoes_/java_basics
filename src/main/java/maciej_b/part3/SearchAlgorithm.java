package maciej_b.part3;

/**
 * Created by RENT on 2017-03-04.
 */
public interface SearchAlgorithm {
    int search (int[] array, int value);

}
