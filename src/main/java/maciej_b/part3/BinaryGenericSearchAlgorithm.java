package maciej_b.part3;

import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-06.
 */
public class BinaryGenericSearchAlgorithm implements GenericSearchAlgorithm {

    @Override
    public <T extends Comparable> int search(T[] array, Predicate<T> predicate) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        final int length = array.length;
        int firstElement = 0;
        int lastElement = length - 1;

        while (firstElement <= lastElement) {
            int middleElement = firstElement + (lastElement - firstElement) / 2;
            if (!predicate.test(array[middleElement])) {
                lastElement = middleElement - 1;
            } else if (!predicate.test(array[middleElement])) {
                firstElement = middleElement + 1;
            } else {
                return middleElement;
            }
        }
        return -1;
    }
}


