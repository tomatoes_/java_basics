package maciej_b.part3;

/**
 * Created by mac on 2017-03-05.
 */
public class NaiveSearchAlgorithm implements SearchAlgorithm {
    @Override
    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array not be null");
        }
        for (int i = 0; i <array.length ; i++) {
            if (array[i] == value)
                return i;
        }
        return -1;
    }
}
