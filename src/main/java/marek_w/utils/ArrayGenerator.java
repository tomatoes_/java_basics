package marek_w.utils;

/**
 * Created by marekwojtowicz on 23.02.2017.
 */
public interface ArrayGenerator {
    int[] generate(int lenght, int rangeFrom, int rangeTo);  // przeciążanie metod
    int[] generate(int lenght);
}
