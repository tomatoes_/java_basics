package marek_w.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by marekwojtowicz on 23.02.2017.
 */
public class RandomArrayGenerator implements ArrayGenerator {

    public int[] generate(int lenght, int rangeFrom, int rangeTo) { //final sugeruje, ze metoda jest juz skoczona i nie ma co na niej robic
        //gdy mamy zamiar dziedziczyc i lekko zmieniac metody, to wtedy nie stosujemy final

        if(rangeFrom >= rangeTo){
            throw new IllegalArgumentException("Range is invalid");  //komunikat, że coś jest nie tak
        }
        int[] array = new int[lenght];
        Random random = new Random();
        for (int i = 0; i < array.length; ++i) {
            array[i] = random.nextInt(rangeTo-rangeFrom)+rangeFrom;
        }
        return array;
    }

    //    @Override //domyslnie kiedy implementujemy interfejs musimy nadpisać metody, wiec nie jest to potrzebne
    public final int[] generate(int lenght) {
        return generate(lenght, 0, MAX_VALUE);
    }
}
