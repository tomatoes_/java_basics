package marek_w.part1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by marekwojtowicz on 27.02.2017.
 */
public class Problem7ADDREV {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;

            String line = scanner.nextLine();

            String[] split = line.split(" ");

            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lenghtA = arrayA.length;
            int lenghtB = arrayB.length;

            int newLenght;
            int smallerLenght;
            int longerLenght;
            char[] longerArray;

            if (arrayA.length == arrayB.length) {
                if (arrayA[lenghtA - 1] + arrayB[lenghtA - 1] >= '9') {
                    newLenght = lenghtA + 1;
                } else {
                    newLenght = lenghtA;
                }
                smallerLenght = lenghtA;
                longerArray = arrayB;
            } else if (lenghtA > lenghtB) {
                newLenght = lenghtA;
                smallerLenght = lenghtB;
                longerArray = arrayA;
            } else {
                newLenght = lenghtB;
                smallerLenght = lenghtA;
                longerArray = arrayB;
            }
            longerLenght = longerArray.length;

            char[] result = new char[newLenght];

            int overflow = 0;
            for (int i = 0; i < smallerLenght; i++) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = smallerLenght; i < longerLenght; i++) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = 0; i < newLenght; i++) {
                System.out.print(result[i]);
            }

            if (newLenght > longerLenght && overflow != 0) {
                System.out.print("1");
            }
        }
    }

    private static void reverseInPlace(char[] toReverse) {
        char tmp;
        int length = toReverse.length;
        for (int i = 0; i < toReverse.length / 2; i++) {
            tmp = toReverse[i];
            toReverse[i] = toReverse[length -1-i];
            toReverse[length -1-i] = tmp;
        }
    }

    private static char[] reverse(char[] toReverse) {
        int length = toReverse.length;
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = toReverse[length-1-i];
        }
        return result;
    }
}
