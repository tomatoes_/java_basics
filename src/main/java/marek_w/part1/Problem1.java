package marek_w.part1;

/**
 * Created by marekwojtowicz on 22.02.2017.
 */
public class Problem1 {
    public static void main(String[] args) {

        //output: 1, 3, 5, 7, 9, 11 ... 61
        //dluższa metoda:
        for (int i = 0; i <= 61; i++) {
            if(i % 2 != 0){
                System.out.print(i + " ");
            }
        }
        System.out.println();
        //krótsza metoda
        for (int i = 1; i <= 61; i+=2) { //i=i+2  to inaczej i+=2
                System.out.print(i + " ");
        }
        System.out.println();

        //output: 0, 2, 4, 6, 4, 2, 0
        for (int i = 0; i <= 6 ; i+=2) {
            System.out.print(i + " ");
        }
        for (int i = 4; i >= 0 ; i-=2) {
            System.out.print(i + " ");
        }
        System.out.println();

        //output: 100, 10, 200, 20 ... 900, 90
        for (int i = 1; i < 10; i++) {
            System.out.print(i*100 + " " + i*10 + " ");
        } System.out.println();

        //output 1, 1, 2, 3, 5, 8, 13, 21, 34 ... 100 ciąg Fibonacciego
        //i - poprzednik
        //j - następnik
        for (int i = 1, j = 1, k; i < 100; k = j, j = j + i, i = k) {
            System.out.print("(" + i + ", " + j + ") ");
        } System.out.println();
        for (int i = 1, j = 1, k; i < 100; k = j, j = j + i, i = k) {
            System.out.print(i + " ");
        } System.out.println();
    }
}