package marek_w.part1;

import java.util.Scanner;

/**
 * Created by marekwojtowicz on 25.02.2017.
 */
public class Problem7 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String rangeInOneLine = input.nextLine();
        String[] split = rangeInOneLine.split(" ");
        String firstPart = split[0];
        String secondPart = split[1];
        int rangeFrom = Integer.parseInt(firstPart);
        int rangeTo = Integer.parseInt(secondPart);

        int flag = 0;
        for (int i = rangeFrom; i <= rangeTo; i++) {
            for (int j = 2; j < i; j++) {
                if(i % j == 0) {
                    flag = 0;
                    break;
                }
                else {
                    flag = 1;
                }
            }
            if (flag == 1) {
                System.out.print(i + " ");
            }
        }
    }
}