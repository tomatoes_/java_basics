package marek_w.part1;

/**
 * Created by marekwojtowicz on 22.02.2017.
 */
public class Problem2 {
    public static void main(String[] args) {
        drawTriangle(6);
        drawDiagonalLine(6);
        drawMirrorDiagonalLine(6);
        drawEmptySquare(8);
        drawCross(6);
    }

    //rysowanie "choinki"
    public static void drawTriangle(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i ; j++) {
                System.out.print('*');
            }
            System.out.println('*');
        }
    }


    //rysowanie przekątnej linii
    public static void drawDiagonalLine(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i ; j++) { //dodaje zawse i-1 spacji
                System.out.print(" ");
            }
            System.out.println("x"); //po dodawaniu spacji nastepuje dodanie '*'
        }
    }

 //rysowanie przekątnej linii w druga strone
    public static void drawMirrorDiagonalLine(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = height-1; j > i ; j = j - 1) { //j to nasza liczba spacji
                System.out.print(" ");
            }
            System.out.println("x"); //po dodawaniu spacji nastepuje dodanie '*'
        }
    }

    //rysowanie pustego kwadratu
    public static void drawEmptySquare(int height) {
        //Pierwsza linia
        for (int i = 1; i <= height; i++) {
            System.out.print("*");
            }
        //Przeskok na doł
        System.out.println();
        //Kolejne linie z przerwami
        for (int i = 1; i < height-1; i++) { //warunek, że lecimy od 2 linii aż do przedostatniej
            System.out.print("*"); //1 znak w danej linii
            for (int j = 2; j < height; j++) { //ilość spacji od 2 pozycji w linii
                System.out.print(" "); //wydruk spacji
            }
            System.out.print("*"); //gwiazdka na koniec linii
            System.out.println(); //nowa linia
        }
        //Ostatnia linia
        for (int i = 1; i <= height; i++) {
                System.out.print("*");
        }
        System.out.println();
    }

    //krzyzyk dla chetnych
    public static void drawCross(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                //jeżeli i(kolumna) = j(wiersz) lub kolumna+wiersz = wysokosci-1
                if (i == j || (i + j == height - 1)) {
                    System.out.print("x"); //to drukuje x
                } else { //jeżeli nie
                    System.out.print(" "); //drukuje spacje
                }
            } //gdy j będzie < height przerywa się pętla for
            System.out.println(); //przechodzimy do kolejnego wiersza i ponownie do petli for z j=0
        }
    }
}