package marek_w.part1;

/**
 * Created by marekwojtowicz on 23.02.2017.
 */
public class Problem4 {
    public static void main(String[] args) {

        //od 1 do 10
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i]=i;
        }
        for (int number:array) {
            System.out.print(number + " ");
        }
        System.out.println();


        //co drugą liczbę zwiększamy o wartość jej poprzednika
        //output: 0 1 2
        for (int i = 0; i < array.length; ++i) {
            array[i]=i;
        }
        for (int i = 1; i < array.length; i+=2) {
            array[i] += array[i-1];
        }
        for (int number:array) {
            System.out.print(number + " ");
        }
        System.out.println();

        //każdą liczbę parzystą dzielimy przez 2
        for (int i = 0; i < array.length; i++) {
            if(array[i] % 2 == 0){
                array[i] = array[i] / 2;
            }
        }
        for (int number:array) {
            System.out.print(number + " ");
        }
        System.out.println();

        //sumowanie
        int sum=0;
        for (int element:array) {
            sum += element;
        }
        System.out.println(sum);
    }
}
