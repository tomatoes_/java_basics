package marek_w.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by marekwojtowicz on 28.02.2017.
 */

//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
//Albo można wpisać tylko:

@Builder
@Data
public class Person {
    private String firstName, lastName;
    private String pesel;
    private char gender;
    private Adress adress;

    public static Person getRandomPerson() {
        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(8))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ? 'M' : 'F')
                .adress(Adress.getRandomAdress())
                .build();
    }
}
