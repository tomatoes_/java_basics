package marek_w.part1;
import lombok.*;
import org.apache.commons.lang3.RandomStringUtils;

import static org.apache.commons.lang3.RandomStringUtils.*;


/**
 * Created by marekwojtowicz on 28.02.2017.
 */

@Data
@Builder
public class Adress {
    private String streetName;
    private String flatNumber;
    private String country;
    private String postalCode;

    public static Adress getRandomAdress() {
        return Adress.builder()
                .streetName(randomAlphabetic(9))
                .flatNumber(randomNumeric(2) + randomAlphabetic(1).charAt(0))
                .country(randomAlphabetic(9))
                .postalCode(randomNumeric(5))
                .build();
    }
}
