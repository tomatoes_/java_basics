package marek_w.part1;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by marekwojtowicz on 28.02.2017.
 */
public class Problem7ADDREVotherSolution {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);

            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();

            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);
            BigInteger sum = bigNumberA.add(bigNumberB);

            // Long.parseLong(numberAReversed);
            // Long.parseLong(numberBReversed);

            // long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);

            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
            System.out.println(sumReversed);
        }
    }
}

// Stream od (split)
// .map(string builder)
// .map(reverse)
// .map(toString)
// .map(Big int)
// .reduce(add)  dotąd pomoże nam stream

// .map(String)
// .map(string builder)
// .map(reduce)
// .map to String