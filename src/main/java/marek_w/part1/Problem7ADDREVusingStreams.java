package marek_w.part1;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by marekwojtowicz on 28.02.2017.
 */
public class Problem7ADDREVusingStreams {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
//            String line = scanner.nextLine();
//            String[] split = line.split(" ");

            Stream.of(scanner.nextLine().split(" "))
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(BigInteger::new)
                    .reduce(BigInteger::add)
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(s -> s.replaceFirst("^0+(?!$)", ""))
                    .ifPresent(System.out::println);

//            StringBuilder numberA = new StringBuilder(split[0]);
//            StringBuilder numberB = new StringBuilder(split[1]);
//
//            String numberAReversed = numberA.reverse().toString();
//            String numberBReversed = numberB.reverse().toString();
//
//            BigInteger bigNumberA = new BigInteger(numberAReversed);
//            BigInteger bigNumberB = new BigInteger(numberBReversed);
//            BigInteger sum = bigNumberA.add(bigNumberB);
//
//            // long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);
//
//            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
//            System.out.println(sumReversed);


        }
    }
}
