package marek_w.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by marekwojtowicz on 07.03.2017.
 */

@Data
@AllArgsConstructor
public class StackElement<T> {
    private T value;
    private StackElement<T> previous;

}
