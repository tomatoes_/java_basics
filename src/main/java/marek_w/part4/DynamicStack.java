package marek_w.part4;

import java.util.Optional;

/**
 * Created by marekwojtowicz on 07.03.2017.
 */
public class DynamicStack<T> implements Stack<T> {

    private StackElement<T> top; //robimy wierzchołem
    private int size = 0;

    @Override
    public T pop() {
        --size;
        StackElement<T> oldTop = top;
        top = top.getPrevious();
        return oldTop.getValue();
    }

    @Override
    public T peek() {
        return Optional.ofNullable(top)
                        .map(StackElement::getValue)
                        .orElse(null);
//        if (nonNull(top)) {  //inny zapis (top != null)
//            return top.getValue();
//        }
//        else {
//            return null;
//        }
    }

    @Override
    public void push(T t) {
//        StackElement<> newElement = new StackElement<>(t, top);
//        top = newElement;
        top = new StackElement<>(t, top); //nie musimy pisać <T> bo java jest mądra
        ++size;
    }

    @Override
    public int size() {
        return size;
    }
}
