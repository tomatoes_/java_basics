package marek_w.part4;

/**
 * Created by marekwojtowicz on 07.03.2017.
 */
public interface Stack<T> {
    void push(T element);
    T pop();
    T peek();
    int size();
    default boolean isEmpty(){
        return size() == 0;
    }
}
