package marek_w.part2;

/**
 * Created by marekwojtowicz on 01.03.2017.
 */
public interface SortAlgorithm {
    //void bo ma nic nie zwracać, tylko posrotować
    default void sort(int[] array) {
        sort(array, true); //domyślnie sortujemy rosnąco
    }
    void sort(int[] array, boolean ascending);
}

