package marek_w.part2;

import java.util.function.Predicate;

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
public interface GroupSortAlgorithm {
    void sort (int[] array, Predicate<Integer> predicate);
}
