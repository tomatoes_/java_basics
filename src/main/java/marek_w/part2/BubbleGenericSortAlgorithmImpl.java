package marek_w.part2;

import java.util.Comparator;

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
public class BubbleGenericSortAlgorithmImpl implements GenericSortAlgorithm {

//    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {

        if(array==null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (comparator.compare(array[i - 1], array[i]) == 1)
                {
                    T tmp = array[i];
                    array[i] = array[i-1];
                    array[i-1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}