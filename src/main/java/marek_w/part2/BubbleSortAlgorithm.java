package marek_w.part2;

import marek_w.utils.ArrayGenerator;

/**
 * Created by marekwojtowicz on 01.03.2017.
 */
public class BubbleSortAlgorithm implements SortAlgorithm {

    @Override
    public void sort(int[] array, boolean ascending) {

        if(array==null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (ascending && array[i - 1] > array[i] ||
                        !ascending && array[i - 1] < array[i]) {
                    int tmp = array[i];
                    array[i] = array[i-1];
                    array[i-1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}
