package marek_w.part2;

import java.util.Comparator;

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
public interface GenericSortAlgorithm {
    default <T extends Comparable<T>> void sort(T[] array) {
        sort(array, T::compareTo);
    }
    <T> void sort (T[] array, Comparator<T> comparator);
}

    //Comparable - interfejs - obiekty mogą się porównywać do typu który ustaliliśmy
    //Comparator - metoda porównywania
