package marek_w.part3;

import marek_w.part2.GenericSortAlgorithm;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
public class GenericNaiveSearchAlgorithm implements GenericSearchAlgorithm {

    @Override
    public <T extends Comparable> int search(T[] array, Predicate<T> predicate) {
        if(array==null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        for (int i = 0; i < array.length; i++) {
            if (predicate.test(array[i])) {
                return i;
            }
        }
        return -1;
    }
}
