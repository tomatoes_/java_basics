package marek_w.part3;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
public interface GenericSearchAlgorithm {
    <T extends Comparable> int search(T[] array, Predicate<T> predicate);
}
