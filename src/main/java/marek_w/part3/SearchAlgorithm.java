package marek_w.part3;

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
public interface SearchAlgorithm {
        int search(int[] array, int value);
}
