package marek_w.part3;

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
public class NaiveSearchAlgorithm implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value) {
        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        if (array.length == 0) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                return i;
        }
        return -1;
    }
}
