package marek_w.part3;

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
public class BinarySearchAlgorithm implements SearchAlgorithm {
    @Override
    public int search(int[] array, int value) {
        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }

        int lowerNumber = 0;
        int higherNumber = array.length - 1;
        while (lowerNumber <= higherNumber) {
            int middleNumber = lowerNumber + (higherNumber - lowerNumber) / 2;
            if (value < array[middleNumber]) {
                higherNumber = middleNumber - 1;
            }
            else if (value > array[middleNumber]) {
                lowerNumber = middleNumber + 1;
            }
            else return middleNumber;
        }
        return -1;
    }
}
