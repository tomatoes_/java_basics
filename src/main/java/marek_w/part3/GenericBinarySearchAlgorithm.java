package marek_w.part3;

import java.util.function.Function;

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
public class GenericBinarySearchAlgorithm {

    public <T> int search(T[] array, Function<T, Integer> function) { //ta linjka będzie w interface

        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }

        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int middle = left + (right-left) / 2;
            if (function.apply(array[middle]) > 0) {
                left = middle + 1;
            }
            else if (function.apply(array[middle]) < 0) {
                right = middle - 1;
            }
            else return middle;
        }


        return -1;
    }
}
