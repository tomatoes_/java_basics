package pawel_f.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

public class RandomUtils {

    private static Random random = new Random();

    public static int[] generateRandomIntArray(int length) {
        return generateRandomIntArray(length, 0, MAX_VALUE);
    }

    public static int[] generateRandomIntArray(int length, int from, int to) {
        int[] array = new int[length];
        if (from >= to) {
            throw new IllegalArgumentException("bottom range limit must be " +
                                               "smaller than the top range limit");
        }
        for (int i = 0; i < array.length; ++i) {
            array[i] = random.nextInt(to - from) + from;
        }
        return array;
    }
}
