package pawel_f.part1;

import java.util.Arrays;

public class Problem4 {
    public static void main(String[] args) {
        int[] myArray = new int[10];

        for (int i = 0; i < myArray.length; ++i) {
            myArray[i] = i;
        }

        System.out.println(Arrays.toString(myArray));

        for (int i = 1; i < myArray.length; i += 2) {
            myArray[i] += myArray[i - 1];
        }

        System.out.println(Arrays.toString(myArray));

        for (int i = 0; i < myArray.length; ++i) {
            if (myArray[i] % 2 == 0) {
                myArray[i] /= 2;
            }
        }

        System.out.println(Arrays.toString(myArray));

        int sum = 0;
        for (int i = 0; i < myArray.length; ++i) {
            sum += myArray[i];
        }

        System.out.println(sum);
    }
}
