package pawel_f.part1;

import java.util.Arrays;

public class Problem3 {
    public static void main(String[] args) {
        int[] myArray = new int[8];
        myArray[2] = 5;
        myArray[4] = 8;

        System.out.println(Arrays.toString(myArray));

        int tmp = myArray[2];
        myArray[2] = myArray[4];
        myArray[4] = tmp;

        System.out.println(Arrays.toString(myArray));
    }
}
