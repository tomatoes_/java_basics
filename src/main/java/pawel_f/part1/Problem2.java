package pawel_f.part1;

public class Problem2 {
    public static void main(String[] args) {
        drawTriangle(6);
        System.out.println();
        drawDiagonalLine(6);
        System.out.println();
        drawSquare(6);
        System.out.println();
        drawCross(6);
    }

    private static void drawTriangle(int height) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j <= i; ++j) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    private static void drawDiagonalLine(int height) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < i; ++j) {
                System.out.print(' ');
            }
            System.out.println('*');
        }
    }

    private static void drawSquare(int size) {
        for (int i = 0; i < size; ++i) {
            System.out.print('*');
            for (int j = 1; j < size - 1; ++j) {
                if (i == 0 || i == size - 1) {
                    System.out.print('*');
                }
                else {
                    System.out.print(' ');
                }
            }
            System.out.println('*');
        }
    }

    private static void drawCross(int size) {
        for (int i = 0; i < size; ++i) {
            if (i < size / 2) {
                for (int j = 0; j < i; ++j) {
                    System.out.print(' ');
                }
                System.out.print('*');
                for (int j = 0; j < size - (i * 2) - 2; ++j) {
                    System.out.print(' ');
                }
            } else {
                for (int j = 0; j < size - i - 1; ++j) {
                    System.out.print(' ');
                }
                System.out.print('*');
                for (int j = 0; j < (i - (size / 2)) * 2; ++j) {
                    System.out.print(' ');
                }
            }
            System.out.println('*');
        }
    }
}
