package kamil_j.part4;


public interface List<T> extends Iterable<T>{
    T getFirst();
    T getLast();
    T get (int index);
    void add(T element);
    void remove (int index);
    int size();
    default boolean isEmpty(){
        return size() == 0;
    }
}
