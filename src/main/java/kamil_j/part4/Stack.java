package kamil_j.part4;


public interface Stack<T> {
    T pop();
    T peek();
    void push(T t);
    int size();
    default boolean isEmpty() {
        return size() == 0;
    }

}
