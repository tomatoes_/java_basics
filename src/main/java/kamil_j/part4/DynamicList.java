package kamil_j.part4;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.lang.model.element.Element;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class DynamicList<T> implements List<T> {

    private ListElement<T> first;
    private ListElement<T> last;
    private int size = 0;


    public T getFirst() {
        return getChecked(first);
//        if(first == null) {
//            throw new IndexOutOfBoundsException();
//        }
//        return first.getValue();
    }

    public T getLast() {
        return getChecked(last);
    }

    private T getChecked(ListElement element){
        return ofNullable(element)
                .map(ListElement::getValue)
                .orElseThrow(IndexOutOfBoundsException::new);
    }

    public T get(int index) {
        ListElement element = first;
        while(index -- > 0){
            element = element.getNext();
        }
        return element.getValue();
    }

    public void add(T element) {
        if(first == null) {
           first = new ListElement(element, null, null);
           last = first;
        } else {
            ListElement newElement = new ListElement(element, last, null);
            last.next = newElement;
            last = newElement;
        }
    }

    public void remove(int index) {
        size--;
    }

    public int size() {
        return size;
    }

    @Data
    @AllArgsConstructor
    public class ListElement {
        private T value;
        private ListElement previous;
        private ListElement next;
    }
}
