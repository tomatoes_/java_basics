package kamil_j.part1;

public class Problem2 {
    public static void main(String[] args) {
//        drawTriangle(5);
//        drawSlash2(5);
//        drawSquare(6);
    }


    public static void drawTriangle(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    public static void drawSlash2(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");
            }
            System.out.println('*');
        }
    }

    public static void drawSquare(int height) {
        for (int i = 0; i <= height; i++) {
            if (i == 0) {
                for (int j = 0; j <= height; j++) {
                    System.out.print('*');
                }
                System.out.println();
            } else if (i == height)
                for (int j = 0; j <= height; j++) {
                    System.out.print('*');
                }
            else {
                System.out.println("*");
            }
        }
    }
}
