package kamil_j.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder

public class Address {
    private String streetName;
    private String flatNumber;
    private String country;
    private String postalCode;

    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(5))
                .flatNumber(randomNumeric(3))
                .country(randomAlphabetic(5))
                .postalCode(randomNumeric(6))
                .build();
    }
}
