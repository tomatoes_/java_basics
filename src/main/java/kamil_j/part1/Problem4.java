package kamil_j.part1;


public class Problem4 {
    public static void main(String[] args) {
        int[] array = new int[100];

        for (int i = 0; i < array.length; ++i) {
            array[i] = i;
        }

        for (int i = 0; i < array.length; ++i) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        for (int i = 1; i < array.length; i += 2) {
            array[i] = array[i] + array[i - 1];
        }

        for (int i = 0; i < array.length; ++i) {
            System.out.print(array[i] + " ");
        }

        for (int i = 0; i < array.length; ++i) {
            if (i % 2 == 0) {
                array[i] = array[i] / 2;
            }
        }

        System.out.println();

        for (int i = 0; i < array.length; ++i) {
            System.out.print(array[i] + " ");
        }

        int sum = 0;
        for (int i = 0; i < array.length; ++i) {
            sum += array[i];
        }
        System.out.println();
        System.out.println(sum);


    }

}


