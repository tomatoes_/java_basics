package kamil_j.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Long.*;


public class Problem7 {
    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAReversed = numberA.reverse()
                    .toString();
            String numberBReversed = numberB.reverse()
                    .toString();
            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);
            BigInteger sum = bigNumberA.add(bigNumberB);
//            long sum = parseLong(numberAReversed) + parseLong(numberBReversed);
            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse()
                    .toString();

            System.out.println(sumReversed);
        }
    }
}

//    public static void main2(String[] args) {

//        System.out.println("Podaj pierwszą liczbe: ");
//        Scanner input = new Scanner(System.in);
//        int x = input.nextInt();
//
//        System.out.println("Podaj drugą liczbe: ");
//        int y = input.nextInt();
//
//        System.out.println("NWD = "+nwd(x,y));
//
//        System.out.println("NWW = " + ((x*y)/nwd(x,y)));
//        char[] test = {'1', '2', '3'};
//        reverseInPlace(test);
//        System.out.println(Arrays.toString(test));
//        reverse(test);
//        System.out.println(Arrays.toString((test)));

//        int lengthA = 5;
//        int lengthB = 8;
//        int length;
//        if (lengthA > lengthB) {
//            length = lengthA + 1;
//        } else {
//            length = lengthB + 1;
//        }

//        char[] arrayA = {'1', '2', '3'};
//        char[] arrayB = {'4', '5', '6'};

//        Scanner scanner = new Scanner(System.in);
//        int problems = Integer.parseInt(scanner.nextLine());
//        while (problems >= 0) {
//            --problems;
//            String line = scanner.nextLine();
//            String[] split = line.split(" ");
//            char[] arrayA = split[0].toCharArray();
//            char[] arrayB = split[1].toCharArray();
//
//            int lengthA = arrayA.length;
//            int lengthB = arrayB.length;
//
//            int newLength = lengthA > lengthB ? lengthA : lengthB;
//
//            int smallerLength;
//            int longerLength;
//            char[] longerArray;
//
//            if (lengthA == lengthB) {
//                if ((arrayA[lengthA - 1] + arrayB[lengthA - 1]) >= '9') {
//                    newLength = lengthA + 1;
//                } else {
//                    newLength = lengthA;
//                }
//                smallerLength = lengthA;
//                longerLength = lengthB;
//                longerArray = arrayA;
//            } else if (lengthA > lengthB) {
//                newLength = lengthA;
//                smallerLength = lengthB;
//                longerLength = lengthA;
//                longerArray = arrayA;
//            } else {
//                newLength = lengthB;
//                smallerLength = lengthA;
//                longerLength = lengthB;
//                longerArray = arrayB;
//            }
//            longerLength = longerArray.length;
//
//            char[] result = new char[newLength];
//
//            int overflow = 0;
//
//            for (int i = 0; i < smallerLength; i++) {
//                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
//                if (result[i] > '9') {
//                    result[i] -= 10;
//                    overflow = 1;
//                } else {
//                    overflow = 0;
//                }
//            }
//
//            for (int i = smallerLength; i < longerLength; i++) {
//                result[i] = (char) (longerArray[i] + overflow);
//                if (result[i] > '9') {
//                    result[i] -= 10;
//                    overflow = 1;
//                } else {
//                    overflow = 0;
//                }
//            }
//
//            for (int i = 0; i < newLength; i++) {
//                System.out.print(result[i]);
//            }
//
//            if (result.length > longerLength && overflow != 0) {
//                System.out.print('1');
//            }
//        }
//    }
//    public static int nwd(int x, int y) {
//
//        while (x != y) {
//            if (x > y)
//                x -= y;
//            else
//                y -= x;
//        }
//        return x;
//    }

//    private static void reverseInPlace(char[] toReverse) {
//        int length = toReverse.length;
//        char tmp;
//        for (int i = 0; i < toReverse.length / 2; i++) {
//            tmp = toReverse[i];
//            toReverse[i] = toReverse[length - i - 1];
//            toReverse[length - i - 1] = tmp;
//        }
//    }
//
//    private static char[] reverse(char[] toReverse) {
//        int length = toReverse.length;
//        char[] result = new char[length];
//        for (int i = 0; i < length; ++i) {
//            result[i] = toReverse[length - i - 1];
//        }
//        return result;
//    }
//}
