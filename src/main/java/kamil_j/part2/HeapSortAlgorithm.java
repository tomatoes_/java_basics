package kamil_j.part2;


import java.util.Arrays;

public class HeapSortAlgorithm implements SortAlgorithm{
    @Override
    public void sort(int[] array, boolean ascending) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null.");
        }
        int n = array.length;

        // Build heap (rearrange array)

        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(array, n, i, ascending);

        // One by one extract an element from heap

        for (int i = n - 1; i >= 0; i--) {
            // Move current root to end
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            // call max heapify on the reduced heap
            heapify(array, i, 0, ascending);
        }

    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    void heapify(int arr[], int n, int i, boolean ascending) {
        int largest = i;  // Initialize largest as root
        int l = 2 * i + 1;  // left = 2*i + 1
        int r = 2 * i + 2;  // right = 2*i + 2

        // If left child is larger than root
        if (l < n && arr[l] > arr[largest]==ascending)
            largest = l;

        // If right child is larger than largest so far
        if (r < n && arr[r] > arr[largest]==ascending)
            largest = r;

        // If largest is not root
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            // Recursively heapify the affected sub-tree
            heapify(arr, n, largest, ascending);
        }
    }
}
