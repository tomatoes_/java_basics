package kamil_j.part2;


public class BubbleSortAlgorithm implements SortAlgorithm {
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        for (int j = 0; j < array.length; j++)
            for (int i = 0; i < array.length - 1; i++) {
                if (ascending && array[i] > array[i + 1]) {
                    int tmp;
                    tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                } else if (!ascending && array[i] < array[i + 1]) {
                    int tmp;
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
    }
}
