package kamil_j.part2;


import java.util.function.Predicate;

public interface GroupSortAlgorithm {
    void sort(int[] array, Predicate<Integer> predicate);
}
