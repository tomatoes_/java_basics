package kamil_j.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;


public class RandomArrayGenerator implements ArrayGenerator {


    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        int[] array = new int[lenght];
        Random random = new Random();
        for (int i = 0; i < lenght; ++i) {
            array[i] = random.nextInt(rangeTo - rangeFrom) + rangeFrom;
        }
        return array;
    }

    public final int[] generate(int lenght) {
        return generate(lenght, 0, MAX_VALUE);

    }
}
