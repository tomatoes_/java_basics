package kamil_j.utils;


public interface ArrayGenerator {
    int[] generate(int lenght, int rangeFrom, int rangeTo);
    int[] generate(int lenght);
}