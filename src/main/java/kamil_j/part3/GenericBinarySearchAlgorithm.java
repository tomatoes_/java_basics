package kamil_j.part3;


import java.util.function.Function;

public class GenericBinarySearchAlgorithm implements GenericBinarySearchAlgorithmInterface{
    public <T> int search(T[] array, Function<T, Integer> function) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int left = 0;
        int right = array.length - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;
            if (function.apply(array[middle]) > 0) {
                return middle + 1;
            }
            if (function.apply(array[middle]) < 0) {
                left = middle - 1;
            } else return middle;
        }
        return -1;
    }
}
