package kamil_j.part3;


import java.util.function.Function;

public interface GenericBinarySearchAlgorithmInterface {
    <T> int search(T[] array, Function<T, Integer> function);
    }
