package kamil_j.part3;


public interface SearchAlgorithm {
    int search(int[] array, int value);
}
