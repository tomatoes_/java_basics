package kamil_j.part3;


public class BinarySearchAlgorithm implements SearchAlgorithm {
    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int left = 0;
        int right = array.length - 1;

        while (left < right) {
            int middle = (left + right) / 2;
            if (array[middle] == value) {
                return middle;
            }
            if (array[middle] < value) {
                left = middle + 1;
            } else if (array[middle] > value) {
                right = middle - 1;
            } else return middle;
        }
        return -1;
    }

}
