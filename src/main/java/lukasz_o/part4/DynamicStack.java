package lukasz_o.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by RENT on 2017-03-07.
 */
public class DynamicStack<T> implements Stack<T> {

    private StackElement top;
    private int size = 0;


    @Override
    public void push(T t) {
        top = new StackElement(t, top);
        size++;
    }

    @Override
    public T pop() {
        --size;
        StackElement oldTop = top;
        top = top.getPrevious();
        return oldTop.getValue();
    }

    @Override
    public T peek() {
        if (top != null) {
            return top.getValue();
        }
        else return null;
    }

    // return Optional.ofNullable(top)         // albo optionalem
    // .map(StackElement::getValue)
    // .orElse(null);

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
    @Data
    @AllArgsConstructor

    public class StackElement {      // nie musi mieć już typu generycznego <T> bo ma go już na górze

        private T value;
        private StackElement previous;  //referencja do poprzendniego elementu         // nie musi mieć już typu generycznego <T> bo ma go już na górze
    }

}
