package lukasz_o.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;
import java.util.Optional;

/**
 * Created by RENT on 2017-03-08.
 */
public class DynamicList<T> implements List<T> {

    private ListElement first;
    private ListElement last;
    private int size = 0; // dla wygody

    @Override
    public T getFirst() {
//        if (first == null) {
//            throw new IndexOutOfBoundsException();
//        }
        return getChecked(first);
    }

    @Override
    public T getLast() {
        return getChecked(last);
    }

    private T getChecked(ListElement element) {
        return Optional.ofNullable(element)
                .map(ListElement::getValue)
                .orElseThrow(IndexOutOfBoundsException::new);
    }

    @Override
    public T get(int index) {
        return getElement(index).getValue();
    }

    private ListElement getElement (int index) {
        ListElement element = first;
        while (index -- >0) {
            element = element.getNext();
        }
        return element;

    }

    @Override
    public void add(T element) {
        if (first == null) {
            first = new ListElement(element, null, null);
            last = first;
        } else {
//        last = last.next =                      // to będzie wykonywane od prawej strony
            ListElement newElement = new ListElement(element, last, null);
            last.next = newElement;
            last = newElement;
        }
    }

    @Override
    public void remove(int index) {    /// przypadki dla pierwszego i ostatniego elementu
       ListElement toRemove = getElement(index);
        toRemove.previous.next = toRemove.next;  // 0 --- 0 --- 0
        toRemove.next.previous = toRemove.previous;
    }

    @Override
    public int size() {
        return 0;
    }


    @Override
    public Iterator<T> iterator() {
        return new Iterator<T> () {
            ListElement ref = first;

            public boolean hasNext() {
                return ref != null;
            }

            public T next() {
                ref = ref.next;
                return ref.previous.getValue();
            }
        };
    }

    @Data
    @AllArgsConstructor

    public class ListElement {

        private T value;
        private ListElement previous;
        private ListElement next;
    }

}
