package lukasz_o.part4;

import java.util.function.Function;

/**
 * Created by RENT on 2017-03-07.
 */
public class ArrayStack<T> implements Stack<T> {

    private T[] array;
    private int size = 0;
    // stos będzie ograniczony przez rozmiar tabeli

    @SuppressWarnings("unchecked")
    public ArrayStack(int size) {

//        top = new T[size];   // new T nie przejdzie bo to obiekt dowolnego typu
        array = (T[]) new Object[size];
    }

    @Override
    public void push(T t) {
       if (size == array.length-1) {
           throw new RuntimeException("Stack overflow");
       }
        array[++size] = t;   // preiknkrementacja - zwiększymy wartość size i dopiero potem przekażemy jako wartość tablicy
    }

    @Override
    public T pop() {

        return array[size--]; // postinkrementacja - najpierw wyrzuci wartość a dopiero potem ją zmniejszy !
    }

    @Override
    public T peek() {
        return array[size];
    }

    // return Optional.ofNullable(top)         // albo optionalem
    // .map(StackElement::getValue)
    // .orElse(null);

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }


}
