package lukasz_o.part4;

/**
 * Created by RENT on 2017-03-07.
 */
public interface Stack <T> {    // od razu stos, ktory jest generyczny

    void push(T element);
    T pop();        // int

    T peek();       // int

    int size();       // int


//    T isEmpty();    // boolean

    default boolean isEmpty() {
        return size() == 0;

    }
}