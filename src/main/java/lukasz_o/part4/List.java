package lukasz_o.part4;

/**
 * Created by RENT on 2017-03-08.
 */
public interface List<T> extends Iterable<T> {

    T getFirst();
    T getLast();
    T get(int index);
    void add(T element);
    void remove (int index);
    int size();
    default boolean isEmpty() {
        return size() == 0;
    }

}
