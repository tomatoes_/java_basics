package lukasz_o.part2;

/**
 * Created by RENT on 2017-03-01.
 */
public class BubbleSortAlgorithm2 implements SortAlgorithm {


    @Override
    public void sort(int[] array, boolean ascending) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }


        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (ascending && array[i - 1] > array[i] || ascending && array[i - 1] < array[i]) {

                    int tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }

            }
            lastSortedElement--;


//    public static void createTable() {
//        int[] ble = new int[10];
//
//        for (int i = 0; i < ble.length; i++) {
//            Random random = new Random();
//            ble[i] = random.nextInt();
//            System.out.println(ble[i]);
//        }
//
//
//    }

        }
    }
    }


