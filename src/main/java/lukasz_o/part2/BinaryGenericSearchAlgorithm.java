package lukasz_o.part2;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-03-07.
 */
public class BinaryGenericSearchAlgorithm {

    public <T> int search(T[] array, Function<T, Integer> function) {
        final int length = array.length;
        int left = 0;
        int right = length - 1;
        int middle;

        while (left <= right) {
            middle = left + (right - left) / 2;
            if (function.apply(array[middle]) > 0) left = middle + 1;
            else if (function.apply(array[middle]) < 0) {
                right = middle - 1;
            } else return middle;

        }
        return -1;
    }
}
