package lukasz_o.part2;

import java.lang.reflect.Array;
import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-04.
 */
public class PolishFlagGroupSortAlgorithm implements GroupSortAlgorithm {


    @Override
    public void sort(int[] array, Predicate<Integer> predicate) {
        // Sortowanie metodą flagi polskiej.

        if (array == null) {
            throw new IllegalArgumentException();
        }

        if (array.length == 0) {
            return;
        }

        int left = 0;
        int right = array.length - 1;
        int tmp;

        do {
            while (predicate.test((array[left])))
                left++;
            while (!predicate.test(array[right]))
                right--;


            if (left < right) {
                tmp = array[left];
                array[left] = array[right];
                array[right] = tmp;
            }

        }while (left < right) ;
    }

}
