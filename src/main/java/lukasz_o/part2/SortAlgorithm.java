package lukasz_o.part2;

/**
 * Created by RENT on 2017-03-01.
 */
public interface SortAlgorithm {

//    void sort(int[] array, boolean ascending);  // statyczne metody należą do klasy a nie obiektu // public nie bo w interfejsie wszystko jest publiczne

    default void sort (int[] array) { sort (array, true);}  // default żeby w interfejsie dąło się dodać ciało do metody // defaulta nie trzeba implementować

    void sort (int[] array, boolean ascending);
}
