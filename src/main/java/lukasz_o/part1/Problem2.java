package lukasz_o.part1;

/**
 * Created by RENT on 2017-02-22.
 */
public class Problem2 {

    public static void main(String[] args) {

        drawTriangle(10);
        System.out.println();
        drawSzczecin(10);
        System.out.println();
        rectangle(7);
    }


    public static void drawTriangle(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");

            }
            System.out.println();

        }
    }

    public static void drawSzczecin(int height) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(" ");

            }
            System.out.println("*");

        }
    }

    public static void rectangle(int t) {
        int i = 0;
        for (i = 0; i < t + 1; i++) {
            if ((i == 0) || (i == t - 1)) {
                for (int j = 0; j < t + 1; j++) {
                    System.out.print("*");

                }
            } else {
                System.out.print("*");
                for (int j = 0; j < t; j++) {
                    System.out.print(" ");
                }
                if ((i > 1) && (i < t - 1)) System.out.print("*");

                System.out.println();
            }

        }
    }

}










