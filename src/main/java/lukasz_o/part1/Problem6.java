package lukasz_o.part1;

import java.math.BigInteger;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by RENT on 2017-02-28.
 */
public class Problem6 {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            problems--;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            Stream.of(split)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(BigInteger::new)
                    .reduce(BigInteger::add) // wynik: optional
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(s -> s.replaceFirst("^0+(?!$)", ""))
                    .ifPresent(System.out::println);  // foreach tu nie zadziała (bo jest optional)



            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAReversed = numberA.reverse()
                    .toString();   // reverse sam w sobie zwraca stringbuildera
            String numberBReversed = numberB.reverse()
                    .toString();   // reverse sam w sobie zwraca stringbuildera

            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);
            BigInteger sum = bigNumberA.add(bigNumberB);

//            Long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);
            // zamienia stringa na longa ( razie jak int za mały)

            String sumReversed = new StringBuilder(String.valueOf(sum))  // nowy stringbuilder na podstawie wartości naszej sumy
                    .reverse()
                    .toString();

            System.out.println(sumReversed);


        }


    }

}
