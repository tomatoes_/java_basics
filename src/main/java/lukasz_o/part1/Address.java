package lukasz_o.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by RENT on 2017-02-28.
 */

@Builder
@Data
public class Address {

    private String streetName;
    private String flatNumber;
    private String postalCode;
    private String country;

    public Address(String streetName, String flatNumber, String postalCode, String country) {
        this.streetName = streetName;
        this.flatNumber = flatNumber;
        this.postalCode = postalCode;
        this.country = country;
    }

    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(5))
                .flatNumber(randomAlphabetic(3))
                .postalCode(randomAlphabetic(5))
                .country(randomAlphabetic(6))
                .build();


    }
}
