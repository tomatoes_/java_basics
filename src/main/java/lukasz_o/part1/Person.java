package lukasz_o.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by RENT on 2017-02-28.
 */
//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter

@Data
@Builder

public class Person {

    String firstName;
    String lastName;
    String pesel;
    char gender;
    Address address;

    public static Person getRandomPerson() {
        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(6))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ? 'M' : 'K') //(randomAlphabetic(1).charAt(0))
                .address(Address.getRandomAddress())
                .build();

    }



}




