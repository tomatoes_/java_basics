package lukasz_o.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by RENT on 2017-02-27.
 */
public class Problem5 {

    public static void main3 (String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            problems--;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAReversed = numberA.reverse()
                    .toString();   // reverse sam w sobie zwraca stringbuildera
            String numberBReversed = numberB.reverse()
                    .toString();   // reverse sam w sobie zwraca stringbuildera

            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);
            BigInteger sum = bigNumberA.add(bigNumberB);

//            Long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);
            // zamienia stringa na longa ( razie jak int za mały)

            String sumReversed = new StringBuilder(String.valueOf(sum))  // nowy stringbuilder na podstawie wartości naszej sumy
                        .reverse()
                        .toString();

            System.out.println(sumReversed);


        }


    }


    public static void main2(String[] args) {

//        char[] table = {'1','3','5'};
//        char[] table2 = {'4','6','7'};
//        System.out.println(reverseTable(table));
//        System.out.println();
//        reverseTable2(table);
//        reverseTable2(table2);
//        System.out.println(Arrays.toString(table));
//        System.out.println();
//        System.out.println(Arrays.toString(table2));

//        int lengthA = 5;
//        int lengthB = 8;
//        int length = lengthA > lengthB ? lengthA + 1 : lengthB + 1;
//        if (lengthA > lengthB) {    to samo co linijka powyżej
//            length = lengthA + 1;
//
//        }
//        else length = lengthB + 1;

        char[] arrayA = {'1', '2', '3'};
        char[] arrayB = {'4', '5', '6'};  // bez cudzysłowiu pokaże np. 4 element w tablicy ASCII


    }

    private static char[] reverseTable(char[] table) {

        int length = table.length;
        char[] table1 = new char[length];   // ważne, żeby stworzyć drugą tablicę
        for (int i = 0; i < length; i++) {
            table1[i] = table[length - i - 1];
        }
        return table1;
    }

    private static void reverseTable2(char[] table) {

        char tmp;

        for (int i = 0; i < table.length / 2; i++) {
            tmp = table[i];
            table[i] = table[table.length - i - 1];
            table[table.length - i - 1] = tmp;
        }

    }


}
