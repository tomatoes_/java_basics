package lukasz_o.part1;

import java.util.Arrays;

/**
 * Created by RENT on 2017-02-22.
 */
public class Problem1 {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(createFirstArray()));

        for (int i = 1; i < 62; i++) {

            if (i % 2 != 0) {
                if (i == 61) {
                    System.out.println(i);
                } else {
                    System.out.print(i + ", ");
                }

            }

        }

        for (int i = 1; i < 62; i += 2) {

            System.out.print(i + " ");


        }
        System.out.println();

        for (int i = 0; i <= 6; i += 2) {

            System.out.print(i + " ");
        }

        for (int i = 4; i >= 0; i -= 2) {

            System.out.print(i + " ");
        }

        System.out.println();

        for (int i = 100; i < 1000; i += 100) {
            int j = i / 10;
            System.out.print(i + " " + j + " ");

        }

        System.out.println();

        // ciąg Fibonacciego
        // 1,1,2,3,5,8,13... 100
        // i = poprzednik
        // j = następnik

        for (int i = 1, j = 1, k; i < 100; k = j, j = i + j, i = k) {
            System.out.print(i + " ");
        }
    }

    public static int[] createFirstArray() {
        int[] array = new int[31];
        for (int i = 0, j = 1; i < array.length; ++i, j += 2) {
            array[i] = j;

        }
        return array;


    }
}





