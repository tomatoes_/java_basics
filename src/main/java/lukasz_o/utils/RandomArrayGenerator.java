package lukasz_o.utils;

import java.util.Random;

/**
Random * Created by RENT on 2017-02-23.
 */
public class RandomArrayGenerator implements ArrayGenerator {

    @Override

        public final int[] generate(int length, int rangeFrom, int rangeTo) {  // z final nie możemy potem zmienić tej metody (dziedzicząc po klasie nie można napisać potem tej metody
            // final to kontrakt między mną a użytkownikiem kodu
            // z final programista stwierdził że metoda jest ok i jej nie zmieniaj, nadaje się tylko do dziediczenia bez zmiany
            // wszystkie metody powinniśmy oznaczać jako final oprócz tylko które mają być zmieniane

            if (rangeFrom >=  rangeTo) {
                throw new IllegalArgumentException("Range is invalid");  // zakres podawany przez użytkownika jest argumentem
            }


            int[] array = new int[length];

            Random random = new Random();   /// klasa Random daje fajne metody

            for (int i = 0; i < length; i++) {
//                array[i] = random.nextInt();  // dla każdego indeks dodaje losowy element (int)
//                int r = 0;
//                while (r < rangeFrom || r > rangeTo) {    // to może trwać tydzień
//                    r = random.nextInt();
//                }
            array[i] = random.nextInt(rangeTo - rangeFrom) + rangeFrom;
            }

            return array;


    }


    public final int[] generate (int length) {
        return generate (length, 0, Integer.MAX_VALUE);
    }


}
