package lukasz_o.utils;

/**
 * Created by RENT on 2017-02-23.
 */
public interface ArrayGenerator {

    int[] generate(int length, int rangeFrom, int rangeTo);

    int[] generate(int length);

}
