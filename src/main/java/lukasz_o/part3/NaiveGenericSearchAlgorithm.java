package lukasz_o.part3;

import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-06.
 */
public class NaiveGenericSearchAlgorithm implements GenericSearchAlgorithm {

    public <T extends Comparable> int search (T[] array, Predicate<T> predicate) {
        for (int i = 0; i < array.length; i++) {
            if(predicate.test(array[i])) {
                return 1;
            }
        }
        return -1;
    }

}
