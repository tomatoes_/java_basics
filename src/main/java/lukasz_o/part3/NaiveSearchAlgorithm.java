package lukasz_o.part3;

/**
 * Created by RENT on 2017-03-06.
 */
public class NaiveSearchAlgorithm {
    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array can't be null");
        }
        if (array.length == 0) {
            throw new IllegalArgumentException("Array has zero elements");
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                return i;
        }
        return -1;
    }
}
