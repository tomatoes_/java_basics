package maciek_z.utils;

/**
 * Created by RENT on 2017-02-23.
 */
public interface ArrayGenerator {
    int[] generate(int lenght, int rangeFrom, int rangeTo);
    int[] generate(int lenght);
}
