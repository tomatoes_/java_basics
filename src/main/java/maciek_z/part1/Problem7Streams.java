package maciek_z.part1;

import java.util.Scanner;

import static java.lang.Long.parseLong;

public class Problem7Streams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);

            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();

            long sum = parseLong(numberAReversed) + parseLong(numberBReversed);

            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();

            System.out.println(sumReversed);


        }


    }

}
