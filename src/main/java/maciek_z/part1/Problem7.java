package maciek_z.part1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class Problem7 {
    private static InputReader in = new InputReader(System.in);
    public static void main(String []args) throws Exception
    {
        int t=in.readInt();
        while(t>0)
        {
            int n=in.readInt();
            int m=in.readInt();
            int sqr=(int)Math.sqrt(m);
            ArrayList<Integer> al=new ArrayList<>();
            al=findP(sqr+1);
            HashMap<Integer,Boolean> mp=new HashMap<>();
            while(n<2)
                n++;
            for(int i=n;i<=m;i++)
                mp.put(i,true);
            for(int y=0;y<al.size();y++)
            {
                int f=n/al.get(y);
                f*=al.get(y);
                for(int g=f;g<=m;g+=al.get(y))
                    if(g!=al.get(y) && mp.containsKey(g))
                        mp.put(g, false);
            }

            System.out.println("");
            Iterator it=mp.entrySet().iterator();
            TreeSet<Integer> set=new TreeSet<>();
            while(it.hasNext())
            {
                Map.Entry pair=(Map.Entry)it.next();
                if(true==(boolean)pair.getValue())
                    set.add((int)pair.getKey());
            }
            it=set.iterator();
            while(it.hasNext())
                System.out.println(it.next());
            t-=1;
            if(t>0)
                System.out.println("");
        }
    }
    public static ArrayList<Integer> findP(int n)
    {
        boolean prime[] = new boolean[n+1];
        ArrayList<Integer>ap =new ArrayList<>();
        for(int i=0;i<n;i++)
            prime[i] = true;

        for(int p = 2; p<=n; p++)
            if(prime[p] == true)
                for(int i = p*2; i <= n; i += p)
                    prime[i] = false;

        for(int i = 2; i <= n; i++)
            if(prime[i] == true)
                ap.add(i);
        return ap;
    }
    private static class InputReader {
        private InputStream stream;
        private byte[] buffer;
        private int currentIndex;
        private int bytesRead;
        public InputReader(InputStream stream) {
            this.stream = stream;
            buffer = new byte[131072];
        }
        public InputReader(InputStream stream, int bufferSize) {
            this.stream = stream;
            buffer = new byte[bufferSize];
        }
        private int read() throws IOException {
            if (currentIndex >= bytesRead) {
                currentIndex = 0;
                bytesRead = stream.read(buffer);
                if (bytesRead <= 0) {
                    return -1;
                }
            }
            return buffer[currentIndex++];
        }
        public int readChar() throws IOException {
            int c = read();
            while (!isPrintable(c)) {
                c = read();
            }
            return c;
        }
        public String readString() throws IOException {
            int c = read();
            while (!isPrintable(c)) {
                c = read();
            }
            StringBuilder result = new StringBuilder();
            do {
                result.appendCodePoint(c);
                c = read();
            } while (isPrintable(c));
            return result.toString();
        }
        public int readInt() throws Exception {
            int c = read();
            int sign = 1;
            while (!isPrintable(c)) {
                c = read();
            }
            if (c == '-') {
                sign = -1;
                c = read();
            }
            int result = 0;
            do {
                if ((c < '0') || (c > '9')) {
                    throw new InputMismatchException();
                }
                result *= 10;
                result += (c - '0');
                c = read();
            } while (isPrintable(c));
            return sign * result;
        }
        public long readLong() throws Exception {
            int c = read();
            int sign = 1;
            while (!isPrintable(c)) {
                c = read();
            }
            if (c == '-') {
                sign = -1;
                c = read();
            }
            long result = 0;
            do {
                if ((c < '0') || (c > '9')) {
                    throw new InputMismatchException();
                }
                result *= 10;
                result += (c - '0');
                c = read();
            } while (isPrintable(c));
            return sign * result;
        }
        public double readDouble() throws Exception {
            int c = read();
            int sign = 1;
            while (!isPrintable(c)) {
                c = read();
            }
            if (c == '-') {
                sign = -1;
                c = read();
            }
            boolean fraction = false;
            double multiplier = 1;
            double result = 0;
            do {
                if ((c == 'e') || (c == 'E')) {
                    return sign * result * Math.pow(10, readInt());
                }
                if ((c < '0') || (c > '9')) {
                    if ((c == '.') && (!fraction)) {
                        fraction = true;
                        c = read();
                        continue;
                    }
                    throw new InputMismatchException();
                }
                if (fraction) {
                    multiplier /= 10;
                    result += (c - '0') * multiplier;
                    c = read();
                } else {
                    result *= 10;
                    result += (c - '0');
                    c = read();
                }
            } while (isPrintable(c));
            return sign * result;
        }
        private boolean isPrintable(int c) {
            return ((c > 32) && (c < 127));
        }
    }
}