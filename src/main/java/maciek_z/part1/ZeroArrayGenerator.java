package maciek_z.part1;

import maciek_z.utils.ArrayGenerator;

/**
 * Created by hudy on 2017-03-03.
 */
public class ZeroArrayGenerator implements ArrayGenerator {
    @Override
    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        return new int[lenght];
    }

    @Override
    public int[] generate(int lenght) {
        return new int[lenght];
    }
}
