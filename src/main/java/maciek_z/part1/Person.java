package maciek_z.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Person {

    String firstName;
    String laseName;
    String pesel;
    char gender;
    private Address address;

public static Person getRandomPerson() {
    return Person.builder()
            .firstName(randomAlphabetic(6))
            .laseName(randomAlphabetic(6))
            .pesel(randomNumeric(11))
            .gender(new Random().nextBoolean()? 'M':'F')
            .address(Address.getRandomAddress())
            .build();
}


}
