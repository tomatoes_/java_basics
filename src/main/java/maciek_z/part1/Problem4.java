package maciek_z.part1;

import java.util.Arrays;

/**
 * Created by RENT on 2017-02-23.
 */
public class Problem4 {

    public static void main(String[] args) {



        int [] tablicaOperacji = new int [100];
        for (int i = 0; i <tablicaOperacji.length; i++) {
            tablicaOperacji[i]=i;

        }
        System.out.println(Arrays.toString(tablicaOperacji));

        for (int i = 1; i < tablicaOperacji.length; i+=2) {
            tablicaOperacji[i]+=tablicaOperacji[i-1];

        }
        System.out.println(Arrays.toString(tablicaOperacji));

        for (int i = 0; i <tablicaOperacji.length ; i++) {
            if (tablicaOperacji[i]%2==0){
                tablicaOperacji[i] /=2;
            }

        }
        System.out.println(Arrays.toString(tablicaOperacji));

        int sum = 0;
        for (int i = 0; i <tablicaOperacji.length ; i++) {
            sum+=tablicaOperacji[i];

        }
        System.out.println(sum);
    }
}
