package maciek_z.part1;

import lombok.Builder;
import lombok.Data;
import org.codehaus.groovy.runtime.StringGroovyMethods;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder

public class Address {
    private String streetName;
    private String flatNumber;
    private String postalCode;
    private String country;

    public Address(String streetName, String flatNumber, String postalCode, String country) {
        this.streetName = streetName;
        this.flatNumber = flatNumber;
        this.postalCode = postalCode;
        this.country = country;
    }

    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(8))
                .flatNumber(randomAlphabetic(4))
                .postalCode(randomAlphabetic(6))
                .country(randomAlphabetic(10))
                .build();
    }
}

