package maciek_z.part2;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Predicate;

/**
 * Created by hudy on 2017-03-05.
 */
public class DutchFlagSortAlgorithm implements GroupSortAlgorithm{

    public void sort(int[] array, Predicate<Integer> predicate) {


//        enum DutchColors {
//            RED, WHITE, BLUE
//        }
//
//
//        DutchColors[] balls = new DutchColors[12];
//
//        DutchColors[] values = DutchColors.values();
//        Random rand = new Random();
//        for (int i = 0; i < balls.length; i++)
//            balls[i] = values[rand.nextInt(values.length)];
//
//        System.out.println("Before: " + Arrays.toString(balls));
//
//        dutchNationalFlagSort(balls);
//
//        System.out.println("After : " + Arrays.toString(balls));

        if (array == null)
            throw new IllegalArgumentException();


//    private static void dutchNationalFlagSort(DutchColors[] items) {
        int lo = 0, mid = 0, hi = array.length - 1;

        while (mid <= hi)
            switch (array[mid]) {
                case 1:
                    int tmp = array[lo++];
                    array[lo++] = array[mid++];
                    array[mid++] = tmp;
                    break;
                case 2:
                    mid++;
                    break;
                case 3:
                    int temp = array[mid];
                    array[mid] = array[hi];
                    array[hi] = temp;
                    break;
            }


//    void swap(DutchColors[] arr, int a, int b) {
//        DutchColors tmp = arr[a];
//        arr[a] = arr[b];
//        arr[b] = tmp;
//    }
}
}
