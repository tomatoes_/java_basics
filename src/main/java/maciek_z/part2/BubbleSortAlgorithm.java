package maciek_z.part2;

public class BubbleSortAlgorithm implements SortAlgorithm {

    @Override
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null.");
        }
        int lastSortedElement = array.length;
        while (lastSortedElement>0){
            for (int i = 1; i < lastSortedElement ; i++) {
                int tmp=0;
                if(ascending && array[i-1]>array[i] || !ascending && array[i-1]<array[i]){
                    tmp = array[i-1];
                    array[i-1]=array[i];
                    array[i]=tmp;
                }
            }
            --lastSortedElement;
        }
//        int temp;
//        for (int i = 0; i < array.length - 1; i++) {
//            for (int j = 1; j < array.length - i; j++) {
//                if (ascending && array[j - 1] > array[j] || !ascending && array[j - 1] < array[j]) {
//                    temp = array[j - 1];
//                    array[j - 1] = array[j];
//                    array[j] = temp;
//                }
//            }
//        }

    }
}



