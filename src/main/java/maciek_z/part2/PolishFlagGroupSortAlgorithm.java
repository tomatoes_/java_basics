package maciek_z.part2;

import java.util.function.Predicate;

public class PolishFlagGroupSortAlgorithm implements GroupSortAlgorithm {
    public void sort(int[] array, Predicate<Integer> predicate) {

        if (array == null)
            throw new IllegalArgumentException();


        int left = 0;
        int right = array.length - 1;

        while (left < right) {
            while (predicate.test(array[left])) {
                left++;
            }
            while (!predicate.test(array[right])) {
                right--;
            }
            if (left < right) {
                int temp = array[left];
                array[left] = array[right];
                array[right] = temp;
            }
        }
    }
}
