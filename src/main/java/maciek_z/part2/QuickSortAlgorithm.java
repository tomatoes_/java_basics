package maciek_z.part2;

/**
 * Moje uwagi:

 zmienne number i numbers są źle nazwane (z samej nazwy wynika, że mogą być wszystkim)
 dużo komentarzy jest zbędnych
 metoda exchange może nazywać się swap - taką nazwę najczęściej się spotyka w przykładach
 Co do samego algorytmu, to wygląda w miarę w porządku.

 W generycznej implementacji wystarczy, że przerobisz metodę quicksort tak, aby posługiwała się tablicą T[].

 Tablicę T możesz przekazać w argumencie i nie musisz jej zapisywać do zmiennej (nawet jest to wskazane).
 * Created by hudy on 2017-03-05.
 */
public class QuickSortAlgorithm implements SortAlgorithm {

    private int[] numbers;
    private int number;

    @Override
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        if (array.length==0){
            return;
        }
        this.numbers = array;
        number = array.length;

        quicksort(0, number - 1);


    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high-low)/2];

        // Divide into two lists

        while (i <= j) {
            // If the current value from the left list is smaller then the pivot
            // element then get the next element from the left list
            while (numbers[i] < pivot) {
                i++;
            }
            // If the current value from the right list is larger then the pivot
            // element then get the next element from the right list
            while (numbers[j] > pivot) {
                j--;
            }

            // If we have found a values in the left list which is larger then
            // the pivot element and if we have found a value in the right list
            // which is smaller then the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }

}
