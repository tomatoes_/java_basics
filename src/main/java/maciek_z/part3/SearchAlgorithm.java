package maciek_z.part3;

/**
 * Created by hudy on 2017-03-04.
 */
public interface SearchAlgorithm {

    int search(int[] array , int value);

}
