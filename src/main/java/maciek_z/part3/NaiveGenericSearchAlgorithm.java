package maciek_z.part3;

import java.util.function.Predicate;

public class NaiveGenericSearchAlgorithm implements GenericSearchAlgorithm {
    public <T extends Comparable> int search(T[] array, Predicate<T> predicate) {
        for (int i = 0; i < array.length; i++) {
            if (predicate.test(array[i])) {
                return i;
            }
        }
        return -1;
    }
}
