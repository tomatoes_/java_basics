package maciek_z.part3;

public class NaiveSearchAlgorithm implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value) {
        int i = 0;
        if (array == null) throw new IllegalArgumentException("NULL NULL NULL");
        if (array.length == 0) {

            i = -1;
        } else {
            while ((array[i] != value) && (i < array.length - 1)) {
                i++;
            }
            if ((i == array.length - 1) && (array[i] != value)) {
                i = -1;
            }
        }

        return i;
    }
}
