package maciek_z.part3;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;


public class BinaryGenericSearchAlgorithm {

    public <T >int search(T[] array, Function<T, Integer> function) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be NULL");
        }

        int low = 0;
        int high = array.length - 1;

        while (low <= high) {
            // Key is in array[low..high] or not present.
            int mid = low + (high - low) / 2;
            if (function.apply(array[mid])<0)
                high = mid - 1;
            else if (function.apply(array[mid])>0)
                low = mid + 1;
            else return mid;
        }
        return -1;
    }

//    int low = 0;
//    int high = array.length - 1;
//        while (low <= high) {
//        // Key is in array[low..high] or not present.
//        int mid = low + (high - low) / 2;
//        if (value < array[mid]) high = mid - 1;
//        else if (value > array[mid]) low = mid + 1;
//        else return mid;
//    }
//        return -1;
}
