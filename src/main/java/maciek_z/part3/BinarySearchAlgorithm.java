package maciek_z.part3;

public class BinarySearchAlgorithm implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value) {
        if (array == null) throw new IllegalArgumentException("NULL NULL NULL");


        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            // Key is in array[low..high] or not present.
            int mid = low + (high - low) / 2;
            if (value < array[mid]) high = mid - 1;
            else if (value > array[mid]) low = mid + 1;
            else return mid;
        }
        return -1;
    }
}
