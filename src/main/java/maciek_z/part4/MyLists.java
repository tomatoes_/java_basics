package maciek_z.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;
import java.util.Optional;

public class MyLists<T> implements List<T> {
    private ListElement first;
    private ListElement last;
    private int size = 0;

    public T getFirst() {
//        if (first == null) {
//            throw new IndexOutOfBoundsException();
//        }
//        return first.getValue();
        return getChecked(first);
    }


    public T getLast() {
        return getChecked(last);
    }

    private T getChecked(ListElement element) {
        return Optional.ofNullable(element)
                .map(ListElement::getValue)
                .orElseThrow(IndexOutOfBoundsException::new);
    }

    public T get(int index) {
        return getElement(index).getValue();
    }

    private ListElement getElement(int index) {
        ListElement element = first;
        while (index-- > 0) {
            element = element.getNext();
        }
        return element;
    }

    public void add(T element) {
        if (first == null) {
            first = new ListElement(element, null, null);
            last = first;
        } else {
            ListElement newElement = new ListElement(element, last, null);
            last.next = newElement;
            last = newElement;
        }
    }


    public void remove(int index) { //TODO przypadki jeśli usuwany jest ostatnim lub pierwszym
        ListElement toRemove = getElement(index);
        toRemove.previous.next = toRemove.next;
        toRemove.next.previous = toRemove.previous;

    }

    public int size() {
        return 0;
    }


    public Iterator<T> iterator() {
        return new Iterator<T>() {
            ListElement ref = first;

            public boolean hasNext() {
                return ref != null;
            }

            public T next() {
                ref = ref.next;
                return ref.previous.getValue();
            }
        };

    }

    @Data
    @AllArgsConstructor
    private class ListElement {
        private T value;
        private ListElement previous;
        private ListElement next;

    }
}
