package maciek_z.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StackElement <T>{
    private T value;
    StackElement<T> previous;
}
