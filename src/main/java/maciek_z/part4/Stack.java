package maciek_z.part4;

public interface Stack<T> {
    void push(T element);

    T pop();

    T peek();

    int size();

    default boolean isEmpty(){
        return size() == 0;
    }
}
