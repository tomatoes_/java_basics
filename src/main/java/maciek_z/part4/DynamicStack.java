package maciek_z.part4;

import javax.swing.plaf.OptionPaneUI;
import java.util.Objects;
import java.util.Optional;

public class DynamicStack<T> implements Stack<T> {
    private StackElement<T> top;
    private int size = 0;


    @Override
    public void push(T t) {
        top = new StackElement<>(t, top);
        ++size;
    }


    @Override
    public T pop() {
        --size;
        StackElement<T> oldTop = top;
        top = top.getPrevious();
        return oldTop.getValue();
    }

    @Override
    public T peek() {
        return Optional.ofNullable(top)
                .map(StackElement::getValue)
                .orElse(null);

//        if (Objects.nonNull(top)) //to jest druga wersja tego co wyżej
//            return top.getValue();
//        else return null;
    }

    @Override
    public int size() {
        return size;
    }


    @Override
    public boolean isEmpty() {
        return size==0;
    }

}
