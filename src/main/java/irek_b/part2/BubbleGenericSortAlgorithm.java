package irek_b.part2;

import java.util.Comparator;
import java.util.Objects;

/**
 * Created by RENT on 2017-03-04.
 */
public class BubbleGenericSortAlgorithm implements GenericSortAlgorithm {
    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {
        if (Objects.isNull(array)) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement; i++) {
                if (comparator.compare(array[i - 1], array[i]) == 1) {
                    T tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}

