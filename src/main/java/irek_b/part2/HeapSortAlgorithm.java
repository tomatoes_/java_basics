package irek_b.part2;

/**
 * Created by RENT on 2017-03-02.
 */

public class HeapSortAlgorithm implements SortAlgorithm
{
    @Override
    public void sort(int array[], boolean ascending)
    {
        if(array==null){
            throw new IllegalArgumentException("Array should not be null");
        }

        if(ascending){
            int length = array.length;

            // Build heap (rearrange array)
            for (int i = length / 2 - 1; i >= 0; i--)
                heapify(array, length, i);

            // One by one extract an element from heap
            for (int i=length-1; i>=0; i--)
            {
                // Move current root to end
                int temp = array[0];
                array[0] = array[i];
                array[i] = temp;

                // call max heapify on the reduced heap
                heapify(array, i, 0);
            }

        }else {
            System.out.println("Uzupełnić o sortowanie malejące");
        }

    }



    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    void heapify(int array[], int length, int i)
    {
        if(array==null){
            throw new IllegalArgumentException("Array should not be null");
        }

        int largest = i;  // Initialize largest as root
        int l = 2*i + 1;  // left = 2*i + 1
        int r = 2*i + 2;  // right = 2*i + 2

        // If left child is larger than root
        if (l < length && array[l] > array[largest])
            largest = l;

        // If right child is larger than largest so far
        if (r < length && array[r] > array[largest])
            largest = r;

        // If largest is not root
        if (largest != i)
        {
            int swap = array[i];
            array[i] = array[largest];
            array[largest] = swap;

            // Recursively heapify the affected sub-tree
            heapify(array, length, largest);
        }
    }

}




