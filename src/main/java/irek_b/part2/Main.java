package irek_b.part2;

import java.util.Arrays;

/**
 * Created by RENT on 2017-03-01.
 */
public class Main {

    public static void main(String[] args) {

        int[] myArray = new int[]{10,2,3,4,0,6,7,8,9,10};
//---------------------- Buuble sort algorithm --------------------------

        System.out.println("This is my array");
        System.out.println(Arrays.toString(myArray));
        BubbleSortAlgorithm arrayToSort = new BubbleSortAlgorithm();

        arrayToSort.sort(myArray);
        System.out.println(Arrays.toString(myArray));

        arrayToSort.sort(myArray,false);
        System.out.println(Arrays.toString(myArray));

//---------------------- HeapSortAlgorithm -------------------------------
        int[] myArray2 = new int[]{20,2,5,4,0,6,7,8,9,20};
        System.out.println("");
        System.out.println("This is my array2");
        System.out.println(Arrays.toString(myArray2));

        HeapSortAlgorithm arrayToHeapSort = new HeapSortAlgorithm();
        arrayToHeapSort.sort(myArray2);

        System.out.println("HeapSortAlgorithm");
            System.out.println(Arrays.toString(myArray2));
    }
}
