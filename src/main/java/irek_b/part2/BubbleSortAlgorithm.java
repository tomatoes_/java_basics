package irek_b.part2;

import java.util.IllegalFormatCodePointException;

/**
 * Created by RENT on 2017-03-01.
 */
public class BubbleSortAlgorithm implements SortAlgorithm{

    @Override
    public void sort(int[] array, boolean ascending)  {

        if(array==null){
            throw new IllegalArgumentException("Array should not be null");
        }

        int n = array.length;
        int temp = 0;



        if(ascending){
            System.out.println("ascending order");
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < (n - i); j++) {
                    if (array[j - 1] > array[j]) {
                        temp = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }else {
            System.out.println("descending order");
            for (int i = 0; i < ( n - 1 ); i++) {
                for (int j = 0; j < n - i - 1; j++) {
                    if (array[j] < array[j+1])
                    {
                        temp = array[j];
                        array[j] = array[j+1];
                        array[j+1] = temp;
                    }
                }
            }
        }


   }
}
