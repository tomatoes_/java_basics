package irek_b.part2;

import java.util.Comparator;

/**
 * Created by RENT on 2017-03-04.
 */
public interface GenericSortAlgorithm {
    default <T extends Comparable<T>> void sort(T[] array){
        sort(array, T::compareTo);
    }

    <T> void sort(T[] array, Comparator<T> comparator);
}
