package irek_b.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by RENT on 2017-03-07.
 */
@Data
@AllArgsConstructor
public class StackElement<T> {
    private T element;
    private StackElement<T> previous;
}
