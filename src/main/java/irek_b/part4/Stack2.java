package irek_b.part4;

/**
 * Created by RENT on 2017-03-08.
 */
public interface Stack2<T> {
    interface Stack<T> {
        Stack<T> push(T element);
        T pop();
    }
}
