package irek_b.part4;

import java.util.Optional;

/**
 * Created by RENT on 2017-03-07.
 */
public class DynamicStack<T> implements Stack<T>{

    private StackElement<T> top;
    private int size=0;


    @Override
    public void push(T element) {
        StackElement<T> newElement = new StackElement<>(element,top);
        top = newElement;
        ++size;
    }

    @Override
    public T pop() {
        --size;
        StackElement<T> oldTop = top;
        top = top.getPrevious();
        return oldTop.getElement();
    }

    @Override
    public T peek() {
        return Optional.ofNullable(top).map(StackElement::getElement).orElse(null);
    }

    @Override
    public int size() {
        return size;
    }
}
