package irek_b.part4;

/**
 * Created by RENT on 2017-03-07.
 */
public interface Stack<T> {
    void push(T element);
    T pop();
    T peek();
    int size();

    default boolean isEmpty(){
        return size()==0;
    }
}
