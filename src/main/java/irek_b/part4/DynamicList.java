package irek_b.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * Created by RENT on 2017-03-08.
 */
public class DynamicList<T> implements List<T> {
    private ListElement first;
    private ListElement last;
    private int size = 0;

    @Override
    public T getFirst() {
        //option 1
        /*if (first==null){
            throw new IndexOutOfBoundsException();
        }
        return first.getValue();*/

        //option 2
        /*return ofNullable(first)
                .map(ListElement::getValue)
                .orElseThrow(IndexOutOfBoundsException::new);*/
        //option 3
        return getChecked(first);
    }

    @Override
    public T getLast() {
        return getChecked(last);
    }

    //function for option 3
    private T getChecked(ListElement element) {
        return ofNullable(element)
                .map(ListElement::getValue)
                .orElseThrow(IndexOutOfBoundsException::new);
    }

    @Override
    public T get(int index) {
        ListElement element = first;
        while (index-- > 0) {
            element = element.getNext();
        }
        return element.getValue();
    }

    @Override
    public void add(T element) {
        if (first == null) {
            first = new ListElement(element, null, null);
            last = first;
        } else {
            ListElement newElement = new ListElement(element, last, null);
            last.next = newElement;
            last = last.next;
        }
    }

    @Override
    public void remove(int index) {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Data
    @AllArgsConstructor
    private class ListElement {
        private T value;
        private ListElement previous;
        private ListElement next;
    }
}
