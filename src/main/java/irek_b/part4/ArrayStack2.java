package irek_b.part4;

/**
 * Created by RENT on 2017-03-07.
 */
public class ArrayStack2<T> implements Stack2<T> {

    private T[] array;
    private int size;

    @SuppressWarnings("unchecked")
    public ArrayStack2()
    {
        array = (T[]) new Object[2];
    }

    @SuppressWarnings("unchecked")
    private void resize(int capacity)
    {
        T[] tmp = (T[]) new Object[capacity];
        System.arraycopy(array, 0, tmp, 0, size);
        array = tmp;
    }

    public ArrayStack2<T> push(T element)
    {
        if (array.length == size) resize(array.length * 2);
        array[size++] = element;
        return this;
    }

    public T pop()
    {
        if (size == 0) throw new java.util.NoSuchElementException();
        T element = array[--size];
        array[size] = null;
        if (size > 0 && size == array.length / 4) resize(array.length / 2);
        return element;
    }

    @Override
    public String toString()
    {
        return java.util.Arrays.toString(array);
    }

}