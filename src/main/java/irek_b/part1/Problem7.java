package irek_b.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by RENT on 2017-02-25.
 */
public class Problem7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberARewersed = numberA.reverse().toString();
            String numberBRewersed = numberB.reverse().toString();
            BigInteger bigNumberA = new BigInteger(numberARewersed);
            BigInteger bigNumberB = new BigInteger(numberBRewersed);

            BigInteger sum = bigNumberA.add(bigNumberB);
            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
            System.out.println(sumReversed);
        }
    }


    public static void main2(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lengthA = arrayA.length;
            int lengthB = arrayB.length;

            int newlength;
            int smallerLength;
            int longerLength;
            char[] longerArray;

            if (lengthA == lengthB) {
                if (arrayA[lengthA - 1] + arrayB[lengthA - 1] >= '9') {
                    newlength = lengthA + 1;
                } else {
                    newlength = lengthA;
                }
                smallerLength = lengthA;
                longerArray = arrayA;
            } else if (lengthA > lengthB) {
                newlength = lengthA;
                smallerLength = lengthB;
                longerArray = arrayA;

            } else {
                newlength = lengthB;
                smallerLength = lengthA;
                longerArray = arrayB;
            }

            longerLength = longerArray.length;

            char[] result = new char[newlength];

            int overflow = 0;
            for (int i = 0; i < smallerLength; i++) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = smallerLength; i < longerLength; ++i) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = 0; i < newlength; i++) {
                System.out.print(result[i]);
            }

            if (result.length > longerLength && overflow != 0) {
                System.out.print('1');
            }
        }

    }


    private static char[] reverse(char[] toReverse) {
        int length = toReverse.length;
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = toReverse[length - 1 - i];
        }
        return result;
    }

    private static void reverseInPlace(char[] toReverse) {
        int length = toReverse.length;
        char tmp;
        for (int i = 0; i < length; i++) {
            tmp = toReverse[i];
            toReverse[i] = toReverse[length - 1 - i];
            toReverse[length - 1 - i] = tmp;
        }
    }
}

