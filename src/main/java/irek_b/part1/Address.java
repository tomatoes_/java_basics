package irek_b.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Address {
    private String streetName;
    private String flatNumber;
    private String country;
    private String postalCode;

    public static Address getRandomAddress(){
        return Address.builder()
                .streetName(randomAlphabetic(6))
                .flatNumber(randomNumeric(3))
                .country(randomAlphabetic(6))
                .postalCode(randomAlphabetic(6))
                .build();
    }
}
