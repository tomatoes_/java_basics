package irek_b.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Person {
    String firstName;
    String lastName;
    String pesel;
    char gender;
    Address address;

    public static Person getRandomPerson(){
        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(6))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ? 'M' : 'F')
                .address(Address.getRandomAddress())
                .build();
    }


}
