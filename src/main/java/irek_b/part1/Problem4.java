package irek_b.part1;

/**
 * Created by RENT on 2017-02-23.
 */
public class Problem4 {
    public static void main(String[] args) {


        int[] tab = new int[10];

        //////////////////////////////////////////////////////////////
        for (int i = 0; i < 10; i++) {
            tab[i]=i;
        }

        for (int tmp: tab) {
            System.out.print(tmp + " ");
        }
        System.out.println();

        //////////////////////////////////////////////////////////////
        for (int i = 1; i < 10 ; i+=2) {
            tab[i] = tab[i-1]+tab[i];
        }

        for (int tmp: tab) {
            System.out.print(tmp + " ");
        }
        System.out.println();

        //////////////////////////////////////////////////////////////
        for (int i = 0; i < 10; i++) {
            if(tab[i]%2==0){
                tab[i]=tab[i]/2;
            }
        }

        for (int tmp: tab) {
            System.out.print(tmp + " ");
        }
        System.out.println();

        //////////////////////////////////////////////////////////////
        int suma = 0;
        for (int i = 0; i < 10; i++) {
            suma+=tab[i];
        }

        System.out.println(suma);

    }
}
