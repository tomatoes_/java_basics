package irek_b.part1;

import java.util.Arrays;

/**
 * Created by RENT on 2017-02-22.
 */
public class Problem1 {
    public static void main(String[] args) {

//        for (int i = 1; i <= 61; i += 2) {
//            System.out.print(i + " ");
//        }
//        System.out.println();
        System.out.println(Arrays.toString(createFirstArray()));

////////////////////////////////////////////////////////////////

        for (int i = 0; i <=6; i+=2) {
            System.out.print(i + " ");
        }
        for (int i = 4; i >=0; i-=2) {
            System.out.print(i + " ");
        }
        System.out.println();

///////////////////////////////////////////////////////////////

        for (int i = 1; i <=9 ; i++) {
            System.out.print(i*100+" "+i*10+" ");
        }
        System.out.println();

///////////////////////////////////////////////////////////////

        for (int i = 1, j=1, k; i < 30; k=j, j=j+i, i=k ) {
            System.out.print(i + " ");
        }
        System.out.println();
    }


    public static int[] createFirstArray(){
        int[] array = new int[31];
        for (int i = 0, j=1 ; i < array.length; ++i, j+=2) {
            array[i]=j;
        }
        return array;
    }

}
