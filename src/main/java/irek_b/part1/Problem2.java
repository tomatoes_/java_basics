package irek_b.part1;

/**
 * Created by RENT on 2017-02-22.
 */
public class Problem2 {
    public static void main(String[] args) {
        printTiangle(6);
        printTiangleLine(6);
        printSquare(6);
        printCross(3);


    }
        public static void printTiangle(int height){
            for (int i = 0; i < height ; i++) {
                for (int j = 0; j <= i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }

        public static void printTiangleLine(int height){
            for (int i = 0; i < height ; i++) {
                for (int j = 0; j <= i; j++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }
        }

        public static void printSquare(int height) {

            for (int i = 0; i < height; i++) {
                System.out.print("*");
            }
            System.out.println();

            for (int i = 1; i < height - 1; i++) {
                System.out.print("*");
                for (int j = 2; j < height; j++) {
                    System.out.print(" ");
                }
                System.out.print("*");
                System.out.println();
            }

            for (int i = 0; i < height; i++) {
                System.out.print("*");
            }
            System.out.println();
        }

        public static void printCross(int height)
        {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < height; j++) {
                    if (i == j || i + j == height - 1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }




}
