package irek_b.part1;

import java.util.stream.IntStream;

/**
 * Created by pewny on 27.02.2017.
 */

public class Problem6 {
    public static void main(String[] args) {
        int sum = IntStream.range(0, 10)
                .map(i -> i % 2 != 0 ? (i * 2) - 1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .sum();
        System.out.println(sum);
    }
}
