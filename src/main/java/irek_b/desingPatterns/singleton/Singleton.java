package irek_b.desingPatterns.singleton;

/**
 * Created by RENT on 2017-03-18.
 */

public class Singleton {
    private static Singleton instance;

    public static Singleton getInstance() {
        if (instance == null) {
            return new Singleton();
        } else {
            return instance;
        }
    }
}

class Singletonfager {
    private static Singleton instance = new Singleton();

    public static Singleton getInstance() {
        return instance;
    }
}

class Singleton2 {
    private static Singleton instance;

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    return new Singleton();
                } else {
                    return instance;
                }
            }
        } else {
            return instance;
        }
    }
}

class Singleton3 {
    private static Singleton instance;

    private static class SingletonHelper {
        private static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {
        return SingletonHelper.instance;
    }
}

enum Enum {
    karo,
    kier,
    pik
}
