package irek_b.desingPatterns.template_method;

/**
 * Created by RENT on 2017-03-20.
 */
public class MargheritaPizzaRecipe extends PizzaRecipe {

    @Override
    public void doCake() {
        System.out.println("do Margherita cake");
    }

    @Override
    public void addSauce() {
        System.out.println("add sauce");
    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella");
    }

    @Override
    public void bake() {
        System.out.println("bake for 20min in 210 degrees");
    }

}
