package irek_b.desingPatterns.template_method;

/**
 * Created by RENT on 2017-03-20.
 */
public class PepperoniPizzaRecipe extends PizzaRecipe {
    @Override
    public void doCake() {
        System.out.println("do Pepperoni cake");
    }

    @Override
    public void addSauce() {

    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella, jalapeno, pepperoni");
    }

    @Override
    public void bake() {
        System.out.println("bake for 20min in 210 degrees");
    }

    void doPizza(){
        this.doCake();
        this.addAdditions();
        this.bake();
    }
}
