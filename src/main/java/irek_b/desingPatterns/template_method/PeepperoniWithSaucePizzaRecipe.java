package irek_b.desingPatterns.template_method;

/**
 * Created by RENT on 2017-03-20.
 */
public class PeepperoniWithSaucePizzaRecipe extends PepperoniPizzaRecipe {
    public void addSauce() {
        System.out.println("add very hot sauce");
    }

    void doPizza(){
        this.doCake();
        this.addSauce();
        this.addAdditions();
        this.bake();
    }
}
