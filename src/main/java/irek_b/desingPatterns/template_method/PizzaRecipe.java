package irek_b.desingPatterns.template_method;

/**
 * Created by RENT on 2017-03-20.
 */
public abstract class PizzaRecipe {
    
    abstract void doCake();
    abstract void addSauce();
    abstract void addAdditions();
    abstract void bake();
    void doPizza(){
        this.doCake();
        this.addSauce();
        this.addAdditions();
        this.bake();
    }
}
