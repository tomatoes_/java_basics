package irek_b.desingPatterns.template_method;

/**
 * Created by RENT on 2017-03-20.
 */
public class HavaiianPizzaRecipe extends PizzaRecipe {
    @Override
    public void doCake() {
        System.out.println("do Hawaii cake");
    }

    @Override
    public void addSauce() {
        System.out.println("add sauce");

    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella, pineapple and ham ");
    }

    @Override
    public void bake() {
        System.out.println("bake for 20min in 210 degrees");

    }

}
