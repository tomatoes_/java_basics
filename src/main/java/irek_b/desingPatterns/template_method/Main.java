package irek_b.desingPatterns.template_method;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-03-20.
 */
public class Main {
    public static void main(String[] args) {

        PizzaRecipe pizza1 = new MargheritaPizzaRecipe();
        PizzaRecipe pizza2 = new HavaiianPizzaRecipe();
        PizzaRecipe pizza3 = new PepperoniPizzaRecipe();
        PizzaRecipe pizza4 = new PeepperoniWithSaucePizzaRecipe();

        LinkedList<PizzaRecipe> pizzaList = new LinkedList<>();
        pizzaList.add(pizza1);
        pizzaList.add(pizza2);
        pizzaList.add(pizza3);
        pizzaList.add(pizza4);

        for (PizzaRecipe pizza: pizzaList) {
            System.out.println("---robimy pizze---");
            pizza.doPizza();
        }
    }
}
