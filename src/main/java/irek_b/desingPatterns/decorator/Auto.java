package irek_b.desingPatterns.decorator;


/**
 * Created by RENT on 2017-03-18.
 */

public abstract class Auto {
    String wersja = "Auto Podstawowe";

    public String getWersja() {
        return wersja;
    }

    public abstract double getCena();

}
