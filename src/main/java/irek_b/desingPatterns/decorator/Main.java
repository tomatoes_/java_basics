package irek_b.desingPatterns.decorator;

/**
 * Created by RENT on 2017-03-18.
 */

public class Main {
    public static void main(String[] args) {
        Auto auto1 = new Audi();
        Auto auto2 = new Maserati();
        Auto auto3 = new Audi();

        System.out.println("Wersja Podstawowa bez dekoratora");
        System.out.println(auto1.getWersja() + " | " + auto1.getCena());
        System.out.println(auto2.getWersja() + " | " + auto2.getCena());
        System.out.println();


        auto1 = new Klimatyzacja(auto1);
        auto2 = new AluFelgi(auto2);

        System.out.println("Wersja Podstawowa + derorator");
        System.out.println(auto1.getWersja() + " | " + auto1.getCena());
        System.out.println(auto2.getWersja() + " | " + auto2.getCena());
        System.out.println();


        auto2 = new Klimatyzacja(new AluFelgi(new Klimatyzacja(auto2)));
        System.out.println("Wersja z dekoratorem + dekorator + dekorator + dekorator");
        System.out.println(auto2.getWersja() + " | " + auto2.getCena());


    }
}
