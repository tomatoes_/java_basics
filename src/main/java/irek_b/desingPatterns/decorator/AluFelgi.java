package irek_b.desingPatterns.decorator;

/**
 * Created by RENT on 2017-03-18.
 */

public class AluFelgi extends Decorator {

    private Auto auto;

    public AluFelgi(Auto auto) {
        this.auto = auto;
    }

    @Override
    public String getWersja() {
        return auto.getWersja() + " plus alufelgi";
    }

    @Override
    public double getCena() {
        return auto.getCena() + 2_000;
    }
}
