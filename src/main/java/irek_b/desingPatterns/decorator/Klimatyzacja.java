package irek_b.desingPatterns.decorator;

/**
 * Created by RENT on 2017-03-18.
 */

public class Klimatyzacja extends Decorator {
    private Auto auto;

    public Klimatyzacja(Auto auto) {
        this.auto = auto;
    }

    @Override
    public String getWersja() {
        return auto.getWersja() + " i kllimatyzacja";
    }

    @Override
    public double getCena() {
        return auto.getCena() + 3_400;
    }
}
