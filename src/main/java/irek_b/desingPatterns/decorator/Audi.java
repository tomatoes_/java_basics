package irek_b.desingPatterns.decorator;

/**
 * Created by RENT on 2017-03-18.
 */

public class Audi extends Auto {
    public Audi() {
        this.wersja = "Audi podstawowe";
    }

    @Override
    public double getCena() {
        return 10_000;
    }
}
