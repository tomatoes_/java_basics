package irek_b.desingPatterns.decorator;

/**
 * Created by RENT on 2017-03-18.
 */

public class Maserati extends Auto {
    public Maserati() {
        this.wersja = "Maserati podstawowe";
    }

    @Override
    public double getCena() {
        return 500_000;
    }
}
