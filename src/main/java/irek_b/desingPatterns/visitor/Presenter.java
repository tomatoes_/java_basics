package irek_b.desingPatterns.visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public class Presenter implements Visitor {
    @Override
    public void visit(PdfFile pdfFile) {
        pdfFile.getType();
    }

    @Override
    public void visit(CsvFile csvFile) {
        csvFile.getType();
        csvFile.getType();
    }
}
