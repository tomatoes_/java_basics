package irek_b.desingPatterns.visitor;

import java.util.LinkedList;

/**
 * Created by RENT on 2017-03-20.
 */
public class Main {
    public static void main(String[] args) {
        File file1 = new PdfFile();
        File file2 = new CsvFile();

        Presenter presenter = new Presenter();

        LinkedList<File> fileList = new LinkedList<>();
        fileList.add(file1);
        fileList.add(file2);

        for(File file : fileList){
            if(file instanceof PdfFile){
                presenter.visit((CsvFile) file1);
            } else if(file instanceof CsvFile){
                presenter.visit((CsvFile) file2);
            }
        }

        presenter.visit((PdfFile) file1);
        presenter.visit((CsvFile) file2);

    }
}
