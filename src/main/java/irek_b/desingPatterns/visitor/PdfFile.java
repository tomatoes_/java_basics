package irek_b.desingPatterns.visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public class PdfFile implements File, Visitable {
    @Override
    public void getType() {
        System.out.println("Pdf File");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}