package irek_b.desingPatterns.visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public interface Visitor {

    void visit(PdfFile pdfFile);
    void visit(CsvFile csvFile);

}
