package irek_b.desingPatterns.visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public class CsvFile implements File, Visitable {
    @Override
    public void getType() {
        System.out.println("Csv File");

    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
