package irek_b.desingPatterns.builder;

/**
 * Created by RENT on 2017-03-18.
 */

public class Main {
    public static void main(String[] args) {

        CarBuilder builder1 = new BusBuilder();
        CarBuilder builder2 = new PersonalCarBuilder();
        CarBuilder builder3 = new TruckBuilder();

        Director d = new Director();

        d.setBuilder(builder1);
        d.construct();
        Car car = d.getCar();
        System.out.println(car.showCar());

        d.setBuilder(builder2);
        d.construct();
        car = d.getCar();
        System.out.println(car.showCar());

        d.setBuilder(builder3);
        d.construct();
        car = d.getCar();
        System.out.println(car.showCar());


    }
}
