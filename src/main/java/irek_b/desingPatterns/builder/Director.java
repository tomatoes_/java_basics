package irek_b.desingPatterns.builder;

/**
 * Created by RENT on 2017-03-18.
 */

public class Director {

    private CarBuilder carBuilder;

    public void setBuilder(CarBuilder builder) {
        this.carBuilder = builder;
    }

    public void construct() {
        carBuilder.buildWheels();
        carBuilder.buildDoors();
        carBuilder.buildBody();
        carBuilder.buildLandingGear();
        carBuilder.buildRadio();
        carBuilder.buildEngine();
    }

    public Car getCar() {
        return this.carBuilder.getCar();
    }

}
