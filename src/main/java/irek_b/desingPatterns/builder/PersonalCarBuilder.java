package irek_b.desingPatterns.builder;

/**
 * Created by RENT on 2017-03-18.
 */

public class PersonalCarBuilder implements CarBuilder {

    private Car car;

    public PersonalCarBuilder() {
        this.car = new Car();
    }

    public Car getCar() {
        return this.car;
    }

    @Override
    public void buildWheels() {
        this.car.setWheels("Buduję koła samochodu osobowego");
    }

    @Override
    public void buildEngine() {
        this.car.setEngine("Buduję silnik samochofu osobowego");
    }

    @Override
    public void buildLandingGear() {
        this.car.setLandingGear("Buduję podwozie samochodu osobowego");
    }

    @Override
    public void buildBody() {
        this.car.setBody("Buduję nadwozie samochodu osobowego");
    }

    @Override
    public void buildRadio() {
        this.car.setRadio("Buduję radio samochodu osobowego");
    }

    @Override
    public void buildDoors() {
        this.car.setDoors("Buduję drzwi samochodu osobowego");
    }
}
