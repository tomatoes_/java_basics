package irek_b.desingPatterns.builder;

/**
 * Created by RENT on 2017-03-18.
 */

public interface CarBuilder {
    void buildWheels();

    void buildEngine();

    void buildLandingGear();

    void buildBody();

    void buildRadio();

    void buildDoors();

    Car getCar();
}
