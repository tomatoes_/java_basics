package irek_b.desingPatterns.adapter;

import java.util.LinkedList;

/**
 * Created by RENT on 2017-03-20.
 */
public class Main {
    public static void main(String[] args) {
        StringData nameData = new NameData();
        StringData surnameData = new SurnameData();
        StringData adaptIntToString = new AdaptIntToString();
        StringData adaptAnythingToStringData = new AdaptAnythingToStringData();

        LinkedList<StringData> dataList = new LinkedList<>();
        dataList.add(nameData);
        dataList.add(surnameData);
        dataList.add(adaptIntToString);
        dataList.add(adaptAnythingToStringData);

        for(StringData data : dataList){
            System.out.println(data.getData());
        }
    }
}
