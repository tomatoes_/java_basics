package irek_b.desingPatterns.adapter;

/**
 * Created by RENT on 2017-03-20.
 */
public class AnythingData {
    String val1;
    String val2;
    int valInt1;
    int valInt2;

    public String getVal1() {
        return val1;
    }

    public String getVal2() {
        return val2;
    }

    public int getValInt1() {
        return valInt1;
    }

    public int getValInt2() {
        return valInt2;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public void setValInt1(int valInt1) {
        this.valInt1 = valInt1;
    }

    public void setValInt2(int valInt2) {
        this.valInt2 = valInt2;
    }
}
