package irek_b.desingPatterns.adapter;

/**
 * Created by RENT on 2017-03-20.
 */
public class AdaptAnythingToStringData implements StringData{

    @Override
    public String getData() {
        AnythingData anythingData = new AnythingData();
        anythingData.setVal1("anything");
        anythingData.setVal2(" data");
        anythingData.setValInt1(123);
        anythingData.setValInt2(456);

        return anythingData.getVal1()+
                anythingData.getVal2()+
                String.valueOf(anythingData.getValInt1())+
                String.valueOf(anythingData.getValInt2());
    }
}
