package irek_b.desingPatterns.adapter;

/**
 * Created by RENT on 2017-03-20.
 */
public class AdaptIntToString implements StringData{

    @Override
    public String getData() {
        IntData intData = new IntData();
        return String.valueOf(intData.getIntData());
    }
}
