package irek_b.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGenerator implements ArrayGenerator{

    public int[] generate(int length, int rangeFrom, int rangeTo) {
        if(rangeFrom>=rangeTo){
            throw new IllegalArgumentException("Range is invalid");
        }

        int[] array = new int[length];
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            array[i]=random.nextInt(rangeTo - rangeFrom) + rangeFrom;
        }
        return array;
    }

    public final int[] generate(int length) {
        return generate(length, 0, MAX_VALUE);
    }
}
