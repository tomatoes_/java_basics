package irek_b.part3;

import java.util.function.Function;

/**
 * Created by RENT on 2017-03-07.
 */
public class BinaryGenericSearchAlgorithm {
    public <T> int search(T[] array, Function<T, Integer> function) {
        final int length = array.length;
        int left = 0, right = length - 1, middle;

        while (left <= right) {
            middle = left + (right - left) / 2;
            if (function.apply(array[middle]) > 0) {
                left = middle - 1;
            } else if (function.apply(array[middle]) < 0) {
                right = middle - 1;
            } else {
                return middle;
            }
        }
        return -1; // not found
    }
}
