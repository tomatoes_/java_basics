package irek_b.part3;

/**
 * Created by RENT on 2017-03-06.
 */
public class BinarySearchAlgorithm implements SearchAlgorithm {
    @Override
    public int search(int[] array, int value) {
        if (array == null) {
            throw new IllegalArgumentException("array can't be null");
        }

        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int middle = left + ((right - left) / 2);
            if (array[middle] > value)
                right = middle - 1;
            else if (array[middle] < value)
                left = middle + 1;
            else
                return middle; // found
        }
        return -1; // not found
    }
}
