package irek_b.part3;

/**
 * Created by pewny on 06.03.2017.
 */
public class NaiveSearchAlgorithm implements SearchAlgorithm{
    @Override
    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("array can't be null");
        }

        int searchResult=-1;

        if (array.length==0){
            searchResult=-1;
        }

        for (int i = 0; i <array.length ; i++) {
            if (array[i] == value){
                searchResult=i;
            }
        }

        return searchResult;
    }
}
