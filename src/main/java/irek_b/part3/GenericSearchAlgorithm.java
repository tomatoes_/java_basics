package irek_b.part3;

import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-06.
 */
public interface GenericSearchAlgorithm {
    <T extends Comparable> int search(T[] array, Predicate<T> predicate);
}
