package maciej_brz.Part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Optional;
import java.util.Stack;

/**
 * Created by RENT on 2017-03-07.
 */
public class DynamicStack<T> implements StackInterface<T> {
    private StackElement top;
    private int size = 0;

    @Override
    public T pop() {
        --size;
        StackElement oldTop = top;
        top=top.getPrevious();
        return oldTop.getValue();

    }

    @Override
    public T peek() {
        return Optional.ofNullable(top)
                .map(StackElement::getValue)
                .orElse(null);

//        if (nonNull(top)) return top.getValue();
//        else return null;
    }

    @Override
    public void push(T t) {
        top = new StackElement(t, top);
        ++size;


    }

    @Override
    public int size() {
        return size;
    }

    @Data
    @AllArgsConstructor

    private class StackElement {

        private T value;
        private StackElement previous;
    }



}
