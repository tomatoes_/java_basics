package maciej_brz.Part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;
import java.util.Optional;

/**
 * Created by RENT on 2017-03-08.
 */
public class DynamicList<T> implements ListInterface<T> {


private int size = 0;
private ListElement first;
private ListElement last;

public T getFirst() {
        return getChecked(first);
        }

public T getLast() {
        return getChecked(last);
        }

private T getChecked(ListElement element) {
        return Optional.ofNullable(element)
        .map(ListElement::getValue)
        .orElseThrow(IndexOutOfBoundsException::new);
        }

public T get(int index) {
        return getElement(index).getValue();
        }

private ListElement getElement(int index) {
        ListElement element = first;
        while (index-- > 0) {
        element = element.getNext();
        }
        return element;
        }

public void add(T element) {
        if (first == null) {
        first = new ListElement(element, null, null);
        last = first;
        size++;
        } else {
        ListElement newElement = new ListElement(element, last, null);
        last.next = newElement;
        last = newElement;
        size++;
        }
}

public void remove(int index) {
        getChecked(first);
        if (!(index == 0 || index == size)){
        ListElement toRemove = getElement(index);
        toRemove.previous.next = toRemove.next;
        toRemove.next.previous = toRemove.previous;

        }
        else{
                if (index == 0){
                        first = getElement(1);
                        first.previous= null;
                }
                else{
                        last = getElement(size-1);
                        last.next=null;
                }
        }
        size--;
}

public int size() {
        return size;
        }

public Iterator<T> iterator() {
        return new Iterator<T>() {
        ListElement ref = first;
public boolean hasNext() {
        return ref != null;
        }
public T next() {
        ref=ref.next;
        return ref.previous.getValue();
        }
        };
        }

@Data
@AllArgsConstructor

private class ListElement {
    private T value;
    private ListElement previous;
    private ListElement next;
}

}
