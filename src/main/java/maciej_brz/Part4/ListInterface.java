package maciej_brz.Part4;

/**
 * Created by RENT on 2017-03-08.
 */
public interface ListInterface<T> extends Iterable<T> {
    T getFirst();
    T getLast();
    T get(int Index);
    void add(T element);
    void remove(int index);
    int size();
    default  boolean isEmpty(){
        return size()==0;
    };
}
