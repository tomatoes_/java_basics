package maciej_brz.Part4;

import java.util.function.Function;

/**
 * Created by RENT on 2017-03-08.
 */
public class ArrayStack <T> implements StackInterface<T> {
   private int size=0;
   private int stackSize;
   private T[] stack; //= (T[]) new Object[stackSize];

    @SuppressWarnings("unchecked")
    public ArrayStack(int size) {
        this.stackSize = size;
        stack = (T[]) new Object[stackSize];
    }



    @Override
    public void push(T element) {
       if(size==stack.length) throw new RuntimeException("Stack overflow");
    stack[size++] = element;

    }

    @Override
    public T pop() {

        return stack[size--];
    }



    @Override
    public T peek() {
        return stack[size-1];
    }

    @Override
    public int size() {
        return size;
    }
}
