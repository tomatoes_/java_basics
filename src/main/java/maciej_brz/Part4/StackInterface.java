package maciej_brz.Part4;

/**
 * Created by RENT on 2017-03-07.
 */
public interface StackInterface<T> {
    T pop();

    T peek();

    void push(T t);

    int size();

    default boolean isEmpty() {
        return size() == 0;
    }

}