package maciej_brz.utils;

import java.util.stream.Stream;

public class BubbleSort implements SortAlgorithm {


    @Override
    public void sort(int[] array, boolean ascending) {
        int tempInt;


        boolean valueSwaped = false;

        do {
            valueSwaped = false;
            for (int i = 0; i < array.length - 1; i++) {
                if ((array[i] > array[i + 1]) && ascending || (array[i] < array[i + 1]) && !ascending) {
                    tempInt = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tempInt;
                    valueSwaped = true;
                }


            }
        } while (valueSwaped == true);

    }
}

/**
 <<<<<<< HEAD
 * Created by Maciek on 2017-03-02.
 */
//public class Problem1  {
//
//
//    @Override
//    public void sortBubble(int[] array) {
//
//    }
//
//    @Override
//    public void sortBubble(int[] array, boolean ascending) {
//        boolean swapMaked = true;
//        int temp;
//
//        for (int i = 0; i < array.length - 2; i++) {
//            swapMaked = false;
//            if (array[i] > array[i + 1]) {
//                temp = array[i];
//                array[i] = array[i + 1];
//                array[i + i] = temp;
//                swapMaked = true;
//            }
//
//
//        }
//    }
//
//}
