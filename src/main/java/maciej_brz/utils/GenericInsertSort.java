package maciej_brz.utils;

import java.util.Comparator;

/**
 * Created by Maciek on 2017-03-05.
 */
public class GenericInsertSort implements GenericSortAlgorithm {

    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {
        T tempValue;
                int valueIndex,j=array.length;
        do {
            for (int i = (array.length-j); i < array.length; i++) {
                valueIndex = findingIndexOfMaximumInArray(array,i,comparator);
                tempValue=array[valueIndex];
                array[valueIndex]=array[array.length-j];
                array[array.length-j]=tempValue;
                j--;
            }

        }while (j>0);

    }

    private static <T> int findingIndexOfMaximumInArray(T[] array, int staringPoint, Comparator<T> comparator)
    {
        int valueIndex=array.length-1;
        for (int i = staringPoint; i <array.length ; i++) {
            if (comparator.compare(array[valueIndex],array[i])==-1||comparator.compare(array[valueIndex],array[i])==1){
                valueIndex=i;
            }
        }
        return valueIndex;
    }
}
