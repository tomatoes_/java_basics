package maciej_brz.utils;

/**
 * Created by Maciek on 2017-03-03.
 */
public class InsertSort implements SortAlgorithm {
    @Override
    public void sort(int[] array, boolean ascending) {
        int tempValue,valueIndex,j=array.length;
        do {
            for (int i = (array.length-j); i < array.length; i++) {
                valueIndex = findingIndexOfMaximumInArray(array,i,ascending);
                tempValue=array[valueIndex];
                array[valueIndex]=array[array.length-j];
                array[array.length-j]=tempValue;
                j--;
            }

        }while (j>0);

    }

   private static int findingIndexOfMaximumInArray(int[] array, int staringPoint, boolean ascending)
    {
        int valueIndex=array.length-1;
        for (int i = staringPoint; i <array.length ; i++) {
            if ((array[valueIndex]<array[i])&&!ascending||(array[valueIndex]>array[i])&&ascending){
                valueIndex=i;
            }
        }
        return valueIndex;
    }
}
