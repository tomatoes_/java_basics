package maciej_brz.utils;

import javafx.css.ParsedValue;

import java.util.Arrays;

/**
 * Created by Maciek on 2017-02-26.
 */
public class ReversedSum {

    public static void main(String[] args) {

        System.out.println(reversingValue(reversedSumValueOfTwoIntegers(123,321)));
    }

    public static int reversingValue(Integer value){
        String stringValue = value.toString();
//        stringValue.toCharArray();  // tworzy tablicę po czym tą tablicę odczytywać od prawej wpisując do nowej od lewej
        char[] tempForReversing = new char[stringValue.length()];
        for (int i = 0; i < stringValue.length() ; i++) {
            tempForReversing[i]=stringValue.charAt(i);
        }

        char temp;
        for (int i = 0; i < tempForReversing.length/2; i++) {
            temp=tempForReversing[i];
            tempForReversing[i]=tempForReversing[tempForReversing.length-i-1];
            tempForReversing[tempForReversing.length-i-1]=temp;
        }

        stringValue="";
        for (int i = 0; i < tempForReversing.length ; i++) {
            stringValue+=tempForReversing[i];
        }

        return Integer.parseInt(stringValue);
    }

    public static int reversedSumValueOfTwoIntegers (Integer value1, Integer value2){

        return reversingValue(reversingValue(value1)+reversingValue(value2));
    }


}
