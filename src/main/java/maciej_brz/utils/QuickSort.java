package maciej_brz.utils;

/**
 * Created by RENT on 2017-03-02.
 */
public class QuickSort implements SortAlgorithm {
    @Override
    public void sort(int[] array, boolean ascending) {

        if(array.length>0) {
            quickSort(array,0,array.length-1,ascending);
        }


    }
    void quickSort(int array[], int left, int right, boolean ascending) {
        int index = partition(array, left, right,ascending);
        if (left < index - 1)
            quickSort(array, left, index - 1, ascending);
        if (index < right)
            quickSort(array, index, right, ascending);
    }


    int partition(int array[], int left, int right, boolean ascending)
    {
        int i = left, j = right;
        int tmp;
        int pivot = array[(left + right) / 2];

        while (i <= j) {
            while ((array[i] < pivot)&ascending||(array[i] > pivot)&!ascending)
                i++;
            while ((array[j] > pivot)&ascending||(array[j] < pivot)&!ascending)
                j--;
            if (i <= j) {
                tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
                i++;
                j--;
            }
        }

        return i;
    }
}
