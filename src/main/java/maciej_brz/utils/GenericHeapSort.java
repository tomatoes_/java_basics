package maciej_brz.utils;

import java.util.Comparator;

/**
 * Created by Maciek on 2017-03-05.
 */
public class GenericHeapSort implements GenericSortAlgorithm {

    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {
        if (array == null) {
            throw new NullPointerException("Array should not be null.");
        }
        int arrayLenght = array.length;

        // Build heap (rearrange array)
        for (int i = arrayLenght / 2 - 1; i >= 0; i--)
            heapify(array, arrayLenght, i,comparator);

        // One by one extract an element from heap
        for (int i=arrayLenght-1; i>=0; i--)
        {
            // Move current root to end
            T temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            // call max heapify on the reduced heap
            heapify(array, i, 0,comparator);
        }

//        if(!ascending){
//            int temp;
//            for (int i = 0; i < array.length/2; i++) {
//                temp=array[i];
//                array[i]=array[array.length-i-1];
//                array[array.length-i-1]=temp;
//            }
//        }
    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    <T> void heapify(T array[], int sizeOfHeap, int i, Comparator<T> comparator) {
        int largest = i;  // Initialize largest as root
        int left = 2*i + 1;  // left = 2*i + 1
        int right = 2*i + 2;  // right = 2*i + 2

        // If left child is larger than root
        if ((left < sizeOfHeap && comparator.compare(array[left] , array[largest])==1))
            largest = left;

        // If right child is larger than largest so far
        if ((right < sizeOfHeap && comparator.compare(array[left] , array[largest])==1))
            largest = right;


        // If largest is not root
        if (largest != i){
            T swap = array[i];
            array[i] = array[largest];
            array[largest] = swap;

            // Recursively heapify the affected sub-tree
            heapify(array, sizeOfHeap, largest,comparator);

        }
    }
}
