package maciej_brz.utils;




import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGenerator implements ArrayGenerator{

    public static void main(String[] args) {

    }



    @Override
    public final int[] generate(int lenght) {
        return  generate(lenght,0, MAX_VALUE);
    }



    @Override
    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        int[] array = new int[lenght];
        if (rangeFrom >= rangeTo) {
        throw new IllegalArgumentException("Range is invalid");
        }


        Random random = new Random();
        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(rangeTo - rangeFrom) + rangeFrom;
            }


        return array;
    }





}
