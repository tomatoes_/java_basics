package maciej_brz.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-04.
 */
public class PolishFlagGroupSort implements GroupSortAlgorithm {

    @Override
    public void sort(int[] array, Predicate<Integer> predicate) {

        if (array == null) throw new IllegalArgumentException();

        if (array.length <=1 ) return;
        int left = 0,
                right = array.length - 1,
                tempInt;

        do {
                while (predicate.test(array[left]) && left < right) left++;
                while (!predicate.test(array[right]) && left < right) right--;

                if (left < right) {
                    tempInt = array[left];
                    array[left] = array[right];
                    array[right] = tempInt;
                }


            } while (left < right);


        }
    }


