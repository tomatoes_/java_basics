package maciej_brz.utils;

import java.util.Comparator;

/**
 * Created by RENT on 2017-03-04.
 */
public class GenericBubbleSort implements GenericSortAlgorithm {
    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {

        if (array == null) throw new NullPointerException("NULL NULL NULL");
        boolean valueSwaped = false;

        do {
            valueSwaped = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (comparator.compare((array[i]),(array[i + 1]))== 1 ) {
                    T tempInt = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tempInt;
                    valueSwaped = true;
                }


            }
        } while (valueSwaped == true);

    }
}
