package maciej_brz.utils;

import maciej_brz.utils.GenericSortAlgorithm;

import java.util.Comparator;
import java.util.Objects;

/**
 * Created by Maciek on 2017-03-05.
 */
public class GenericMergeSort implements GenericSortAlgorithm {
//
//    T[] numbers;
//    T[] helper;
//    int  number;
   // private Comparator<T> comparator;

    @Override
    public <T> void sort(T[] array, Comparator<T> comparator) {

//         T[] numbers;
//    //<T>[] helper;
//         int  number;


        if (array == null) {
            throw new NullPointerException("Array should not be null");
        }


        mergesort(array, 0, array.length-1, comparator);
    }

    private <T> void mergesort(T[] array,int low, int high, Comparator<? super T> comparator) {


        if (low < high) {
            int middle = low + (high - low) / 2;
            mergesort(array,low, middle, comparator);
            mergesort(array,middle + 1, high, comparator);
            merge(array,low, middle, high, comparator);
        }
    }

    private <T> void merge(T[] array ,int low, int middle, int high, Comparator<T> comparator) {

        //for (int i = low; i <= high; i++) {
            T[] helper  =   (T[]) new Object[high-low+1];

        int i = low;
        int j = middle + 1;
        int k = low;

        while (i <= middle && j <= high) {

            if ( comparator.compare(helper[i],helper[j]) !=1 || comparator.compare(helper[i], helper[j])!=-1 ) {
                array[k] = helper[i];
                i++;
            } else {
                array[k] = helper[j];
                j++;
            }
            k++;
        }

        while (i <= middle) {
            array[k] = helper[i];
            k++;
            i++;
        }
    }


}
