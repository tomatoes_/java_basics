package maciej_brz.utils;

/**
 * Created by Maciek on 2017-03-03.
 */
public class MergeSort implements SortAlgorithm {

    private int[] numbers;
    private int[] helper;
    private int number;


    @Override
    public void sort(int[] array, boolean ascending) {

        if (array == null) {
            throw new NullPointerException("Array should not be null");
        }

        this.numbers = array;
        number = array.length;
        this.helper = new int[number];
        mergesort(0, number - 1, ascending);

    }

    private void mergesort(int low, int high, boolean ascending) {

        if (low < high) {
            int middle = low + (high - low) / 2;
            mergesort(low, middle, ascending);
            mergesort(middle + 1, high, ascending);
            merge(low, middle, high, ascending);
        }
    }

    private void merge(int low, int middle, int high, boolean ascending) {

        for (int i = low; i <= high; i++) {
            helper[i] = numbers[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;

        while (i <= middle && j <= high) {

            if (ascending && helper[i] <= helper[j] || !ascending && helper[i] >= helper[j]) { // >
                numbers[k] = helper[i];
                i++;
            } else {
                numbers[k] = helper[j];
                j++;
            }
            k++;
        }

        while (i <= middle) {
            numbers[k] = helper[i];
            k++;
            i++;
        }
    }
}


