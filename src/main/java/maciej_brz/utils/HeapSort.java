package maciej_brz.utils;

/**
 * Created by Maciek on 2017-03-03.
 */
public class HeapSort implements SortAlgorithm {
    @Override
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new NullPointerException("Array should not be null.");
        }
        int arrayLenght = array.length;

        // Build heap (rearrange array)
        for (int i = arrayLenght / 2 - 1; i >= 0; i--)
            heapify(array, arrayLenght, i,ascending);

        // One by one extract an element from heap
        for (int i=arrayLenght-1; i>=0; i--)
        {
            // Move current root to end
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            // call max heapify on the reduced heap
            heapify(array, i, 0,ascending);
        }

//        if(!ascending){
//            int temp;
//            for (int i = 0; i < array.length/2; i++) {
//                temp=array[i];
//                array[i]=array[array.length-i-1];
//                array[array.length-i-1]=temp;
//            }
//        }
    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    void heapify(int arr[], int sizeOfHeap, int i,boolean ascending) {
        int largest = i;  // Initialize largest as root
        int left = 2*i + 1;  // left = 2*i + 1
        int right = 2*i + 2;  // right = 2*i + 2

        // If left child is larger than root
        if ((left < sizeOfHeap && arr[left] > arr[largest]==ascending))
            largest = left;

        // If right child is larger than largest so far
        if ((right < sizeOfHeap && arr[right] > arr[largest]==ascending))
            largest = right;


        // If largest is not root
        if (largest != i){
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            // Recursively heapify the affected sub-tree
            heapify(arr, sizeOfHeap, largest,ascending);

        }
    }
}
