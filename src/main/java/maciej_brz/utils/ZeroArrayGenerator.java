package maciej_brz.utils;

/**
 * Created by RENT on 2017-02-25.
 */
public class ZeroArrayGenerator implements ArrayGenerator {
    @Override
    public int[] generate(int lenght) {
        return new int[lenght];
    }

    @Override
    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        return new int[lenght];
    }
}
