package maciej_brz.Part3;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-06.
 */
public interface GenericSearchAlgorithm {


    <T extends Comparable> Integer  search(T[] array , Comparable<T> compare);


}