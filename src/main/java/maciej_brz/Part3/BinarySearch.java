package maciej_brz.Part3;

/**
 * Created by RENT on 2017-03-06.
 */
public class BinarySearch implements SearchAlgoritm {


    @Override
    public int search(int[] array, int value) {
        if (array==null) throw new IllegalArgumentException("Array is nul");

        int minimum = 0;
        int maximum = array.length - 1;
        while (minimum <= maximum) {
            int middle = minimum + (maximum - minimum) / 2;
            if      (value < array[middle]) maximum = middle - 1;
            else if (value > array[middle]) minimum = middle + 1;
            else return middle;
        }
        return -1;



    }
}
