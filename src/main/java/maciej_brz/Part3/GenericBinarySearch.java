package maciej_brz.Part3;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-03-06.
 */
public class GenericBinarySearch implements GenericSearchAlgorithm {
    @Override
    public <T extends Comparable> Integer search(T[] array,  Comparable<T> compare) {

        if (array==null) throw new IllegalArgumentException("Array is null");

        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int middle = left + (right - left ) /2;
            if      ( array[middle].compareTo(array[middle+1]) >0) {right = middle - 1;}
            else if ( array[middle].compareTo(array[middle-1]) <0 ) {left = middle + 1;}
            else return middle;
        }

        return -1;
    }

}


// if      (! predict.equals(array[middle]) && ! predict.equals(array[middle+1])  ) right = middle - 1;
//         else if (! predict.equals(array[middle])  && ! predict.equals(array[middle-1])) left = middle + 1;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//if (array==null) throw new IllegalArgumentException("Array is null");
//
//        int left = 0;
//        int right = array.length - 1;
//        while (left <= right) {
//        int middle = left + (right - left ) /2;
//        if      (/*! predict.equals(array[middle])&&*/ array[middle].compareTo(predict.)<0) right = middle - 1;
//        else if (/*! predict.equals(array[middle])&& */array[middle].compareTo(predict)>0) left = middle + 1;
//        else return middle;
//        }
//        return -1;