package maciej_brz.Part3;

/**
 * Created by Maciek on 2017-03-05.
 */
public class NaiveSearch implements SearchAlgoritm {
    @Override
    public int search(int[] array, int value) {


        if (array == null) throw  new IllegalArgumentException("ARRAY IS  NULL");
        if (array.length!=0) {
            for (int i = 0; i < array.length; i++) {
                if (array[i]==value) {
                    return i;
                }
            }
        }


        return -1;
    }


//    @Override
//    public int search(int[] array, int value) {
//        int i = 0;
//        if (array == null) throw  new IllegalArgumentException("ARRAY IS  NULL");
//        if(array.length==0) {
//        i=-1;
//        }
//        else{
//           while ((array[i] != value)&&(i < array.length-1)) {
//           i++;
//           }
//           if((i==array.length-1)&&(array[i]!=value)) {i = -1;}
//        }
//
//        return i;
//    }
}


/*





 */