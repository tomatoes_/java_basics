package maciej_brz.Part3;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Created by RENT on 2017-03-06.
 */
public class GenericNaiveSearch implements GenericSearchAlgorithm {

    @Override
    public <T extends Comparable> Integer  search(T[] array, Comparable<T> compare) {


        if (array == null) throw  new IllegalArgumentException("ARRAY IS  NULL");
        if (array.length!=0) {
            for (int i = 0; i < array.length; i++) {
                if (compare.equals(array[i])) {
                    return i;
                }
            }
        }


        return -1;
    }



}

