package maciej_brz.part1;

import java.util.Scanner;

/**
 * Created by Maciek on 2017-02-28.
 */
public class Problem7UsingArraysOryginalConcept {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();

            String[] split = line.split(" ");


            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            reversingMetod(arrayA, arrayB);
        }
    }

//=======================================================================

public static void reversingMetod(char[] arrayA,char[] arrayB){
            int lenghtA = arrayA.length;
            int lenghtB = arrayB.length;
            int newLenght;
            int smallerLenght;
            int longerLenght;
            char[] longerArray;

            if (lenghtA == lenghtB) {
                if (arrayA[lenghtA - 1] + arrayB[lenghtA - 1] >= '9') {
                    newLenght = lenghtA + 1;
                } else {
                    newLenght = lenghtA;
                }
                smallerLenght = lenghtA;
                longerArray = arrayA;

            } else if (lenghtA > lenghtB) {
                newLenght = lenghtA;
                smallerLenght = lenghtB;
                longerArray = arrayA;

            } else {
                newLenght = lenghtB;
                smallerLenght = lenghtA;
                longerArray = arrayB;
            }
            longerLenght = longerArray.length;

            char[] result = new char[newLenght];

            int overflow = 0;
            for (int i = 0; i < smallerLenght; i++) {
                result[i] += (char) (arrayA[i] + arrayB[i] - 48+overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                    if ((i==smallerLenght-1)&&(overflow==1)){result[i+1]='1';}
                } else {
                    overflow = 0;
                }

            }
            for (int i = smallerLenght; i < longerLenght; i++) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }
            int correctionForZeroDisplayAtEndOfResult=0;
            if(overflow != 0 && (lenghtA!=lenghtB)&&result[result.length]=='0'){
                while(result[result.length-correctionForZeroDisplayAtEndOfResult]!='0'){
                    correctionForZeroDisplayAtEndOfResult++;
                }
            }


        boolean runingIfStatmentDependentFormZerosAtStartOfResultTable=true;
            for (int i = 0; i < newLenght-correctionForZeroDisplayAtEndOfResult; i++) {


                if(!((result[i]=='0')&&runingIfStatmentDependentFormZerosAtStartOfResultTable))
                {
                    runingIfStatmentDependentFormZerosAtStartOfResultTable=false;
                    System.out.print(result[i]);
                }

            }


            if ( overflow != 0 && (lenghtA!=lenghtB)) {
                System.out.print('1');

            }
    System.out.println();
        }

//////////////////////////////////////////////////////////////////////////////////////
}

