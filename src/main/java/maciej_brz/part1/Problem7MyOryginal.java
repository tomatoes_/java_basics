package maciej_brz.part1;

import java.util.Scanner;

/**
 * Created by RENT on 2017-02-28.
 */
public class Problem7MyOryginal {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            Integer value1 = scanner.nextInt();
            Integer value2 = scanner.nextInt();
            System.out.println(reversedSumValueOfTwoIntegers(value1, value2));
        }
    }


    public static int reversingValue(Integer value){
        String stringValue = value.toString();
//        stringValue.toCharArray();  // tworzy tablicę po czym tą tablicę odczytywać od prawej wpisując do nowej od lewej
        char[] tempForReversing = new char[stringValue.length()];
        for (int i = 0; i < stringValue.length() ; i++) {
            tempForReversing[i]=stringValue.charAt(i);
        }

        char temp;
        for (int i = 0; i < tempForReversing.length/2; i++) {
            temp=tempForReversing[i];
            tempForReversing[i]=tempForReversing[tempForReversing.length-i-1];
            tempForReversing[tempForReversing.length-i-1]=temp;
        }

        stringValue="";
        for (int i = 0; i < tempForReversing.length ; i++) {
            stringValue+=tempForReversing[i];
        }

        return Integer.parseInt(stringValue);
    }

    public static int reversedSumValueOfTwoIntegers (Integer value1, Integer value2){

        return reversingValue(reversingValue(value1)+reversingValue(value2));
    }


}

