package maciej_brz.part1;

import java.math.BigInteger;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by RENT on 2017-02-28.
 */
public class Problem7Streams {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems>0){
            --problems;

           Stream.of(scanner.nextLine().split(" "))
                            .map(StringBuilder::new)
                            .map(StringBuilder::reverse)
                            .map(StringBuilder::toString)
                            .map(BigInteger::new)
                            .reduce(BigInteger::add)
                            .map(String::valueOf)
                            .map(StringBuilder::new)
                            .map(StringBuilder::reverse)
                            .map(StringBuilder::toString)
                            .map(s -> s.replaceFirst("^0+(?!$)", ""))
                            .ifPresent(System.out::println);


//            StringBuilder numberA = new StringBuilder(split[0]);
//            StringBuilder numberB = new StringBuilder(split[1]);
//            String numberAreversed = numberA.reverse().toString();
//            String numberBreversed = numberB.reverse().toString();
//            BigInteger bigNumberA = new BigInteger(numberAreversed);
//            BigInteger bigNumberB = new BigInteger(numberBreversed);
//            BigInteger sum = bigNumberA.add(bigNumberB);


            // long sum =  parseLong(numberAreversed) +parseLong(numberBreversed);
//            String sumReversed = new StringBuilder(String.valueOf(sum))
//                    .reverse()
//                    .toString();
//            System.out.println(sumReversed);
            //System.out.println(reversedSumValueOfTwoIntegers(12, 356));
        }



    }
}
