

package maciej_brz.part1;

import javafx.css.ParsedValue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Long.parseLong;

//
 //* Created by Maciek on 2017-02-26.
//
public class Problem7 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAreversed = numberA.reverse().toString();
            String numberBreversed = numberB.reverse().toString();
            BigInteger bigNumberA = new BigInteger(numberAreversed);
            BigInteger bigNumberB = new BigInteger(numberBreversed);
            BigInteger sum = bigNumberA.add(bigNumberB);


            // long sum =  parseLong(numberAreversed) +parseLong(numberBreversed);
            String sumReversed = new StringBuilder(String.valueOf(sum))
                    .reverse()
                    .toString();
            System.out.println(sumReversed);

        }


    }
}