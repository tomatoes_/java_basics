package adrian_k.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.lang.Long.parseLong;

public class Problem7 {

    public static void main(String[] args) {
        // Streams (java 8)
        Scanner sc = new Scanner(System.in);
        int problems = Integer.parseInt(sc.nextLine());

        while (problems > 0) {
            --problems;
            Stream.of(sc.nextLine()
                    .split(" "))
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(BigInteger::new)
                    .reduce(BigInteger::add)
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(s -> s.replaceFirst("^0+(?!$)", ""))
                    .ifPresent(System.out::println);
        }
    }

    public static void main3(String[] args) {
        // Java way
        Scanner sc = new Scanner(System.in);
        int problems = Integer.parseInt(sc.nextLine());

        while (problems > 0) {
            --problems;
            String line = sc.nextLine();
            String[] split = line.split(" ");

            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);

            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();
            BigInteger bigIntegerA = new BigInteger(numberAReversed);
            BigInteger bigIntegerB = new BigInteger(numberBReversed);
            BigInteger sum = bigIntegerA.add(bigIntegerB);
            //long sum = parseLong(numberAReversed) + parseLong(numberBReversed);
            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
            System.out.println(sumReversed);
        }
    }

    public static void main2(String[] args) {
        // Classic way
        Scanner sc = new Scanner(System.in);
        int problems = Integer.parseInt(sc.nextLine());
        while (problems > 0) {
            --problems;
            String line = sc.nextLine();
            String[] split = line.split(" ");
            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();
            int lengthA = arrayA.length;
            int lengthB = arrayB.length;
            int newLength;
            int smallerLength;
            int longerLength;
            char[] longerArray;
            if (lengthA == lengthB) {
                if (arrayA[lengthA - 1] + arrayB[lengthB - 1] >= '9') {
                    newLength = lengthA + 1;
                } else {
                    newLength = lengthA;
                }
                smallerLength = lengthA;
                longerArray = arrayA;
            } else if (lengthA > lengthB) {
                newLength = lengthA;
                smallerLength = lengthB;
                longerArray = arrayA;
            } else {
                newLength = lengthB;
                smallerLength = lengthA;
                longerArray = arrayB;
            }
            longerLength = longerArray.length;
            char[] result = new char[newLength];
            int overflow = 0;
            for (int i = 0; i < smallerLength; ++i) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }
            for (int i = smallerLength; i < longerLength; i++) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }
            for (int i = 0; i < result.length; i++) {
                System.out.print(result[i]);
            }
            if (result.length > longerLength && overflow != 0) {
                System.out.print('1');
            }
        }
        /*char[] someTable = {'a', 'b', 'c', 'd', 'e'};
        System.out.println(Arrays.toString(someTable));
        char[] newTable = addrev(someTable);
        System.out.println(Arrays.toString(newTable));
        addrev1(someTable);
        System.out.println(Arrays.toString(someTable));*/

    }

    public static char[] addrev(char[] someTable) {

        int length = someTable.length;
        char[] newTable = new char[length];
        for (int i = 0; i < length; i++) {
            newTable[i] = someTable[length - 1 - i];
        }
        return newTable;
    }

    public static void addrev1(char[] someTable) {
        int length = someTable.length;
        for (int i = 0; i < length / 2; i++) {
            char temp = someTable[i];
            someTable[i] = someTable[length - i - 1];
            someTable[length - i - 1] = temp;
        }
    }

    public static void prime1Task(int t) {

        Scanner sc = new Scanner(System.in);
        int n, m;
        for (int i = 0; i < t; i++) {
            System.out.println("Write two numbers seperated with Enter: ");
            n = sc.nextInt();
            m = sc.nextInt();
            for (int j = n; j <= m; j++) {
                if (checkIfNumberIsPrime(j)) {
                    System.out.println(j);
                }
            }
        }
    }

    public static boolean checkIfNumberIsPrime(int n) {
        if (n < 2) return false; //gdy liczba jest mniejsza niż 2 to nie jest pierwszą

        for (int i = 2; i * i <= n; i++)
            if (n % i == 0) return false; //gdy znajdziemy dzielnik, to dana liczba nie jest pierwsza
        return true;
    }

    public static int nwd(int x, int y) {
        while (x != y) {
            if (x > y) x -= y;
            else y -= x;
        }
        return x;
    }

    public static void nww() {
        int x, y;

        Scanner sc = new Scanner(System.in);

        System.out.print("Podaj pierwsza liczbe: ");
        x = sc.nextInt();

        System.out.print("Podaj druga liczbe: ");
        y = sc.nextInt();

//Obliczamy i wyswietlamy NWW
        System.out.println("NWW liczb " + x + " i " + y + " wynosi: " + ((x * y) / nwd(x, y)));
    }

}

