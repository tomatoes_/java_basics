package adrian_k.part1;


public class Problem4 {

    public static void main(String[] args) {

        int[] array1 = new int[50];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = i;
            System.out.print(array1[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < array1.length; i++) {
            if (i % 2 != 0 && i != 0) {
                array1[i] = array1[i] + array1[i - 1];
                System.out.print(array1[i] + " ");
            } else
                System.out.print(array1[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < array1.length; i++) {
            if (array1[i] % 2 == 0) {
                array1[i] = array1[i] / 2;
            }
            System.out.print(array1[i] + " ");
        }

        System.out.println();

        int sum = 0;
        for (int i = 0; i < array1.length; i++) {
            sum = sum + array1[i];
        }

        System.out.println(sum);
    }

}
