package adrian_k.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Person implements Comparable {

    private String firstName;
    private String lastName;
    private String pesel;
    private char gender;
    private Address address;

    public static Person getRandomPerson() {
        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(7))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ? 'M' : 'F')
                .address(Address.getRandomAddres())
                .build();
    }


    @Override
    public int compareTo(Object o) {
        Person person = (Person) o;
        if (this.getFirstName().toLowerCase().charAt(0) > person.getFirstName().toLowerCase().charAt(0))
            return 1;
        else if (this.getFirstName().toLowerCase().charAt(0) < person.getFirstName().toLowerCase().charAt(0))
            return -1;
        else
            return 0;
    }
}
