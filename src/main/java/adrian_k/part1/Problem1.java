package adrian_k.part1;


import java.util.Arrays;

import static java.lang.System.*;

public class Problem1 {
    public static void main(String[] args) {

        out.println(Arrays.toString(createFirstArray()));
        out.println(Arrays.toString(createSecondArray()));

        for (int i = 1; i < 10; i++) {
            out.print(i * 100 + " " + i * 10 + " ");
        }
        out.println();
        for (int i = 0, j = 1, k; i < 100; k = j, j+=i, i = k) {
            out.print( i + " ");
        }
    }
    public static int[] createFirstArray(){
        int[] array = new int[31];
        for (int i = 0, j=1; i < array.length; i++, j+=2) {
            array[i] = j;
        }
        return array;
    }

    public static int[] createSecondArray(){
        int[] array = new int[7];
        for (int i = 0, j=0; j <= 6; i++, j += 2) {
            array[i]=j;
        }
        for (int i = 4, j=4; j > 0; i++, j-=2) {
            array[i]=j;
        }
        return array;
    }

    public static int[] createThirdArray(){
        int[] array = new int[20];
        for (int i = 0, j = 1; i < 10; i++) {
            out.print(i * 100 + " " + i * 10 + " ");
        }
        return array;
    }
}