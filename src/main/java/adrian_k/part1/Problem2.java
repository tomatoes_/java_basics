package adrian_k.part1;


import static java.lang.System.*;

public class Problem2 {


    public static void main(String[] args) {

        drawTriangle(5);
        out.println();
        drawSlashLine(5);
        out.println();
        drawSquare(6);
        out.println();
        drawCross(6);

    }

    public static void drawTriangle(int h) {

        for (int i = 1; i <= h; i++) {
            for (int j = 1; j <= i; j++) {
                out.print("*");
            }
            out.println();

        }
    }

    public static void drawSlashLine(int h) {
        for (int i = 1; i <= h; i++) {
            for (int j = 1; j < i; j++) {
                out.print(" ");
            }
            out.println("*");

        }
    }

    public static void drawSquare(int a) {

        for (int i = 1; i <= a; i++) {

            out.print("*");
            char toPrint;
            if (i == 1 || i == a) {
                toPrint = '*';
            } else {
                toPrint = ' ';
            }
            for (int j = 1; j+1 < a; j++) {
                out.print(toPrint);
            }
            out.print("*");
            out.println();
        }
    }

    public static void drawCross(int h){
        int halfH = h/2;

        for (int i = 0; i < halfH; i++) {

            for (int j = 0; j < i; j++) {
                out.print(" ");
            }

            out.print("*");

            for (int j = halfH-1; j > i; j--) {
                out.print("  ");
            }

            out.print("*");
            out.println();
        }

        for (int i = 0; i < halfH; i++) {

            for (int j = halfH-1; j > i; j--) {
                out.print(" ");
            }

            out.print("*");

            for (int j = 0; j < i; j++) {
                out.print("  ");
            }

            out.print("*");
            out.println();
        }

    }
}


