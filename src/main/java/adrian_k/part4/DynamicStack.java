package adrian_k.part4;

import java.util.Optional;

public class DynamicStack<T> implements Stack<T> {

    private StackElement<T> top;
    private int size = 0;

    public void push(T elements) {
        top = new StackElement<>(elements, top);
        size++;
    }

     public T pop() {
        --size;
        T value = top.getElement();
        this.top = top.getPrevious();

        return value;
    }

    public T peek() {
        return Optional.ofNullable(top).map(StackElement::getElement).orElse(null);
//        if (top != null) return top.getElement();
//        else return null;
    }

    public int size() {
        return size;
    }

}
