package adrian_k.part4;


public interface Stack<T> {

    void push(T elements);

    T pop();

    T peek();

    int size();

    default boolean isEmpty() {
        return size() == 0;
    }

}
