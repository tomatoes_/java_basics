package adrian_k.part4;


public class ArrayStack<T> implements Stack<T> {
    // usuwać referencje przy pop
    // wykorzystac element o indeksie 0

    private T[] array;
    private int size = 0;

    @SuppressWarnings("unchecked")
    public ArrayStack(int size) {
        array =  (T[]) new Object[size];
    }

    public void push(T elements) {
        if (size == array.length-1){
            throw new RuntimeException("Stack overflow");
        }
        array[++size] = elements;
    }

     public T pop() {
        return array[size--];
    }

    public T peek() {
        return array[size];
    }

    public int size() {
        return size;
    }

}
