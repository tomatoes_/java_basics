package adrian_k.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StackElement<T>  {

    private T element;
    private StackElement<T> previous;

}
