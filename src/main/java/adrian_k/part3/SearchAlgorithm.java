package adrian_k.part3;

public interface SearchAlgorithm {

    int search(int[] array, int value);

}
