package adrian_k.part3;

import java.util.function.Predicate;
import java.util.function.Supplier;

public interface GenericSearchAlgorithm {

    <T extends Comparable> int search(T[] array, Predicate<T> predicate);

//    <T> Supplier<T> search(T[] array, Predicate<T> predicate);

}
