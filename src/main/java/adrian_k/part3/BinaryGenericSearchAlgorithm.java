package adrian_k.part3;

import java.util.function.Function;


public class BinaryGenericSearchAlgorithm {

    public <T> int search(T[] array, Function<T, Integer> comparator) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int first = 0;
        int last = array.length - 1;
        int middle = (first + last) / 2;

        while (first <= last) {

            if (comparator.apply(array[middle]) > 0)
                first = middle + 1;
            else if (comparator.apply(array[middle]) < 0)
                last = middle - 1;
            else return middle;

            middle = (first + last) / 2;
        }

        return -1;

    }
}
