package adrian_k.part3;

import java.util.function.Predicate;


public class NaiveGenericSearchAlgorithm implements GenericSearchAlgorithm {

    public <T extends Comparable> int search(T[] array, Predicate<T> predicate) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        for (int i = 0; i < array.length; i++) {
            if (predicate.test(array[i]))
                return i;
        }

        return -1;
    }
}
