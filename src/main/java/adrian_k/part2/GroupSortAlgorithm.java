package adrian_k.part2;

import java.util.function.Predicate;

public interface GroupSortAlgorithm {

    default void sort (int[] array, Predicate<Integer> predicate) {}
    default void sort (int[] array, Predicate<Integer> predicate1, Predicate<Integer> predicate2) {}

}