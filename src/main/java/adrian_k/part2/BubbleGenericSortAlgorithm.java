package adrian_k.part2;

import java.util.Comparator;
import static java.util.Objects.*;


public class BubbleGenericSortAlgorithm implements GenericSortAlgorithm {

    public <T> void sort(T[] array, Comparator<T> comparator) {

        if (isNull(array)) {
            throw new IllegalArgumentException("Array should not be null");
        }
        for (int j = 0; j < array.length; j++)
            for (int i = 0; i < array.length - 1; i++) {
                if (comparator.compare(array[i], array[i + 1]) == 1) {
                    T tmp;
                    tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                }
            }

    }
}
