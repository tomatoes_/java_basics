package adrian_k.part2;

import java.util.Comparator;


public class QuickGenericSortAlgorithm implements GenericSortAlgorithm {

    public <T> void sort(T[] array, Comparator<T> comparator) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        if (array.length==0){
            return;
        }

        quicksort(array, 0, array.length - 1, comparator);
    }
    private <T> void quicksort(T[] array, int low, int high, Comparator<T> comparator) {

        int i = low, j = high;

        T pivot = array[low + (high-low)/2];

        while (i <= j) {

            while (comparator.compare(array[i], pivot) == -1) {
                i++;
            }

            while (comparator.compare(array[j], pivot) == 1) {
                j--;
            }

            if (i <= j) {
                T temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
//         Recursion
        if (low < j)
            quicksort(array, low, j, comparator);
        if (i < high)
            quicksort(array, i, high, comparator);
    }

}
