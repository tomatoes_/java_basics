package krystian_m.TemplateMethod;

public class MargheritaPizzaRecipe extends PizzaRecipe {
    @Override
    public void doCake() {
        System.out.println("do Margherita cake");
    }

    @Override
    public void addSauce() {
        System.out.println("add Tomato sauce");
    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella and basil");
    }

    @Override
    public void bake() {
        System.out.println("bake for 20 min in 210 degrees");
    }

}
