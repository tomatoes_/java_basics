package krystian_m.TemplateMethod;

public abstract class PizzaRecipe {

    public abstract void doCake();
    public abstract void addSauce();
    public abstract void addAdditions();
    public abstract void bake();

    public void doPizza() {
        this.doCake();
        this.addSauce();
        this.addAdditions();
        this.bake();
    }
}
