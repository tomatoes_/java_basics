package krystian_m.TemplateMethod;

public class PepperoniWithSaucePizzaRecipe extends PepperoniPizzaRecipe {
    public void addSauce() {
        System.out.println("add spicy sauce");
    }

    public void doPizza() {
        this.doCake();
        this.addSauce();
        this.addAdditions();
        this.bake();
    }

}
