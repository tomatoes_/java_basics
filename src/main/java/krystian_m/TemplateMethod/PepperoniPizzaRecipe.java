package krystian_m.TemplateMethod;

public class PepperoniPizzaRecipe extends PizzaRecipe {
    @Override
    public void doCake() {
        System.out.println("do Pepperoni cake");
    }

    @Override
    public void addSauce() {
    }

    @Override
    public void addAdditions() {
        System.out.println("add pepperoni sausage, and pepperoni pepper");
    }

    @Override
    public void bake() {
        System.out.println("bake to finish");
    }

    public void doPizza() {
        this.doCake();
        this.addAdditions();
        this.bake();
    }
}
