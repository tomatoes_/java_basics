package krystian_m.TemplateMethod;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PizzaRecipe pizza1 = new MargheritaPizzaRecipe();
        PizzaRecipe pizza2 = new HawaiianPizzaRecipe();
        PizzaRecipe pizza3 = new PepperoniPizzaRecipe();
        PizzaRecipe pizza4 = new PepperoniWithSaucePizzaRecipe();


        List<PizzaRecipe> pizzaList = new LinkedList<PizzaRecipe>();

        pizzaList.add(pizza1);
        pizzaList.add(pizza2);
        pizzaList.add(pizza3);
        pizzaList.add(pizza4);

        for(PizzaRecipe pizza : pizzaList){
            System.out.println("----- we're going to bake the pizzas!----");
            pizza.doPizza();
        }
    }
}
