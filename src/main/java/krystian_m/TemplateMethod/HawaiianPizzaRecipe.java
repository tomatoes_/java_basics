package krystian_m.TemplateMethod;

public class HawaiianPizzaRecipe extends PizzaRecipe {
    @Override
    public void doCake() {
        System.out.println("do the Hawaiian cake");
    }

    @Override
    public void addSauce() {
        System.out.println("add tomato sauce");
    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella, pineapple, ham");
    }

    @Override
    public void bake() {
        System.out.println("bake for 20 min in 210 degrees");
    }
}
