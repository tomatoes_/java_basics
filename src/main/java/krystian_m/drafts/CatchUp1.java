package krystian_m.drafts;

public class CatchUp1 {

    public static void main(String[] args) {
        System.out.println("First Java Program");
        excercise2_1();
        excercise2_2();
        excercise2_3();
        excercise2_4();
    }

   public static void excercise2_1(){
       int firstValue;
       int secondValue;
       firstValue=10;
       secondValue=12;
       System.out.println(firstValue + " " + secondValue);
       System.out.println(secondValue + " " + firstValue);
   }


   public  static void excercise2_2(){
       int firstValue = 1;
       int secondValue = 2;
       System.out.println("First Value: " + firstValue);
       System.out.println("Second Value: " + secondValue);
   }


   public static void excercise2_3(){
       int firstValue = 10, secondValue = 20, i, j, k;
       System.out.println("First Value: " + firstValue);
       System.out.println("Second Value: " + secondValue);

   }

   public static void excercise2_4(){
       int ar_ray[] = new int[2];
       ar_ray[0] = 11;
       System.out.println("Zero element of an array is: " +ar_ray[0]);
   }
}
