package krystian_m.utils;

import krystian_m.utils.ArrayGenerator;

public class ZeroesArrayGenerator implements ArrayGenerator{

    @Override
    public int[] generate(int length, int rangeFrom, int rangeTo) {
        return new int[0];
    }

    @Override
    public int[] generate(int length) {
        return new int[0];
    }
}
