package krystian_m.utils;

import java.util.Random;

public class RandomArrayGenerator implements ArrayGenerator {


    public final int[] generate(int length, int rangeFrom, int rangeTo) {
        if (rangeFrom >= rangeTo) {
            throw new IllegalArgumentException("Range is invalid");
        }
        int[] array = new int [length];
        Random random =new Random();
        for (int i = 0; i < length; i++){
            array[i]= random.nextInt(rangeTo - rangeFrom) + rangeFrom;
        }
        return array;
    }

    public int[] generate(int length) {
        return generate(length, 0, Integer.MAX_VALUE);
    }

}
