package krystian_m.utils;

public interface ArrayGenerator {
     int[] generate(int length, int rangeFrom, int rangeTo);
     int[] generate(int length);
}
