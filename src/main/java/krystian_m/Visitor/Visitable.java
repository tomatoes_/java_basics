package krystian_m.Visitor;

public interface Visitable {
    public void accept(Visitor visitor);

}
