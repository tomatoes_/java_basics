package krystian_m.Visitor;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        File f1 = new PdfFile();
        File f2 = new CSVFile();

        Presenter p = new Presenter();
        LinkedList<File> fileList = new LinkedList<File>();

        fileList.add(f1);
        fileList.add(f2);

        for(File f : fileList){
            if (f instanceof PdfFile){
                p.visit((PdfFile) f);
            } else if (f instanceof CSVFile){
                p.visit((CSVFile) f);
            }
        }

    }
}
