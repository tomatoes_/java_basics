package krystian_m.Visitor;

public interface Visitor {
    void visit(PdfFile pdfFile);
    void visit(CSVFile csvFile);
}
