package krystian_m.Visitor;

public class Presenter implements Visitor {
    @Override
    public void visit(PdfFile pdfFile) {
        pdfFile.getType();
    }

    @Override
    public void visit(CSVFile csvFile) {
        csvFile.getType();
    }
}
