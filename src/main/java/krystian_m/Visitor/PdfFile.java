package krystian_m.Visitor;

public class PdfFile implements File,Visitable{
    @Override
    public void getType() {
        System.out.println("PDF File");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
