package krystian_m.Visitor;

public class CSVFile implements File, Visitable {
    @Override
    public void getType() {
        System.out.println("CSV File");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
