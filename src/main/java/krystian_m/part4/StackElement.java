package krystian_m.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StackElement<T> {
    private T value;
    private StackElement <T> previous;
}
