package krystian_m.part4;

import java.util.Objects;

public class DynamicStack<T> implements Stack<T> {
    private StackElement<T> top;
    private int size = 0;

    @Override
    public void push(T t) {
        top = new StackElement<>(t, top);
        ++size;
    }

    @Override
    public T pop() {
        --size;
        StackElement<T> oldTop = top;
        top = top.getPrevious();
        return oldTop.getValue();
    }

    @Override
    public T peek() {
        if (Objects.nonNull(top)){
            return top.getValue();
        }
        else {
            return null;
        }
    }

    @Override
    public int size() {
        return size;

    }

}
