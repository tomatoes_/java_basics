package krystian_m.part3;

import java.util.function.Function;

public class BinaryGenericSearchAlgorithm {

    public <T> int search(T[] array, Function<T, Integer> comparator) {
        if (array == null) {
            throw new IllegalArgumentException("array can't be null");
        }

        final int length = array.length;
        int left = 0, right = length - 1, middle;
        while (left <= right) {
            middle = left + (right - left) / 2;
            if (comparator.apply(array[middle]) > 0) {
                left = middle + 1;
            } else if (comparator.apply(array[middle]) < 0) {
                right = middle - 1;
            } else
                return middle; // found
        }
        return -1; // not found
    }

}
