package krystian_m.part3;

public class BinarySearchAlgorithm implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value) {
        if (array == null) {
            throw new IllegalArgumentException("array can't be null");
        }

        int lowArrayNumber = 0;
        int highArrayNumber = array.length - 1;
        while (lowArrayNumber <= highArrayNumber) {
            int midArrayNumber = lowArrayNumber + ((highArrayNumber - lowArrayNumber) / 2);
            if (array[midArrayNumber] > value)
                highArrayNumber = midArrayNumber - 1;
            else if (array[midArrayNumber] < value)
                lowArrayNumber = midArrayNumber + 1;
            else
                return midArrayNumber; // found
        }
        return -1; // not found
    }

}
