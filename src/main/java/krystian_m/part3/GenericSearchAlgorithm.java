package krystian_m.part3;

import java.util.function.Predicate;

public interface GenericSearchAlgorithm {
    <T extends Comparable> int search(T[] array, Predicate<T> predicate);
}
