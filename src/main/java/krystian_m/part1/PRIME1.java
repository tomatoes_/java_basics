package krystian_m.part1;

public class PRIME1 {
    public static void main(String[] args) {
        //define limit
        int limitFrom = 2;
        int limitTo = 97;

        System.out.println("Prime numbers between " + limitFrom + " and " + limitTo);

        //loop through the numbers one by one
        for(int i= limitFrom; i <= limitTo; i++){

            boolean isPrime = true;

            //check to see if the number is prime
            for(int j=2; j < i ; j++){

                if(i % j == 0){
                    isPrime = false;
                    break;
                }
            }
            // print the number
            if(isPrime)
                System.out.print(i + " ");
        }
    }
}
