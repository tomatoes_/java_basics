package krystian_m.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Long.parseLong;

public class Problem7 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();
            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);

            BigInteger sum = bigNumberA.add(bigNumberB);

//            long sum = parseLong(numberAReversed) + parseLong(numberBReversed);
            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
            System.out.println(sumReversed);
        }

    }

    private static void reversing() {
        char[] rightTable = new char[]{'A', 'B', 'C', 'D', 'E'};
        char[] reverseTable = new char[5];

        for (int i = 0; i < rightTable.length; i++) {
            rightTable[i] = reverseTable[reverseTable.length - i - 1];
        }

        for (int i = 0; i < reverseTable.length; i++) {
            System.out.println(reverseTable[i] + " ");

        }
    }

    private char[] reverse(char[] toReverse) {
        int length = toReverse.length;
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = toReverse[length - 1 - i];
        }
        return result;
    }

    private static void reverseInPlace(char[] toReverse) {
        char tmp;
        for (int i = 0; i < toReverse.length / 2; i++) {
            tmp = toReverse[i];
            toReverse[i] = toReverse[toReverse.length - i - 1];
            toReverse[toReverse.length - i - 1] = tmp;
        }
    }

    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lengthA = arrayA.length;
            int lengthB = arrayB.length;

            int newLength;
            int smallerLength;
            int longerLength;
            char[] longerArray;
            if (lengthA == lengthB) {
                if (arrayA[lengthA - 1] + arrayB[lengthA - 1] >= '9') {
                    newLength = lengthA + 1;
                } else {
                    newLength = lengthA;
                }
                smallerLength = lengthA;
                longerArray = arrayA;
            } else if (lengthA > lengthB) {
                newLength = lengthA;
                smallerLength = lengthB;
                longerArray = arrayA;
            } else {
                newLength = lengthB;
                smallerLength = lengthA;
                longerArray = arrayB;
            }
            longerLength = longerArray.length;

            char[] result = new char[newLength];

            int overflow = 0;
            for (int i = 0; i < smallerLength; i++) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = smallerLength; i < longerLength; i++) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }

            for (int i = 0; i < newLength; i++) {
                System.out.print(result[i]);
            }

            if (result.length > longerLength && overflow != 0) {
                System.out.println('1');
            }

        }
    }


}
