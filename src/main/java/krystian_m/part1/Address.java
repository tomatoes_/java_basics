package krystian_m.part1;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Address {
    private String streetName;
    private String flatNumber;
    private String country;
    private String postalCode;


    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(8))
                .flatNumber(randomAlphanumeric(3))
                .country(randomAlphabetic(6))
                .postalCode(randomAlphanumeric(5))
                .build();

    }
}
