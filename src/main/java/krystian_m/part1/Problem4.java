package krystian_m.part1;

public class Problem4 {
    public static void main(String[] args) {


        /////////////////////////////////
        int[] array = new int[10];
        for (int i = 0; i < 10 ; i++) {
            array[i]=i;
        }

        for (int tmp:array) {
            System.out.print(tmp + " ");
        }
        System.out.println(" ");

        /////////////////////////////////
        for (int i = 1; i < 10 ; i+=2) {
            array[i] = array[i-1]+array[i];
        }


        for (int tmp:array) {
            System.out.print(tmp + " ");
        }
        System.out.println(" ");

        /////////////////////////////////

        for (int i = 0; i < 10 ; i++) {
            if(array[i]%2==0){
                array[i]=array[i]/2;
            }
        }

        for (int tmp:array) {
            System.out.print(tmp + " ");
        }
        System.out.println(" ");

        /////////////////////////////////

        int sum = 0;
        for (int element : array) {
            sum+= element;
        }
        System.out.println(sum);
    }
    // private static final int TEST_ARRAY_LENGTH = 100;
}
