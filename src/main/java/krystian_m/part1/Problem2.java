package krystian_m.part1;

public class Problem2 {
    public static void main(String[] args) {


    }
    public static void printAsterisks(int height){
        for (int i = 0; i < height; i++) {
            for(int j = 0; j <= i; j++){
                System.out.print('*');
            }
            System.out.println();
        }
    }

    public static void slashLine(int height){
        for (int i = 0; i < height; i++) {
            for(int j = 0; j <= i; j++){
                System.out.print(' ');
            }
            System.out.println('*');
        }
    }
}
