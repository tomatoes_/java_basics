package krystian_m.Adapter;

public class AnythingStringData implements StringData{

    @Override
    public String getData() {
       AnythingData ad = new AnythingData();
        ad.setVal1("qwerty");
        ad.setVal2("123456");
        ad.setValInt(123);
        ad.setValInt2(34);

        return ad.getVal1() + ad.getVal2() + String.valueOf(ad.getValInt() + ad.getValInt2());
    }
}
