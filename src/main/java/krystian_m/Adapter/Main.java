package krystian_m.Adapter;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        StringData s1 = new NameData();
        StringData s2 = new SurnameData();
        StringData s3 = new IntStringData();
        StringData s4 = new AnythingStringData();
//        IntData s4 = new IntData();

        LinkedList<StringData> dataList = new LinkedList<StringData>();

        dataList.add(s1);
        dataList.add(s2);
        dataList.add(s3);
        dataList.add(s4);

//        System.out.println(s4.getIntData());

        for(StringData s: dataList){
            System.out.println(s.getData());
        }
    }
}
