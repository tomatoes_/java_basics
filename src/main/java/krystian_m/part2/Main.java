package krystian_m.part2;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] myArray = new int[]{10,2,3,4,0,6,7,8,9,10};
//
//        System.out.println("This is my array");
//        System.out.println(Arrays.toString(myArray));
//        BubbleSortAlgorithm arrayToSort = new BubbleSortAlgorithm();
//
//        arrayToSort.sort(myArray);
//        System.out.println(Arrays.toString(myArray));
//
//        arrayToSort.sort(myArray,false);
//        System.out.println(Arrays.toString(myArray));

//        HeapSortAlgorithm heapSortAlgorithm = new HeapSortAlgorithm();
//        heapSortAlgorithm.sort(myArray);
//        System.out.println(Arrays.toString(myArray));

///////////////////////////////////////////////
            HeapSortAlgorithm arrayToHeapSort = new HeapSortAlgorithm();
            arrayToHeapSort.sort(myArray);

            System.out.println("Sorted array is");
            System.out.println(Arrays.toString(myArray));
/////////////////////////////////////////////////

    }

}
