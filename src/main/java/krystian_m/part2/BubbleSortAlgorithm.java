package krystian_m.part2;

import java.util.IllegalFormatCodePointException;

public class BubbleSortAlgorithm implements SortAlgorithm {
//    @Override
//    public void sort(int[] array, boolean ascending) {
//
//    if (array == null) {
//        throw new IllegalArgumentException("Array should not be null");
//    }
//
//        int n = array.length;
//        int tmp = 0;
//
//        if (ascending) {
//            System.out.println("ascending order");
//            for (int i = 0; i < n; i++) {
//                for (int j = 1; j < (n - i); j++) {
//                    if (array[j - 1] > array[j]) {
//                        tmp = array[j - 1];
//                        array[j - 1] = array[j];
//                        array[j] = tmp;
//                    }
//                }
//            }
//        } else {
//            System.out.println("descending order");
//            for (int i = 0; i < (n - 1); i++) {
//                for (int j = 0; j < n - i - 1; j++) {
//                    if (array[j] < array[j + 1]) {
//                        tmp = array[j];
//                        array[j] = array[j + 1];
//                        array[j + 1] = tmp;
//                    }
//                }
//            }
//        }
//

//    }


    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement ; i++) {
                if (ascending && array[i - 1] > array[i] ||
                      !ascending && array[i - 1] < array[i]) {
                    int tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }
            }
         --lastSortedElement;
        }
    }


}

