package krystian_m.part2;

import java.util.function.Predicate;

public interface GroupSortAlgorithm  {
    void sort(int[] array, Predicate<Integer> predicate);
}
