package krystian_m.part2;

import java.util.Comparator;
import java.util.Objects;

import static java.util.Objects.isNull;

public class BubbleGenericSortAlgorithm implements GenericSortAlgorithm {

    public <T> void sort(T[] array, Comparator<T> comparator) {
        if (isNull(array)) {
            throw new IllegalArgumentException("Array should not be null");
        }
        int lastSortedElement = array.length;
        while (lastSortedElement > 1) {
            for (int i = 1; i < lastSortedElement ; i++) {
                if (comparator.compare(array[i - 1], array[i]) == 1) {

                    T tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                }
            }
            --lastSortedElement;
        }
    }
}
