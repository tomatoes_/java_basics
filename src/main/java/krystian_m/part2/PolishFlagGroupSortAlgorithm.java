package krystian_m.part2;

import java.util.function.Predicate;

public class PolishFlagGroupSortAlgorithm implements GroupSortAlgorithm {

    public void sort(int[] array, Predicate<Integer> predicate) {
//        int firstGroup = 0;
//        int secondGroup = array.length -1;
//
//        do {
//            while ((array[firstGroup] == 0 && firstGroup < secondGroup && isAscending) || (array[firstGroup] !=0 && firstGroup < secondGroup && !isAscending))
//            firstGroup++;
//            while ((array[secondGroup] == 1 && firstGroup < secondGroup && isAscending) || (array[secondGroup] !=1 && firstGroup < secondGroup && !isAscending))
//            secondGroup--;
//
//            if (firstGroup < secondGroup)
//                swapChars(&(array[firstGroup]), &(array[secondGroup]));
//        } while (firstGroup < secondGroup);
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }
        if (array.length == 0) {
            return;
        }
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            while (predicate.test(array[left])) {
                left++;
            }
            while (!predicate.test(array[right])) {
                right--;
            }
            if (left < right) {
                int temp = array[left];
                array[left] = array[right];
                array[right] = temp;
            }
        }


//        while (left < right) {
//            if(array[left] == 0){
//                swap(array, left, right);
//                left ++;
//            } else(array[right] == 1){
//                swap (array, left, right);
//                right --;
//            }
//        }
//
//    }
//    private static void swap (int[] array, int from, int to){
//        int temp = array[from];
//        array[from] = array[to];
//        array[to] = temp;
//    }
    }
}
