package marcin_s.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Iterator;

import static java.util.Objects.*;
import static java.util.Optional.*;


public class DynamicList<T> implements List<T> {
    private ListElement first;
    private ListElement last;

    private int size = 0;

    public T getFirst() {
//        if (first == null){
//            return new IndexOutOfBoundsException()
//        }
//        return first.getValue();
        return getChecked(last);
    }

    public T getLast() {
        return getChecked(last);
    }

    private T getChecked(ListElement element) {
        return ofNullable(element).map(ListElement::getValue).orElseThrow(IndexOutOfBoundsException::new);
    }

    public T get(int index) {
        if (getElement(index) == null){
            throw new IndexOutOfBoundsException();
        }
        return getElement(index).getValue();
    }

    private ListElement getElement(int index) {
        ListElement element = first;
        while (index-- > 0) {
            element = element.getNext();
        }
        return element;
    }

    public void add(T element) {
        if (isNull(first)) {
            first = new ListElement(element, null, null);
            last = first;
        } else {
            last.next = new ListElement(element, last, null);
            last = last.next;
        }
        size++;
    }

    public void remove(int index) {
        if (getElement(index) == null){
            throw new IndexOutOfBoundsException();
        }
        ListElement toRemove = getElement(index);
        if (toRemove.equals(first) && !iterator().hasNext()) {
            toRemove = null;
        } else if (toRemove.equals(first) && iterator().hasNext()) {
            toRemove.getNext().previous = toRemove.previous;
        } else if (toRemove.equals(last)) {
            toRemove.getPrevious().next = toRemove.next;

        } else {
            toRemove.getPrevious().next = toRemove.next;
            toRemove.getNext().previous = toRemove.previous;
        }
        size--;
    }

    public int size() {
        return size;
    }

    public Iterator iterator() {
        return new Iterator<T>() {
            ListElement ref;

            public boolean hasNext() {
                return ref != null;
            }

            public T next() {
                ref = ref.next;
                return ref.previous.getValue();
            }
        };
    }
    @Data
    @AllArgsConstructor
    private class ListElement {

        private T value;
        private ListElement previous;
        private ListElement next;
    }
}
