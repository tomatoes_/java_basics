package marcin_s.part4;

public class DynamicStack<T> implements Stack<T> {

    private StackElement<T> top;
    private int size = 0;

    public T pop() {
        --size;
        StackElement<T> oldTop = top;
        top = top.getPrevious();
        return oldTop.getValue();
    }

    public T peek() {
        return top != null ? top.getValue() : null;
    }

    public void push(T t) {
        top = new StackElement<>(t, top);
        ++size;
    }

    public int size() {
        return size;
    }


}
