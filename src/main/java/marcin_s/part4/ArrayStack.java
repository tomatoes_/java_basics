package marcin_s.part4;

import java.util.function.Function;

public class ArrayStack<T> implements Stack<T> {

    private T[] array;
    private int size = 0;

    @SuppressWarnings("unchecked")
    public ArrayStack(int size) {
        array = (T[]) new Object[size];

    }

    public T pop() {
        return array[size--];
    }

    public T peek() {
        return array[size];
    }

    public void push(T t) {
        if (size == array.length -1) {
            throw new RuntimeException("Stack overflow");
        }
        array[++size] = t;
    }

    public int size() {
        return size;
    }


}
