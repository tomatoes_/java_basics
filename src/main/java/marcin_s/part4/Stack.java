package marcin_s.part4;

public interface Stack<T> {

    void push(T elements);

    T pop();

    T peek();

    int size();

    default boolean isEmpty() {
        return size() == 0;
    }

}
