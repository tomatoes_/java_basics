package marcin_s.part2;

import java.util.function.Predicate;

public class DutchFlagGroupSortAlgorithm implements GroupSortAlgorithm {

    public void sort(int[] array, Predicate<Integer> predicate1, Predicate<Integer> predicate2) {
        if (array == null)
            throw new IllegalArgumentException("Array should not be null");

        int lo = 0, mid = 0, hi = array.length - 1;

        while (mid <= hi) {
            if (!predicate1.test(array[mid])) {
                swap(array, lo++, mid++);

            } else if (predicate1.test(array[mid]) && predicate2.test(array[mid])) {
                mid++;

            } else if (!predicate2.test(array[mid])) {
                swap(array, mid, hi--);
            }

        }
    }

    private static void swap(int[] arr, int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }
}