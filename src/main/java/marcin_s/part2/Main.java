package marcin_s.part2;

import marcin_s.part2.HeapSortAlgorithm;

/**
 * Created by przemas on 03/03/2017.
 */
public class Main {
    public static void main(String[] args) {

        int[] array = {4,6,2,1,8,9,3,5};

        new HeapSortAlgorithm().sort(array, false);

        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i]+" ");
        }

    }
}
