package marcin_s.part2;

import java.util.Comparator;

// dzięki temu będziemy mogli dac dowolny typ w tablicy i nam posortuje
public interface GenericSortAlgorithm {
    default <T extends Comparable<T>> void sort(T[] array) {
        sort(array, T::compareTo);
    }
    <T> void sort (T[] array, Comparator<T> comparator);
}
