package marcin_s.part3;

public class BinarySearchAlgorithm implements SearchAlgorithm {

    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        int first = 0;
        int last = array.length - 1;
        int middle = (first + last) / 2;

        while (first <= last) {
            if (array[middle] < value)
                first = middle + 1;
            else if (array[middle] > value)
                last = middle - 1;
            else
                return middle;

            middle = (first + last) / 2;
        }

        return -1;
    }
}