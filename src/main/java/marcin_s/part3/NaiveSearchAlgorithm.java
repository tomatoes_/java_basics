package marcin_s.part3;


public class NaiveSearchAlgorithm implements SearchAlgorithm {

    public int search(int[] array, int value) {

        if (array == null) {
            throw new IllegalArgumentException("Array should not be null");
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] == value)
                return i;
            }

        return -1;
    }
}
