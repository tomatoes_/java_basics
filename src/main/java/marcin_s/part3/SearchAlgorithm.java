package marcin_s.part3;

public interface SearchAlgorithm {

    int search (int[] array, int value);
    }

