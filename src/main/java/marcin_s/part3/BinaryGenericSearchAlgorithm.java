package marcin_s.part3;

import java.util.function.Function;
import java.util.function.Predicate;

public class BinaryGenericSearchAlgorithm {
    public <T> int search(T[] array, Function<T, Integer> function) {
        final int length = array.length;
        int left = 0; int right = length - 1;
        int middle;

        while (left <= right) {
            middle = left + right/2;
            if (function.apply(array[middle])>0){
                left = middle + 1;
            }
            else if (function.apply(array[middle])<0) {
                right = middle - 1;
            }
            else{
                    return middle;
            }
        }

        return -1;
    }
}

