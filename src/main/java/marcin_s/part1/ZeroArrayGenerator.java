package marcin_s.part1;

import marcin_s.utils.ArrayGenerator;

public class ZeroArrayGenerator implements ArrayGenerator {
    @Override
    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        return new int[lenght];
    }

    @Override
    public int[] generate(int lenght) {
        return new int[lenght];
    }
}
