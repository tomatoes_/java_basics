package marcin_s.part1;


import lombok.Builder;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Builder
public class Address {
        private String streetName;
        private String flatNumber;
        private String postalCode;
        private String country;

    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(6))
                .flatNumber(randomNumeric(3))
                .postalCode(randomNumeric(5))
                .country(randomAlphabetic(10))
                .build();
    }
    }

