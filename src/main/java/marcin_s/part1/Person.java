package marcin_s.part1;

import lombok.Builder;
import lombok.Data;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
// to @Data tworzy nam automatycznie w czasie kompilacji
// konstruktory ze wszystkimi argumentami oraz bezargumentowe
// oraz wszystkie gettery i settery oraz jeszcze trochę tajemniczych rzeczy, np. hashCode i equals (czyli nie musimy ich pisać)
// a jeśli chcemy jednak mieć to wszystko w kodzie, to dajemy prawy - Refactor - Delombok


public class Person {
    private String firstName;
    private String lastName;
    private String pesel;
    private char gender;
    private Address address;

    public static Person getRandomperson(){

        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(6))
                .pesel(randomNumeric(10))
                .gender(new Random().nextBoolean() ? 'M' : 'F')
                .address(Address.getRandomAddress())
                .build();
    }

}
