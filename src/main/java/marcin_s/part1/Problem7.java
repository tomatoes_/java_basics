package marcin_s.part1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

// Odwracanie 2 liczb, sumowanie tych odwróconych i odwrócenie wyniku
public class Problem7 {

    static Scanner input = new Scanner(System.in);

    public static void main4(String[] args) {
        // to samo co nizej ale z uzyciem streamów

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0){
            --problems;
            Stream.of(scanner.nextLine().split(" "))
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .map(BigInteger::new)
                    .reduce(BigInteger::add)
                    .map(String::valueOf)
                    .map(StringBuilder::new)
                    .map(StringBuilder::reverse)
                    .map(StringBuilder::toString)
                    .ifPresent(System.out::println);
            ;


            ; //zamiany na bigIntegera nie zrobiłem w przykladzie ponizej, tam jest long


        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0){
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" "); // dzielenie stringa tam gdzie spacje
            StringBuilder numberA = new StringBuilder (split[0]); // inicjowanie stringa z klasy StringBuilder(większe możliwości0
            StringBuilder numberB = new StringBuilder (split[1]);
            String numberAReversed = numberA.reverse().toString(); // Odwrócenie stringa
            String numberBReversed = numberB.reverse().toString();
            long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed); //konwersja stringów na longi i ich zsumowanie
            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString(); // odwróvcenie sumy

           // System.out.println(sumReversed);

        }

    }

// Poniżej starsze rozwiązania tego problemu
//    public static void main3(String[] args) {

//        System.out.println("Podaj pierwszą liczbe: ");
//        Scanner input = new Scanner(System.in);
//        int x = input.nextInt();
//
//        System.out.println("Podaj drugą liczbe: ");
//        int y = input.nextInt();
//
//        System.out.println("NWD = "+nwd(x,y));
//
//        System.out.println("NWW = " + ((x*y)/nwd(x,y)));
//        char[] test = {'1', '2', '3'};
//        reverseInPlace(test);
//        System.out.println(Arrays.toString(test));
//        reverse(test);
//        System.out.println(Arrays.toString((test)));
//
////        int lengthA = 5;
//        int lengthB = 8;
//        int length;
//        if (lengthA > lengthB) {
//            length = lengthA + 1;
//        } else {
//            length = lengthB + 1;
//        }

//        char[] arrayA = {'1', '2', '3'};
//        char[] arrayB = {'4', '5', '6'};

//        Scanner scanner = new Scanner(System.in);
//        int problems = Integer.parseInt(scanner.nextLine());
//        while (problems >= 0) {
//            --problems;
//            String line = scanner.nextLine();
//            String[] split = line.split(" ");
//            char[] arrayA = split[0].toCharArray();
//            char[] arrayB = split[1].toCharArray();
//
//            int lengthA = arrayA.length;
//            int lengthB = arrayB.length;
//
//            int newLength = lengthA > lengthB ? lengthA : lengthB;
//
//            int smallerLength;
//            int longerLength;
//            char[] longerArray;
//
//            if (lengthA == lengthB) {
//                if ((arrayA[lengthA - 1] + arrayB[lengthA - 1]) >= '9') {
//                    newLength = lengthA + 1;
//                } else {
//                    newLength = lengthA;
//                }
//                smallerLength = lengthA;
//                longerLength = lengthB;
//                longerArray = arrayA;
//            } else if (lengthA > lengthB) {
//                newLength = lengthA;
//                smallerLength = lengthB;
//                longerLength = lengthA;
//                longerArray = arrayA;
//            } else {
//                newLength = lengthB;
//                smallerLength = lengthA;
//                longerLength = lengthB;
//                longerArray = arrayB;
//            }
//            longerLength = longerArray.length;
//
//            char[] result = new char[newLength];
//
//            int overflow = 0;
//
//            for (int i = 0; i < smallerLength; i++) {
//                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
//                if (result[i] > '9') {
//                    result[i] -= 10;
//                    overflow = 1;
//                } else {
//                    overflow = 0;
//                }
//            }
//
//            for (int i = smallerLength; i < longerLength; i++) {
//                result[i] = (char) (longerArray[i] + overflow);
//                if (result[i] > '9') {
//                    result[i] -= 10;
//                    overflow = 1;
//                } else {
//                    overflow = 0;
//                }
//            }
//
//            for (int i = 0; i < newLength; i++) {
//                System.out.print(result[i]);
//            }
//
//            if (result.length > longerLength && overflow != 0) {
//                System.out.print('1');
//            }
//        }
//    }
//    public static int nwd(int x, int y) {
//
//        while (x != y) {
//            if (x > y)
//                x -= y;
//            else
//                y -= x;
//        }
//        return x;
//    }

//    private static void reverseInPlace(char[] toReverse) {
//        int length = toReverse.length;
//        char tmp;
//        for (int i = 0; i < toReverse.length / 2; i++) {
//            tmp = toReverse[i];
//            toReverse[i] = toReverse[length - i - 1];
//            toReverse[length - i - 1] = tmp;
//        }
//    }
//
//    private static char[] reverse(char[] toReverse) {
//        int length = toReverse.length;
//        char[] result = new char[length];
//        for (int i = 0; i < length; ++i) {
//            result[i] = toReverse[length - i - 1];
//        }
//        return result;
//    }



//    public static void main2(String[] args) {
//
//        char[] arrayA = {'1','2','3'};
//        char[] arrayB = {'4','5','6'};
//
//        int lengthA = arrayA.length;
//        int lengthB = arrayB.length;
//
//        int newlength;
//        int smallerLength;
//
//
//        if (lengthA == lengthB) {
//            if (arrayA[lengthA - 1] + arrayB[lengthA - 1] >= '9') {
//                newlength = lengthA + 1;
//            } else {
//                newlength = lengthA;
//            }
//            smallerLength = lengthA;
//        }
//        else if (lengthA> lengthB) {
//            newlength = lengthA;
//            smallerLength = lengthB;
//        }else {
//        newlength=lengthB;
//        smallerLength = lengthA;
//        }
//
//        char[] result = new char[newlength];
//
//            boolean overflow = false;
//            for (int i = 0; i < smallerLength; i++) {
//                result[i] = (char)(arrayA[i] + arrayB[i] -48 +overflow);
//                if (result[i]>'9'){
//                    result[i]-=10;
//                    overflow = 1;
//            }
//            else{
//                    overflow = 0;
//                }
//        }
//        int lengthA = 5;
//        int lengthB = 8;
//        int length;
//        if (lengthA > lengthB){
//            length = lengthA+1;}
//        else {
//            length = lengthB +1;
//            }


//        reverseNumbers();
 //       reverseNumbersZTablicami();

//        reverseNumbersZTablicami();
//
//        char[] test = {'1','2','3'};
//        reverseInPlace(test);
//        System.out.println(Arrays.toString(test));
//
//        reverse(test);
//        System.out.println(Arrays.toString(test));
//    }

    // Moja wersja bez używania tablic

    //    public static void reverseNumbers() {
//        System.out.println("Podaj pierwszą liczbę:");
//        Scanner input = new Scanner(System.in);
//        String string1 = input.nextLine();
//        System.out.println("Podaj drugą liczbę:");
//        Scanner input2 = new Scanner(System.in);
//        String string2 = input2.nextLine();
//
//        String reversedString1 = new StringBuffer(string1).reverse().toString(); //odwrócenie stringa
//        int reversedInt1 = Integer.parseInt(reversedString1); //konwersja stringa na inta (usunie ewentualne zera z przodu)
//        System.out.println("Odwrócona 1 liczba wynosi:" + reversedInt1);
//
//        String reversedString2 = new StringBuffer(string2).reverse().toString(); //odwrócenie stringa
//        int reversedInt2 = Integer.parseInt(reversedString2); //konwersja stringa na inta (usunie ewentualne zera z przodu)
//        System.out.println("Odwrócona 2 liczba wynosi:" + reversedInt2);
//
//        int sum = reversedInt1 + reversedInt2;
//        String sumString = Integer.toString(sum); //konwersja sumy z inta do stringa
//        System.out.println("Suma odwróconych liczb wynosi:" + sumString);
//
//        String reversedSum = new StringBuffer(sumString).reverse().toString(); //odwrócenie stringa
//        int reversedSumInt = Integer.parseInt(reversedSum); //konwersja stringa na inta (usunie ewentualne zera z przodu)
//        System.out.println("Odwrócona suma (bez zer) to:" + reversedSumInt);
//
//    }

    //A to jakies tam moje jeszcze inne rozwiązanie odwracania tablicy
//    public static void reverseNumbersZTablicami() {
//        System.out.println("Podaj pierwszą liczbę:");
//        Scanner input = new Scanner(System.in);
//        String string1 = input.nextLine();
//        System.out.println("Podaj drugą liczbę:");
//        Scanner input2 = new Scanner(System.in);
//        String string2 = input2.nextLine();

//        char[] table1 = {1, 2, 3, 4};
//        int length1 = table1.length;
//        char[] table2 = new char[length1];
//
//        for (int i = length1, j = 0; i > 0; i--, j++)
//
//        {
//        table2[i - 1] = table1[j];
//        }
//            for(int x : table2)
//                System.out.println(x);
//        }
//
//
//    private static void reverseInPlace(char[] toReverse){
//        char tmp;
//        int length = toReverse.length;
//        for (int i = 0; i < toReverse.length/2; ++i) {
//            tmp = toReverse[i];
//            toReverse[i] = toReverse[length - i - 1];
//                    toReverse[length - i -1] = tmp;
//
//
//        }
//
//    }
//
//    private static char[] reverse (char[] toReverse){
//        int length = toReverse.length;
//        char[] result = new char[length];
//        for (int i = 0; i < length ; ++i) {
//
//            result[i]=toReverse[length-1-i];
//        }
//        return result;
//    }

}