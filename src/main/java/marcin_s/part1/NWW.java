package marcin_s.part1;

import java.util.Scanner;

// wykorzystałem kod ze strony: http://www.algorytm.org/algorytmy-arytmetyczne/najmniejsza-wspolna-wielokrotnosc/nww-1-j.html
public class NWW {

    public static void main(String[] args) {
        int x,y;

        Scanner sc = new Scanner(System.in);
        //Pobieramy pierwsza liczbe
        System.out.print("Podaj pierwsza liczbe: ");
        x = sc.nextInt();
        //Pobieramy druga liczbe
        System.out.print("Podaj druga liczbe: ");
        y = sc.nextInt();

        //Obliczamy i wyswietlamy NWW
        System.out.println("NWW liczb " + x + " i " + y + " wynosi: " + ((x*y)/nwd(x,y)));

        System.out.println("NWD tych liczb wynosi: "+nwd(x,  y));
    }

    public static int nwd(int x, int y) {
        while (x != y) {
            if (x > y)
                x -= y;
            else
                y -= x;
        }
        return x;
    }
}

