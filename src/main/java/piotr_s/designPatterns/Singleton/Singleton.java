package piotr_s.designPatterns.Singleton;

/**
 * Created by RENT on 2017-03-18.
 */
public class Singleton {
    private static class SingletonHelper {
        private static final Singleton instance = new Singleton();
    }


    public static Singleton getInstance() {

        return SingletonHelper.instance;
    }
}

class SingletonEager {

    private static SingletonEager instance = new SingletonEager();

    public static SingletonEager getInstance() {
        return instance;
    }
}
