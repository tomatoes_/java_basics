package piotr_s.designPatterns.Decorator;

/**
 * Created by RENT on 2017-03-18.
 */
public class Audi extends Auto {

    public Audi() {
        this.wersja = "Audi basic";
    }

    @Override
    public double getCena() {
        return getCena() + 100000;
    }
}
