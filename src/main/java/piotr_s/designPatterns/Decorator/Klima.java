package piotr_s.designPatterns.Decorator;

/**
 * Created by RENT on 2017-03-18.
 */
public class Klima extends Decorator{
    private Auto auto;
    public Klima(Auto auto){
        this.auto=auto;
    }

    @Override
    public String getWersja() {
        return auto.getWersja() + " i Klima";
    }

    @Override
    public double getCena() {
        return getCena() + 5000;
    }
}
