package piotr_s.designPatterns.Decorator;

/**
 * Created by RENT on 2017-03-18.
 */
public class Main {
    public static void main(String[] args) {
        Auto auto1 = new Audi();
        Auto auto2 = new Maseratti();

        System.out.println(auto1.getWersja() + " " + auto1.getCena());
        System.out.println(auto2.getWersja() + " " + auto2.getCena());

        Auto auto3 = new Klima(auto1);
        auto1 = new Klima(new Alufelgi(auto1));

        auto2 = new Klima(auto2);
        auto2= new Alufelgi(auto2);

        System.out.println(auto1.getWersja()+ " " + auto1.getCena());
        System.out.println(auto3.getWersja()+ " " + auto3.getCena());

    }
}
