package piotr_s.designPatterns.Decorator;

/**
 * Created by RENT on 2017-03-18.
 */
public class Alufelgi extends Decorator {

    private Auto auto;
    public Alufelgi(Auto auto){
        this.auto=auto;
    }


    @Override
    public String getWersja() {
        return auto.getWersja() + " i Alufelgi";
    }

    @Override
    public double getCena() {
        return 10000;
    }
}
