package piotr_s.designPatterns.Decorator;


public abstract class Auto {
    String wersja = "Auto podstawowe";

    public String getWersja(){
        return wersja;
    }

    public abstract double getCena();


    }

