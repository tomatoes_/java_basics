package piotr_s.designPatterns.Builder;

/**
 * Created by RENT on 2017-03-18.
 */
public class TruckBuilder implements CarBuilder {

    private Car car;

    public TruckBuilder(){
        this.car= new Car();
    }

    public Car getCar(){
        return this.car;
    }

    @Override
    public void buildWheels() {
        this.car.setWheels("Buduję koła ciężarówki");
    }

    @Override
    public void buildEngine() {
        this.car.setEngine("Buduję silnik ciężarówki");
    }

    @Override
    public void buildLandingGear() {
        this.car.setLandingGear("Buduję podwozie ciężarówki");
    }

    @Override
    public void builderBody() {
        this.car.setBody("Buduję nadwozie ciężarówki");
    }

    @Override
    public void buildRadio() {
        this.car.setRadio("Buduję radio ciężarówki");
    }

    @Override
    public void buildDoors() {
        this.car.setDoors("Buduję drzwi ciężarówki");
    }

}
