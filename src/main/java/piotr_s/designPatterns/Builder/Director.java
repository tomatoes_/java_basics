package piotr_s.designPatterns.Builder;

/**
 * Created by RENT on 2017-03-18.
 */
public class Director {

    private CarBuilder CarBuilder;

    public void setCarBuilder(CarBuilder builder){
        this.CarBuilder = builder;
    }


    public void construct(){
        this.CarBuilder.buildWheels();
        this.CarBuilder.buildDoors();
        this.CarBuilder.builderBody();
        this.CarBuilder.buildLandingGear();
        this.CarBuilder.buildRadio();
        this.CarBuilder.buildEngine();


    }

    public Car getCar(){
        return this.CarBuilder.getCar();
    }


}
