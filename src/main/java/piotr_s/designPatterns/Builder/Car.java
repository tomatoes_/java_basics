package piotr_s.designPatterns.Builder;

/**
 * Created by RENT on 2017-03-18.
 */
public class Car {

    private String wheels;
    private String engine;
    private String landingGear;
    private String body;
    private String radio;
    private String doors;

    public Car(String wheels, String engine, String landingGear, String body, String radio, String doors) {
        this.wheels = wheels;
        this.engine = engine;
        this.landingGear = landingGear;
        this.body = body;
        this.radio = radio;
        this.doors = doors;
    }
    public Car() {}

    public String getWheels() {
        return wheels;
    }

    public String getEngine() {
        return engine;
    }

    public String getLandingGear() {
        return landingGear;
    }

    public String getBody() {
        return body;
    }

    public String getRadio() {
        return radio;
    }

    public String getDoors() {
        return doors;
    }

    public void setWheels(String wheels) {
        this.wheels = wheels;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setLandingGear(String landingGear) {
        this.landingGear = landingGear;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String showCar(){
        return this.getBody()+ " || " +
                this.getDoors() + " || " +
                this.getWheels() + " || " +
                this.getRadio() + " || " +
                this.getEngine() + " || " +
                this.getLandingGear();


    }

}
