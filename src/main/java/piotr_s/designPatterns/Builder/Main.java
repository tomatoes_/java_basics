package piotr_s.designPatterns.Builder;

/**
 * Created by RENT on 2017-03-18.
 */
public class Main {
    public static void main(String[] args) {

        CarBuilder builder1=new BusBuilder();
        CarBuilder builder2=new PersonalCarBuilder();
        CarBuilder builder3=new TruckBuilder();

        Director director = new Director();

        director.setCarBuilder(builder1);
        director.construct();
        Car car = director.getCar();
        System.out.println(car.showCar());

        director.setCarBuilder(builder2);
        director.construct();
        car = director.getCar();
        System.out.println(car.showCar());

        director.setCarBuilder(builder3);
        director.construct();
        car = director.getCar();
        System.out.println(car.showCar());

    }
}
