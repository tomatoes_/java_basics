package piotr_s.designPatterns.Builder;

    public class BusBuilder implements CarBuilder {

        private Car car;

        public BusBuilder(){
            this.car= new Car();
        }

        public Car getCar(){
            return this.car;
        }

        @Override
        public void buildWheels() {
            this.car.setWheels("Buduję koła busa");
        }

        @Override
        public void buildEngine() {
            this.car.setEngine("Buduję silnik busa");
        }

        @Override
        public void buildLandingGear() {
            this.car.setLandingGear("Buduję podwozie busa");
        }

        @Override
        public void builderBody() {
            this.car.setBody("Buduję nadwozie busa");
        }

        @Override
        public void buildRadio() {
            this.car.setRadio("Buduję radio busa");
        }

        @Override
        public void buildDoors() {
            this.car.setDoors("Buduję drzwi busa");
        }
    }


