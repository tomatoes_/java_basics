package piotr_s.designPatterns.Builder;

/**
 * Created by RENT on 2017-03-18.
 */
public interface CarBuilder {

    void buildWheels();
    void buildEngine();
    void buildLandingGear();
    void builderBody();
    void buildRadio();
    void buildDoors();

    Car getCar();

}
