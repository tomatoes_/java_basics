package piotr_s.designPatterns.TemplateMethod;

/**
 * Created by RENT on 2017-03-20.
 */
public class PepperoniPizzaRecipe extends PizzaRecipe {


    @Override
    public void doCake() {
        System.out.println("do Margherita cake");
    }

    @Override
    public void addSauce() {
        System.out.println("add spicy sauce");
    }

    @Override
    public void addAdditions() {
        System.out.println("add mozzarella and pepperoni and chilli");
    }

    @Override
    public void bake() {
        System.out.println("bake 30 min i 220 degrees");
    }


}

