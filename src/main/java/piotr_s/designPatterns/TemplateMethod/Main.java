package piotr_s.designPatterns.TemplateMethod;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-03-20.
 */
public class Main {
    public static void main(String[] args) {

        PizzaRecipe pizza1 = new MargheritaPizzaRecipe();
        PizzaRecipe pizza2 = new HawaiianPizzaRecipe();
        PizzaRecipe pizza3 = new PepperoniPizzaRecipe();
        PizzaRecipe pizza4 = new PepperoniWithSauceRecipe();

        LinkedList<PizzaRecipe> pizzaRecipeList = new LinkedList<PizzaRecipe>();

        pizzaRecipeList.add(pizza1);
        pizzaRecipeList.add(pizza2);
        pizzaRecipeList.add(pizza3);
        pizzaRecipeList.add(pizza4);

        for (PizzaRecipe pizza : pizzaRecipeList) {
            System.out.println("--------robimy pizze----");
            pizza.doPizza();
        }


    }
}
