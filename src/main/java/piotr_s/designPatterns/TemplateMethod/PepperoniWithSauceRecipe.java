package piotr_s.designPatterns.TemplateMethod;

/**
 * Created by RENT on 2017-03-20.
 */
public class PepperoniWithSauceRecipe extends PepperoniPizzaRecipe {
    public void addSauce() {

    }

    public void doPizza() {
        this.doCake();
        this.addSauce();
        this.addAdditions();
        this.bake();
    }
}