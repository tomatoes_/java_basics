package piotr_s.designPatterns.Visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public interface Visitor {

    void visit(PDFFile pdfFile);


    void visit(CsvFile csvFile);


}
