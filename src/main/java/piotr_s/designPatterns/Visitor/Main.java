package piotr_s.designPatterns.Visitor;

import java.io.*;
import java.util.LinkedList;

/**
 * Created by RENT on 2017-03-20.
 */
public class Main {
    public static void main(String[] args) {
        File f1 = new PDFFile();
        File f2 = new CsvFile();

        Presenter presenter = new Presenter();
        LinkedList<File> fileLinkedList = new LinkedList<File>();

        fileLinkedList.add(f1);
        fileLinkedList.add(f2);

        for (File f : fileLinkedList) {
            if (f instanceof PDFFile) {
                presenter.visit((PDFFile) f);
            }else if (f instanceof CsvFile){
                presenter.visit((CsvFile) f);
            }
        }

        presenter.visit((PDFFile) f1);
        presenter.visit((CsvFile) f2);
    }
}
