package piotr_s.designPatterns.Visitor;


public interface Visitable {
    void accept (Visitor visitor);
}
