package piotr_s.designPatterns.Visitor;

/**
 * Created by RENT on 2017-03-20.
 */
public class Presenter implements Visitor{
    @Override
    public void visit(PDFFile pdfFile) {
        pdfFile.getType();
    }

    @Override
    public void visit(CsvFile csvFile) {
        csvFile.getType();
    }
}
