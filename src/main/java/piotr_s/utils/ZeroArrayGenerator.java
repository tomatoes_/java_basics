package piotr_s.utils;

import piotr_s.utils.ArrayGenerator;

/**
 * Created by RENT on 2017-02-25.
 */
public class ZeroArrayGenerator implements ArrayGenerator {

    @Override
    public int[] generate(int length, int rangeFrom, int rangeTo) {
        return new int[length];
    }

    @Override
    public int[] generate(int length) {
        return new int[length];

    }


}
