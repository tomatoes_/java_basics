package piotr_s.part1;

/**
 * Created by RENT on 2017-02-25.
 */
public class PrimeGenerator {

    public static void main(String[] args) {


        int max = 150;


        System.out.println("Generate Prime numbers between 1 and " + max);


        // loop through the numbers one by one

        for (int i = 1; i < max; i++) {


            boolean isPrimeNumber = true;


            // check to see if the number is prime

            for (int j = 2; j < i; j++) {

                if (i % j == 0) {

                    isPrimeNumber = false;

                    break; // exit the inner for loop

                }

            }


            // print the number if prime

            if (isPrimeNumber) {

                System.out.print(i + " ");

            }

        }


    }


}























