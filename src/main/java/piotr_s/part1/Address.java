package piotr_s.part1;

import lombok.Builder;
import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Builder
public class Address {
    private String streetName;
    private String flatNumber;
    private String postCode;
    private String country;

    public static Address getRandomAddress() {
        return Address.builder()
                .streetName(randomAlphabetic(10))
                .flatNumber(randomAlphabetic(5))
                .postCode(randomAlphabetic(8))
                .country(randomAlphabetic(9))
                .build();
    }
}
