package piotr_s.part1;

import groovy.xml.StreamingMarkupBuilder;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

public class Problem7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;



            String line = scanner.nextLine();
            String[] split = line.split(" ");
            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);
            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();

            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);
            BigInteger sum = bigNumberA.add(bigNumberB);

//            long sum = Long.parseLong(numberAReversed) + Long.parseLong(numberBReversed);

            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();
            System.out.println(sumReversed);
        }


    }


    public static void main2(String[] args) {
//char[] test={'1', '2', '3'};
//reverseInPlace(test);
//        System.out.println(Arrays.toString(test));
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");
            char[] arrayA = split[0].toCharArray();
            char[] arrayB = split[1].toCharArray();

            int lengthA = arrayA.length;
            int lengthB = arrayB.length;

            int newLength;
            int smallerLength;
            int longerLength;
            char[] longerArray;
            if (lengthA == lengthB) {
                if (arrayA[lengthA - 1] + arrayB[lengthA - 1] >= '9') {
                    newLength = lengthA + 1;
                } else {
                    newLength = lengthA;
                }
                smallerLength = lengthA;
                longerArray = arrayA;

            } else if (lengthA > lengthB) {
                newLength = lengthA;
                smallerLength = lengthB;
                longerLength = lengthA;
                longerArray = arrayA;
            } else {
                newLength = lengthB;
                smallerLength = lengthA;
                longerLength = lengthB;
                longerArray = arrayB;
            }
            longerLength = longerArray.length;

            char[] result = new char[newLength];

            int overflow = 0;
            for (int i = 0; i < smallerLength; ++i) {
                result[i] = (char) (arrayA[i] + arrayB[i] - 48 + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }
            for (int i = smallerLength; i < longerLength; ++i) {
                result[i] = (char) (longerArray[i] + overflow);
                if (result[i] > '9') {
                    result[i] -= 10;
                    overflow = 1;
                } else {
                    overflow = 0;
                }
            }
            for (int i = 0; i < newLength; ++i) {
                System.out.print(result[i]);

            }

            if (result.length > longerLength && overflow != 0) {
                System.out.print('1');
            }
        }
    }

    private static void reverseInPlace(char[] toReverse) {
        char tmp;
        int lenght = toReverse.length;
        for (int i = 0; i < toReverse.length / 2; ++i) {
            tmp = toReverse[i];
            toReverse[i] = toReverse[lenght - i - 1];
            toReverse[lenght - i - 1] = tmp;

        }
    }

    private char[] reverse(char[] toReverse) {
        int length = toReverse.length;
        char[] result = new char[toReverse.length];
        for (int i = 0; i < toReverse.length; i++) {
            result[i] = toReverse[toReverse.length - 1 - i];
        }
        return result;
    }

}
