package konrad_k.utils;


public interface ArrayGenerator {
    int [] generate(int lenght, int rangeFrom, int rangeTo);
    int[] generate(int lenght);
}
