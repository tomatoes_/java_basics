package maciej_m.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Data
@Builder
public class Address {

   private String streetName;
   private String flatName;
   private String postalCode;
   private String country;

   public static Address getRandomAddress(){
      return Address.builder()
              .streetName(randomAlphabetic(10))
              .flatName(randomAlphabetic(3))
              .postalCode(randomNumeric(6))
              .country(randomAlphabetic(8))
              .build();
   }

}
