package maciej_m.part1;

/**
 * Created by RENT on 2017-02-22.
 */
public class problem2 {
    public static void main(String[] args) {
        drawTriangle(5);
    }

    public static void drawTriangle(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print('*');
            }
            System.out.println();
        }

        drawStars(5);

    }

    public static void drawStars(int line) {
        for (int i = 0; i < line; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(" ");
            }
            System.out.println('*');
        }
    }


    //    public static void drawSquare(int number) {
//        for (int i = 0; i <= number; i++) {
//            for (int j = 0; j <= number; j++) {
//                System.out.println("*");
//
//                if (i == 1 || number == 1 || j == 1 || number == 1) {
//
//                    System.out.print("*");
//
//                } else {
//                    System.out.println(" ");
//
//                }
//                drawSquare(4);
//            }
//        }
//    }
    public static void drawSquare(int number) {
        for (int i = 0; i < number; i++) {
            System.out.print('*');
            char toPrint;
            if (i == 0 || i == number - 1) {
                toPrint = '*';
            } else {
                toPrint = ' ';
            }
            for (int j = 0; j <number ; j++) {
                System.out.println('*');
            }
        }
    }


}