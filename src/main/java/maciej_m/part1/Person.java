package maciej_m.part1;

//import com.sun.jndi.cosnaming.IiopUrl;


import lombok.Builder;
import lombok.Data;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;


@Data
@Builder

public class Person {

    private String firstName;
    private String lastName;
    private String pesel;
    private char gender;
    private Address address;

    public static Person getRandomPerson(){
        return Person.builder()
                .firstName(randomAlphabetic(6))
                .lastName(randomAlphabetic(6))
                .pesel(randomAlphabetic(11))
                .gender(new Random().nextBoolean() ? 'M' : 'F')  // generuje
                .address(Address.getRandomAddress())
                .build();
    }



}

