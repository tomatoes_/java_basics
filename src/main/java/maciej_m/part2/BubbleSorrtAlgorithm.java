package maciej_m.part2;

/**
 * Created by RENT on 2017-03-01.
 */
public class BubbleSorrtAlgorithm implements SortAlgorithm {

    @Override
    public void sort(int[] array, boolean ascending){

        int lastSortedElement = array.length;
        int tmp;
        if (ascending=true) {
            while (lastSortedElement != 0) {
                for (int i = 1; i < lastSortedElement; i++) {
                    if (array[i - 1] > array[i]) {
                    tmp = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = tmp;
                    }
            --lastSortedElement;
                }
            }
        }else {
            while (lastSortedElement != 0) {
                for (int i = 1; i < lastSortedElement; i++) {
                    if (array[i - 1] < array[i]) {
                        tmp = array[i - 1];
                        array[i - 1] = array[i];
                        array[i] = tmp;
                    }
                    --lastSortedElement;
                }
            }
        }

    }

}

