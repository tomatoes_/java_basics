package maciej_m.part2;

/**
 * Created by RENT on 2017-03-01.
 */
public interface SortAlgorithm {

     default void sort(int[] array) {
         sort(array, true);}

     void sort(int[] array, boolean ascending);

     }
