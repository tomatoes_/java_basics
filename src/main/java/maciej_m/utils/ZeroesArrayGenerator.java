package maciej_m.utils;

import maciej_m.utils.ArrayGenerator;

/**
 * Created by RENT on 2017-02-25.
 */
public class ZeroesArrayGenerator implements ArrayGenerator {

    @Override
    public int[] generate(int length, int rangeFrom, int rangeTo) {
        return new int[0];
    }

    @Override
    public int[] generate(int length) {
        return new int[0];
    }
}