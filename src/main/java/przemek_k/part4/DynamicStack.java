package przemek_k.part4;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.*;

public class DynamicStack<T> implements Stack<T> {
    private StackElement top;
    private int size = 0;

    @Override
    public T pop() {
        --size;
        StackElement oldTop = top;
        top=top.getPrevious();
        return oldTop.getValue();

    }

    @Override
    public T peek() {
        return Optional.ofNullable(top)
                .map(StackElement::getValue)
                .orElse(null);

//        if (nonNull(top)) return top.getValue();
//        else return null;
    }

    @Override
    public void push(T t) {
        top = new StackElement(t, top);
        ++size;


    }

    @Override
    public int size() {
        return size;
    }

    @Data
    @AllArgsConstructor

    private class StackElement {

        private T value;
        private StackElement previous;
    }

}
//do domu implementacja tablicowa