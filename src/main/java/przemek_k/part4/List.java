package przemek_k.part4;

import java.util.Iterator;

public interface List<T> extends Iterable<T> {

    T getFirst();

    T getLast();

    T get(int index);

    void add(T element);

    void remove(int index);

    int size();

    default boolean isEmpty() {
        return size() == 0;
    }

}
