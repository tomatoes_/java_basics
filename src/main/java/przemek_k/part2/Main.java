package przemek_k.part2;

import przemek_k.utils.RandomArrayGenarator;

/**
 * Created by przemas on 03/03/2017.
 */
public class Main {
    public static void main(String[] args) {

        int[] array = {4,6,2,1,8,9,3,5};

        new HeapSortAlgorithm().sort(array, false);

        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i]+" ");
        }

    }
}
