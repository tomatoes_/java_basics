package przemek_k.part2;

public class SortBubble1 implements SortAlgorithm {

    @Override
    public void sort(int[] array, boolean ascending) {
        if (array == null) {
            throw new IllegalArgumentException("Array should not be null.");
        }
        int temp;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (ascending && array[j - 1] > array[j] || !ascending && array[j - 1] < array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }

    }
}
