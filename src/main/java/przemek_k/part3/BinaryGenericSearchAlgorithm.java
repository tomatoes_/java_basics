package przemek_k.part3;

import java.util.function.Function;

public class BinaryGenericSearchAlgorithm {

    public <T> int search(T[] array, Function<T, Integer> function) {
        if (array == null) {
            throw new IllegalArgumentException("array can't be null");
        }

        final int lenght = array.length;
        int left = 0, right = lenght - 1, middle;
        while (left < right) {
            middle = left + (right - left) / 2;
            if (function.apply(array[middle]) >0) {
                left = middle + 1;
            } else if (function.apply(array[middle]) < 0) {
                right = middle - 1;
            } else {
                return middle;
            }
        }
        return -1;

    }
}
