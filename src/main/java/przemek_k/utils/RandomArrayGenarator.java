package przemek_k.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

public class RandomArrayGenarator implements ArrayGenerator {

    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        if(rangeFrom>=rangeTo){
            throw new IllegalArgumentException("Range is invalid.");
        }
        int[] array = new int[lenght];
        Random random = new Random();
        for (int i = 0; i < lenght; i++) {
            array[i] = random.nextInt(rangeTo - rangeFrom) + rangeFrom;
        }
        return array;
    }


    public int[] generate(int lenght) {
        return generate(lenght, 0, MAX_VALUE);
    }
}
