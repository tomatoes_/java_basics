package przemek_k.part1;

public class Problem2 {

    public static void main(String[] args) {


        //drawTriangle(6);
        //drawSkewLine(4);
        drawSquare(6);
        //drawCross(5);
        //drawBackslash(5);


    }

    public static void drawTriangle(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.println("*");
        }
    }

    public static void drawSkewLine(int height) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(" ");
            }
            System.out.println("%");
        }

    }

    public static void drawSquare(int height) {
        for (int row = 0; row < height; ++row) {
            System.out.print("*");
            char toPrint;
            if (row == height - 1 || row == 0) {
                toPrint='*';
            } else {
                toPrint=' ';
            }
            for (int col = 0; col < height; col++) {
                System.out.print(toPrint);
            }
            System.out.println("*");

        }

    }



}

