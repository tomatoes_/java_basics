package przemek_k.part1;

import przemek_k.utils.ArrayGenerator;

public class ZeroArrayGenerator implements ArrayGenerator {
    @Override
    public int[] generate(int lenght, int rangeFrom, int rangeTo) {
        return new int[lenght];
    }

    @Override
    public int[] generate(int lenght) {
        return new int[lenght];
    }
}
