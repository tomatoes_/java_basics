package przemek_k.part1;

import java.math.BigInteger;
import java.util.Scanner;

public class Problem7Short {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int problems = Integer.parseInt(scanner.nextLine());
        while (problems > 0) {
            --problems;
            String line = scanner.nextLine();
            String[] split = line.split(" ");

            StringBuilder numberA = new StringBuilder(split[0]);
            StringBuilder numberB = new StringBuilder(split[1]);

            String numberAReversed = numberA.reverse().toString();
            String numberBReversed = numberB.reverse().toString();

            BigInteger bigNumberA = new BigInteger(numberAReversed);
            BigInteger bigNumberB = new BigInteger(numberBReversed);

            BigInteger sum = bigNumberA.add(bigNumberB);

//            long sum = parseLong(numberAReversed) + parseLong(numberBReversed);

            String sumReversed = new StringBuilder(String.valueOf(sum)).reverse().toString();

            System.out.println(sumReversed.replaceFirst("^0+(?!$)", ""));

        }


    }
}
