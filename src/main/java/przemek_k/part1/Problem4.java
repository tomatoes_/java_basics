package przemek_k.part1;

import java.lang.reflect.Array;

public class Problem4 {

    public static void main(String[] args) {
        //tworzenie tablicy
        int[] anArray = new int[100];
        //dodanie do tablicy
        for (int i = 0; i < anArray.length; i++) {
            anArray[i] = i;
        }
        //wyswietlenie tablicy
        for (int i = 0; i < anArray.length; i++) {
            System.out.print(anArray[i] + " ");
        }
        //nowa linia
        System.out.println();
        //do codrugiego indeksu dodanie poprzedniego
        for (int i = 1; i < anArray.length; i += 2) {
            anArray[i] += anArray[i - 1];
        }
        //wyswietlenie
        for (int i = 0; i < anArray.length; i++) {
            System.out.print(anArray[i] + " ");
        }
        //nowa linia
        System.out.println();
        //wyszukanie i podzielenie przez 2 liczb parzystych
        for (int i = 0; i < anArray.length; i++) {

            if (anArray[i] % 2 == 0) {
                anArray[i] /= 2;
            }

        }

        for (int i = 0; i < anArray.length; i++) {
            System.out.print(anArray[i] + " ");
        }
        //nowa linia
        System.out.println();
        //sumowanie tablicy
        int sum = 0;
        for (int i = 0; i < anArray.length; i++) {
            sum += anArray[i];
        }
        System.out.println(sum);

    }


}
