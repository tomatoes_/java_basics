package przemek_k.part1;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import static org.apache.commons.lang3.RandomStringUtils.*;

@Data
@Builder
public class Address {

    private String streetName;
    private String flatNumber;
    private String postalCode;
    private String country;


    public static Address getRandomAddress(){
        return Address.builder()
                .streetName(randomAlphabetic(5))
                .flatNumber(randomNumeric(3))
                .postalCode(randomNumeric(5))
                .country(randomAlphabetic(6))
                .build();
    }
}
