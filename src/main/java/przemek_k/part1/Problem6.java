package przemek_k.part1;

import java.util.stream.IntStream;


public class Problem6 {

    public static void main(String[] args) {

        IntStream.range(0, 10)
                .forEach((i) -> System.out.print(i + " "));

        System.out.println();

        IntStream.range(0, 10)
                .map(i -> i % 2 != 0 ? (i * 2) - 1 : i)
                .forEach((i) -> System.out.print(i + " "));

        System.out.println();

        IntStream.range(0, 10)
                .map(i -> i % 2 != 0 ? (i * 2) - 1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .forEach((i) -> System.out.print(i + " "));

        System.out.println();

        int sum = IntStream.range(0, 10)
                .map(i -> i % 2 != 0 ? (i * 2) - 1 : i)
                .map(i -> i % 2 == 0 ? i / 2 : i)
                .sum();
        System.out.println(sum);





    }
}
