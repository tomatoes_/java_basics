package wojciech_w.utils;

import java.util.Random;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGenerator implements ArrayGenerator{

    public final int[] generate(int length) {
      return generate(length, 0, MAX_VALUE);
    }

    public int[] generate(int length, int rangeFrom, int rangeTo) {

        if (rangeFrom>=rangeTo){
            throw new IllegalArgumentException("Range is invalid");
        }

        int [] array = new  int[length];
        Random random = new Random();
        int r=0;
        for (int i = 0; i < length; i++) {
            while (r< rangeFrom || r>rangeTo){
                r=random.nextInt(rangeTo);
            }
            array [i]=r;
        }
        return array;
    }
}
