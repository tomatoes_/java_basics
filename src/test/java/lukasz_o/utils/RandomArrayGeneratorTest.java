package lukasz_o.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGeneratorTest {

    private static final int TEST_ARRAY_LENGTH = 100;

    @Test            // to adnotacja

    public void shouldGenerateNumbersInGivenRange() {
//       given
        int testRangeFrom = 10;
        int testRangeTo = 10;

//        when
        int[] array = new RandomArrayGenerator().
                generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo );

//      then
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
            System.out.println();
        }
    }

    @Test (expected = IllegalArgumentException.class)

    public void shouldThrowexceptionIfRangeIsInvalid () {

        int testRangeFrom = 20;
        int testRangeTo = 10;


        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

    }

//    @Test
//
//    public void shouldGenerateZerosArray () {
//        Runnable runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                System.out.println("SortAlgoritmTest");
//            }
//        };
//        runnable.run();
//    }

    @Test

    public void shouldGenerateZerosArray () {
        Runnable r = () -> {                            // lambda
            System.out.println("SortAlgoritmTest");

        };

        Runnable runnable = new Runnable() {

            @Override
            public void run() {                     // to samo co powyżej ale tu zwykla klasa anonimowa
                System.out.println("SortAlgoritmTest");
            }
        };
        r.run();
        runnable.run();
    }

}
