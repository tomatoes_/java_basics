package lukasz_o.part2

import junit.framework.Assert
import lukasz_o.utils.RandomArrayGenerator
import spock.lang.Specification

import java.util.function.Predicate

/**
 * Created by RENT on 2017-03-04.
 */
class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100

//    def "should leave zero-indexed array unchanged" () {           // zbędny test
//
//        given:
//        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
//        int[] actualArray = expectedArray.clone()
//        GroupSortAlgorithm algorithm = grouping
//
//        when:
//        algorithm.sort(array, {e -> true})
//
//        then:
//
//        Assert.assertArrayEquals(expectedArray, actualArray)
//
//        where:
//
//        _ | grouping
//
//    }

    def "should divide array into two groups" () {

        given:

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100)
        GroupSortAlgorithm algorithm = grouping

        when:

        algorithm.sort(array, {e -> e < 50})

        then:

//        int changesAmount = 0       // zadziała, ale zrobimy krócej
//        int changePoint = 0//        for (int i = 0; i < array.length; i++) {
//
//            if (array[i] < 50) {
//                changesAmount++
//        }else {
//                changePoint = i
//                break
//            }
//        }
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] >= 50) {
//                changesAmount++
//            } else {
//                break
//            }
//        }
//        assert changesAmount == TEST_ARRAY_LENGTH
//
//    }

        boolean currentGroup = true
        boolean wasGroupChanged = false

        for (int element : array) {
            if (element >= 50 && currentGroup) {
                currentGroup = false
                wasGroupChanged = true
            }
            else if (element < 50 && wasGroupChanged) {
                Assert.fail()
            }
        }

        where:

        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()

    }

    def "should leave array with one group unchanged" () {

        given:

        GroupSortAlgorithm algorithm = grouping
        int[] array = new int[0]

        when:

        algorithm.sort(array, {e -> e < 50})   // (e -> e<= 5))

        then:

        noExceptionThrown()

        where:

        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()
    }

    def "should throw if array is null" () {

        given:

        int[] array = null
        GroupSortAlgorithm algorithm = grouping

        when:
        algorithm.sort(array, {e -> e < 50})


        then:

        thrown(IllegalArgumentException)

        where:

        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()




    }

}
