package lukasz_o.part2;

import lukasz_o.utils.RandomArrayGenerator;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by RENT on 2017-03-04.
 */
public class BubbleSortTest {
    int TEST_ARRAY_LENGHT = 100;
    @Test
    public void shouldReturnedSortedArrayAscending(){
        //given

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new BubbleSortAlgorithm2().sort(array,true);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]<array[i+1]);
        }


    }
    @Test
    public void shouldReturnedSortedArrayDescending(){
        //given

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new BubbleSortAlgorithm2().sort(array,false);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]>array[i+1]);
        }


    }
    @Test
    public void shouldLeaveSortedArrayUnchanged(){
        //given

        int[] array = new int[TEST_ARRAY_LENGHT];
        for (int i = 0; i <array.length ; i++) {
            array[i]=i;
        }


        int[] resoult = Arrays.copyOf(array,array.length);
        //when
        new BubbleSortAlgorithm2().sort(array,true);


        //then
        Assert.assertArrayEquals(array,resoult);


    }

//    @Test
//    public void shouldSortZeroElementsArray() {
//        //given
//        int[] array = new int[0];
//
//        //when
//        new Problem1().sort(array);
//
//        //
//    }

}
