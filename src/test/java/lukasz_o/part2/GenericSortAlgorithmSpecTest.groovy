package lukasz_o.part2

import junit.framework.Assert
import lukasz_o.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class GenericSortAlgorithmSpecTest extends Specification {

    private final int TEST_ARRAY_LENGTH = 100


    def "should sort array ascending by default"() {

        given:

        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        GenericSortAlgorithm algorithm = sortingAlgorithm
        when:


        algorithm.sort(array)

        then:


        for (int i = 1; i < array.length; i++) {
            assert compare(array[i], (array[i - 1])) != 1
        }

        where:

        _ | sortingAlgorithm
        _ | new GenericBubbleSortAlgorithm()
    }

    def "should sort array if elements are not unique"() {

        given:

        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:

        algorithm.sort(array)


        then:

        for (int i = 1; i < array.length; i++) {
            assert compare(array[i], (array[i - 1])) != 1


            where:

            _ | sortingAlgorithm
            _ | new GenericBubbleSortAlgorithm()
        }
    }

    def "should throw if array is null"() {

        given:

        Integer[] array = null
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)


        then:

        thrown(IlegalArgumentException)

        where:

        _ | sortingAlgorithm
        _ | new GenericBubbleSortAlgorithm()


    }


}