package lukasz_o.part2

import lukasz_o.part1.BubbleSortAlgorithm
import lukasz_o.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-02.
 */
class SortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by deafult"() {

        given:

        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH);
        SortAlgorithm algorithm = sortingAlgorithm


        when:

        algorithm.sort(array)

        then:

        for (int i = 1; i < array.length; i++) {
            assert array[i] > array[i - 1]
        }

        where:

        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()

    }


    def "should sort array depending on the order"() {

        given:

        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH);
        SortAlgorithm algorithm = sortingAlgorithm


        when:

        algorithm.sort(array, isAscending)

        then:

        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i - 1])

        }

        where:

        _ | sortingAlgorithm          | isAscending | comparison
        _ | new BubbleSortAlgorithm() | true        | Integer.&max  // .& = ::
        _ | new BubbleSortAlgorithm() | false        | Integer.&min

    }

    def "should throw exception if array is null" () {

        given:

        int[] array = new int[TEST_ARRAY_LENGTH]
        SortAlgorithm algorithm = sortingAlgorithm



        when:

        algorithm.sort(array)

        then:

        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:

        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()

    }

    def "should sort array if elements are not unique" () {

        given:

        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        for (int i = 0; i < array.length; i++) {
            assert array[i] > array[i- 1]
        }


        when:

        algorithm.sort(array)

        then:

        thrown (IllegalArgumentException)

        where:

        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()

    }

    }



