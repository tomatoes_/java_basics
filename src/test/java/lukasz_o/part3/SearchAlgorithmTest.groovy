package lukasz_o.part3

import lukasz_o.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class SearchAlgorithmTest extends Specification {

    private final static TEST_LENGTH_ARRAY = 100;

    def "should find element in random array"() {

        given:

        int[] array = new RandomArrayGenerator().generate(100)
        int index = new Random().nextInt()
        int valueToFind = array[index]    // wczytujemy losowy indeks
        //SearchAlgorithm algorithm = searchAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:

        array[searchResult] == valueToFind

        where:

        _ | searchAlgorithm

    }
    def "should find element in sorted array"() {}
    def "should return invalid index if element not present"() {}
    def "should return invalid index if array length is zero"() {





    }


    def "should throw if array is null" () {

        given:

        int[] array = null
//        int valueToFind;
       // SearchAlgorithm algorithm = searchAlgorithm

        when:

        SearchAlgorithm(array, valueToFind)

        then:

        thrown (IllegalArgumentException)

        where:

        _ | searchAlgorithm
    }


}
