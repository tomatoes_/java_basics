package lukasz_o.part3

import junit.framework.Assert
import lukasz_o.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class SearchAlgorithmSpecTest extends Specification {

    private final static TEST_LENGTH_ARRAY = 100;

    def "should find element in random array"() {

        given:

        int[] array = new RandomArrayGenerator().generate(100)
        int index = new Random().nextInt()
        int valueToFind = array[index]    // wczytujemy losowy indeks
        SearchAlgorithm algorithm = searchAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:

        array[searchResult] == valueToFind

        where:

        _ | searchAlgorithm
        _| new BinarySearchAlgorithm()

    }
    def "should find element in sorted array"() {


        given:

        int[] array = new int [TEST_LENGTH_ARRAY];
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }

        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]

        searchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:

        array[searchResult] == valueToFind

        where:

        _ | searchAlgorithm
        _| new BinarySearchAlgorithm()

    }
    def "should return invalid index if element not present"() {
        given:

        int[] array = arrayGenerator.generate(TEST_LENGTH_ARRAY, 0, 20)
        searchAlgorithm algorithm = searchingAlgorithm

        when:

        int returnIndex = algorithmsearch(array,60)

        then:

        Assert.assertTrue(returnIndex == -1)

        where:

        _ | searchAlgorithm
        _| new BinarySearchAlgorithm()

    }
    def "should return invalid index if array length is zero"() {

        given:

        int[] array = new int[0]
        searchAlgorithm algorithm = searchingAlgorithm
        int valueToFind = new Random().nextInt()

        when:

        int returnValue = algorithm.search(array, valueToFind)

        then:

        Assert.assertEquals(-1, returnValue)

        where:

        _ | searchAlgorithm
        _| new BinarySearchAlgorithm()


    }


    def "should throw if array is null" () {

        given:

        int[] array = null
//        int valueToFind;
        SearchAlgorithm algorithm = searchAlgorithm

        when:

        SearchAlgorithm(array, valueToFind)

        then:

        thrown (IllegalArgumentException)

        where:

        _ | searchAlgorithm
        _| new BinarySearchAlgorithm()
    }


}
