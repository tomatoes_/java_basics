package lukasz_o.part3;

/**
 * Created by RENT on 2017-03-06.
 */
public class BinarySearchAlgorithm implements SearchAlgorithm {

    public int search(int[] array, int value)
    {
        int lewo = 0, prawo = array.length - 1, middle = 0;

        while(lewo <= prawo)
        {
            middle = (lewo + prawo) / 2;
            if(array[middle] == value)
                return middle;
            else if(array[middle] < value)
                lewo = middle + 1;
            else
                prawo = middle - 1;
        }

        return -1; //jeżeli liczba nie została odnaleziona zwróć -1
    }


}
