package lukasz_o.part1;

import lukasz_o.utils.RandomArrayGenerator;

import java.io.Console;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by RENT on 2017-03-02.
 */
public class HeapSortAlgorithm {
    public static void quicksort(int[] a, int lower, int higher)
    {
        //  lower is the lower index, higher is the upper index
        //  of the region of array a that is to be sorted
        int i=lower, j=higher, tmp;
        int point=a[(lower+higher)/2];
        //  partition
        do
        {
            while (a[i]<point) i++;
            while (a[j]>point) j--;
            if (i<=j)
            {
                tmp=a[i]; a[i]=a[j]; a[j]=tmp;
                i++; j--;
            }
        } while (i<=j);
        //  recursion
        if (lower<j) quicksort(a, lower, j);
        if (i<higher) quicksort(a, i, higher);
    }

    public static void main(String[] args) {

        int[] ble = new RandomArrayGenerator().generate(10);
        quicksort(ble, 0,9);

        System.out.println(Arrays.toString(ble));
    }
}
