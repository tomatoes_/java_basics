package lukasz_o.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-02-25.
 */
public class LambdaTest {

    @Test

    public void shouldCreateLambdas() {

        Runnable runnable = () -> {};  // tak tworzymy lambdę
        Supplier<Integer> supplier = () -> 98;  //np. niech zwraca 2, to samo co { return 2}
        // implementacja metody tu

        System.out.println(supplier.get()); // a wywołanie tu

//        Integer i = new Integer (56);   ==     Integer i = 56;
        useSupplier(supplier);
        useSupplier(() -> 5);
        Consumer<Integer> consumer = arg -> {   // akceptuje jakąś wartość
            System.out.println(arg);
        };
        consumer.accept(10);
        Function<Integer, String>  function = arg -> "arg is " + arg;     // Integer to to co przekazujemy do funkcji a String to co zwracamy
        System.out.println(function.apply(25));

        TestInterface testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + " " + c + " = " + result);
            return result;
        };
        testLambda.test('a', "Test");

    }

    @Test
    public void methodReferences () {
        Consumer<Integer> consumer = System.out::println;  // :: to referencja do metody, ekwiwalent x -> System.out.println(x)
        consumer.accept(2);

        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);

    }

    private void useSupplier (Supplier<Integer> supplier) {  // supplier to akcja (interfejs) polegająca tylko na pobraniu wartości
        System.out.println(supplier.get());
    }



}
