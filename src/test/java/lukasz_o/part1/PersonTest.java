package lukasz_o.part1;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.sun.jmx.snmp.ThreadContext.contains;
import static java.lang.System.out;

/**
 * Created by RENT on 2017-02-28.
 */
public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;



    @Test

    public void shouldCreateAPerson() {

//        Person person = Person.getRandomPerson();
//        System.out.println(person);

        Person[] people = getPersons();

        /*
        Szukamy osób, które:
        1) imię zaczynające się literami A - D
        2) są męzczyznami
        3) zbieramy ich pesel i kod pocztowy jako pary wartości
         */

        Stream.of(people)
                .map(this::print) // odwołanie do T print (w tej klasie)
                .filter(person -> person.getGender() == 'M')
                .filter(person -> person.getFirstName()
                        .matches("^[A-Da-d]].*"))
                .map(person -> Pair.of(person.getPesel(), person.getAddress().getPostalCode()))
                .collect(Collectors.toList())
                .forEach(out::println);

//        print (new Integer(5), "test");
//        this.<Integer>print(5, "test)");



    }

    private Person[] getPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];

        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }

    //    private <T extends Integer> T print (T a, T b) {  // możesz przekazać do metody tylko obiekty typu T dziedziczące po klasie Integer
    private <T> T print (T a) {  // taki typ jaki przekażemy do metody taki zwróci
        System.out.println(a);
        return a;
    }

    @Test
    public void shouldExample2() {
        Person[] people = getPersons();

        Stream.of(people)
                .filter(person -> person.getLastName().contains("m")||contains("M"))  //albo .toUpperCase().contains("M)
                .filter(person -> Integer.parseInt(person.getPesel().substring(0,2)) <= Calendar.getInstance().get(Calendar.YEAR) - 1960)
                .filter(person -> person.getAddress().getPostalCode().startsWith("20"))
                .map(this::print)
                .collect(Collectors.toSet());




    }










}