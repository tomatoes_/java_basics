//package lukasz_o.part4
//
//import spock.lang.Specification
//
///**
// * Created by RENT on 2017-03-07.
// */
//class StackSpecTest extends Specification {
//
//    def "should push element to empty stack" () {
//
//        given:
//
//       String toPush = "test";
//        Stack<String> stack = stackInstance
//
//        when:
//
//        stack.push(toPush);
//
//
//        then:
//
//        toPush == stack.peek();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "should push element to filled stack" () {
//
//        given:
//
//        String toPush = "test";
//        Stack<String> stack = stackInstance
//        stack.push("abc");
//
//        when:
//
//        stack.push(toPush);
//
//
//        then:
//
//        toPush == stack.peek();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "should delete value after popping" () {
//
//        given:
//
//
//        Stack<String> stack = stackInstance
//        stack.push("abc");
//
//        when:
//
//        stack.pop();
//
//
//        then:
//
//       null == stack.peek();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "should receive value when peeking but not delete it" () {
//
//        given:
//
//        String toPush = "test";
//        Stack<String> stack = stackInstance
//        stack.push(toPush);
//
//        when:
//
//        stack.peek();
//
//
//        then:
//
//        toPush == stack.peek();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "should return zero as size of empty stack" () {
//
//        given:
//
//
//        Stack<String> stack = stackInstance
//
//
////        when:
////
////        stack.size();
//
//
//        expect: //zamiast then jak usuwamy when, bo wtedy then podkreśli na czerwono
//
//        0 == stack.size();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "should return correct size for filled stack" () {
//
//        given:
//
//
//        Stack<String> stack = stackInstance
//        stack.push("abc")
//
//
//        expect:
//
//        1 == stack.size()
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//    def "isEmpty should return true if there were (are) no values put on stack" () {
//
//        given:
//
//
//        Stack<String> stack = stackInstance
//
//
//        expect:
//
////       true == stack.size();
//        stack.isEmpty();  // przy (are) !stack.isEmpty();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//    }
//
//    def "should return true if element was pushed and poped from stack" () {
//        given:
//
//
//        Stack<String> stack = stackInstance
//        stack.push("abc")
//        stack.pop();
//
//        expect:
//
//
//        stack.isEmpty();
//
//        where:
//
//        _| stackInstance
//        _| new DynamicStack()
//
//    }
//
//
//
//}
