package pawel_f.utils;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static pawel_f.utils.RandomUtils.generateRandomIntArray;

public class RandomUtilsTest {

    private static final int TEST_ARRAY_LENGTH = 100;

@Test
public void shouldFillArrayWithNonZeroValues() {
    for (int element : generateRandomIntArray(TEST_ARRAY_LENGTH)) {
        assertNotEquals(0, element);
    }
}

@Test
public void shouldFillArrayWithValuesOnlyFromSpecificRange() {
    for (int element : generateRandomIntArray(TEST_ARRAY_LENGTH, 0, 5)) {
        assertFalse(element < 0 || element >= 5);
    }

    for (int element : generateRandomIntArray(TEST_ARRAY_LENGTH, 5, 10)) {
        assertFalse(element < 5 || element >= 10);
    }
}

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIfBottomRangeLimitIsTheSameAsTop() {
        generateRandomIntArray(TEST_ARRAY_LENGTH, 5, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIfBottomRangeLimitIsGreaterThanTop() {
        generateRandomIntArray(TEST_ARRAY_LENGTH, 10, 5);
    }
}