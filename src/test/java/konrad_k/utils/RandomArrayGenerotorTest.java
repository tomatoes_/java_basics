package konrad_k.utils;

import org.junit.Assert;
import org.junit.Test;


public class RandomArrayGenerotorTest {
    private static final int Test_Array_Length = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {
        // given

        int testRangeFrom = 10;
        int testRangeTo = 20;
        //when
        int array[] = new RandomArrayGenerator()
                .generate(Test_Array_Length, testRangeFrom, testRangeTo);
        //then
        for (int element : array) {
            Assert.assertTrue(element>=testRangeFrom&&element<=testRangeTo);
        }
    }

}
