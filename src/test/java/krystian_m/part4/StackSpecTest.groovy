package krystian_m.part4

import spock.lang.Specification

class StackSpecTest extends Specification {

    def "should push element to an empty stack"() {
        given:
        String toPush = "test"
        Stack<String> stack = stackInstance

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should push element to a filled stack"() {
        given:
        String toPush = "test"
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should delete value after popping"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.pop()

        then:
        null == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should receive value when peeking but not delete it"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.peek()

        then:
        "abc" == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return zero as size of an empty stack"() {
        given:
        Stack<String> stack = stackInstance

        expect:
        0 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return correct size for a filled stack"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        expect:
        1 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return true if there were no values put on stack"() {
        given:
        Stack<String> stack = stackInstance

        expect:
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return false if push was called previously"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        expect:
        !stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)

    }

    def "should return true if element was pushed and popped"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")
        stack.pop()

        expect:
        true == stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }
}
