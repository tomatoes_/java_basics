package krystian_m.part2

import krystian_m.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import static org.junit.Assert.assertArrayEquals
import static org.junit.Assert.fail

class GroupSortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100;


    def "should throw exception when array is null" () {

        given:
        int[] array = null;
        GroupSortAlgorithm algorithm = groupSortingAlgorithm

        when:
        algorithm.sort(array, {e -> true})

        then:
        thrown(IllegalArgumentException)

        where:
        _ | groupSortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()

    }

    def "should leave zero-sized array unchanged"(){
        given:
        int[]array = new int[0]
        GroupSortAlgorithm algorithm = groupSortingAlgorithm

        when:
        algorithm.sort(array, {e -> true})

        then:
        noExceptionThrown()

        where:
        _| groupSortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()
    }

    def "should divide array into two groups"(){
        given:
        int[]array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100)
        GroupSortAlgorithm algorithm = groupSortingAlgorithm

        when:
        algorithm.sort(array, {e -> e < 50})

        then:
        boolean currentGroup = true
        boolean wasGroupChanged = false
        for (int element: array) {
            if (element >= 50 && currentGroup) {
                currentGroup = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged){
                fail()
            }
        }
//        int changesAmount = 0
//        int changePoint = 0
//        for (int i = 0; i < array.length; i++){
//            if (array[i] < 50){
//                changesAmount++
//            } else {
//                changePoint = i
//                break
//            }
//        }
//
//        for (int i = changePoint; i < array.length ; i++) {
//            if (array[i] >= 50){
//                changesAmount++
//            }else {
//                break
//            }
//            assert changesAmount == TEST_ARRAY_LENGTH
//        }

        where:
        _| groupSortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()
    }

}
