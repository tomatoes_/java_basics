package krystian_m.part2

import krystian_m.utils.RandomArrayGenerator
import spock.lang.Specification

import static java.lang.Integer.compare

class GenericSortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {
            assert compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }


    def "should leave sorted array unchanged"() {
        given:
        Integer[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length ; i++) {
            array[i] = i
        }
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {

            assert compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should throw exception if array is null"() {
        given:
        Integer[] array = null
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should not throw if array is zero"() {
        given:
        Integer[] array = new int[0]
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should sort array if elements are not unique"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {

            assert compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

}
