package krystian_m.part3

import krystian_m.utils.RandomArrayGenerator
import spock.lang.Specification

class SearchAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in a random array"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        int index = new Random().nextInt(array.length)
        int valuteToFind = array[index]
        SearchAlgorithm algorithm = searchAlgorithm
        when:
        int searchResult = algorithm.search(array, valuteToFind)

        then:
        array[searchResult] == valuteToFind

        where:
        _| searchAlgorithm

    }

    def "should find element in sorted array"(){

    }

    def "should return invalid index if element is not present"(){

    }

    def "should return invalid index if array length is zero"(){
        given:
        int[] array = new int[0]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        algorithm.search(array, 0)

        then:
        thrown(NullPointerException)

        where:
        _ | searchingAlgorithm

    }

    def "throw if array is null"(){
        given:
        int[] array = null
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        algorithm.search(array, 0)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
    }
}
