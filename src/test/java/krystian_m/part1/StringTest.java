package krystian_m.part1;

import junit.framework.Assert;
import org.junit.Test;

public class StringTest {
    @Test
    public void stringObjectsShouldHaveTheSameReferenceIfAssigned(){
        String a = "test";
        String b = "test";
        String c = a;
        Assert.assertSame(a ,c);
        if (a == b) {
            System.out.println("Są równe");
        } else{
            System.out.println("Nie są równe");
        }

    }
}
