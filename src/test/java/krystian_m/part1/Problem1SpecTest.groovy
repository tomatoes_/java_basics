package krystian_m.part1

import spock.lang.Specification

class Problem1SpecTest extends Specification {
    def "should create specific array"(){
        given:
        int [] array

        when:
        array = Problem1.createFirstArray();

        then:
        array[0] == 1
        array[array.length - 1] == 61
        for (int i = 0; i < array.length; i++){
            array[i] == array[i - 1] + 2
        }

    }

}
