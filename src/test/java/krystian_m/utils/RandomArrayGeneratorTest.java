package krystian_m.utils;

import org.junit.Assert;
import org.junit.Test;

public class RandomArrayGeneratorTest {
    private static final int TEST_ARRAY_LENGTH = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {
        int testRangeFrom = 10;
        int testRangeTo = 20;


        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }
    }

    @Test
    public void shouldThrowExceptionIfRangeIsInvalid() {
        int testRangeFrom = 10;
        int testRangeTo = 20;


        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);
    }

    @Test
    public void lambdasTest() {
        Runnable r = () -> {
            System.out.println("SortAlgoritmTest");
        };
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("SortAlgoritmTest");
            }

        };
        r.run();
        runnable.run();
    }

    public void testRun(Runnable runnable){
        runnable.run();
    }



}
