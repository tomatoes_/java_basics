package krystian_m.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

class RandomArrayGeneratorSpecTest extends Specification {
    private static final int TEST_ARRAY_LENGTH = 100;


    @Unroll("should generate numbers in range from #testRangeFrom to #testRangeTo")
    def "should generate numbers in given range"() {
        when:
        int[] array = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);
        then:
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }

        where:
        testRangeFrom | testRangeTo
        -10           | 20
        10            | 30
        -100          | 0
        -50           | -20
    }


    @Unroll("should generate array of length = #length")
    def "should generate array of given length"() {
        when:
        int[] array = new RandomArrayGenerator().generate(length);

        then:
        assert array.length == length

        where:
        _ | length
        _ | 10
        _ | 20
        _ | 30
    }


    @Unroll("should throw NegativeArraySizeException for length = #length")
    def "should throw NegativeArraySizeException for negative lengths"() {
        when:
        def array = new RandomArrayGenerator().generate length;

        then:
        thrown(NegativeArraySizeException)

        where:
        _ | length
        _ | - 10
        _ | - 20
        _ | - 30
    }

    def "should generate two different arrays"() {
        given:
        RandomArrayGenerator generator = new RandomArrayGenerator()
        int[] arrayA
        int[] arrayB

        when:
        arrayA = generator.generate(TEST_ARRAY_LENGTH)
        arrayB = generator.generate(TEST_ARRAY_LENGTH)

        then:
        for (int i = 0; i < TEST_ARRAY_LENGTH; ++i) {
            assert arrayA[i] != arrayB[i]
                return
        }
        fail("arrays are equal")
    }

}
