package wojciech_w.utils;

import org.junit.Assert;
import org.junit.Test;


public class RandomArrayGeneratorTest {
    private static final int TEST_ARRAY_LENGTH=100;

    @Test
    public void shouldGenerateNumbersInGivenRange() throws Exception {

        //given
        int testRangeFrom=-10;
        int testRangeTo=20;



        int array [] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

        for (int element:array) {
            Assert.assertTrue(element>=testRangeFrom&&element <=testRangeTo);
        }
        //when

        //then
    }

    @Test
    public void shouldThrowsExceptionIfRangeIsInvalid() throws Exception {
        //given

        //when

        //then
    }
}
