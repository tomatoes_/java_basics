package przemek_k.part1;

import org.junit.Test;
import przemek_k.utils.RandomArrayGenarator;

import java.util.Random;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaTest {

    @Test
    public void shouldCrateLambdas() {

        Runnable runnable = () -> {
        };
        Supplier<Integer> supplier = () -> 2;
        useSupplier(supplier);
        useSupplier(() -> 5);
        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };
        consumer.accept(10);

        Function<Integer, String> function = arg -> "arg is " + arg;
        System.out.println(function.apply(25));
        TestInterface testLambda = (c, comment) -> {

            int result = (char) c;
            System.out.println(comment + " " + c + " = " + result);
            return result;
        };
        testLambda.test('a', "Test");
    }

    private void useSupplier(Supplier<Integer> supplier) {
        System.out.println(supplier.get());
    }

    @Test
    public void methodReference(){
        Consumer<Integer> consumer = System.out::println;
        Consumer<Integer> consumerLambda = (x) -> System.out.println(x);
        consumer.accept(2);
        consumerLambda.accept(2);


        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);
    }

}
