package przemek_k.part1;


import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static jdk.internal.util.xml.impl.Pair.*;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.*;

public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;

    @Test
    public void shouldCreateAPerson() {
        Person[] people = getRandomPeople();


//        Stream.of(people)
//                .filter(person -> person.getGender() == 'M')
//                .map(this::print)
//                .filter(person -> person.getFirstName().matches("^[A-Da-d].*"))
//                .map(person -> Pair.of(person.getPesel(), person.getAddress().getPostalCode()))
//                .forEach(System.out::println);

        Stream.of(people)
//                .filter(person -> person.getLastName().matches(".*[mM].*"))
                .filter(person -> person.getLastName().toUpperCase().contains("M"))
                .filter(person -> Integer.parseInt(person.getPesel().substring(0, 2)) <= Calendar.getInstance().get(Calendar.YEAR) - 1960)
                .filter(person -> person.getAddress().getPostalCode().startsWith("20"))
                .map(this::print)
                .collect(Collectors.toSet());


    }


    private static Person[] getRandomPeople() {
        Person[] people = new Person[PEOPLE_AMOUNT];

        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }

    private <T> T print(T a) {
        System.out.println(a);
        return a;
    }

}