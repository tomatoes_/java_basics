package przemek_k.part1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public class StringTest {

    @Test
    public void stringObjesctsShouldHaveTheSameReferenceIfAssigned() {

        String a = "Test";
        String b = "Test";
        String c = a;

        assertSame(a, c);

        if(a==b){
            System.out.println("Sa rowne");
        }else
        {
            System.out.println("nie");
        }
    }

}
