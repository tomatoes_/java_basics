package przemek_k.utils;

import org.junit.Assert;
import org.junit.Test;
import przemek_k.part1.ZeroArrayGenerator;

import static org.junit.Assert.*;

public class RandomArrayGenaratorTest {
    private static final int TEST_ARRAY_LENGHT = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {

        // given:
        int testRangeFrom = 0;
        int testRangeTo =20;
        // when:
        int array[] = new RandomArrayGenarator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        //then:
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExeptionIfRangeIsInvalid(){
        //given
        int testRangeFrom = 10;
        int testRangeTo = 10;

        //when
        int array[] = new RandomArrayGenarator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        //then
        //fail("This method should throw SomeException");

    }
    @Test
    public void sholudGenerateZaroesArray(){
        Runnable r =() -> {
            System.out.printf("Test");
        };


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Test");
            }
        };
        r.run();
        runnable.run();
    }

    public void testRun(Runnable runnable){
        runnable.run();
    }



}