package przemek_k.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

class RandomArrayGenaratorSpecTest extends Specification {

    private static final int TEST_ARRAY_LENGHT = 100

    @Unroll("should generate numbers in range from #testRangeFrom to #testRangeTo")
    def "should Generate Numbers In Given Range"() {

        when:
        int[] array = new RandomArrayGenarator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo)

        then:
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }

        where:
        testRangeFrom | testRangeTo
        -10           | 20
        10            | 30
        0             | 50
        -100          | 0
        -50           | -20
        -20           | 50
        10            | 9

    }

    @Unroll("should generate array of given lenght = #lenght")
    def "should generate array of given lenght"() {

        when:
        int[] array = new RandomArrayGenarator().generate(lenght)

        then:
        assert array.length == lenght

        where:
        _ | lenght
        _ | 10
        _ | 20
        _ | 50
        _ | -20

    }

    @Unroll("should throw NegativeArraySizeExeption for lenght = #lenght")
    def "should throw NegativeArraySizeExeption for lenght"() {

        when:
        def array = new RandomArrayGenarator().generate(lenght)

        then:
        thrown(NegativeArraySizeException)

        where:
        _ | lenght
        _ | -20
    }


    def "should generate two different arrays"() {
        given:
        RandomArrayGenarator generator = new RandomArrayGenarator()
        int[] arrayA
        int[] arrayB

        when:
        arrayA = generator.generate(TEST_ARRAY_LENGHT)
        arrayB = generator.generate(TEST_ARRAY_LENGHT)

        then:
        for (int i = 0; i < TEST_ARRAY_LENGHT; i++) {
            if (arrayA[i] != arrayB[i])
                return
        }
        fail("arrays are equal")
    }
}
