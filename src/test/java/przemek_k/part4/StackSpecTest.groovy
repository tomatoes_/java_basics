package przemek_k.part4


import spock.lang.Specification

class StackSpecTest extends Specification {


    def "should push element to empty stack"() {

        given:
        String toPush = "STack"
        Stack<String> stack = stackInsance

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()


        where:
        _ | stackInsance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should push element to filled stack"() {

        given:
        String toPush = "test"
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should delete value after poping"() {

        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.pop()

        then:
        null == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should receive value when peeking but not delete it"() {

        given:
        String toPush = "test"
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.peek()

        then:
        stack.peek() == "abc"

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return zero as size of empty stack"() {

        given:
        Stack<String> stack = stackInstance

        expect:
        0 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return correct size for filled stack"() {

        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        expect:
        1 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return true if there were no value put on stack"() {

        given:
        Stack<String> stack = stackInstance


        expect:
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "isEmpty should return true if there are no values on stack"() {

        given:
        Stack<String> stack = stackInspection

        expect:
        stack.isEmpty()

        where:
        _ | stackInspection
        _ | new DynamicStack()
        _ | new ArrayStack(10)

    }

    def "isEmpty should return true if there are values on stock"() {

        given:
        Stack<String> stack = stackInspection

        expect:
        stack.isEmpty()

        where:
        _ | stackInspection
        _ | new DynamicStack()
        _ | new ArrayStack(10)

    }

    def "isEmpty should return true if value was pushed and poped from stack"() {

        given:
        Stack<String> stack = stackInspection
        stack.push("abc")
        stack.pop()

        expect:
        stack.isEmpty()

        where:
        _ | stackInspection
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }


}
