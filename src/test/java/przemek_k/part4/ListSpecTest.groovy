package przemek_k.part4

import org.junit.Assert
import spock.lang.Specification

class ListSpecTest extends Specification {

    def "should add element if list is empty"() {

        given:
        List<String> list = listInstance
        String toAdd = "abc"

        when:
        list.add(toAdd)

        then:
        noExceptionThrown()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should get first element of non-empty list"() {
        given:
        List<String> list = listInstance


        when:
        list.add("abc")

        then:
        "abc"==list.getFirst()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should get last element of non-empty list"() {

        given:
        List<String> list = listInstance


        when:
        list.add("abc")
        list.add("abd")
        list.add("abr")

        then:
        "abr"==list.getLast()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }


    def "should get specific element from the list"() {

        given:
        List<String> list = listInstance
        String toAdd = "abc"
        list.add(toAdd)
        list.add("qwerty")

        when:
        String result = list.get(1)

        then:
        "qwerty"==result

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should remove element from non-empty list"() {

        given:
        List<String> list = listInstance
        String toAdd = "abc"
        list.add(toAdd)
        list.add("qwerty")

        when:
        list.remove(1)

        then:
        list.size() == 1

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should throw exception if get called on empty list"() {

        given:
        List<String> list = listInstance

        when:
        list.getFirst()

        then:
        thrown(IndexOutOfBoundsException)

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should throw exception if remove called on empty list"() {

        given:
        List<String> list = listInstance

        when:
        list.remove(0)

        then:
        thrown(IndexOutOfBoundsException)

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }
}
