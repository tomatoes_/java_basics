package przemek_k.part2

import przemek_k.utils.RandomArrayGenarator
import spock.lang.Specification

class SortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGHT = 100
    RandomArrayGenarator arrayGenarator = new RandomArrayGenarator();

    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT);
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]

        }

        where:
        _ | sortingAlgorithm
        _ | new SortBubble1()
        _ | new HeapSortAlgorithm()
    }

    def "should sort array depends on the order"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i - 1])

        }

        where:
        _ | sortingAlgorithm        | isAscending | comparison
        _ | new SortBubble1()       | true        | Integer.&max
        _ | new SortBubble1()       | false       | Integer.&min
        _ | new HeapSortAlgorithm() | true        | Integer.&max
        _ | new HeapSortAlgorithm() | false       | Integer.&min

    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGHT]
        SortAlgorithm algorithm = sortingAlgorithm
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }


        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] > array[i - 1]

        }
        where:
        _ | sortingAlgorithm
        _ | new SortBubble1()
        _ | new HeapSortAlgorithm()
    }

    def "should throw exeption if array is null"() {

        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new SortBubble1()
        _ | new HeapSortAlgorithm()


    }

    def "should not throw if array is zero sized"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new SortBubble1()
        _ | new HeapSortAlgorithm()

    }

    def "should sort if elements are not unique"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT, 0, 10);
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]

        }

        where:
        _ | sortingAlgorithm
        _ | new SortBubble1()
        _ | new HeapSortAlgorithm()


    }
}
