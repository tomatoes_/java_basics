package przemek_k.part2

import org.junit.Assert
import przemek_k.utils.RandomArrayGenarator
import spock.lang.Specification

class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGHT = 100
    RandomArrayGenarator arrayGenarator = new RandomArrayGenarator()

    def "should leave zero-sized array unchanged"() {
        given:
        int[] array = new int[0]
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> e <= 5 })

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _|new PolishFlagGroupSortAlgorithm()


    }

    def "should divide array into two groups"() {
        given:
        int array = arrayGenarator(TEST_ARRAY_LENGHT, 0, 100);
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> e <= 50 })

        then:
        boolean  currentGroup = true;
        boolean wasGroupChanged = false;

        for(int element: array){
            if (element >= 50 && currentGroup){
                currentGroup = false;
                wasGroupChanged = true;
            }else if (element <50 && wasGroupChanged){
                Assert.fail();
            }
        }


            where:
            _ | sortingAlgorithm
            _|new PolishFlagGroupSortAlgorithm()



    }

    def "should throw if array is null"() {
        given:
        int[] array = null
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> true })

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _|new PolishFlagGroupSortAlgorithm()

    }
}
