package przemek_k.part2

import przemek_k.utils.RandomArrayGenarator
import spock.lang.Specification

class BubbleGenericSortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenarator arrayGenarator = new RandomArrayGenarator()

    def "should sort array ascending by default"() {

        given:
        Integer[] array = arrayGenarator.generate(TEST_ARRAY_LENGTH)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithmSpecTest()

    }

    def "should leave sorted array unchanged"() {

        given:
        Integer[] array = new Integer[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()

    }

    def "should throw exception if array is null"() {
        given:
        Integer[] array = null
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should not throw is array is zero size"() {
        given:
        Integer[] array = new Integer[0]
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should sorted if element is not unique"() {
        given:
        Integer[] array = arrayGenarator.generate(TEST_ARRAY_LENGTH, 0, 10)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i]), (array[i - 1])
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }


}
