//package maciej_brz.part4
//
//import maciej_brz.Part4.DynamicList
//import spock.lang.Specification
//
///**
// * Created by RENT on 2017-03-08.
// */
//class ListInterfaceSpecTest extends Specification {
//
//    def "should  add element if list is empty"(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.add(1)
//
//        then:
//       noExceptionThrown()
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//
//    }
//
//    def "should return first element on non-empty list"(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.add(1)
//
//        then:
//        1 == list.getFirst()
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
//    def "should return last element on non empty list"(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.add(1)
//         list.add(2)
//        list.add(3)
//
//        then:
//        3 == list.getLast()
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
////    def "should return null if there are no elements"(){
////        given:
////
////        DynamicList<Integer> list = listInstance
////
////        expected:
////        null == list.get(0)
////
////        where:
////        _| listInstance
////        _| new DynamicList<>()
////
////    }
//
//    def "should get specific element from list"(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.add(1)
//        list.add(2)
//        list.add(3)
//
//        then:
//        2 == list.get(1)
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
//    def "should remove element from non empty list"(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.add(1)
//        list.remove(0)
//
//
//        then:
//        0 == list.size()
//
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
//    def "should  throw exception if get called on empty list "(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.get(0)
//
//        then:
//        thrown IndexOutOfBoundsException
//
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
//    def "should  throw exception if remove called on empty list "(){
//        given:
//
//        DynamicList<Integer> list = listInstance
//
//        when:
//
//        list.remove(0)
//
//        then:
//        thrown IndexOutOfBoundsException
//
//
//        where:
//        _| listInstance
//        _| new DynamicList<>()
//    }
//
//
//
//
//
//
//}
