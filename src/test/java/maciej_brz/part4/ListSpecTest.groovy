package maciej_brz.part4

import maciej_brz.Part4.DynamicList
import org.junit.Assert
import spock.lang.Specification

class ListSpecTest extends Specification {

    def "should add element if list is empty"() {

        given:
        List<String> list = listInstance as List<String>
        String toAdd = "abc"

        when:
        list.add(toAdd)

        then:
        noExceptionThrown()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should get first element of non-empty list"() {
        given:
        List<String> list = listInstance as List<String>


        when:
        list.add("abc")

        then:
        "abc"==list.getFirst()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should get last element of non-empty list"() {

        given:
        List<String> list = listInstance as List<String>


        when:
        list.add("abc")
        list.add("abd")
        list.add("abr")

        then:
        "abr"==list.getLast()

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }


    def "should get specific element from the list"() {

        given:
        List<String> list = listInstance as List<String>
        String toAdd = "abc"
        list.add(toAdd)
        list.add("qwerty")

        when:
        String result = list.get(1)

        then:
        "qwerty"==result

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should remove element from non-empty list"() {

        given:
        List<String> list = listInstance as List<String>
        String toAdd = "abc"
        for (int i = 0; i < elementsToAdd ; i++) {
            list.add(toAdd)
        }

        when:
        list.remove(2)

        then:
        list.size() == expectedListSize

        where:
        _ | listInstance        | elementsToAdd | expectedListSize
        _ | new DynamicList<>() | 2             | 1
        _ | new DynamicList<>() | 5             | 4

    }

    def "should throw exception if get called on empty list"() {

        given:
        List<String> list = listInstance as List<String>

        when:
        list.getFirst()

        then:
        thrown(IndexOutOfBoundsException)

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }

    def "should throw exception if remove called on empty list"() {

        given:
        List<String> list = listInstance as List<String>

        when:
        list.remove(0)

        then:
        thrown(IndexOutOfBoundsException)

        where:
        _ | listInstance
        _ | new DynamicList<>()

    }
}
