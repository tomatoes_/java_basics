package maciej_brz.part4

import maciej_brz.Part4.ArrayStack
import maciej_brz.Part4.DynamicStack
import maciej_brz.Part4.StackInterface
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-07.
 */
class StackInterfaceSpecTest extends Specification {

    int generator = new Random().nextInt()

    def "should push element to empty stack"() {

        given:

        StackInterface<Integer> stack = stackAlgorithm
        when:

        stack.push(valueToPush)
        then:
        Assert.assertEquals(stack.peek(), valueToPush)

        where:
        _ | stackAlgorithm     | valueToPush
        _ | new DynamicStack() | 100
        _ | new ArrayStack(10) | 100
    }

    def "should push element to filed stack"() {

        given:
        StackInterface<Integer> stack = stackAlgorithm
        for (int i = 0; i < stack.size() - 1; i++) {
            stack.push(generator)
        }

        when:

        stack.push(valueToPush)
        then:
        Assert.assertEquals(stack.peek(), valueToPush)

        where:
        _ | stackAlgorithm     | valueToPush
        _ | new DynamicStack() | 100
        _ | new ArrayStack(10) | 100

    }

    def "should delete value after poping"() {

        given:
        StackInterface<Integer> stack = stackAlgorithm
        for (int i = 0; i < 8 ; i++) {
            stack.push(generator)
        }
        Integer stackTop = stack.peek()

        when:
        stack.push(valueToPush)
        stack.pop()
        then:
//        Assert.assertNotEquals(stack.peek(), valueToPush)
        Assert.assertEquals(stack.peek(), stackTop)

        where:
        _ | stackAlgorithm     | valueToPush
        _ | new DynamicStack() | 100
        _ | new ArrayStack(10) | 100
    }

    def "should receive value when peeking but not delete it"() {
        given:
        StackInterface<Integer> stack = stackAlgorithm
        for (int i = 0; i < stack.size() - 1; i++) {
            stack.push(generator)
        }


        when:
        stack.push(valueToPush)
        stack.peek()

        then:
        Assert.assertEquals(stack.peek(), valueToPush)
        Assert.assertEquals(stack.peek(), valueToPush)

        where:
        _ | stackAlgorithm     | valueToPush
        _ | new DynamicStack() | 100
        _ | new ArrayStack(10) | 100

    }

    def "should return zero as size of empty stack"() {
        given:
        StackInterface<Integer> stack = stackAlgorithm

        expect:
        Assert.assertEquals(stack.size(), 0)


        where:
        _ | stackAlgorithm
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should return correct size for filed stack stack"() {
        given:


        StackInterface<Integer> stack = stackAlgorithm
//        for (int i = 0; i < stackSize() - 1; i++) {
//            stack.push(generator)
//        }


        when:
        stack.push(1)
        stack.push(1)
        stack.push(1)
        stack.push(1)


        then:
        Assert.assertEquals(stack.size(), 4)


        where:
        _ | stackAlgorithm     | stackSize
        _ | new DynamicStack() | 10
        _ | new ArrayStack(10) | _
    }

    def "should return true if there were no values on stack"() {
        given:

        StackInterface<Integer> stack = stackAlgorithm

        expect:
        Assert.assertTrue(stack.isEmpty())

        where:
        _ | stackAlgorithm
        _ | new DynamicStack()
    }

    def "should return false if push was called previously"() {
        given:

        StackInterface<Integer> stack = stackAlgorithm

        when:
        stack.push(100)


        then:
        Assert.assertFalse(stack.isEmpty())

        where:
        _ | stackAlgorithm
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }


    def "should return true if element was push and poped"() {
        given:

        StackInterface<Integer> stack = stackAlgorithm

        when:
        stack.push(100)
        stack.pop()

        then:
        Assert.assertTrue(stack.isEmpty())

        where:
        _ | stackAlgorithm
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }


}