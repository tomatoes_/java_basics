package maciej_brz.part2;

import maciej_brz.utils.QuickSort;
import maciej_brz.utils.RandomArrayGenerator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;

/**
 * Created by RENT on 2017-03-02.
 */
public class QuickSortTest {

    int TEST_ARRAY_LENGHT = 100;
    @Test
    public void shouldReturnedSortedArrayAscending(){
        //given

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new QuickSort().sort(array,true);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]<array[i+1]);
        }


    }


    @Test
    public void shouldReturnedSortedArrayDescending(){
        //given

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new QuickSort().sort(array,false);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]>array[i+1]);
        }


    }
    @Test
    public void shouldLeaveSortedArrayUnchanged() {
        //given

        int[] array = new int[TEST_ARRAY_LENGHT];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }


        int[] resoult = Arrays.copyOf(array, array.length);
        //when
        new QuickSort().sort(array);


        //then
        Assert.assertArrayEquals(array, resoult);
    }


        @Test (expected = NullPointerException.class)
        public void shouldThrowExceptionIfArrayIsNull(){
            //given
            int[] array  = null;

            new QuickSort().sort(array);

        }

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        public void shouldNotThrowIfArrayIsZeroSized(){
            //given
            int[] array= new int[0];
            //when
            new QuickSort().sort(array);
            //then

        }

        @Test
        public void shouldSortIfElementsAreNotUnique(){

            //given
            int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT,0,20);
            //when
            new QuickSort().sort(array);
            //then
            for (int i = 0; i < array.length-1 ; i++) {
                Assert.assertTrue(array[i]<=array[i+1]);
            }

        }



}

