package maciej_brz.part2

import maciej_brz.utils.BubbleSort
import maciej_brz.utils.HeapSort
import maciej_brz.utils.InsertSort
import maciej_brz.utils.MergeSort
import maciej_brz.utils.QuickSort
import maciej_brz.utils.SortAlgorithm
import spock.lang.Specification
import maciej_brz.utils.RandomArrayGenerator

//import org.junit.Assert

class SortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[(i - 1)]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSort()
        _ | new QuickSort()
        _ | new HeapSort()
        _ | new MergeSort()
        _ | new InsertSort()
    }

    def "should sort array depending on the order"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
//            println array[i] + " " + array[i - 1] + " " + comparison(array[i], array[i - 1])
            assert array[i] == comparison(array[i], array[i - 1])
        }

        where:
        _ | sortingAlgorithm   | isAscending | comparison
        _ | new BubbleSort()   | true        | Integer.&max
        _ | new BubbleSort()   | false       | Integer.&min
        _ | new QuickSort()    | true        | Integer.&max
        _ | new QuickSort()    | false       | Integer.&min
        _ | new HeapSort()     | true        | Integer.&max
        _ | new HeapSort()     | false       | Integer.&min
        _ | new MergeSort()    | true        | Integer.&max
        _ | new MergeSort()    | false       | Integer.&min
        _ | new InsertSort()   | true        | Integer.&max
        _ | new InsertSort()   | false       | Integer.&min



    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSort()
        _ | new QuickSort()
        _ | new HeapSort()
        _ | new MergeSort()
        _ | new InsertSort()
    }

    def "should throw exception if array is null"() {
        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(NullPointerException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleSort()
        _ | new QuickSort()
        _ | new HeapSort()
        _ | new MergeSort()
        _ | new InsertSort()
    }

    def "should not throw if array is zero-sized"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleSort()
        _ | new QuickSort()
        _ | new HeapSort()
        _ | new MergeSort()
        _ | new InsertSort()
    }

    def "should sort array if elements are not unique"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSort()
        _ | new QuickSort()
        _ | new HeapSort()
        _ | new MergeSort()
        _ | new InsertSort()
    }
}