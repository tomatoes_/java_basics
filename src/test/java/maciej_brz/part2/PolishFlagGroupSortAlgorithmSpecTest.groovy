package maciej_brz.part2

import maciej_brz.utils.PolishFlagGroupSort
import maciej_brz.utils.GroupSortAlgorithm
import org.junit.Assert
import spock.lang.Specification
import maciej_brz.utils.RandomArrayGenerator

import static org.junit.Assert.assertArrayEquals

/**
 * Created by RENT on 2017-03-04.
 */
class PolishFlagGroupSortAlgorithmSpecTest extends Specification {

    def TEST_ARRAY_LENGTH = 1000;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def predicate = {e -> e <=5}

    def "should leave zero-sized array unchanged"(){

        given:
        int[] array = new int[0]


        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array,predicate)

        then:

        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSort()
    }

    def "should divide array into two groups"(){
        given:
      int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0,100)

        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array,{e -> e <=50})

        then:
        boolean  currentGroup = true;
        boolean wasGroupChanged= false;
        for (int element:array){
            if (element > 50 && currentGroup){
                currentGroup = false;
                wasGroupChanged = true
            }
            else
            {
                if (element <= 50 && wasGroupChanged)
                {
                    Assert.fail();
                }
            }
        }
//        boolean secondChange = false;
//        boolean firstChange = false;
//
//        for (int i = 0; i < array.length-1; i++) {
//
//            if (array[i] < 50&&!firstChange){
//            ;
//            }
//            else{
//                firstChange=true;
//                if(array[i] > 50&&!secondChange){
//                ;
//                }else{
//                    secondChange=true
//                    }
//                }
//        }
//
//        Assert.assertFalse(secondChange);
        where:
        _ | sortingAlgorithm
        _ |new PolishFlagGroupSort()
    }

    def "should leave array with one group unchanged"(){
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int[] sourceArray = array.clone();
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array,predicate)

        then:
        assertArrayEquals(array,sourceArray)


        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSort()
    }

    def "should throw if array is null"(){
        given:
        int[] array = null
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array,predicate)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSort()
    }




}
