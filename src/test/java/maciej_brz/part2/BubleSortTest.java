package maciej_brz.part2;

import groovy.json.internal.Exceptions;
import maciej_brz.utils.BubbleSort;
import maciej_brz.utils.RandomArrayGenerator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;

/**
 * Created by RENT on 2017-03-01.
 * 2. Test dla algorytmu sortującego - ustalenie przypadków
 * should sort array ascending by default
 * should sort array depending on the order
 * should leave sorted array unchanged
 * should throw exception if array is null
 * should not throw if array is zero-sized
 * should sort if elements are not unique
 *
 */
public class BubleSortTest {
    int TEST_ARRAY_LENGHT = 100;
    @Test
    public void shouldReturnedSortedArrayAscending(){
     //given

      int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new BubbleSort().sort(array,true);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]<array[i+1]);
        }


    }
    @Test
    public void shouldReturnedSortedArrayDescending(){
        //given

        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        //when
        new BubbleSort().sort(array,false);


        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]>array[i+1]);
        }


    }
@Test
    public void shouldLeaveSortedArrayUnchanged(){
        //given

        int[] array = new int[TEST_ARRAY_LENGHT];
    for (int i = 0; i <array.length ; i++) {
        array[i]=i;
    }


        int[] resoult = Arrays.copyOf(array,array.length);
        //when
        new BubbleSort().sort(array,true);


        //then
        Assert.assertArrayEquals(array,resoult);


    }

@Test (expected = NullPointerException.class)
    public void shouldThrowExceptionIfArrayIsNull(){
    //given
    int[] array  = null;

    new BubbleSort().sort(array);

}
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldNotThrowIfArrayIsZeroSized(){
    //given
        int[] array= new int[0];
    //when
        new BubbleSort().sort(array);
        //then

    }

    @Test
    public void shouldSortIfElementsAreNotUnique(){

        //given
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT,0,20);
        //when
        new BubbleSort().sort(array);
        //then
        for (int i = 0; i < array.length-1 ; i++) {
            Assert.assertTrue(array[i]<=array[i+1]);
        }

    }

}