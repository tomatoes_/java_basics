package maciej_brz.part2


import maciej_brz.utils.GenericBubbleSort

import maciej_brz.utils.RandomArrayGenerator
import maciej_brz.utils.GenericSortAlgorithm
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class GenericSortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert  (array[i].compareTo(array[i-1])!= -1)
        }

        where:
        _ | sortingAlgorithm
        _ | new GenericBubbleSort()

    }

//    def "should sort array depending on the order"() {
//        given:
//        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
//        GenericSortAlgorithm algorithm = sortingAlgorithm
//
//        when:
//        algorithm.sort(array, isAscending)
//
//        then:
//        for (int i = 1; i < array.length; i++) {
////            println array[i] + " " + array[i - 1] + " " + comparison(array[i], array[i - 1])
//            assert array[i] == comparison(array[i], array[i - 1])
//        }
//
//        where:
//        _ | sortingAlgorithm   | isAscending | comparison
//        _ | new GenericBubbleSort()| 0  | 0
//
//
//
//    }

//    def "should leave sorted array unchanged"() {
//        given:
//        Integer[] array = new int[TEST_ARRAY_LENGTH]
//        for (int i = 0; i < array.length; i++) {
//            array[i] = i
//        }
//        GenericSortAlgorithm algorithm = sortingAlgorithm
//
//        when:
//        algorithm.sort(array)
//
//        then:
//        for (int i = 1; i < array.length; i++) {
//            assert Integer(array[i]).compareTo(array[i - 1]) != -1
//        }
//
//        where:
//        _ | sortingAlgorithm
//        _ | new GenericBubbleSort()
//    }

    def "should throw exception if array is null"() {
        given:
        Integer[] array = null
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(NullPointerException)

        where:
        _ | sortingAlgorithm
        _ | new GenericBubbleSort()
    }

    def "should not throw if array is zero-sized"() {
        given:
        Integer[] array = new Integer[0]
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new GenericBubbleSort()
    }

    def "should sort array if elements are not unique"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new GenericBubbleSort()
    }
}
