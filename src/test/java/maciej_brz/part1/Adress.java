package maciej_brz.part1;

import lombok.Builder;
import lombok.Data;

import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by RENT on 2017-02-28.
 */

@Data
@Builder
public class Adress {
   private String streetname, flatNumber,country, postalCode;


   public static Adress getRandomAdress(){
      return Adress.builder()
                   .country(randomAlphabetic(7))
                   .postalCode(randomNumeric(5))
                   .streetname(random(20))
                   .flatNumber(randomNumeric(2))
                   .build();
   }

}
