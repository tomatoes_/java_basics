package maciej_brz.part1;

import lombok.*;

import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

/**
 * Created by RENT on 2017-02-28.
 */
@Data
@Builder

public class Person {
   private String firstName, lastName ,pesel;
    private char gender;
    private Adress adress;

    public static Person getRandomPerson(){
        return Person.builder().firstName(randomAlphabetic(6))
                .firstName(randomAlphabetic(9))
                .pesel(randomNumeric(11))
                .gender(new Random().nextBoolean() ?'M':'K')
                .adress(Adress.getRandomAdress())
                .build();

    }




}
