package maciej_brz.part1

import spock.lang.Specification

/**
 * Created by RENT on 2017-02-25.
 */
class Problem1SpecTest extends Specification {
    def "test generating Odd Numbers array depended from last"() {
        given:
       int  maxValue = 61;
        int[] array = Problem1.generatingOddNumbers(maxValue);

        when:
        array = Problem1.generatingOddNumbers(maxValue);
        then:
        array[0]==1;
        array[array.length-1]==maxValue;
        for (int i = 1; i < array.length  ; i++) {
            assert  array[i-1]+2 == array[i];

        }

    }
}
