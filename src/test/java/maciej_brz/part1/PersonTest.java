package maciej_brz.part1;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.time.Year;
import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.*;



/**
 * Created by RENT on 2017-02-28.
 */
public class PersonTest {

   private static final int PEOPLE_AMOUNT =1000;

    @Test
    public void PersonBuildTest(){
      Person randomPerson = Person.getRandomPerson();
        System.out.println(randomPerson);
        Person[] people = getRandomPersons();

        Stream.of(people)

                .filter(person -> person.getGender()=='M')
                .map(this::print)
                .filter(person -> person.getFirstName().matches("^[A-Da-d].*"))
                .map(person -> Pair.of(person.getPesel(),person.getAdress().getPostalCode()))
                .collect(toList());



    }




    private Person[] getRandomPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < PEOPLE_AMOUNT ; i++) {
            people[i]=Person.getRandomPerson();
        }
        return people;
    }


    private <T> T print(T a){
    System.out.println(a);
    return a;
    }


    @Test
    public void example2() {
//
//        Person[] people1 = getRandomPersons();
//
//
//        Stream.of(people1)
//                .filter(person -> person.getAdress().getPostalCode().startsWith("20"))
//                .filter(person -> person.getLastName().toUpperCase().contains("M"))
//                .filter(person -> Integer.parseInt(person.getPesel().substring(0,2))<= Calendar.getInstance().get(Calendar.YEAR)-1960)
//                .map(this::print)
//                .collect(Collectors.toSet());

    }

}