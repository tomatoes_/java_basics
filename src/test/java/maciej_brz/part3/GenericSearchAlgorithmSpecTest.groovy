package maciej_brz.part3

import maciej_brz.Part3.GenericBinarySearch
import maciej_brz.Part3.GenericNaiveSearch
import maciej_brz.Part3.GenericSearchAlgorithm
import maciej_brz.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 should find element in a random array
 should find element in sorted array
 should return invalid index if element not present
 should return invalid index if array length is zero
 should throw if array is null
 *
 */
class GenericSearchAlgorithmSpecTest extends Specification {

    final static int TEST_ARRAY_LENGHT = 1000;

    def "should find value in random array"() {
        given:
        Integer [] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);
        Integer expectedValue = array[expectedIndex];

        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array, { e -> e == expectedValue })

        then:
        Assert.assertEquals(array[returnedIndex] , expectedValue)

        where:
        _ | searchAlgorithm          | expectedIndex
        _ | new GenericNaiveSearch() | 50
        _ | new GenericNaiveSearch() | TEST_ARRAY_LENGHT-1
        _ | new GenericNaiveSearch() | new Random().nextInt(TEST_ARRAY_LENGHT-1)


    }


    def "should find value in sorted array"() {
        given:
        Integer[] array = new Integer[TEST_ARRAY_LENGHT];
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        Integer expectedValue = array[expectedIndex];
        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        Integer returnedIndex = algorithm.search(array, { e -> e == expectedValue   })

        then:
        Assert.assertEquals( expectedIndex , returnedIndex )

        where:

        _ | searchAlgorithm          | expectedIndex
        _ | new GenericNaiveSearch() | 50
        _ | new GenericNaiveSearch() | TEST_ARRAY_LENGHT - 1
        _ | new GenericNaiveSearch() | new Random().nextInt(TEST_ARRAY_LENGHT - 1)
         _ | new GenericBinarySearch()| 50
    }

    def "should return invalid index if element not present"() {
        given:
        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT, 0, 12);

        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array, { e -> false })

        then:
        Assert.assertEquals(returnedIndex ,-1)

        where:
        _ | searchAlgorithm
        _ | new GenericNaiveSearch()
       // _ | new GenericBinarySearch()



    }

    def "should return invalid index if array length is zero"() {
        given:
        Integer [] array = new int[0];

        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array, { e -> e == 888 })

        then:
        Assert.assertEquals(returnedIndex , -1)

        where:
        _ | searchAlgorithm
        _ | new GenericNaiveSearch()
        _ | new GenericBinarySearch()


    }

    def "should throw if array is null"() {
        given:
        Integer[] array = null;
        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        algorithm.search(array, {e -> e==666})

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchAlgorithm
        _ | new GenericNaiveSearch()
       // _ | new GenericBinarySearch()
    }


}