package maciej_brz.part3


import maciej_brz.Part3.BinarySearch
import maciej_brz.Part3.NaiveSearch
import maciej_brz.Part3.SearchAlgoritm
import maciej_brz.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 should find element in a random array
 should find element in sorted array
 should return invalid index if element not present
 should return invalid index if array length is zero
 should throw if array is null
 *
 */
class SearchAlgorithmSpecTest extends Specification {

    final static int TEST_ARRAY_LENGHT = 1000;

    def "should find value in random array"(){
        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);
        int expectedValue = array[expectedIndex];

        SearchAlgoritm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array,expectedValue)

        then:
        Assert.assertTrue(array[returnedIndex]==expectedValue)

        where:
        _| searchAlgorithm   | expectedIndex
        _| new NaiveSearch() | 50
        _| new NaiveSearch() | TEST_ARRAY_LENGHT-1
        _| new NaiveSearch() | new Random().nextInt(TEST_ARRAY_LENGHT-1)


    }




    def "should find value in sorted array"(){
        given:
        int[] array = new int[TEST_ARRAY_LENGHT];
        for (int i = 0; i < array.length ; i++) {
            array[i]=i
        }
        int expectedValue =array[expectedIndex];
        SearchAlgoritm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array,expectedValue)

        then:
        Assert.assertTrue(returnedIndex == expectedIndex)

        where:

        _| searchAlgorithm    |expectedIndex
        _| new NaiveSearch()  | 50
        _| new NaiveSearch()  | TEST_ARRAY_LENGHT-1
        _| new NaiveSearch()  | new Random().nextInt(TEST_ARRAY_LENGHT-1)
        _| new BinarySearch() | new Random().nextInt(TEST_ARRAY_LENGHT-1)
    }

   def "should return invalid index if element not present"(){
       given:
       int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT,0,12);

       SearchAlgoritm algorithm = searchAlgorithm

       when:
       int returnedIndex = algorithm.search(array,invalidValue)

       then:
       Assert.assertTrue(returnedIndex == -1)

       where:
       _| searchAlgorithm   | invalidValue
       _| new NaiveSearch() | 20
       _| new NaiveSearch() | new Random().nextInt()+13
       _| new BinarySearch() | new Random().nextInt()+13

   }

    def "should return invalid index if array length is zero"(){
        given:
        int[] array = new int[0];

        SearchAlgoritm algorithm = searchAlgorithm

        when:
        int returnedIndex = algorithm.search(array,value)

        then:
        Assert.assertTrue(returnedIndex == -1)

        where:
        _| searchAlgorithm    | value
        _| new NaiveSearch()  | new Random().nextInt()
        _| new BinarySearch() | new Random().nextInt()



    }

    def "should throw if array is null"(){
        given:
        int[] array = null;
        SearchAlgoritm algorithm = searchAlgorithm

        when:
        algorithm.search(array,666)

        then:
        thrown(IllegalArgumentException)

        where:
        _| searchAlgorithm
        _| new NaiveSearch()
        _| new BinarySearch()
    }
}
