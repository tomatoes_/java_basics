package maciej_brz.utils

import maciej_brz.part1.Problem7MyOryginal;
import org.junit.Assert;
import spock.lang.Specification;

/**
 * Created by Maciek on 2017-02-27.
 */
public class ReversedSumTest extends Specification {

    def shouldReturnReversedSumOfTwoIntegers() {

        when:
        int resoult = Problem7MyOryginal.reversedSumValueOfTwoIntegers(value1, value2);


        then:
        Assert.assertEquals(expected, resoult);


        where:
        value1 | value2 | expected
        24     | 1      | 34
        4358   | 754    | 1998
        305    | 794    | 1
        1111   | 9999   | 1111
        23400  | 890    | 35

    }

}
