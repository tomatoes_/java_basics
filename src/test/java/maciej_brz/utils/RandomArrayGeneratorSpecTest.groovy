package maciej_brz.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by RENT on 2017-02-25.
 */

//TESTY SPOCK'owe // Żyj długo i szczęśliwie
class RandomArrayGeneratorSpecTest extends Specification {

    private static final int TEST_ARRAY_LENGHT = 100;

    @Unroll("should generate numbers from #fromRange to #toRange")
    def "shouldGenerateNumbersInGivenRange"(){

        when:
        int[] resoult = new  RandomArrayGenerator().generate(TEST_ARRAY_LENGHT,fromRange,toRange);
        for (int element:resoult){
            Assert.assertTrue((element >= fromRange) && (element<= toRange));
        }

        then:
        Assert.assertEquals(TEST_ARRAY_LENGHT,resoult.length);


        where:
        fromRange|toRange
        -10      |20
        20       |100
        -100     |0
        -50      |-20
        -600     | 300
        -70       | 30
    }


    @Unroll ("should generate array of lenght #lenght")
    def"shoud generate array of given lenght"(){

        when:
        int[] array = new  RandomArrayGenerator().generate(lenght);

        then:
        assert array.length == lenght;

        where:
        _|lenght
        _|10
        _|100
        _|12
        _|38
        _|55
        _|666
        _|-10

    }


    @Unroll("should throw NegativeArraySizeException for negativ lenght = #lenght")
    def "should throw NegativeArraySizeException for negativ lenght"(){



        when:
        int[] array = new  RandomArrayGenerator().generate(lenght);


        then:
        thrown(NegativeArraySizeException);

        where:
        _|lenght
        _|-10
        _|-800
        _|-1
        _|-666
    }

    def "should generate two different arrays"(){
        given:
        RandomArrayGenerator generator = new RandomArrayGenerator()
        int[] arrayA;
        int[] arrayB;

        when:

        arrayA =  generator.generate(TEST_ARRAY_LENGHT);
        arrayB = generator.generate(TEST_ARRAY_LENGHT);

        then:

        for (int i = 0; i <TEST_ARRAY_LENGHT ; i++) {
            if ( arrayA[i] != arrayB[i]) return;


        }
        fail("arrays are equal");
    }


}
