package maciej_brz.utils;

import maciej_brz.part1.TestInterface;
import org.junit.Test;

import java.util.Random;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-02-25.
 */
public class LambaTest {

    @Test
    public void test(){
        Supplier<Integer> supplier = () -> {
            return 98;
        };


        useSupplier(supplier);
        useSupplier(()->5);
             Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
             };
        consumer.accept(10);
        Function<Integer,String> function = arg ->  "arg is " + arg ;


        System.out.println(function.apply(123456789));

        TestInterface testLambda =(c,comment)-> {
        int result = (int) c;
       System.out.println(comment + " " + c + " = "+ result);
       return result;
        };

        testLambda.test('a',"Test");

        }


    @Test
    public void metodReferences(){
        Consumer<Integer> consumer1 = System.out::println;
        Consumer<Integer> consumer2 = x ->  System.out.println(x);
        consumer1.accept(2);
        consumer2.accept(2);

        Random random = new Random();
        random.nextInt(32);
        useSupplier(random::nextInt);
    }

//    public int reversNumber(Integer number){
//
//                String numberString = number.toString();
//        char[] temporaryString = new char[numberString.length()];
//        for (int i = 0; i <numberString.length() ; i++) {
//            temporaryString[i]= numberString.charAt(i);
//        }
//        if (temporaryString.length%2==0){
//            for (int i = 0; i <temporaryString.length/2 ; i++) {
//                char tempChar = temporaryString[i];
//                temporaryString[i]= temporaryString[temporaryString.length-i-1];
//                temporaryString[temporaryString.length-i-1]=tempChar;
//                }
//        }else {
//            for (int i = 0; i <temporaryString.length/2+1 ; i++) {
//                char tempChar = temporaryString[i];
//                temporaryString[i]= temporaryString[temporaryString.length-i-1];
//                temporaryString[temporaryString.length-i-1]=tempChar;
//                }
//            }
//
//
//
//        number=0;
//        for (int i = 0; i <temporaryString.length ; i++) {
//            number += (temporaryString[i]-48)*(int) (Math.pow(10,(temporaryString.length-i-1)));
//
//        }
//
//    return number;
//    }
//
//
//    @Test
//    public void shouldReverseInteger()
//    {
//        Scanner input = new Scanner(System.in);
//        System.out.println("Podaj ilosc liczb:");
//        int[] integers = new int[input.nextInt()];
//        int sum=0;
//
//        for (int i = 0; i < integers.length; i++) {
//            System.out.println("podaj "+ (i+1)+" liczbę :");
//
//
//            integers[i] = integers[reversNumber(input.nextInt())];
//            sum = sum + integers[1];
//        }
//
//        System.out.println(sum);
//
//    }
 ///////////////////////////////////////////////////////////////////////////////////////////////////
    private  void useSupplier  (Supplier<Integer> supplier){
        System.out.println(supplier.get());
    }
}
