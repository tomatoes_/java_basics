package kamil_j.part1;


import org.junit.Test;

import static org.junit.Assert.assertSame;

public class StringTest {
    @Test
    public void stringObjectShouldHaveTheSameReferenceIfAssigned(){
        String a = "test";
        String b = "test";
        String c = a;
        assertSame(a, c);
        if(a==b){
            System.out.println("Są równe");
        } else{
            System.out.println("Nie sa równe");
        }
    }
}
