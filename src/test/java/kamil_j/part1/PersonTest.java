package kamil_j.part1;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Calendar;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static kamil_j.part1.Person.getRandomPerson;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.*;


public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;

    @Test
    public void shouldCreateAPerson() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < people.length ; i++) {
            people[i] = getRandomPerson();
        }
//example 2
        Stream.of(getRandomPerson())
                .filter(person -> person.getLastName().matches(".*[Mm].*"))
                .filter(person -> Integer.parseInt(person.getPesel()
                                                    .substring(0,2)) <=
                        Calendar.getInstance()
                                .get(Calendar.YEAR) - 1960)
                .filter(person -> person.getAddress()
                        .getPostalCode()
                        .startsWith("20"))
                .map(this::print)
                .collect(toSet());
//example 2

//example 1
//        Stream.of(people)
//                .map(this::print)
//                .filter(person -> person.getGender() == 'M')
//                .filter(person -> person.getFirstName()
//                        .matches("^[A-Da-d].*"))
//                .map(person -> Pair.of(person.getPesel(), person.getAddress().getPostalCode()))
//                .collect(toList());
//example 1
    }
    private <T> T print(T a) {
        System.out.println(a);
        return a;
    }
}