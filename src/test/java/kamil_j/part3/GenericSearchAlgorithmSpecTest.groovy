package kamil_j.part3

import kamil_j.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

class GenericSearchAlgorithmSpecTest extends Specification {

    Integer TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in a random array"() {

        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        Integer index = new Random().nextInt(array.length)
        Integer valueToFind = array[index]

        GenericSearchAlgorithm algorithm = searchAlgorithm

        when:
        Integer searchResult = algorithm.search(array, { p -> p.equals(valueToFind) })

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
        _ | new GenericBinarySearchAlgorithm()
    }

    def "should find element in sorted array"() {

        given:
        Integer[] array = new Integer[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i]
        }

        Integer index = new Random().nextInt(array.length)
        Integer valueToFind = array[index]

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        Integer searchResult = algorithm.search(array, { p -> p.equals(valueToFind) })

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
        _ | new GenericBinarySearchAlgorithm()
    }

    def "should return invalid index if element not present"() {

        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 20)

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        Integer returnIndex = algorithm.search(array, {v -> false})

        then:
        Assert.assertTrue(returnIndex == -1)

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
        _ | new GenericBinarySearchAlgorithm()
    }

    def "should return invalid index if array length zero"() {

        given:

        Integer[] array = new Integer[0]

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        Integer valueToFind = new Random().nextInt()

        when:

        Integer returnValue = algorithm.search(array, { p -> p.equals(valueToFind) })

        then:

        Assert.assertEquals(-1, returnValue)

        where:

        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
        _ | new GenericBinarySearchAlgorithm()
    }

    def "should throw if array is null"() {

        Integer[] array = null

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:

        algorithm.search(array, {v -> false} )

        then:

        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
        _ | new GenericBinarySearchAlgorithm()
    }
}
