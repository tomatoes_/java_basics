package kamil_j.part2

import kamil_j.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import static org.junit.Assert.*
import static org.junit.Assert.assertArrayEquals


class GroupSortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100

    def "should leave zero-sized array unchanged"() {
        given:
        int[] array = new int[0]
        GroupSortAlgorithm algorithm = grouping
        when:
        algorithm.sort(array, { e -> true })

        then:
        noExceptionThrown()

        where:
        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()

    }

    def "should divide array into two gruops"() {
        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0 , 100)
        GroupSortAlgorithm algorithm = grouping

        when:
        algorithm.sort(array, { e -> e < 50 })

        then:
        boolean currentGroup = true
        boolean wasGroupChanged = false
        for (int element: array){
            if (element >= 50 && currentGroup){
                currentGroup = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged) {
                fail()
            }
        }

//        then:
//        int changesAmount = 0
//        int changePoint = 0
//        for (int i = 1; i < TEST_ARRAY_LENGTH ; i++) {
//            if(array[i] < 50){
//                changesAmount++
//            } else if (array[i] > 50) {
//                changePoint = i
//                break
//            }
//            for (int i = changePoint; i < TEST_ARRAY_LENGTH; i++) {
//                if (array[i] >= 50) {
//                    changesAmount++
//            } else {
//                    break
//            }
//                assert  changesAmount == TEST_ARRAY_LENGTH
//            }
//        }

        where:
        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()
    }

    def "should throw if array is null"() {
        given:
        int[] array = null
        GroupSortAlgorithm algorithm = grouping
        when:
        algorithm.sort(array, { e -> true })

        then:
        noExceptionThrown()

        where:
        _ | grouping
        _ | new PolishFlagGroupSortAlgorithm()
    }
}
