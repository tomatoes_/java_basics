package kamil_j.part2

import kamil_j.utils.RandomArrayGenerator
import spock.lang.Specification


class GenericSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGHT = 100
    RandomArrayGenerator arrayGenarator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        Integer[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT)
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should leave sorted array unchanged"() {
        given:
        Integer[] array = new Integer[TEST_ARRAY_LENGHT]
        GenericSortAlgorithm algorithm = sortingAlgorithm
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }


        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i], array[i - 1]) != -1
        }
        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should throw exeption if array is null"() {

        given:
        Integer[] array = null
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should not throw if array is zero sized"() {
        given:
        Integer[] array = new int[0]
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }

    def "should sort if elements are not unique"() {
        given:
        Integer [] array = arrayGenarator.generate(TEST_ARRAY_LENGHT, 0, 10);
        GenericSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert Integer.compare(array[i], array[i - 1]) != -1
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleGenericSortAlgorithm()
    }
}