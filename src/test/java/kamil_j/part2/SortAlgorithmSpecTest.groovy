package kamil_j.part2

import kamil_j.utils.RandomArrayGenerator
import spock.lang.Specification


class SortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGHT = 100
    RandomArrayGenerator arrayGenarator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

    def "should sort array depends on the order"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i - 1])

        }

        where:
        _ | sortingAlgorithm          | isAscending | comparison
        _ | new BubbleSortAlgorithm() | true        | Integer.&max
        _ | new BubbleSortAlgorithm() | false       | Integer.&min
        _ | new HeapSortAlgorithm()   | true        | Integer.&max
        _ | new HeapSortAlgorithm()   | false       | Integer.&min

    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGHT]
        SortAlgorithm algorithm = sortingAlgorithm
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }


        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] > array[i - 1]

        }
        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

    def "should throw exeption if array is null"() {

        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()


    }

    def "should not throw if array is zero sized"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()

    }

    def "should sort if elements are not unique"() {
        given:
        int[] array = arrayGenarator.generate(TEST_ARRAY_LENGHT, 0, 10);
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]

        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()

    }
}

