package kamil_j.part4

import spock.lang.Specification
import spock.lang.Unroll


class StackSpecTest extends Specification {

    def "should push element to empty stack"() {

        given:
        String toPush = "test"
        Stack<String> stack = stackObject

        when:
        stack.push(toPush)

        then:
        toPush == stack.push()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

//    @Unroll("#numberA + #numberB should return #numbersSum")
//    def "adding two numbers should return sum"() {
//        given:
//        int a = numberA
//        int b = numberB
//        int expected = numbersSum
//
//        when:
//        int actual = a + b
//
//        then:
//        actual == expected
//
//        where:
//        numberA | numberB | numbersSum
//        2       | 3       | 5
//        0       | 0       | 0
//
//    }

    def "should push element to filled stack"() {

        given:
        String toPush = "test"
        Stack<String> stack = stackObject

        when:
        stack.push(toPush)

        then:
        toPush == stack.push()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should delete value after poping"() {

        given:
        String toPush = "test"
        Stack<String> stack = stackObject
        stack.push("abc")

        when:
        stack.pop()

        then:
        null == stack.push()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should receive value when peeking but not delete it"() {

        given:
        Stack<String> stack = stackObject
        String toPush = "test"

        when:
        stack.peek()

        then:
        stack.peek() == toPush

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return zero as size of empty stack"() {

        given:
        Stack<String> stack = stackObject

        expect:
        stack.size() == 0

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return correct size for filled stack"() {
        given:
        Stack<String> stack = stackObject
        stack.push("abc")

        expect:
        1 == stack.size()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return true if there were no values put on stack"() {

        given:
        Stack<String> stack = stackObject

        expect:
        stack.isEmpty()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

//    def "should return false if push was called previosuly"() {
//    }
//    def "should return true if element was pushed and poped"() {
//    }

    def "isEmpty should return true if there are no values on stack"() {

        given:
        Stack<String> stack = stackObject

        expect:
        stack.isEmpty()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "isEmpty should return false if there are values on stack"() {

        given:
        Stack<String> stack = stackObject
        stack.push("abc")

        expect:
        !stack.isEmpty()

        where:
        _ | stackObject
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }
}
