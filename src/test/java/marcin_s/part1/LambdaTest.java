package marcin_s.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaTest {

    @Test
    public void shouldCreatelambdas(){
        Runnable runnable = () -> {};
        Supplier<Integer> supplier = () -> 90; // lambda zwraca stałą wartość
        useSupplier(supplier);
        useSupplier(() -> 5);
        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };
        consumer.accept(10); // tak uzywamy tę funkcję
        Function<Integer,String> function = arg -> "arg is " +arg;
        System.out.println(function.apply(25)); //wynikiem będzie ciąg znaków arg is 25

        TestInterface testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + " " +c + " = "+ result);
            return result;
        };
        testLambda.test('a', "Test"); //drugi argument jest przekazywany do lambdy czy jakos tak
    }
    @Test
    public void methodreferences(){
        Consumer<Integer> consumer = System.out::println;
        Consumer<Integer> consumerLambda = x -> System.out.println(x); // to jest ekwiwalentem linijki powyzej

        consumer.accept(2);
        consumerLambda.accept(2);

        Random random = new Random();
        random.nextInt();
        useSupplier((random::nextInt));
    }

    private void useSupplier(Supplier<Integer> supplier){
        System.out.println(supplier.get());
    }

    // chcemy, zeby nasza aplikacja (interface) przyjmowala znak i iomentarz i zamieniala go na inta

}
