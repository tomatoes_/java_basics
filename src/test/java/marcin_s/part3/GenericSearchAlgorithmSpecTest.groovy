package marcin_s.part3

import marcin_s.utils.RandomArrayGenerator
import spock.lang.Specification

class GenericSearchAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100
    int INVALID_INDEX = -1

    def "should find element in random array"() {

        given:
        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
        int testIndex = new Random().nextInt(array.length)
        int valueToFind = array[testIndex]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, { v -> v == valueToFind })

        then:
        array[actualIndex] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()

    }

    def "should find element in sorted array"() {

        given:
        Integer[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int expectedIndex = new Random().nextInt(array.length)
        int valueToFind = array[expectedIndex]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, { v -> v == valueToFind })

        then:
        array[actualIndex] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()


    }

    def "should return invalid index if element not present"() {

        given:
        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100)
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, { v -> false })

        then:
        INVALID_INDEX == actualIndex

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()


    }

    def "should return invalid index if array length is zero"() {

        given:
        Integer[] array = new Integer[0]
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer invalidValue = new Random().nextInt()


        when:
        int actualIndex = algorithm.search(array, { v -> v == invalidValue })

        then:
        INVALID_INDEX == actualIndex

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()


    }

    def "should throw exception if array is null"() {

        given:
        Integer[] array = null
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer randomValue = 101


        when:
        algorithm.search(array, { v -> v == randomValue })

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()


    }
}