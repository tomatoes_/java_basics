package marcin_s.part4

import spock.lang.Specification

class StackTest extends Specification {

    def "should push element to empty stack"() {
        given:
        Stack<Integer> stack = stackImplemetation
        Integer valueToPush = 100

        when:
        stack.push(valueToPush)

        then:
        valueToPush == stack.peek()

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)

    }

    def "should push element to filled stack"() {
        given:
        Stack<Integer> stack = stackImplemetation
        stack.push(120)
        Integer valueToPush = 10

        when:
        stack.push(valueToPush)

        then:
        stack.peek() == valueToPush

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should delete element after popping"() {
        given:
        Stack<Integer> stack = stackImplemetation
        stack.push(120)

        when:
        stack.pop()

        then:
        null == stack.peek()

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should receive element when peeking but not delete it"() {
        given:
        Stack<Integer> stack = stackImplemetation
        stack.push(120)

        when:
        Integer peekedValue = stack.peek()

        then:
        peekedValue == 120

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return zero as size of empty stack"() {
        given:
        Stack<Integer> stack = stackImplemetation
        int expectedSize = 0

        when:
        int actualSize = stack.size()

        then:
        expectedSize == actualSize

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return correct size in filled stack"() {
        given:
        Stack<Integer> stack = stackImplemetation
        int testPushes = new Random().nextInt(100) + 1
        for (int i = 0; i < testPushes; i++) {
            stack.push(new Random().nextInt())
        }
        int expectedSize = testPushes

        when:
        int actualSize = stack.size()

        then:
        expectedSize == actualSize

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(100)
    }

    def "should return true if there were no value put on stack"() {
        given:
        Stack<Integer> stack = stackImplemetation

        when:
        boolean checkIfStackIsEmpty = stack.isEmpty()

        then:
        checkIfStackIsEmpty == true

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)

    }

    def "should return false if push was called previously"() {
        given:
        Stack<Integer> stack = stackImplemetation
        stack.push(120)

        when:
        boolean checkIfStackIsEmpty = stack.isEmpty()

        then:
        !checkIfStackIsEmpty

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should return true if element was pushed and peek"() {
        given:
        Stack<Integer> stack = stackImplemetation
        stack.push(120)
        stack.pop()

        when:
        boolean checkIfStackIsEmpty = stack.isEmpty()

        then:
        checkIfStackIsEmpty

        where:
        _ | stackImplemetation
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

}
