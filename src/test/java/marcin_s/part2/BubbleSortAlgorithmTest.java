package marcin_s.part2;

import org.junit.Assert;
import org.junit.Test;

public class BubbleSortAlgorithmTest {

    private static final int TEST_ARRAY_LENGHT = 100;

    @Test
    public void shouldBubbleSortAnArray() {

        // given:
        int[] anArray = {3, 8, 9, 2, 5, 8, 1, 0};
        int[] sortedArray = {0, 1, 2, 3, 5, 8, 8, 9};

        // when:
        new BubbleSort().sortBubble(anArray);

        //then:
        Assert.assertArrayEquals(sortedArray,anArray);
    }


}