package marcin_s.part2

import marcin_s.utils.RandomArrayGenerator
import spock.lang.Specification

import static org.junit.Assert.*


class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 15

    def "should leave zero-sized array unchanged"() {

        given:
        int[] array = new int[0]
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> true })

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()

    }

    def "should divide into two groups"() {

        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100);
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> e < 50 })

        then:
        boolean currentGroup = true
        boolean wasGroupChanged = false
        for (int element : array) {
            if (element >= 50 && currentGroup == true) {
                currentGroup = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged) {
                fail()
            }
        }
//        Different way
//        int changesAmount = 0
//        int changePoint = 0
//
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] < 50) {
//                changesAmount++
//            } else {
//                changePoint = i
//                break
//            }
//        }
//        for (int i = changePoint; i < array.length; i++) {
//            if (array[i] >= 50) {
//                changesAmount++
//            } else
//                break
//        }
//        assert changesAmount == TEST_ARRAY_LENGTH

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()

    }

    def "should divide into three groups"() {

//        Test for dutch flag sort algorithm

        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100);
        GroupSortAlgorithm algorithm = sortingAlgorithm
        int lower = 33
        int higher = 66

        when:
        algorithm.sort(array, { e -> lower < e }, { e -> e < higher })
        for (int e : array) {
            println e
        }

        then:
        int numberChanges = 0
        for (int element : array) {
            if (element >= lower && numberChanges == 0) {
                numberChanges++
            } else if (element >= higher && numberChanges == 1) {
                numberChanges++
            } else if ((element < lower && numberChanges != 0) || (element > higher && numberChanges != 2)) {
                fail()
            }
        }
        where:
        _ | sortingAlgorithm
        _ | new DutchFlagGroupSortAlgorithm()

    }


    def "should throw exception if array is null"() {
        given:
        int[] array = null
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> true })

        then:
        thrown IllegalArgumentException

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()

    }


}