package marcin_s.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.internal.ArrayComparisonFailure;


import java.util.function.Supplier;

import static org.hamcrest.core.Is.is;


public class RandomArrayGeneratorTest {
    private static final int TEST_ARRAY_LENGTH = 100;

    @Test
    public void shouldGenerateNumbersInGivenNumbers() {

//        given:

        int testRangeFrom = -100;
        int testRangeTo = 20;

//        when:

        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

//        then:

        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }
    }

    @Test (expected = IllegalArgumentException.class)
    public void shouldThrowexceptionIfRangeIsInvalid(){

//        given:

        int testRangeFrom = 10;
        int testRangeTo = 5;

//        when:

        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

//////////////////////////////  Second version  //////////////////////////////////////////////////

        /*
        try {
            int array[] = new RandomArrayGenerator()
                    .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);
            //fail("Range is invalid");
       } catch (IllegalArgumentException e){
            Assert.assertThat(e.getMessage(), is("Range is invalid"));
       }
       */
    }

    @Test
    public void shouldCheckWheterAmountOfGeneratedNumbersIsAccurate(){

//        given:
        int testLength = 120;

//        when:
        int randomArray[] = new RandomArrayGenerator().generate(testLength);

//        then:
        Assert.assertEquals(testLength, randomArray.length);

    }

    @Test (expected = ArrayComparisonFailure.class)
    public void shouldCheckWheterGeneratedArraysAreDifferent(){

        int randomArray1[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH);
        int randomArray2[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH);

        Assert.assertArrayEquals(randomArray1, randomArray2);

    }
    @Test
    public void shouldGenerateZeroesArray(){

        testRun(()-> System.out.println("SortAlgoritmTest"));
    }

    public void testRun(Runnable runnable){
        runnable.run();
    }
}
