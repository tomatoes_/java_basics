package maciej_m.part1;

import maciej_b.part1.TestInterfejs;
import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-02-25.
 */
public class LambdaTest {

    @Test
    public void shouldCreateLambda () {
        Runnable runnable = () -> {
        };     // Tworzenie lambdy
        // lambdy można robić po każdym interfejsie z jedną metodą ...
        Supplier<Integer> supplier = () -> 52;   // to oznacza, że lambda zwraca stałą wartość, lub
//                                                    zmienną jeśli obliczenia mieszczą się w jednej linijce kodu
        System.out.println(supplier.get());
        useSuplplier(() -> 5);

        Consumer<Integer> consumer = arg -> {    //arg - argument, dane, wyrażenie
            System.out.println(arg);
        };
        consumer.accept(10);

        Function<Integer, String> function = arg -> " arg is " + arg;  // zamienia wejscie na wyjście
        System.out.println(function.apply(25));

        TestInterfejs testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + " " + c + "=" + result);
            return result;
        };

        testLambda.test('a', "Test");


    }

    private void useSuplplier(Supplier<Integer> supplier) {
        System.out.println(supplier.get());
    }


    public void methodReferences() {
        Consumer<Integer> consumer = System.out::println;
        Consumer<Integer> consumerLambda = x -> System.out.println (x);

        consumer.accept(2);
        consumerLambda.accept(2);


        Random random = new Random();
        random.nextInt();


        useSuplplier(random::nextInt);
    }
}


// supplier - pobiera wartości
// consumer - pobiera i działa na wartościach