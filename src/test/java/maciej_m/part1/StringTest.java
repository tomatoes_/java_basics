package maciej_m.part1;

import org.junit.Test;

import static org.junit.Assert.assertSame;

/**
 * Created by RENT on 2017-02-28.
 */
public class StringTest {

    @Test
    public void StringObjectsShouldHaveTheSameReferenceIfAssigned() {
        String a = "SortAlgoritmTest";
        String b = "SortAlgoritmTest";
        String c = a;

        assertSame(a, b);

        if (a == b){
            System.out.println("są równe");
        }else {
            System.out.println(" są róne ");
        }
    }
}
