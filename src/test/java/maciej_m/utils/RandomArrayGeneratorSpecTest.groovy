package maciej_m.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by RENT on 2017-02-25.
 */
class RandomArrayGeneratorSpecTest extends Specification {


    private static final int TEST_ARRAY_LENGHT = 100


    @Unroll ("should generate number in range from #testRangeFrom to #testRangeTo")
    def "should Generate Number In Given Range"() {

        when:
        int[] array = new maciej_b.utils.RandomArrayGenerator().
                generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo)


        then:
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo)
        }

        where:
        testRangeFrom | testRangeTo
        -10             | 20
        10              | 30
        0               | 50
        -100            | 0
        -50             | -20
    }

    @Unroll ("should generate array of lenght = #lenght")
    def "should generate array given length"(){
        when:
        int[] array = new maciej_b.utils.RandomArrayGenerator().generate(length)

        then:
        assert array.length == length

        where:
        _| length
        _| 10
        _| 20
        _| 30
    }

    @Unroll("should throw NegativeArraySizeException for negative lenghts = #length")
    def "should throw NegativeArraySizeException for negative lenghts"(){

        when:
        int array = new maciej_b.utils.RandomArrayGenerator().generate(length)

        then:

        thrown(NegativeArraySizeException)

        where:
        _| length
        _| -10
        _| -50
    }

    def " should generate two different arrays"() {


        given:
        maciej_b.utils.RandomArrayGenerator generator = new maciej_b.utils.RandomArrayGenerator()
        int[] arrayA
        int[] arrayB

        when:
        arrayA  = new maciej_b.utils.RandomArrayGenerator().generate(TEST_ARRAY_LENGHT)
        arrayB = new maciej_b.utils.RandomArrayGenerator().generate(TEST_ARRAY_LENGHT)

        then:
        for (int i = 0; i < TEST_ARRAY_LENGHT; ++i) {
            if (arrayA[i] != arrayB[i])
                return
        }
        assert fail("array are equal")
    }


}
