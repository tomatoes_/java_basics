package irek_b.part2

import irek_b.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100;
    //RandomArrayGenerator arrayGenerator = new RandomArrayGenerator().generate(100,0,100)

        def "should leave zero-sized array unchanged"() {
            given:
            int[] array = new int[0];
            GroupSortAlgorithm algorithm = grouping;

            when:
            algorithm.sort(array, { e -> e < 50 })

            then:
            noExceptionThrown();

            where:
            _ | grouping
            _ | new PolishFlagGroupSortAlgorithm()
        }

        def "should divide array into two groups"() {
            given:
            int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH,0,100);
            GroupSortAlgorithm algorithm = grouping;

            when:
            algorithm.sort(array, { e -> e < 50 })

            then:
            boolean  currentGroup = true;
            boolean wasGroupChanged = false;

            for(int element: array){
                if (element >= 50 && currentGroup){
                    currentGroup = false;
                    wasGroupChanged = true;
                }else if (element <50 && wasGroupChanged){
                    Assert.fail();
                }
            }

            where:
            _ | grouping
            _ | new PolishFlagGroupSortAlgorithm()

//            int changesAlmount = 0;
//            int changePoint = 0;
//
//            for (int i = 0; i < array.length; i++) {
//                if(array[i]<50){
//                    changesAlmount++
//                } else {
//                    changePoint = 1;
//                    break;
//                }
//            }
//            for (int i = changePoint; i < array.length; i++) {
//                if(array[i]>=50){
//                    changesAlmount++
//                } else {
//                    break;
//                }
//            }
//            assert changesAlmount == TEST_ARRAY_LENGTH;
        }

        def "should thow is array is null"() {

            given:
            int[] array = null
            GroupSortAlgorithm algorithm = grouping;

            when:
            algorithm.sort(array, { e -> e <= 5 })

            then:
            thrown(IllegalArgumentException)

            where:
            _ | grouping
            _ | new PolishFlagGroupSortAlgorithm()
        }

    }
