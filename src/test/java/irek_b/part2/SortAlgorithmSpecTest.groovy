package irek_b.part2

import irek_b.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-02.
 */
class SortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

    def "should sort array depending on the order"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i-1])
        }

        where:
        _ | sortingAlgorithm          | isAscending | comparison
        _ | new BubbleSortAlgorithm() | true        | Integer.&max
        _ | new BubbleSortAlgorithm() | false       | Integer.&min
        _ | new HeapSortAlgorithm() | true        | Integer.&max
        //_ | new HeapSortAlgorithm() | false       | Integer.&min
        //odkomentować jak będzie uzupełnione sortowanie malejące



    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i-1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }


    def "should throw exception if array is null"() {
        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

    def "should not throw is array is zero size"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

    def "should sorted if element is not unique"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new HeapSortAlgorithm()
    }

}