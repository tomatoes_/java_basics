package irek_b.part3

import irek_b.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import java.util.function.Predicate

/**
 * Created by RENT on 2017-03-06.
 */
class GenericSearchAlgorithmSpecTest extends Specification {

//    int TEST_ARRAY_LENGTH = 10
//    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()
//    int[] myArray = arrayGenerator.generate(TEST_ARRAY_LENGTH,0,10)
//
//    def "should find element in random array"() {
//        given:
//
//        Integer[] array = myArray;
//        Integer index = new Random().nextInt(array.length);
//        Integer valueToFind = array[index];
//        SearchAlgorithm algorithm = searchAlgorithm;
//
//        when:
//        int searchResult = algorithm.search(array, e->e.equals(array[valueToFind]));
//
//        then:
//        array[searchResult] == valueToFind;
//
//        where:
//        _ | searchAlgorithm
//        _ | GenericSearchAlgorithm()
//
//
//    def "should find element in sorted list"() {
//        given:
//
//        int[] array = new int[TEST_ARRAY_LENGTH];
//        for (int i = 0; i < array.length; i++) {
//            array[i] = i;
//        }
//        int index = new Random().nextInt(array.length);
//        int valueToFind = array[index];
//        SearchAlgorithm algorithm = searchAlgorithm;
//
//        when:
//        int searchResult = algorithm.search(array, valueToFind);
//
//        then:
//        array[searchResult] == valueToFind;
//
//        where:
//        _ | searchAlgorithm
//        _ | GenericSearchAlgorithm()
//    }
//
//    def "should return invalid index if element not present"() {
//        given:
//        int[] array = myArray;
//        int index = new Random().nextInt(array.length);
//        int valueToFind = array[index];
//        SearchAlgorithm algorithm = searchAlgorithm;
//
//        when:
//        int searchResult = algorithm.search(array, 40);
//
//        then:
//        Assert.assertTrue(searchResult == -1);
//
//        where:
//        _ | searchAlgorithm
//        _ | GenericSearchAlgorithm()
//
//    }
//
//    def "should return invalid index if array is zero"() {
//        given:
//        int[] array = new int[0];
//        SearchAlgorithm algorithm = searchAlgorithm;
//        int valueToFind = new Random().nextInt();
//
//        when:
//        int returnValue = algorithm.search(array, valueToFind)
//
//        then:
//        Assert.assertEquals(-1,returnValue)
//
//        where:
//        _ | searchAlgorithm
//        _ | GenericSearchAlgorithm()
//
//    }
//
//    def "should throw is array is null"() {
//        given:
//        int[] array = null
//        SearchAlgorithm algorithm = searchAlgorithm
//
//        when:
//        algorithm.search(array, 0)
//
//        then:
//        thrown(IllegalArgumentException)
//
//        where:
//        _ | searchAlgorithm
//        _ | GenericSearchAlgorithm()
//    }
//

}
