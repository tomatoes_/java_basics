package irek_b.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by RENT on 2017-02-25.
 */
public class LambdaTest {

    @Test
    public void shouldCreateLambdas(){
        Runnable runnable = () -> {};

        /////////////////////////////////////////////////

        Supplier<Integer> supplier = () -> 2;
        //System.out.println(supplier.get());
        useSupplier(supplier);
        useSupplier(() -> 5);

        /////////////////////////////////////////////////

        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };
        consumer.accept(10);

        /////////////////////////////////////////////////

        Function<Integer, String> function = arg -> "arg is " + arg;
        System.out.println(function.apply(25));

        /////////////////////////////////////////////////

        TestInterface testLambda  = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + " " + c + " = " + result);
            return result;
        };
        testLambda.test('a', "Test");

        //////////////////////////////////////////////////

        methodReferences();
    }




    private void useSupplier(Supplier<Integer> supplier){
        System.out.println(supplier.get());
    }

    public void methodReferences(){
        Consumer<Integer> consumer = System.out::println;
        Consumer<Integer> comsumerLambda = x -> System.out.println(x);

        consumer.accept(2);
        comsumerLambda.accept(2);

        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);
    }

}
