package maciej_b.part3

import maciej_b.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import java.util.function.Predicate

/**
 * Created by RENT on 2017-03-06.
 */
class GenericSearchAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in random array"() {

        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        Integer searchResult =  algorithm.search(array, { p -> p == valueToFind})


        then:
        array[searchResult].equals(valueToFind)

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
    }

    def " should find element in sorted array"() {
        given:
        Integer[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        Integer index = new Random().nextInt(array.length)
        Integer valueToFind = array[index]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, {e -> e == valueToFind})

        then:
        array[searchResult].equals(valueToFind)

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
    }

    def " should return invalid index if element not present"() {
        given:
        Integer[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 20);
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer invalidValue = 50;

        when:
        int returnIndex = algorithm.search(array, {e -> false})

        then:
        Assert.assertTrue(returnIndex == -1)


        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
    }

    def "should return invalid index if array length is zero"() {
        given:
        Integer [] array = new int[0]
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer valueToFind = new Random().nextInt()

        when:
        Integer returnValue = algorithm.search(array, {e -> e == valueToFind})

        then:
        Assert.assertEquals(-1, returnValue)
//        noExceptionThrown()


        where:
        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }  // correct

    def "should throw if array is null"() {
        Integer [] array = null;
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        int value = 0
        when:
        algorithm.search(array, {e -> e == value})

        then:
        thrown (IllegalArgumentException)

        where:
        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }   // correct
}


