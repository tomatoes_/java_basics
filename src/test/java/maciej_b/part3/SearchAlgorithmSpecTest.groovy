package maciej_b.part3

import maciej_b.part2.BubbleSortAlgorithm
import maciej_b.part2.SortAlgorithm
import maciej_b.utils.RandomArrayGenerator
import org.junit.Assert
import org.omg.CORBA.DynAnyPackage.Invalid
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class SearchAlgorithmSpecTest extends Specification {



    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in random array"() {

        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()

    }

    def " should find element in sorted array"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:
        array[searchResult].equals(valueToFind)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()
    }

    def " should return invalid index if element not present"() {
        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 20);
        SearchAlgorithm algorithm = searchingAlgorithm
        int invalidValue = 50;

        when:
        int returnIndex = algorithm.search(array, invalidValue)

        then:
        Assert.assertTrue(returnIndex == -1)


        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()
    }

    def "should return invalid index if array length is zero"() {
        given:
        int [] array = new int[0]
        SearchAlgorithm algorithm = searchingAlgorithm
        int valueToFind = new Random().nextInt()

        when:
        int returnValue = algorithm.search(array, valueToFind)

        then:
        Assert.assertEquals(-1, returnValue)
//        noExceptionThrown()


        where:
        _| searchingAlgorithm
        _| new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()
    }  // correct

    def "should throw if array is null"() {
        int [] array = null;
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        algorithm.search(array, 0)

        then:
        thrown (IllegalArgumentException)

        where:
        _| searchingAlgorithm
        _| new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()
    }   // correct
}
