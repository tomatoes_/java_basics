package maciej_b.part4

import spock.lang.Specification


/**
 * Created by RENT on 2017-03-07.
 */
class StackSpecTest extends Specification {


    def "should push element to empty stack"(){
        given:
        String toPush = "test"
        Stack<String> stack = stackInterface

        when:
        stack.push(toPush)


        then:
        toPush ==  stack.peek()

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)

    }

    def "should push element to filed stack"(){
        given:
        String toPush = "test"
        Stack<String> stack = stackInterface
        stack.push("abc")   // bo mamy miec zapełniony stos
        when:
        stack.push(toPush)


        then:
        stack.peek() == toPush

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "should delete value after poping"(){
        given:
        String toPush = "test"
        Stack<String> stack = stackInterface
        stack.push(toPush)

        when:
        stack.pop()


        then:
        stack.peek() == null

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "receive value when peeking but not delete it "(){
        given:
        String toPush = "test"
        Stack<String> stack = stackInterface
        stack.push("abc")

        when:
        stack.peek()


        then:
        stack.peek() == "abc"

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "should return zero as size of empty stack"(){
        given:

        Stack<String> stack = stackInterface


        expect:
        stack.size() == 0

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "should return correct size for filled stack "(){
        given:
        Stack<String> stack = stackInterface
        stack.push("abc")

        when:
        int size = stack.size()

        then:
        size == 1

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "should return true if there were no values put on stack"(){
        given:
        Stack<String> stack = stackInterface


        expect:
        stack.isEmpty()

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }

    def "should return false if push was called previously"(){
        given:
        Stack<String> stack = stackInterface
        stack.push("abc")


        expect:
        !stack.isEmpty()

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)

    }

    def "should return true if element was pushed and poped from stack"(){
        given:
        Stack<String> stack = stackInterface
        stack.push("abc")
        stack.pop()

        expect:
        stack.isEmpty()

        where:
        _| stackInterface
        _| new DynamicStack()
        _| new ArrayStack(10)
    }


}
