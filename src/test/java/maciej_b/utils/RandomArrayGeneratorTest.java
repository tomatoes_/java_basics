package maciej_b.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGeneratorTest {

    private static final int TEST_ARRAY_LENGHT = 100;
    @Test
    public void shouldGenerateNumberInGivenRange() {
        // Given
        int testRangeFrom = 10;
        int testRangeTo = 20;

        //when
        int[] array = new RandomArrayGenerator().
                generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        //then
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }
    }

    @Test (expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfRangeIsInvalid() {

        int testRangeFrom = 20;
        int testRangeTo = 10;

        int array[] = new RandomArrayGenerator()
                    .generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

    }

    @Test
    public void shouldCheckRandomArrayLenght(){

        int testRandomArrayLenght = 200;

        int array[] = new  RandomArrayGenerator().generate(testRandomArrayLenght);

        Assert.assertEquals(testRandomArrayLenght, array.length);
    }

    @Test
    public void shouldCheckRandomArrayAreDifferent(){

        int testRandomArrayFirst[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);
        int testRandomArraySecond[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT);

        Assert.assertNotSame(testRandomArrayFirst, testRandomArraySecond);
    }

    @Test
    public void lambdaTest() {

        Runnable r  = () ->{System.out.println("SortAlgoritmTest");   // LAMBDA
        };

        Runnable runable = new Runnable() {
            @Override
            public void run() {
                System.out.println("SortAlgoritmTest");
            }
        };
        r.run();
       runable.run();
    }



    public void testRun (Runnable runnable){
        runnable.run();
    }


}
