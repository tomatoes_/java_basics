package maciej_b.part1;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static maciej_b.part1.Person.getRandomPerson;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.*;

public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;

    @Test
    public void shouldFindPersonAndCollectAsPairs() {
//        Person person = getRandomPerson();
//        System.out.println(person);

        Person[] people = getRandomPersons();
        /*
        Sukamy osoby która:
        1. mają pierwse imie acynające się literami A- D
                2. są meczynami
                3.łącymy pesel i kod pocztowy
                */


        Stream.of(people)   // tworzenie strima
                .map(this::print)    // funkcja na podstawie tej metody poniżej
                // wyświetla wszystko co dostała
                // private<T> T print (T a){
//          System.out.println(a);
//                return a;
//       }
                .filter(person -> person.getGender() == 'M')
                .filter(person -> person.getFirstName().matches("^[A-Da-d].*"))
                .map(person -> Pair.of(person.getPesel(), person.getAddress().getPostalCode()))
                .collect(toList());
//                .forEach (System.out::println);
    }

    private Person[] getRandomPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }
//        this.<Integer > print(5, "Test");
//        this.<Object > print(5, "Test");
//        this.<Object> print (5, "Test");




    @Test
    public void shouldFindPersonAndCollectAsPair() {
        Person[] people = getRandomPersons();

        Stream.of(people)
                .filter(person -> person.getLastName().toUpperCase().contains("M"))    // matches (".*[M m].*"))
//                .filter(person -> person.getLastName().matches(".*[Mm].*"))
                .filter(person -> Integer.parseInt(person.getPesel()
                        .substring(0, 2)) <=
                        Calendar.getInstance().get(Calendar.YEAR) - 1960)   // pobiera aktualny rok.
                .filter(person -> person.getAddress()
                        .getPostalCode()
                        .startsWith("20"))
                .map(this::print);
//                .collect(Collectors.toSet());

    }
    private <T> T print(T a) {
        System.out.println(a);
        return a;
    }
}
