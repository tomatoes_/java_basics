package maciej_b.part2

import maciej_b.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by RENT on 2017-03-04.
 */
class GenericSortAlgorithmSpecTest extends Specification {

        int TEST_ARRAY_LENGTH = 100;
        RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

        def "should sort array ascending by default"(){
            given:
            Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
            GenericSortAlgorithm algorithm = sortingAlgorithm

            when:
            algorithm.sort(array)

            then:
            for (int i = 1; i < array.length ; i++) {
                assert Integer.compare(array[i], array[(i-1)]) != -1
            }

            where:
            _| sortingAlgorithm
            _| new BubbleGenericSortAlgorithm()
        }

//        def "should sort array depending on the order"(){
//            given:
//            int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
//            GenericSortAlgorithm algorithm = sortingAlgorithm
//
//            when:
//            algorithm.sort(array, isAscending)
//
//            then:
//            for (int i = 1; i < array.length ; i++) {
//                println array[i] +" "+  array[i - 1] +" "+ comparison(array[i], array[i - 1])
//            }
//
//            where:
//            _| sortingAlgorithm           | isAscending  | comparison
//            _| new BubbleSortAlgorithm()  | true         | Integer.&max
//            _| new BubbleSortAlgorithm()  | false        | Integer.&min
//            _| new QuickSortAlgorithm()   | true         | Integer.&max
//            _| new QuickSortAlgorithm()   | false        | Integer.&min
//        }

        def "should leave sorted array unchanged"(){
            given:
            Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
            for (int i = 0; i < array.length; i++) {
                array [i] = 1
            }
            GenericSortAlgorithm algorithm = sortingAlgorithm

            when:
            algorithm.sort(array)

            then:
            for (int i = 1; i < array.length ; i++) {
                assert new Integer(array[i]).compareTo(array[(i-1)]) != -1
            }

            where:
            _| sortingAlgorithm
            _| new BubbleGenericSortAlgorithm()
        }

        def "should throw exception if array is null"(){
            given:
            Integer [] array = null;
            GenericSortAlgorithm algorithm = sortingAlgorithm

            when:
            algorithm.sort(array)

            then:
            thrown(IllegalArgumentException)

            where:
            _| sortingAlgorithm
            _| new BubbleGenericSortAlgorithm()
        }

        def "should not throw is array is zero-sized"(){
            given:
            Integer[] array = new int[0]
            GenericSortAlgorithm algorithm = sortingAlgorithm

            when:
            algorithm.sort(array)

            then:
            for (int i = 1; i < array.length ; i++) {
                assert array[i] >= array[(i-1)]
            }

            where:
            _| sortingAlgorithm
            _| new BubbleGenericSortAlgorithm()
        }

        def "should sort array if element are not unique"(){
            given:
            Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
            GenericSortAlgorithm algorithm = sortingAlgorithm

            when:
            algorithm.sort(array)

            then:
            for (int i = 1; i < array.length ; i++) {
                assert Integer.compare(array[i], array[(i-1)]) != -1
            }

            where:
            _| sortingAlgorithm
            _| new BubbleGenericSortAlgorithm()
        }






    }


