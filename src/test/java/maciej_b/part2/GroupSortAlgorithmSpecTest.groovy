package maciej_b.part2

import maciej_b.utils.RandomArrayGenerator
import org.junit.Assert

import static org.junit.Assert.assertArrayEquals
import spock.lang.Specification

import java.util.function.Predicate

/**
 * Created by RENT on 2017-03-04.
 */
class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should leave zero-sized array unchanged" () {

        given:
        int [] array = new int[0]
        GroupSortAlgorithm algorithm = grouping

        when:
        algorithm.sort(array, {e -> e <=50})

        then:
        noExceptionThrown()

        where:
        _| grouping
        _| new PolishFlagGroupSortAlgorithm()
        _| new NetherlandFlagGroupSortAlgorithm()
    }

    def "should divide into two group" () {

        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100)
        GroupSortAlgorithm algorithm = grouping

        when:
        algorithm.sort(array, { e -> e < 50 })

        then:
        boolean currentGroupe = true // deklarujemy że lewa strona to true
        boolean wasGroupChanged = false //deklaracja, że drupa się zmieniła
        for (int element : array) {
            if (element >= 50 && currentGroupe) { // jeśli jesteśmy w grupie drugiej
                currentGroupe = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged) {
                //i znalazł się w grupie drugiej element z pierwszej grupy to wywalamy błąd
                Assert.fail()
            }
        }

        where:
        _ | grouping
        _| new PolishFlagGroupSortAlgorithm()
//        _| new NetherlandFlagGroupSortAlgorithm()

    }

    def "should throw if array is null"(){
        given:
        int [] array = null;
        GroupSortAlgorithm algorithm = grouping

        when:
        algorithm.sort(array, {e -> true})

        then:
        thrown(IllegalArgumentException)

        where:
        _| grouping
        _| new PolishFlagGroupSortAlgorithm()
//        _| new NetherlandFlagGroupSortAlgorithm()
    }


    //    def "should leave with one group unchanged"() {
//        // jeśli jest jedna grupa to jej nie zmienia
//        given:
//        int [] expectedArray = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
//        int [] actualArray = expectedArray.clone()
//        GroupSortAlgorithm algorithm = grouping
//
//        when:
//        algorithm.sort(expectedArray, {e->true})
//
//        then:
//        assertArrayEquals(expectedArray, actualArray)
//
//        where:
//        _| grouping
//        _| new PolishFlagGroupSortAlgorithm()
//
//    }


    }

