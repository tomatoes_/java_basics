package maciej_b.part2

import maciej_b.utils.RandomArrayGenerator
import spock.lang.Specification


class SortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {
            assert array[i] >= array[(i-1)]
        }

        where:
        _| sortingAlgorithm
        _| new BubbleSortAlgorithm()
        _ | new QuickSortAlgorithm()
    }

    def "should sort array depending on the order"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length ; i++) {
            println array[i] +" "+  array[i - 1] +" "+ comparison(array[i], array[i - 1])
        }

        where:
        _| sortingAlgorithm           | isAscending  | comparison
        _| new BubbleSortAlgorithm()  | true         | Integer.&max
        _| new BubbleSortAlgorithm()  | false        | Integer.&min
        _| new QuickSortAlgorithm()   | true         | Integer.&max
        _| new QuickSortAlgorithm()   | false        | Integer.&min
    }

    def "should leave sorted array unchanged"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        for (int i = 0; i < array.length; i++) {
            array [i] = 1
        }
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {
            assert array[i] >= array[(i-1)]
        }

        where:
        _| sortingAlgorithm
        _| new BubbleSortAlgorithm()
        _| new QuickSortAlgorithm()
    }

    def "should throw exception if array is null"(){
        given:
        int [] array = null;
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _| sortingAlgorithm
        _| new BubbleSortAlgorithm()
        _| new QuickSortAlgorithm()
    }

    def "should not throw is array is zero-sized"(){
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {
            assert array[i] >= array[(i-1)]
        }

        where:
        _| sortingAlgorithm
        _| new BubbleSortAlgorithm()
        _| new QuickSortAlgorithm()
    }

    def "should sort array if element are not unique"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length ; i++) {
            assert array[i] >= array[(i-1)]
        }

        where:
        _| sortingAlgorithm
        _| new BubbleSortAlgorithm()
        _| new QuickSortAlgorithm()
    }






}
