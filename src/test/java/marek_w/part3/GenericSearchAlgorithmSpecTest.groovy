package marek_w.part3

import marek_w.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by marekwojtowicz on 06.03.2017.
 */
class GenericSearchAlgorithmSpecTest extends Specification {

    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()
    int TEST_ARRAY_LENGHT = 100;

    def "should find value in random array"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, {e -> e.equals(valueToFind)})

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()
    }

    def "should find element in sorted array"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)

        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }

        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, {e -> e.equals(valueToFind)})

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()

    }

    def "should return invalid index if element not present"(){
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int search = algorithm.search(array, {e -> false})

        then:
        Assert.assertEquals(-1, search)

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()

    }

    def "should return invalid index if array length is zero"(){
        given:
        Integer[] array = new int[0]
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        int valueToFind = new Random().nextInt()

        when:
        int returnValue = algorithm.search(array, {e -> e.equals(valueToFind)})

        then:
        Assert.assertEquals(-1, returnValue)

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()

    }

    def "should thorw if array is null"(){
        given:
        Integer[] array = null
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        algorithm.search(array, {e -> false})

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new GenericNaiveSearchAlgorithm()

    }

}
