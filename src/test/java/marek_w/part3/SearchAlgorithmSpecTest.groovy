package marek_w.part3
import marek_w.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
class SearchAlgorithmSpecTest extends Specification {

    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()
    int TEST_ARRAY_LENGHT = 100;

    def "should find value in random array"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind) //serach znajduje indeks elemtnu szukanego

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
    }

    def "should find element in sorted array"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)

        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }

        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should return invalid index if element not present"(){
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 1, 10)
        SearchAlgorithm algorithm = searchingAlgorithm
        int invalidValue = 60;

        when:
        int search = algorithm.search(array, invalidValue)

        then:
        Assert.assertEquals(-1, search)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()


    }

    def "should return invalid index if array length is zero"(){
        given:
        int[] array = new int[0]
        SearchAlgorithm algorithm = searchingAlgorithm
        int valueToFind = new Random().nextInt()

        when:
        int returnValue = algorithm.search(array, valueToFind)

        then:
        Assert.assertEquals(-1, returnValue)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()


    }

    def "should thorw if array is null"(){
        given:
        int[] array = null
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        algorithm.search(array, 0)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

}
