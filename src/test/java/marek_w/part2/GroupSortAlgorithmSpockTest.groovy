package marek_w.part2

import marek_w.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import static org.junit.Assert.assertArrayEquals

/**
 * Created by marekwojtowicz on 04.03.2017.
 */
class GroupSortAlgorithmSpockTest extends Specification {

    int TEST_ARRAY_LENGHT = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should leave zero-sized array unchanged"() {
        given:
        int[] array = new int[0]
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, {e -> e <= 5}) // w { } nie ma znaczenia co będzie, możę być np e -> true

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()


    }

    def "should divide array into two groups"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 0, 100)
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, {e -> e < 50})

        then:
        boolean currentGroup = true
        boolean wasGroupChanged = false
        for (int element:array) {
            if (element >= 50 && currentGroup) {
                currentGroup = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged) {
                Assert.fail()
            }
        }

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()
    }

    //ten test jest niepotrzebny
//    def "should leave array with one group unchanged"() {
//        given:
//        int[] expectedArray = arrayGenerator.generate(TEST_ARRAY_LENGHT)
//        int[] actualArray = expectedArray.clone()
//        GroupSortAlgorithm algorithm = sortingAlgorithm
//
//        when:
//        algorithm.sort(actualArray, {e -> true})
//
//        then:
//        assertArrayEquals(expectedArray, actualArray)
//
//        where:
//        _ | sortingAlgorithm
//
//    }

    def "should throw if array is null"() {
        given:
        int[] array = null
        GroupSortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, { e -> true})

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
        _ | new PolishFlagGroupSortAlgorithm()

    }
}