package marek_w.part2

import marek_w.utils.RandomArrayGenerator
import spock.lang.Specification

/**
 * Created by marekwojtowicz on 02.03.2017.
 */
class SortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGHT = 10;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()


    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i-1]
        }

        where:
        _ | sortingAlgorithm
//        _ | new BubbleSortAlgorithm()
        _ | new MergeSortAlgorithm()
    }

    def "should sort array depending on the order"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i-1])
        }

        where:
        _ | sortingAlgorithm          | isAscending | comparison
//        _ | new BubbleSortAlgorithm() | true        | Integer.&max
//        _ | new BubbleSortAlgorithm() | false       | Integer.&min
        _ | new MergeSortAlgorithm()  | true        | Integer.&max
        _ | new MergeSortAlgorithm()  | false       | Integer.&min


    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGHT]

        SortAlgorithm algorithm = sortingAlgorithm

        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i-1]
        }

        where:
        _ | sortingAlgorithm
//        _ | new BubbleSortAlgorithm()
        _ | new MergeSortAlgorithm()
    }

    def "should throw exception if array is null"() {
        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm
//        _ | new BubbleSortAlgorithm()
        _ | new MergeSortAlgorithm()
    }

    def "should not throw if array is zero-sized"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        _ | sortingAlgorithm
//        _ | new BubbleSortAlgorithm()
        _ | new MergeSortAlgorithm()
    }

    def "should sort if elements are not unique"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 0, 20)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i-1]
        }

        where:
        _ | sortingAlgorithm
//        _ | new BubbleSortAlgorithm()
        _ | new MergeSortAlgorithm()
    }


}
