package marek_w.part1;


import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by marekwojtowicz on 28.02.2017.
 */
public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;

    @Test
    public void shouldFindPeopleAndCollectAsPairs() {
        Person[] people = getRandomPersons();
        Stream.of(people)
                .filter(person -> person.getGender() == 'M')  //tu nie zastosuje .matches bo gender() to char
                .filter(person -> person.getFirstName().matches("^[A-Da-d].*"))
                .map(person -> Pair.of(person.getPesel(), person.getAdress().getPostalCode()))
                .map(this::print)
                .collect(Collectors.toList());

    }


    @Test
    public void shouldFindPeople2() {
        Person[] people = getRandomPersons();
        Stream.of(getRandomPersons())
                .filter(person -> person.getLastName().matches(".*[Mm].*"))
//                .filter(person -> person.getLastName().toUpperCase().matches("M")) //mozna też tak
                .filter(person -> Integer.parseInt(person.getPesel()
                        .substring(0, 2)) <=
                        Calendar.getInstance().get(Calendar.YEAR) - 1960)
                .filter(person -> person.getAdress().getPostalCode().startsWith("20"))
                .map(this::print)
                .collect(Collectors.toSet());
    }

    private Person[] getRandomPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }

    private <T> T print (T a) {
        System.out.println(a);
        return a;
    }
}
