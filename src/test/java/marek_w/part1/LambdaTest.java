package marek_w.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by marekwojtowicz on 25.02.2017.
 */
public class LambdaTest {

    @Test
    public void shouldCreateLambdas() {
        Runnable runnable = () -> {};  //nie pobiera ale cos wykonuje
        Supplier<Integer> supplier = () -> 92; //lambda zwraca nam tutaj stałą wartość, więc nie musimy wstawiać { }
        useSupplier(supplier);
        useSupplier(() -> 7);

        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };

        consumer.accept(11);
        Function<Integer, String> function = arg -> "arg is "+arg;
        System.out.println(function.apply(90));;

        TestInterface testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment+" "+c+" = "+ result);
            return result;
        };
        testLambda.test('a',"Test");
    }


    public void useSupplier(Supplier<Integer> supplier) {
        System.out.println(supplier.get());
    }


    @Test
    public void methodReeferences() {
        Consumer<Integer> consumer = System.out::println; //System.out::println referencje do metody
        Consumer<Integer> consumerLambda = (x) -> System.out.println(x); //to samo co wyżej

        consumer.accept(2);
        consumerLambda.accept(3);

        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);
    }
}
