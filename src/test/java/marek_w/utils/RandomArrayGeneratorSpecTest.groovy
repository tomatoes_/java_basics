package marek_w.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by jacek on 25.02.17.
 */


class RandomArrayGeneratorSpecTest extends Specification {

    private static final int TEST_ARRAY_LENGTH = 100

    @Unroll ("should generate numbers in range from #testRangeFrom to #testRangeTo")

    def "should generate numbers in given range"() {

        when:

        int []array = new jacek_s.utils.RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo)

        then:

        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo)

        }

        where:

        testRangeFrom | testRangeTo
            -10       |  20
             10       |  30
             20       |  50
           -100       |  20
            -50       | -20

    }

    @Unroll ("should generate array of given length= #length")

    def "should generate array of given length"() {

        when:
        int[] array = new jacek_s.utils.RandomArrayGenerator().generate(length)

        then:

        assert array.length == length

        where:

        _| length // ten znak to dowolna zmienna
        _| 10
        _| 20
        _| 30

    }

    @Unroll ("should throw NegativeArraySizeException for negative length = #length")

    def "should throw NegativeArraySizeException for negative lengths"() {

        when:

        int[] array = new jacek_s.utils.RandomArrayGenerator().generate(length) // może być def zamiast int[]

        then:

        thrown(NegativeArraySizeException)

        where:

        _| length
        _| -10

    }

    def "should generate two diffrent arrays"() {

        given:

        jacek_s.utils.RandomArrayGenerator generator = new jacek_s.utils.RandomArrayGenerator()

        int[] arrayA
        int[] arrayB

        when:

        arrayA = generator.generate(TEST_ARRAY_LENGTH)
        arrayB = generator.generate(TEST_ARRAY_LENGTH)

        then:

        for (int i = 0; i < TEST_ARRAY_LENGTH ; i++) {
            if (arrayA[i] != arrayB[i])
            return
        }

        Assert.fail("arrays are equal")
    }
}
