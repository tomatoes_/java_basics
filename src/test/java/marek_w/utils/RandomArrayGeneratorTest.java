package marek_w.utils;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.fail;

/**
 * Created by marekwojtowicz on 23.02.2017.
 */
public class RandomArrayGeneratorTest {

    private static final int TEST_ARRAY_LENGTH = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {
        //given
        int testRangeFrom = -10;
        int testRangeTo = 10;

        //when
        int[] array = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

        //then
        for (int element:array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
        }
    }

    @Test
    public void shouldThrowExceptionIfRangeIsInvalid() {
//      Given:

        int testRangeFrom = 14;
        int testRangeTo = 10;

//      When:

        int array[] = new jacek_s.utils.RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

//      Then:

        fail("Range is invalid");
    }

    //uzupełnić testy!!!!!

    @Test
    public void lamdasTest() {
        Runnable r = () -> {
            System.out.println("test");
        };

        Runnable runnable = new Runnable() {
            public void run() {
                System.out.println("test");
            }
        };

        r.run();
        runnable.run();
    }

    public void testRun(Runnable runnable) {
        runnable.run();
    }
}