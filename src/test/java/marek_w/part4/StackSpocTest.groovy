package marek_w.part4

import spock.lang.Specification

/**
 * Created by marekwojtowicz on 07.03.2017.
 */
class StackSpocTest extends Specification {

    def " should push element to empty stack"(){
        given:
        String toPush = "test";
        Stack<String> stack =  stackInstance

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should push element to filled stack"(){
        given:
        String toPush = "test";
        Stack<String> stack =  stackInstance
        stack.push("abc")

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should delete value after poping"(){
        given:
        Stack<String> stack =  stackInstance
        stack.push("abc")

        when:
        stack.pop()

        then:
        null == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should receive value when peeking but not delete it"(){
        given:
        Stack<String> stack =  stackInstance
        stack.push("abc")

        when:
        stack.peek()

        then:
        "abc" == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should return zero as size of empty stack"(){
        given:
        Stack<String> stack =  stackInstance

//        when:
//        stack.size()
//        then:
//        0 == stack.size()

        expect:
        0 == stack.size()


        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should return correct size for filled stack"(){
        given:
        Stack<String> stack =  stackInstance
        stack.push("abc")

        expect:
        1 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should return true if there were no values put on stack"(){
        given:
        Stack<String> stack =  stackInstance

        expect:
//        true == stack.isEmpty()
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should return false if push was called previously"(){
        given:
        Stack<String> stack =  stackInstance
        stack.push("abc")

        expect:
        false == stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }

    def "should return true if element was pushed and poped"(){
        given:
        Stack<String> stack =  stackInstance

        when:
        stack.push("abc")
        stack.pop()

        then:
        true == stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
    }
}
