package jacek_s.utils;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.fail;

/**
 * Created by jacek on 23.02.17.
 */
public class RandomArrayGeneratorTest {

    private static final int TEST_ARRAY_LENGTH = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {

//          given:

        int testRangeFrom = -10;
        int testRangeTo = 10;

//          when:

        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

//          then:

        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);

        }

    }

    @Test(expected = IllegalArgumentException.class)

    public void shouldThrowExceptionIfRangeIsInvalid() {

//      Given:

        int testRangeFrom = 14;
        int testRangeTo = 10;

//      When:

        int array[] = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

    }

    @Test

    public void lamdaTest() {
        Runnable r = () -> {

            System.out.println("SortAlgoritmTest");

        };

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("SortAlgoritmTest");
            }
        };

        r.run();
        runnable.run();
    }

    public void testRun (Runnable runnable) {
        runnable.run();
    }
}

