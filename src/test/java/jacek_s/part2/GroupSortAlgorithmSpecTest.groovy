package jacek_s.part2

import jacek_s.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import java.util.function.Predicate

/**
 * Created by jacek on 04.03.17.
 */

class GroupSortAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should leave zero-sized array unchanged"() {

        given:

        int[] array = new int[0]

        GroupSortAlgorithm algorithm = grouping

        when:

        algorithm.sort((array), { e -> true }) //

        then:

        noExceptionThrown()

        where:

        _| grouping
        _| new PolishFlagGroupSortAlgorithm()


    }

    def "should divide array into two groups"() {

        given:

        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 100)
        GroupSortAlgorithm algorithm = grouping

        when:

        algorithm.sort(array, { e -> e < 50 })

        then:

        boolean currentGroup = true
        boolean wasGroupChanged = false
        for (int element : array) {
            if (element >= 50 && currentGroup) {
                currentGroup = false
                wasGroupChanged = true
            } else if (element < 50 && wasGroupChanged) {
                Assert.fail()
            }
        }

//        int changesAmount = 0
//        int changePoint = 0
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] < 50) {
//                changesAmount++
//            } else {
//                changePoint = 1
//                break
//            }
//        }
//        for (int i = changePoint; i < array.length; i++) {
//            if (array[i] >= 50) {
//                changesAmount++
//            } else {
//                break
//            }
//        }
//        assert changesAmount == TEST_ARRAY_LENGTH

        where:

        _| grouping
        _| new PolishFlagGroupSortAlgorithm()

    }

//    def "should leave array with one group unchanged"() {
//
//        given:
//
//        int[] expectedArray = arrayGenerator.generate(TEST_ARRAY_LENGTH)
//        int[] actualArray = expectedArray.clone()
//        GroupSortAlgorithm algorithm = grouping
//
//        when:
//
//        algorithm.sort(array, {e -> true})
//
//        then
//        Assert.assertArrayEquals(expectedArray, actualArray)
//
//        where:
//
//        _| grouping
//
//    } ten test jest niepotrzebny

    def "should throw if array is null"() {

        given:

        int[] array = null

        GroupSortAlgorithm algorithm = grouping

        when:

        algorithm.sort(array, { e -> true })

        then:

        thrown(IllegalArgumentException)

        where:

        _| grouping
        _| new PolishFlagGroupSortAlgorithm()


    }
}
