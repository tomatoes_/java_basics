package jacek_s.part1

import spock.lang.Specification

/**
 * Created by jacek on 25.02.17.
 */
class Problem1Test extends Specification {

    def "test should create specific array"() {

        int[] array

        when:

        array = Problem1.createFirstAarray()

        then:

        array[0] == 1
        array[array.length-1] == 61

        for (int i = 1; i <array.length ; i++) {
            assert array[i] == array[i-1]+2
        }
    }
}
