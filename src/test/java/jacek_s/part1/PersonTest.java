package jacek_s.part1;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.codehaus.groovy.runtime.DefaultGroovyMethods.collect;
import static org.junit.Assert.*;

/**
 * Created by jacek on 28.02.17.
 */
public class PersonTest {

    private static final int PEOPLE_AMOUNT = 1000;

    @Test
    public void shouldCreateAPerson() {
        Person[] people = getRandomPersons();

        Stream.of(people)
                .filter(person -> person.getGender() == 'M')
                .map(this::print)
                .filter(person -> person.getFirstName().matches("^[A-Da-d].*"))
                .map(person -> Pair.of(person.getPesel(), person.getAdress().getPostalCode()))
                .collect(Collectors.toList());
//              .forEach(System.out::println);
    }

    private <T> T print(T a) {
        System.out.println(a);
        return a;
    }

    @Test
    public void shouldFindPeopleAndCollectAsPairs() {
        Person[] people = getRandomPersons();


        Stream.of(getRandomPersons())
                .filter(person -> person.getLastName().matches(".*[Mm].*"))
                .filter(person -> person.getLastName().toUpperCase().contains("M"))
                .filter(person -> Integer.parseInt(person.getPesel().substring(0, 2)) <=
                        Calendar.getInstance().get(Calendar.YEAR) - 1960)
                .filter(person -> person.getAdress().getPostalCode().startsWith("20"))
                .map(this::print)
                .collect(Collectors.toSet());
    }

    private Person[] getRandomPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }

//    Dodatkowo streamy:
//
//    wyszukać wszystkie osoby, których imię zaczyna się od A i nazwisko kończy na literę Z
//    wyłuskać z wyniku datę urodzenia
//    wszystkie unikalne daty urodzenia zebrać w listę


    @Test
    public void shouldFindPeopleAndCollect() {
        Person[] people = getRandomPersons();


        Stream.of(getRandomPersons())
                .filter(person -> person.getFirstName().matches("^[Aa]"))
                .filter(person -> person.getLastName().matches("[Zz]$"))
                // ????????
                .map(this::print)
                .collect(Collectors.toList());

    }
}