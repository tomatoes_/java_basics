package jacek_s.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by jacek on 25.02.17.
 */

public class LambdaTest {

    @Test

    public void shouldCreateLambdas() {

        Runnable runnable = () -> {}; // wykonuje kod

        Supplier<Integer> supplier = () -> 98; // zamiast return 2 (oznacza, że zwraca jedną wartość)
        System.out.println(supplier.get());

        Supplier<Character> supplier1 = () -> (char)98;
        System.out.println(supplier1.get()); // daje wartości

        useSupplier(supplier);
        useSupplier(() -> 5);  // () - lista argumentów

        Consumer<Integer> consumer = arg -> {}; // t- argument
        System.out.println(10);
        consumer.accept(10);

        Function<Integer, String> function = arg -> "arg is " + arg;    //1. argument to ten, kótry przekazujemy do Lambdy, a 2. to ten który jest zwracany
        System.out.println(function.apply(25));

        TestInterface testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + c + "=" + result);
            return result;
        };

        testLambda.test('a', "Test");
    }

    private void useSupplier(Supplier<Integer>supplier) {
        System.out.println(supplier.get());
    }

    @Test

    public void methodReferences() {
       Consumer<Integer> consumer = System.out::println; // :: referencje do metody
       Consumer<Integer> consumerLambda = x -> System.out.println(x); //to samo co wyżej

       consumer.accept(2);
       consumerLambda.accept(2);

       Random random = new Random();
       random.nextInt();
       useSupplier(random::nextInt);
    }




}
