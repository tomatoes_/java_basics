package jacek_s.part1;

import org.junit.Test;

import static org.junit.Assert.assertSame;

/**
 * Created by jacek on 28.02.17.
 */
public class StringTest {

    @Test
    public void stringObjectShouldHaveTheSameReferenceIfAssigned() {
        String a = "SortAlgoritmTest";
        String b = "SortAlgoritmTest";
        String c = a;
        assertSame(a,c);

        if (a==b) {
            System.out.println("Są równe");
        } else {
            System.out.println("Nie są równe");
        }

    }
}
