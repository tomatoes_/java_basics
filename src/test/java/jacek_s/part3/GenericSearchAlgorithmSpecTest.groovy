package jacek_s.part3

import jacek_s.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by jacek on 06.03.17.
 */
class GenericSearchAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 100
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in random array"() {

        given:

        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:

        int searchResult = algorithm.search(array, {e -> (e == valueToFind) })

        then:
        array[searchResult] == valueToFind

        where:
        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }

    def "should find element in sorted array"() {

        given:

        Integer[] array = new int[TEST_ARRAY_LENGTH]

        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }

        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:

        int searchResult = algorithm.search(array, {e -> (e == valueToFind)})

        then:

        array[searchResult] == valueToFind

        where:
        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }

    def "should return invalid index if element not present"() {

        given:

        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 20)
        int invalidValue = 60

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:

        Integer search = algorithm.search(array, {e -> false})

        then:

        Assert.assertEquals(-1, search)

        where:

        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }

    def "should return invalid index if array length zero"() {

        given:

        Integer[] array = new int[0]

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        int valueToFind = new Random().nextInt()

        when:

        int returnValue = algorithm.search(array, {e ->(e == valueToFind)})

        then:

        Assert.assertEquals(-1, returnValue)

        where:

        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }

    def "should throw if array is null"() {

        Integer[] array = null

        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:

        algorithm.search(array, {e -> false})

        then:

        thrown (IllegalArgumentException)

        where:
        _| searchingAlgorithm
        _| new NaiveGenericSearchAlgorithm()
    }
}
