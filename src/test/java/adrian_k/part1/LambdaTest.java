package adrian_k.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaTest {
    @Test
    public void shouldCreateLambdas(){
        Runnable runnable = () -> {};
        Supplier<Integer> supplier = () -> 1;
        useSupplier(supplier);
        useSupplier(() -> 40);

        Consumer<Integer> consumer = arg -> System.out.println(arg);
        consumer.accept(10);

        Function<Integer, String> function = arg -> "arg is " + arg;
        System.out.println(function.apply(25));

        TestInterface testInterface = (c, s) -> {
            System.out.println(s);
            return (int)c;
        };
        System.out.println(testInterface.test('A', "Test lamdy wykonany pomyślnie."));


    }


        @Test
        public void methodReferance(){

        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);
        }


    private void useSupplier(Supplier<Integer> supplier){
        System.out.println(supplier.get());
    }
}

interface TestInterface{
    int test(char c, String comment);
}
