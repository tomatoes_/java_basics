package adrian_k.part1

import spock.lang.Specification

class Problem1SpecTest extends Specification {

    def "should create specific array"() {
        given:
        int[] testArray

        when:
        testArray = Problem1.createFirstArray()

        then:
        testArray[0] == 1
        testArray[testArray.length - 1] == 61;
        for (int i = 1; i < testArray.length; i++) {
            assert testArray[i]  == testArray[i-1] + 2
        }
    }


}
