package adrian_k.part1;

import org.junit.Test;

import static org.junit.Assert.assertSame;

public class StringTest {
    @Test
    public void StringObjectsShouldHaveTheSameReferenceAssigned() {
        String a = "test";
        String b = "test";
        String c = a;
        assertSame(a, c);
        assertSame(a, b);
    }
}
