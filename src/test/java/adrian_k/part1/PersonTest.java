package adrian_k.part1;


import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

import static org.junit.Assert.*;


public class PersonTest {
    private static final int PEOPLE_AMOUNT = 10000;

    @Test
    public void shouldCreatePerson() {

        Person[] people = getRandomPersons();

        Arrays.stream(people)
                .filter(person -> person.getGender() == 'M')
                .filter(p -> p.getFirstName()
                        .matches("^[A-Da-d].*"))
                .map(person -> Pair.of(person.getPesel(), person.getAddress().getPostalCode()))
                .collect(toList());


    }

    @Test
    public void example2(){
        Person[] people = getRandomPersons();
        Arrays.stream(people)
                .filter(p -> p.getLastName().toUpperCase().contains("M"))
                .filter(p -> Integer.parseInt(p.getPesel().substring(0, 2)) <= Calendar.getInstance().get(Calendar.YEAR)-1960)
                .filter(p-> p.getAddress().getPostalCode().startsWith("20"))
                .map(this::print)
                .collect(Collectors.toSet());
    }

    private Person[] getRandomPersons() {
        Person[] people = new Person[PEOPLE_AMOUNT];
        for (int i = 0; i < people.length; i++) {
            people[i] = Person.getRandomPerson();
        }
        return people;
    }

    private <T> T print (T a){
        System.out.println(a);
        return a;
    }

}