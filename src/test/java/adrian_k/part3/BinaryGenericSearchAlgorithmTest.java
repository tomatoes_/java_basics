package adrian_k.part3;

import org.junit.Assert;
import org.junit.Test;

public class BinaryGenericSearchAlgorithmTest {

    @Test
    public void test() {
        int length = 100;
        Integer[] array = new Integer[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        int expectedIndex = 30;
        Integer toFind = array[expectedIndex];

        int actualIndex = new BinaryGenericSearchAlgorithm().search(array, toFind::compareTo);

        //int actualIndex = new BinaryGenericSearchAlgorithm().search(array, anotherInteger -> toFind.compareTo(anotherInteger));

        /*int actualIndex = new BinaryGenericSearchAlgorithm().search(array, b -> {
            if (b < expectedIndex) {
                return -1;
            } else if (b > expectedIndex) {
                return 1;
            } else {
                return 0;
            }
        });*/

        Assert.assertEquals(expectedIndex, actualIndex);
    }
}