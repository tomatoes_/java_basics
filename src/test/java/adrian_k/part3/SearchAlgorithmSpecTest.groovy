package adrian_k.part3

import adrian_k.utils.RandomArrayGenerator
import spock.lang.Specification

class SearchAlgorithmSpecTest extends Specification {

    int TEST_ARRAY_LENGTH = 100
    int INVALID_INDEX = -1

    def "should find element in random array"(){

        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH)
        int testIndex = new Random().nextInt(array.length)
        int valueToFind = array[testIndex]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, valueToFind)

        then:
        array[actualIndex] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()

    }

    def "should find element in sorted array"(){

        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int expectedIndex = new Random().nextInt(array.length)
        int valueToFind = array[expectedIndex]

        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, valueToFind)

        then:
        array[actualIndex] == valueToFind

        where:
        _ | searchingAlgorithm
      //  _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should return invalid index if element not present"(){

        given:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, 0, 100)
        SearchAlgorithm algorithm = searchingAlgorithm
        int invalidValue = 101

        when:
        int actualIndex = algorithm.search(array, invalidValue)

        then:
        INVALID_INDEX == actualIndex

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should return invalid index if array length is zero"(){

        given:
        int[] array = new int[0]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int actualIndex = algorithm.search(array, 101)

        then:
        INVALID_INDEX == actualIndex

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should throw exception if array is null"(){

        given:
        int[] array = null
        SearchAlgorithm algorithm = searchingAlgorithm


        when:
        algorithm.search(array, 101)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }
}
