package adrian_k.part2

import adrian_k.utils.RandomArrayGenerator
import spock.lang.Specification


class SortAlgorithmSpecTest extends Specification {
    int TEST_ARRAY_LENGTH = 10
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should sort array ascending by default"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
        _ | sortingAlgorithm
        _ | new BubbleSortAlgorithm()
        _ | new QuickSortAlgorithm()
        _ | new MergeSortAlgorithm()

    }

    def "should sort array depending on the order"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] == comparison(array[i], array[i - 1])
        }

        where:
        _ | sortingAlgorithm          | isAscending | comparison
        _ | new BubbleSortAlgorithm() | true        | Integer.&max
        _ | new BubbleSortAlgorithm() | false       | Integer.&min
        _ | new QuickSortAlgorithm()  | true        | Integer.&max
        _ | new MergeSortAlgorithm()  | true        | Integer.&max

    }

    def "should leave sorted array unchanged"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < TEST_ARRAY_LENGTH; i++) {
            array[i] = i
        }
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        for (int i = 1; i < array.length; i++) {
            assert array[i] >= array[i - 1]
        }

        where:
         sortingAlgorithm          | isAscending | comparison
        new BubbleSortAlgorithm() | true        | Integer.&max
        new QuickSortAlgorithm()  | true        | Integer.&max
        new MergeSortAlgorithm()  | true        | Integer.&max

    }

    def "should throw exception if array is null"() {
        given:
        int[] array = null
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array, isAscending)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | sortingAlgorithm          | isAscending | comparison
        _ | new BubbleSortAlgorithm() | true        | Integer.&max
        _ | new BubbleSortAlgorithm() | false       | Integer.&min
        _ | new QuickSortAlgorithm()  | true        | Integer.&max
        _ | new MergeSortAlgorithm()  | true        | Integer.&max

    }

    def "should not throw if array is zero-sized"() {
        given:
        int[] array = new int[0]
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        noExceptionThrown()

        where:
        sortingAlgorithm          | isAscending | comparison
        new BubbleSortAlgorithm() | true        | Integer.&max
        new BubbleSortAlgorithm() | false       | Integer.&min
        new QuickSortAlgorithm()  | true        | Integer.&max
        new MergeSortAlgorithm()  | true        | Integer.&max

    }

    def "should sort if elements are not unique"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 10)
        SortAlgorithm algorithm = sortingAlgorithm

        when:
        algorithm.sort(array)

        then:
        for (int i = 1; i < array.length; i++) {

            assert array[i] >= array[i - 1]
        }

        where:
        sortingAlgorithm          | isAscending | comparison
        new BubbleSortAlgorithm() | true        | Integer.&max
        new BubbleSortAlgorithm() | false       | Integer.&min
        new QuickSortAlgorithm()  | true        | Integer.&max
        new MergeSortAlgorithm()  | true        | Integer.&max


    }
}
