package adrian_k.part4

import spock.lang.Specification

class ListSpecTest extends Specification {

    def "should add element if list is empty"() {
        given:
        List<String> testList =  listImplement
        String testString = "test"

        when:
        testList.add(testString)

        then:
        noExceptionThrown()

        where:
        _ | listImplement
        _ | new DynamicList()

    }

    def "should get first element of non-empty list"() {
        given:
        List<String> testList = listImplement
        testList.add("non-empty list")
        String testString = "test"
        testList.add(testString)

        when:
        String expectedFirstString = testList.getFirst()

        then:
        expectedFirstString == testString

        where:
        _ | listImplement
        _ | new DynamicList()
    }

    def "should get last element of non-empty list"() {
        given:
        List<String> testList = listImplement
        testList.add("non-empty list")
        testList.add("some more on list")
        String testString = "test"
        testList.add(testString)

        when:
        String expectedLastString = testList.getLast()

        then:
        expectedLastString == testString

        where:
        _ | listImplement
        _ | new DynamicList()
    }

    def "should get specific element from the list"() {
        given:
        List<String> testList = listImplement
        testList.add("test1")
        testList.add("test2")
        testList.add("test3")

        when:
        def result = testList.get(1)

        then:
        result == "test2"


        where:
        _ | listImplement
        _ | new DynamicList()
    }

    def "should remove element from non-empty list"() {
        given:
        List<String> testList = listImplement
        testList.add("test")
        testList.add("test")

        when:
        testList.remove(1)

        then:
        1 == testList.size()

        where:
        _ | listImplement
        _ | new DynamicList()
    }

    def "should throw exception if get called on empty list"() {
        given:
        List<String> testList = listImplement

        when:
        testList.get(0)

        then:
        thrown IndexOutOfBoundsException

        where:
        _ | listImplement
        _ | new DynamicList()
    }

    def "should throw exception if remove called on empty list"() {
        given:
        List<String> testList = listImplement

        when:
        testList.remove(0)

        then:
        thrown IndexOutOfBoundsException

        where:
        _ | listImplement
        _ | new DynamicList()
    }
}
