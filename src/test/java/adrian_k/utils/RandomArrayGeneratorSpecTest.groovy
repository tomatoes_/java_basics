package adrian_k.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll

import static org.spockframework.util.Assert.fail


class RandomArrayGeneratorSpecTest extends Specification {

    private static final int TEST_ARRAY_LENGTH = 100
    @Unroll("should generate numbers in range #testRangefrom to #testRangeTo")
    def "should generate numbers in given numbers"() {
        when:
        int[] array = new RandomArrayGenerator()
                .generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo)

        then:
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo)
        }
        where:
        testRangeFrom | testRangeTo
        -10           | 20
        12            | 100
        0             | 12200
        -2133         | 100
        0             | 1
    }

    @Unroll("should generate array of length = #length")
    def "should generate array off given length" (){
        when:
        int[] array = new RandomArrayGenerator().generate(length)

        then:
        assert array.length == length

        where:
        _ | length
        _ | 10
        _ | 100
        _ | 2000
    }

    @Unroll("should throw NegativeArraySizeException for length = #length")
    def "should throw NegativeArraySizeException for negative lengths" (){
        when:
        def array = new RandomArrayGenerator().generate(length)

        then:
        thrown(NegativeArraySizeException)

        where:
        _ | length
        _ | -20
        _ | -10000
        _ | -200
    }

    def "should generate two different arrays"(){
        given:
        RandomArrayGenerator generator = new RandomArrayGenerator()
        def arrayA
        def arrayB

        when:
        arrayA = generator.generate(TEST_ARRAY_LENGTH)
        arrayB = generator.generate(TEST_ARRAY_LENGTH)

        then:
        for (int i = 0; i < TEST_ARRAY_LENGTH; i++){
            if (arrayA[i] != arrayB[i])
                return
        }
        fail("arrays are equal")

    }
}
