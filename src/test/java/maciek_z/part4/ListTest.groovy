package maciek_z.part4

import spock.lang.Specification


class ListTest extends Specification {

    def "should add element if list is empty"() {
        given:
        List<Integer> list = listInstance


        when:
        list.add(1)

        then:
        noExceptionThrown()

        where:
        _ | listInstance
        _ | new MyLists<>()

    }

    def "should get first element of non-empty list"() {
        given:
        List<Integer> list = listInstance


        when:
        list.add(1)

        then:
        1 == list.getFirst()

        where:
        _ | listInstance
        _ | new MyLists<>()

    }

    def "should get last element of non-empty list"() {
        given:
        List<Integer> list = listInstance


        when:
        list.add(1)
        list.add(2)
        list.add(3)

        then:
        3 == list.getLast()

        where:
        _ | listInstance
        _ | new MyLists<>()

    }

//    def "should return null if there are no elements"() {
//        given:
//        List<String> list = listInstance
//
//
//        when:
//        def result = list.get(0)
//
//        then:
//        result == null
//        where:
//        _ | listInstance
//        _ | new MyLists<>()
//
//    }

    def "should get specific element from the list"() {

        given:
        List<Integer> list = listInstance
        list.add(1)
        list.add(2)
        list.add(3)

        when:
        Integer result = list.get(1)
        then:
        2 == result

        where:
        _ | listInstance
        _ | new MyLists<>()
    }

    def "should remove element from non-empty list"() {

        given:
        List<Integer> list = listInstance
        list.add(1)


        when:
        list.remove(0)


        then:
        0 == list.size()

        where:
        _ | listInstance
        _ | new MyLists<>()
    }

    def "should throw exception if get called on empty list"() {

        given:
        List<Integer> list = listInstance


        when:
        list.get(0)


        then:
        thrown IndexOutOfBoundsException

        where:
        _ | listInstance
        _ | new MyLists<>()
    }

    def "should throw exception if remove called on empty list"() {

        given:
        List<Integer> list = listInstance


        when:
        list.remove(0)


        then:
        thrown IndexOutOfBoundsException

        where:
        _ | listInstance
        _ | new MyLists<>()
    }

}
