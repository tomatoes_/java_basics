package maciek_z.part4

import spock.lang.Specification


class StackTest extends Specification {

    def "should push element to empty stack"() {
        given:
        String toPush = "test";
        Stack<String> stack = stackInstance

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)
    }

    def "should push element to filled stack"() {
        given:
        String toPush = "test";
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.push(toPush)

        then:
        toPush == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should delete value after poping"() {
        given:
        String toPush = "test";
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.pop()

        then:
        null == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should receive value when peeking but not delete it"() {
        given:
        String toPush = "test";
        Stack<String> stack = stackInstance
        stack.push("abc")

        when:
        stack.peek()

        then:
        "abc" == stack.peek()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should return zero as sieze of empty stack"() {
        given:
        Stack<String> stack = stackInstance

        expect:
        0 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()

    }

    def "should return correct size for filled stack"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")

        expect:
        1 == stack.size()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should return true if there were no value put on stack"() {
        given:
        Stack<String> stack = stackInstance

        expect:
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should return false if push was called previously"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")
        stack.pop()

        expect:
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }

    def "should return true if element was pushed and poped"() {
        given:
        Stack<String> stack = stackInstance
        stack.push("abc")
        stack.pop()

        expect:
        stack.isEmpty()

        where:
        _ | stackInstance
        _ | new DynamicStack()
        _ | new ArrayStack(10)


    }
}
