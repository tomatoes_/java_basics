package maciek_z.utils

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll


class RandomArrayGeneratorSpecTest extends Specification {

    private static final int TEST_ARRAY_LENGHT = 100

    @Unroll("Should generate number in range from #testRangeFrom to #testRangeTo")
    def "should Generate Numbers In Given Range"() {
        //given

        when:
        int[] array = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        then:
        for (int element : array) {
            try {
                Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
            } catch (AssertionError e) {
                System.out.println(testRangeFrom + " " + testRangeTo + " ... " + element);
                throw e;
            }
        }
        where:
        testRangeFrom | testRangeTo
        -10           | 20
        0             | 50
        10            | 30
        -100          | 0
        -50           | -20

    }

    @Unroll("should generat array of given lenght =#lenght")
    def "should generate array of given lenght"() {
        when:
        int[] array = new RandomArrayGenerator().generate(lenght)

        then:
        assert array.length == lenght

        where:
        _ | lenght
        _ | 10
        _ | 20
        _ | 30

    }

    @Unroll("should throw NegativeArraySizeException for negative lenght = #lenght")
    def "should throw NegativeArraySizeException for negative lenght"() {
        when:
        def array = new RandomArrayGenerator().generate(lenght)
        then:
        thrown(NegativeArraySizeException)
        where:
        _ | lenght
        _ | -30
    }

    def "should gernerate two different arrays"() {

        given:
        RandomArrayGenerator generator = new RandomArrayGenerator()
        int[] arrayA
        int[] arrayB

        when:
        arrayA = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT)
        arrayB = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT)

        then:
        for (int i = 0; i < TEST_ARRAY_LENGHT; i++) {
            if (arrayA[i] != arrayB[i])
                return
        }
        fail("arrays are equal")

    }
}
