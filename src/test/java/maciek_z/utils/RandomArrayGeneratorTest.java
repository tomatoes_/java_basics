package maciek_z.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RandomArrayGeneratorTest {

    private static final int TEST_ARRAY_LENGHT = 100;

    @Test

    public void shouldGenerateNumbersInGivenRange() {
        //given
        int testRangeFrom = -10;
        int testRangeTo = 10;
        //when
        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        //then
        for (int element : array) {
            try {
                Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);
            } catch (AssertionError e) {
                System.out.println(testRangeFrom + " " + testRangeTo + " ... " + element);
                throw e;
            }

        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionRangeIsInvalid() {
        //given
        int testRangeFrom = 20;
        int testRangeTo = 10;

        //when
        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGHT, testRangeFrom, testRangeTo);

        //then
        fail("This method should throw SomeException");

    }

    @Test
    public void shouldGenerateZerosArray(){
        Runnable r = () -> {
            System.out.println("SortAlgoritmTest");
        };
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("SortAlgoritmTest");
            }
        };
        r.run();
        runnable.run();
    }

    public void testRun(Runnable runnable){
        runnable.run();
    }



}