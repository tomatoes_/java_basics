package maciek_z.part1;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class LambdaTest {
    @Test
    public void shouldCreateLambdas() {
        Runnable runnable = () -> {
        };
        Supplier<Integer> supplier = () -> 98;
        useSupplier(supplier);
        useSupplier(() -> 5);
        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };

        consumer.accept(10);
        Function<Integer, String> function = arg -> "arg is " + arg;
        System.out.println(function.apply(25));

        TestInterface testLambda = (c, comment) -> {
            int result = (int) c;
            System.out.println(comment + " " + c + " = " + result);
            return result;
        };
        testLambda.test('a', "Test");
    }

    private void useSupplier(Supplier<Integer> supplier) {
        System.out.println(supplier.get());
    }

    @Test
    public void methodReferences() {
        Consumer<Integer> consumer = System.out::println; //referencja do metody
        Consumer<Integer> consumerLambda = (x) -> System.out.println(x); //to jest to samo co wyzej
        consumer.accept(2);
        consumerLambda.accept(2);

        Random random = new Random();
        random.nextInt();
        useSupplier(random::nextInt);
    }
}
