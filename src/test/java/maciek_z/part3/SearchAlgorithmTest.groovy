package maciek_z.part3

import maciek_z.part2.SortAlgorithm
import maciek_z.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

/**
 * Created by hudy on 2017-03-04.
 */
class SearchAlgorithmTest extends Specification {

    int TEST_ARRAY_LENGTH = 100;
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in random array"() {

        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()

    }


    def " should find element in sorted array"() {
        given:
        int[] array = new int[TEST_ARRAY_LENGTH]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]

        SearchAlgorithm searchAlgorithm = searchingAlgorithm


        when:
        int searchResult = searchAlgorithm.search(array, valueToFind)


        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def " should return invalid index if element not present"() {
        given:
        int[] array = arrayGenerator.generate(TEST_ARRAY_LENGTH, 0, 50)
        int valueToFind = 66
        SearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, valueToFind)

        then:
        Assert.assertEquals(-1, searchResult)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should return invalid index if array length is zero"() {
        given:
        int[] array = new int[0]
        SearchAlgorithm algorithm = searchingAlgorithm
        int valueToFind = new Random().nextInt()
        when:

        int returnValue = algorithm.search(array, valueToFind)

        then:
        Assert.assertEquals(-1, returnValue)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()

    }

    def "should throw if array is null"() {
        int[] array = null
        SearchAlgorithm algorithm = searchingAlgorithm
        when:
        algorithm.search(array, 0)

        then:
        thrown(IllegalArgumentException)

        where:
        _ | searchingAlgorithm
        _ | new NaiveSearchAlgorithm()
        _ | new BinarySearchAlgorithm()
    }


}

