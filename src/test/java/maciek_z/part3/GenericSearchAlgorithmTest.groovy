package maciek_z.part3

import maciek_z.part2.GenericSortAlgorithm
import maciek_z.utils.RandomArrayGenerator
import org.junit.Assert
import spock.lang.Specification

import java.util.function.Predicate

import static java.lang.Integer.compare


class GenericSearchAlgorithmTest extends Specification {

    int TEST_ARRAY_LENGHT = 100
    int INVALID_INDEX = -1
    RandomArrayGenerator arrayGenerator = new RandomArrayGenerator()

    def "should find element in random array"() {

        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT)
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, { e -> e == valueToFind })

        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()

    }


    def " should find element in sorted array"() {
        given:
        Integer[] array = new int[TEST_ARRAY_LENGHT]
        for (int i = 0; i < array.length; i++) {
            array[i] = i
        }
        int index = new Random().nextInt(array.length)
        int valueToFind = array[index]

        GenericSearchAlgorithm searchAlgorithm = searchingAlgorithm


        when:
        int searchResult = searchAlgorithm.search(array, { e -> e == valueToFind })


        then:
        array[searchResult] == valueToFind

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
//        _ | new BinaryGenericSearchAlgorithm()

    }

    def " should return invalid index if element not present"() {
        given:
        Integer[] array = arrayGenerator.generate(TEST_ARRAY_LENGHT, 0, 50)
        GenericSearchAlgorithm algorithm = searchingAlgorithm

        when:
        int searchResult = algorithm.search(array, { e -> false })

        then:
        INVALID_INDEX == searchResult

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
//        _ | new BinaryGenericSearchAlgorithm()

    }

    def "should return invalid index if array length is zero"() {
        given:
        Integer[] array = new Integer[0]
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer valueToFind = new Random().nextInt()
        when:

        int returnValue = algorithm.search(array, { e -> e == valueToFind })

        then:
        Assert.assertEquals(-1, returnValue)

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
//        _ | new BinaryGenericSearchAlgorithm()

    }

    def "should throw if array is null"() {
        Integer[] array = null
        GenericSearchAlgorithm algorithm = searchingAlgorithm
        Integer randomValue = 101

        when:
        algorithm.search(array, { e -> e == randomValue })

        then:
        thrown(NullPointerException)

        where:
        _ | searchingAlgorithm
        _ | new NaiveGenericSearchAlgorithm()
//        _ | new BinaryGenericSearchAlgorithm()
    }


}


