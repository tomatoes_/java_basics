package piotr_s.part1;

import org.junit.Test;
import piotr_s.utils.RandomArrayGenerator;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambaTest {
    @Test
    public void shouldCreateKambdas() {
        Runnable runnable = () -> {
        };
        Supplier<Integer> suppiler = () -> 98;
        useSuppiler(suppiler);
        useSuppiler(() -> 5);
        Consumer<Integer> consumer = arg -> {
            System.out.println(arg);
        };
        consumer.accept(10);
        Function<Integer, String> function = arg -> "arg is " + arg;
        System.out.println(function.apply(25));
        TestInterface testLambda = (c, comment) -> {
        int result=(int) c;
            System.out.println(comment + " " + c + " = " + result);
        return result;
        };
        testLambda.test('a', "Test");
    }
    private void useSuppiler(Supplier<Integer> suppiler) {

        System.out.println(suppiler.get());
    }
@Test
    public void methodReferences (){

        Consumer<Integer> consumer = System.out::println;
    Consumer<Integer> consumerLambda = x -> System.out.println(x);

    consumer.accept(2);
    consumerLambda.accept(2);

    Random random = new Random();
    random.nextInt();
    useSuppiler(random::nextInt);

}

}
