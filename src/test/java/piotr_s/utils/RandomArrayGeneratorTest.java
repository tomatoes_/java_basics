package piotr_s.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RandomArrayGeneratorTest {
    private static final int TEST_ARRAY_LENGTH = 100;

    @Test
    public void shouldGenerateNumbersInGivenRange() {

        //given
        int testRangeFrom = 10;
        int testRangeTo = 20;
        //when
        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);
        //then
        for (int element : array) {
            Assert.assertTrue(element >= testRangeFrom && element <= testRangeTo);

        }
    }

    @Test
    public void shouldThrowExceptionIfRangeIsInvalid() {

        //given
        int testRangeFrom = 10;
        int testRangeTo = 10;
        //when
        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);
        //then
        fail("This method should throw Exception");

    }

    @Test
    public void shouldGenerateZerosArray() {

        int[] array = generateTestArray(new ZeroArrayGenerator());

    }

    int[] generateTestArray() {
        return generateTestArray(new RandomArrayGenerator());

    }

    int[] generateTestArray(ArrayGenerator generator) {
        return generator.generate(TEST_ARRAY_LENGTH);
    }

    @Test
    public void shouldGenerateZeroesArray() {
        Runnable r= () -> {
            System.out.println("SortAlgoritmTest");
        };

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("SortAlgoritmTest");
            }
        };
        runnable.run();
    }
    public void testRun (Runnable runnable){
        runnable.run();
    }
}