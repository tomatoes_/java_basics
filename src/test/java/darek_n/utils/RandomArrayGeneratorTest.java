package darek_n.utils;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by RENT on 2017-02-23.
 */
public class RandomArrayGeneratorTest {
    private  static final int TEST_ARRAY_LENGTH = 100;

    @Test
    @Ignore
    // given

    public void  shouldGeneratenumbersInGivenRange () {
       int testRangeFrom = -10;
       int testRangeTo = 10;

       // when:

        int array[] = new RandomArrayGenerator().generate(TEST_ARRAY_LENGTH, testRangeFrom, testRangeTo);

//      then :
        for  (int element :array) {

            Assert.assertTrue( element > testRangeFrom && element < testRangeTo);
        }


    }

}
