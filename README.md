# Programowanie I - poziom podstawowy #

## Część czwarta: struktury danych, Collections API ##

### Zadania ###

```
1. Stos - zaprojektowanie interfejsu (Stack<T>)
```
```
2. Stos - testy
    should push element to empty stack
    should push element to filled stack
    should delete value after poping
    should receive value when peeking but not delete it
    should return zero as size of empty stack
    should return correct size for filled stack
    should return true if there were no values put on stack
    should return false if push was called previously
    should return true if element was pushed and poped
```
```
3. Stos - implementacja i klasa elementu (StackElement<T>)
```
```
4. Lista dwukierunkowa - interfejs
```
```
5. Lista dwukierunkowa - testy
    should add element if list is empty
    should get first element of non-empty list
    should get last element of non-empty list
    should return null if there are no elements
    should get specific element from the list
    should remove element from non-empty list
    should throw exception if get called on empty list
    should throw exception if remove called on empty list
```
```
6. Lista dwukierunkowa - implementacja
```

### Linki ###

* https://docs.google.com/presentation/d/1DfJQ-QxvZfAoTLQRi5-BpN10xBeAUoGM0zMjQ1dEv_s/edit?usp=sharing

* https://www.tutorialspoint.com/data_structures_algorithms/stack_algorithm.htm - o stosie


## Część trzecia: algorytmy wyszukiwania ##

### Zadania ###

```
1. Wyszukiwanie naiwne - test
    should find element in a random array
    should find element in sorted array
    should return invalid index if element not present
    should return invalid index if array length is zero
    should throw if array is null
```
```
2. Wyszukiwanie naiwne - implementacja (NaiveSearchAlgorithm)
```
```
3. Wyszukiwanie naiwne - refactoring interfejsu (GenericSearchAlgorithm)
```
```
4. Wyszukiwanie naiwne - poprawienie testów i implementacja generyczna
```
```
5. Wyszukiwanie binarne - implementacja (BinarySearchAlgorithm)
```
```
6. Wyszukiwanie binarne - implementacja generyczna (dla chętnych)
```
```
7. Wyszukiwanie KMP - implementacja (dla chętnych)
```

### Linki ###

* https://docs.google.com/presentation/d/1nTjnIDmJFE_JGF2hkfxqBR_14CP--PhKHKAH_-Dbi2M/edit?usp=sharing - prezentacja

* https://www.google.com/insidesearch/howsearchworks/thestory/ - prezentacja jak działa Google


## Część druga: algorytmy sortowania ##

### Zadania ###

```
#!java
1. Interfejs SortAlgorithm
    public interface SortAlgorithm {
        default void sort(int[] array) {sort(array, true);}
        void sort(int[] array, boolean ascending);
    }
```
```
2. Test dla algorytmu sortującego - ustalenie przypadków
 * should sort array ascending by default
 * should sort array depending on the order
 * should leave sorted array unchanged
 * should throw exception if array is null
 * should not throw if array is zero-sized
 * should sort if elements are not unique
```
```
3. Implementacja BubbleSortAlgorithm
    https://pl.wikipedia.org/wiki/Sortowanie_b%C4%85belkowe#Przyk.C5.82ad_dzia.C5.82ania
```
```
4. Implementacja algorytmów sortujących - praca w parach
    Czerwoni - merge sort
    Zieloni - heap sort
    Niebiescy - quick sort
```
```
5. Flaga polska - implementacja
```
```
6. Flaga holenderska - implementacja
```
```
Refactoring algorytmów wyszukiwania tak, aby można było przeszukiwać tablicę dowolnych obiektów
```

### Linki ###

* https://docs.google.com/presentation/d/1G8kAlO3GUHu7tAT9q7AZWr-ADswgoDn8yguOJiXzR_s/edit?usp=sharing - prezentacja

* https://en.wikipedia.org/wiki/Sorting_algorithm - algorytmy sortowania
* https://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting - implementacja algorytmów w różnych językach
* https://www.youtube.com/watch?v=lyZQPjUT5B4 - sortowanie zaprezentowane przez tańcujących ludzi
* https://pl.wikipedia.org/wiki/Sortowanie_b%C4%85belkowe - sortowanie bąbelkowe na wiki
* https://justin.abrah.ms/computer-science/big-o-notation-explained.html - notacja wielkiego O
* https://justin.abrah.ms/computer-science/how-to-calculate-big-o.html - jak obliczać wielkie O
* https://en.wikipedia.org/wiki/Big_O_notation#Orders_of_common_functions - przykłady złożoności
* https://bost.ocks.org/mike/algorithms/ - wizualizacja algorytmów
* https://www.doc.ic.ac.uk/~kb/REASONING/JavaFlags4up0809.pdf - opis flag
* https://en.wikipedia.org/wiki/Dutch_national_flag_problem - flaga holenderska na wiki


## Część pierwsza: wprowadzenie do algorytmów ##

### Zadania ###

```
1. Wypisać na ekran następujące ciągi liczb:
    1, 3, 5, 7, 9, 11... 61
    0, 2, 4, 6, 4, 2, 0
    100, 10, 200, 20... 900, 90
    1, 1, 2, 3, 5, 8, 13, 21, 34... (ciąg Fibonacciego)
```
```
2. Rysowanie figur w konsoli:
    *         *         ******    *    *
    **         *        *    *     *  *
    ***         *       *    *      **
    ****         *      *    *      **
    *****         *     *    *     *  *
    ******         *    ******    *    *
   Krzyżyk dla chętnych (;
```
```
#!java
3. Zamienić miejscami elementy tablicy o indeksach 2 i 4:
    int[] myArray = new int[8];
    myArray[2] = 5;
    myArray[4] = 8;
```
```
4. Wykonać operacje na tablicach (po każdym punkcie wyświetlamy):
    • wypełniamy tablicę kolejnymi wartościami od 1 do 10
               [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    • co drugą liczbę zwiększamy o wartość jej poprzednika
              [0, 1, 2, 5, 4, 9, 6, 13, 8, 17]
    • każdą liczbę parzystą dzielimy przez 2
              [0, 1, 1, 5, 2, 9, 3, 13, 4, 17]
    • sumujemy wszystkie liczby w tablicy, wynik = ?
    • jaka będzie suma dla 100 elementów?
```
```
5. Utworzyć klasę RandomUtils która generuje tablicę wypełnioną losowymi wartościami.
   Utworzyć test dla tej klasy.
```
```
6. Wykonać te same operacje co w zadaniu 4, ale tym razem wykorzystać streamy.
```
```
7. Zadania do wyboru:
    • Obliczanie NWD i NWW
    • Liczby pierwsze: http://www.spoj.com/problems/PRIME1/
    • Odwrócona suma: http://www.spoj.com/problems/ADDREV/ (rozwiązanie do którego doszliśmy na zajęciach - http://wklej.org/id/3052956/)
    • Odwrotna notacja polska: http://www.spoj.com/problems/ONP/
```

### Linki ###

* https://docs.google.com/presentation/d/1xMIgnbNb8ME2iMQiPGRU6mTwJZCyiGxTT8hcXWI-AKE/edit?usp=sharing - prezentacja

* http://www.spoj.com/ - SPOJ wersja angielska
* http://www.pl.spoj.com/ - SPOJ wersja polska
* http://www.spoj.com/problems/TEST/ - SPOJ testowe zadanie
* https://en.wikipedia.org/wiki/Timeline_of_algorithms - oś czasu algorytmów
* http://www.theannotatedturing.com/ - książka o maszynie Turinga
* http://stackoverflow.com/a/9521417/5922757 - tablice a dziedziczenie
* http://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/ - tutorial o streamach
* https://zeroturnaround.com/rebellabs/java-8-streams-cheat-sheet/ - streamy, ściągawka do wydrukowania
* http://tomorrowcorporation.com/humanresourcemachine - gra na androida w której "piszemy" algorytmy
* https://drive.google.com/file/d/0B0ttBfhP299VTmF3NHNpUWV5VEk/view?usp=sharing - książka o Spocku dla zainteresowanych

## Kontakt ##

### Paweł Fiuk ###

* 518 815 159
* pawelfiu@gmail.com